package com.woyou.utils;

public class CharUtil {
	
	//获取字符串长度
    public static int GetStringNum(String str)
    {
    	int Countnum = 0;
    	for (int i = 0; i < str.length(); i++) {  
    		  
            char c = str.charAt(i);  
            if(isChinese(c))
            {
            	Countnum=Countnum+2;  
            }
            else if (c >= '0' && c <= '9') {  
            	Countnum++;  
            }else if((c >= 'a' && c<='z') || (c >= 'A' && c<='Z')){  
            	Countnum++;  
            }else{  
            	Countnum++;  
            }  
        }  
    	return Countnum;
    }
    
    //判断是否为中文
    public static boolean isChinese(char c) {  
    	  
        Character.UnicodeBlock ub = Character.UnicodeBlock.of(c);  
  
        if (ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS  
  
                || ub == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS  
  
                || ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A  
  
                || ub == Character.UnicodeBlock.GENERAL_PUNCTUATION  
  
                || ub == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION  
  
                || ub == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS) {  
  
            return true;  
  
        }  
        return false;  
    }  
    
}
