package com.woyou.bean.rpc;

import com.woyou.bean.SuperModel;

/**
 * 获取新订单列表的返回bean
 * 
 * @author 荣
 * 
 */
public class QueryNewOidListRes extends SuperModel {
	/**
	 * 新订单id
	 */
	private String oId = "";

	public String getoId() {
		return oId;
	}

	public void setoId(String oId) {
		this.oId = oId;
	}

}
