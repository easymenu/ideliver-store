package com.woyou.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.renderscript.Element.DataType;
import android.telephony.TelephonyManager;

import com.jing.biandang.R;

public class StateJudgeTools {
	/**
	 * 检测sd卡是否可用
	 * 
	 * @return
	 */
	public static boolean isSDAvailable() {
		String sdStatus = Environment.getExternalStorageState();
		if (!sdStatus.equals(Environment.MEDIA_MOUNTED)) {
			return false;
		}
		return true;
	}

	/**
	 * 获取本地版本号名称 versionName
	 * 
	 * @param context
	 * @return
	 */
	public static String getLocalVersionName(Context context) {
		PackageInfo info = null;
		try {
			info = context.getPackageManager().getPackageInfo(
					context.getPackageName(), 0);
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		return info.versionName;
	}

	/**
	 * 检测当的网络（WLAN、3G/2G）状态
	 * 
	 * @param context
	 *            Context
	 * @return true 表示网络可用
	 */
	public static boolean isNetworkAvailable(Context context) {
		ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity != null) {
			NetworkInfo info = connectivity.getActiveNetworkInfo();
			if (info != null && info.isConnected()) {
				// 当前网络是连接的
				if (info.getState() == NetworkInfo.State.CONNECTED) {
					// 当前所连接的网络可用
					return true;
				}
			}
		}
		return false;
	}
	/**
	 * 测试WIFI信号强度
	 * @param context
	 * @return
	 */
	public static int getWifiRssi(Context context){
		WifiManager wifi_service = (WifiManager)context.getSystemService(Context.WIFI_SERVICE); 
		WifiInfo wifiInfo = wifi_service.getConnectionInfo();
		return wifiInfo.getRssi();
	}
	/**
	 * 获取当前的网络状态 -1：没有网络 ，1：WIFI网络， 2：2G网络， 3：3G网络，4：4G网络，5：以太网
	 * 
	 * @author 荣
	 * @param context
	 * @return
	 */
	public static int getNetworkState(Context c) {
		int netType = -1;
		ConnectivityManager connectivityManager = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = connectivityManager.getActiveNetworkInfo();
		if (netInfo == null) {
			return netType;
		}
		int type = netInfo.getType();
		if (type == ConnectivityManager.TYPE_WIFI) {
			//wifi
			netType = 1;
		}else if(type==ConnectivityManager.TYPE_ETHERNET){
			//以太网
			netType=5;
		}else if (type == ConnectivityManager.TYPE_MOBILE) {
			switch(netInfo.getSubtype()){
				case TelephonyManager.NETWORK_TYPE_UMTS:
				case TelephonyManager.NETWORK_TYPE_HSUPA:
				case TelephonyManager.NETWORK_TYPE_HSDPA:
				case TelephonyManager.NETWORK_TYPE_HSPA:
				case TelephonyManager.NETWORK_TYPE_HSPAP:
				case TelephonyManager.NETWORK_TYPE_EVDO_0:
				case TelephonyManager.NETWORK_TYPE_EVDO_A:
				case TelephonyManager.NETWORK_TYPE_EVDO_B:
				case TelephonyManager.NETWORK_TYPE_EHRPD:
				//3G
				netType = 3;
				break;
				case TelephonyManager.NETWORK_TYPE_EDGE:
				case TelephonyManager.NETWORK_TYPE_GPRS:
				case TelephonyManager.NETWORK_TYPE_CDMA:
				case TelephonyManager.NETWORK_TYPE_1xRTT:
				case TelephonyManager.NETWORK_TYPE_IDEN:
				//2G
				netType = 2;
				break;
			case TelephonyManager.NETWORK_TYPE_LTE:
				//4G
				netType=4;
				break;
			}
		}
		return netType;
	}

}
