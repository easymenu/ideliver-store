/** 
 * @Title:  MenuViewController.java 
 * @author:  xuron
 * @data:  2015年11月25日 下午4:59:15 <创建时间>
 * 
 * @history：<以下是历史记录>
 *
 * @modifier: <修改人>
 * @modify date: 2015年11月25日 下午4:59:15 <修改时间>
 * @log: <修改内容>
 *
 * @modifier: <修改人>
 * @modify date: 2015年11月25日 下午4:59:15 <修改时间>
 * @log: <修改内容>
 */
package com.woyou.controller;

import java.util.Date;

import com.jing.biandang.R;
import com.squareup.picasso.Picasso;
import com.woyou.Constants;
import com.woyou.activity.HomeActivity;
import com.woyou.bean.DeliveryTime;
import com.woyou.bean.WYList;
import com.woyou.bean.rpc.UserLoginRes;
import com.woyou.component.ActionbarView;
import com.woyou.component.MenuView;
import com.woyou.utils.ACache;
import com.woyou.utils.FormatTools;
import com.woyou.utils.TypeJudgeTools;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import net.simonvt.menudrawer.MenuDrawer;

/**
 * 抽离MenuView中的逻辑Controller
 * 
 * @author xuron
 * @versionCode 1 <每次修改提交前+1>
 */
public class MenuViewController extends SuperController {
	/**
	 * 营业订单
	 */
	public final int MENU_ORDER = 0;
	/**
	 * 暂缺商品
	 */
	public final int MENU_SOLDOUT = 1;
	/**
	 * 消息中心
	 */
	public final int MENU_MESSAGE = 2;
	/**
	 * 老板
	 */
	public final int MENU_BOSS = 3;
	/**
	 * 商品管理
	 */
	public final int MENU_GOODS = 4;
	/**
	 * 营销分析
	 */
	public final int MENU_ANALYSIS = 5;
	/**
	 * 设置老板密码
	 */
	public final int MENU_BOSSPWD = 6;
	/**
	 * 设置
	 */
	public final int MENU_SETTING = 7;
	/**
	 * 关于
	 */
	public final int MENU_ABOUT = 8;
	/**
	 * 打款管理
	 */
	public final int MNEU_PLAYMANEY = 9;

	private HomeActivity homeActivity;
	private FragmentManager fm;
	private FragmentTransaction ft;
	private MenuDrawer menuDrawer;

	public MenuViewController(Context mContext, FragmentManager fm, MenuDrawer menuDrawer) {
		super(mContext);
		this.mContext = mContext;
		this.fm = fm;
		this.menuDrawer = menuDrawer;
		homeActivity = (HomeActivity) mContext;
	}

	/**
	 * 隐藏mask使actionbar上的点击事件可用
	 */
	public void hideMask() {
		runOnUI(new Runnable() {
			@Override
			public void run() {
				menuDrawer.setTouchMode(MenuDrawer.TOUCH_MODE_BEZEL);
				homeActivity.actionbarView.layout_actionbar_mask.setVisibility(View.GONE);
			}
		});
	}

	/**
	 * 判断是否显示打款管理选项
	 */
	public void isShowPMManager(MenuView menuView) {
		ACache aCache = ACache.get(mContext);
		if (aCache.getAsObject("loginUserInfo") != null) {
			UserLoginRes userLoginRes = (UserLoginRes) aCache.getAsObject("loginUserInfo");
			if (userLoginRes.getIsChain() == 1) {
				android.view.ViewGroup.LayoutParams layoutParams = menuView.menu_folder.getLayoutParams();
				layoutParams.height = 210;
				menuView.menu_folder.setLayoutParams(layoutParams);
				menuView.menu_pmmanager.setVisibility(View.GONE);
			}
		}
	}

	/**
	 * 初始化数据操作
	 */
	public void init(final MenuView menuView, final ActionbarView actionbarView) {
		runOnUI(new Runnable() {

			@Override
			public void run() {
				menuView.isRefreshAtTime = true;
				// 获取根据时间表获取当前店铺营业状态
				getShopStatusByTimerList();
				// 连接中
				actionbarView.actionBarController.linkingShopStatus();
				// 获取店铺信息
				UserLoginRes loginRes = (UserLoginRes) aCache.getAsObject("loginUserInfo");
				if (loginRes != null) {
					if (!TextUtils.isEmpty(loginRes.getShopLogo())) {
						Picasso.with(mContext).load(loginRes.getShopLogo()).placeholder(R.raw.icon_default)
								.error(R.raw.icon_default).into(menuView.menuImage);
					} else {
						menuView.menuImage.setImageResource(R.raw.icon_default);
					}

					String[] sNameArray = loginRes.getsName().split("\\(");
					menuView.menuName.setText(sNameArray[0]);
					if (!TextUtils.isEmpty(loginRes.getNotice())) {
						menuView.menuInfo.setText(loginRes.getNotice());
					} else if (sNameArray.length > 1) {
						menuView.menuInfo.setText("(" + sNameArray[1]);
					} else {
						menuView.menuInfo.setText("");
					}

					if (loginRes.getScoreNum() != 0) {
						menuView.menuScorenum.setText("(" + loginRes.getScoreNum() + ")");
					} else {
						menuView.menuScorenum.setText("");
					}

					// 星级
					setPraiseRate(menuView.menuStart, loginRes.getScore());
					// 判断是否支持在线支付
					if (loginRes.getIsOnlinePay() == 1) {
						menuView.menuPay.setVisibility(View.VISIBLE);
					} else {
						menuView.menuPay.setVisibility(View.GONE);
					}
					// 20元起送 / 月售200份
					String prompt = FormatTools.String2Money(loginRes.getStartPrice() + "") + "元起送 / ";
					menuView.menuSendup.setText(prompt);
					menuView.menuSalesnum.setText("月售" + loginRes.getSalesNum() + "份");
				}
			}
		});
	}

	/**
	 * 获取根据时间表获取当前店铺营业状态
	 */
	public void getShopStatusByTimerList() {
		String ServerTime = aCache.getAsString("ServerTime");
		if (!TypeJudgeTools.isNull(ServerTime)) {
			// 构建一个新的时间字符串
			String newTime = "";
			long stime = Long.valueOf(ServerTime);
			Date newDate = new Date(stime);
			int h = newDate.getHours();
			int m = newDate.getMinutes();
			if (h <= 9) {
				newTime = "0" + h;
			} else {
				newTime = "" + h;
			}

			if (m >= 30) {
				newTime = newTime + ":30";
			} else {
				newTime = newTime + ":00";
			}

			DeliveryTime deliveryTime = null;
			// 获取外送时间段
			WYList<DeliveryTime> deliveryWyList = (WYList<DeliveryTime>) aCache.getAsObject("delivertime");
			for (int i = 0; i < deliveryWyList.size(); i++) {
				if (!TextUtils.isEmpty(deliveryWyList.get(i).getContent())) {
					String time = deliveryWyList.get(i).getContent().substring(0, 5);
					if (time.equals(newTime)) {
						deliveryTime = deliveryWyList.get(i);
						if (deliveryTime.getFlag().equals("0")) {
							Constants.shopStatus = "S";
						} else {
							Constants.shopStatus = "B";
						}
						break;
					}
				}
			}
		}
	}

	/**
	 * 评分星级
	 * 
	 * @param img
	 * @param score
	 */
	public void setPraiseRate(ImageView img, float score) {
		if (score == 0) {
			img.setImageResource(R.raw.icon_menu_star0);
		} else if (score <= 1) {
			img.setImageResource(R.raw.icon_menu_star05);
		} else if (score <= 2) {
			img.setImageResource(R.raw.icon_menu_star1);
		} else if (score <= 3) {
			img.setImageResource(R.raw.icon_menu_star15);
		} else if (score <= 4) {
			img.setImageResource(R.raw.icon_menu_star2);
		} else if (score <= 5) {
			img.setImageResource(R.raw.icon_menu_star25);
		} else if (score <= 6) {
			img.setImageResource(R.raw.icon_menu_star3);
		} else if (score <= 7) {
			img.setImageResource(R.raw.icon_menu_star35);
		} else if (score <= 8) {
			img.setImageResource(R.raw.icon_menu_star4);
		} else if (score <= 9) {
			img.setImageResource(R.raw.icon_menu_star45);
		} else {
			img.setImageResource(R.raw.icon_menu_star5);
		}
	}

	/**
	 * 判断是否显示打款管理选项
	 */
	public void isShowPMManager(LinearLayout menu_folder, RelativeLayout menu_pmmanager) {
		ACache aCache = ACache.get(mContext);
		if (aCache.getAsObject("loginUserInfo") != null) {
			UserLoginRes userLoginRes = (UserLoginRes) aCache.getAsObject("loginUserInfo");
			if (userLoginRes.getIsChain() == 1) {
				android.view.ViewGroup.LayoutParams layoutParams = menu_folder.getLayoutParams();
				layoutParams.height = 210;
				menu_folder.setLayoutParams(layoutParams);
				menu_pmmanager.setVisibility(View.GONE);
			}
		}
	}

	/**
	 * 根据点击 显示当前选中的mark 0:营业订单 1:暂缺商品 2:消息中心 3:老板 4:商品管理 5:营销分析 6:设置老板密码 7:设置
	 * 8:关于 9:打款管理
	 */
	public void showMark(MenuView menuView, int i) {
		menuView.menu_order_mark.setVisibility(View.GONE);
		menuView.menu_soldout_mark.setVisibility(View.GONE);
		menuView.menu_message_mark.setVisibility(View.GONE);
		menuView.menu_boss_mark.setVisibility(View.GONE);
		menuView.menu_goodsmanager_mark.setVisibility(View.GONE);
		menuView.menu_analysis_mark.setVisibility(View.GONE);
		menuView.menu_pmmanager_mark.setVisibility(View.GONE);
		menuView.menu_bosspw_mark.setVisibility(View.GONE);
		menuView.menu_setting_mark.setVisibility(View.GONE);
		menuView.menu_about_mark.setVisibility(View.GONE);
		switch (i) {
		case MENU_ORDER:
			menuView.menu_order_mark.setVisibility(View.VISIBLE);
			break;
		case MENU_SOLDOUT:
			menuView.menu_soldout_mark.setVisibility(View.VISIBLE);
			break;
		case MENU_MESSAGE:
			menuView.menu_message_mark.setVisibility(View.VISIBLE);
			break;
		case MENU_BOSS:
			menuView.menu_boss_mark.setVisibility(View.VISIBLE);
			break;
		case MENU_GOODS:
			menuView.menu_goodsmanager_mark.setVisibility(View.VISIBLE);
			break;
		case MENU_ANALYSIS:
			menuView.menu_analysis_mark.setVisibility(View.VISIBLE);
			break;
		case MENU_BOSSPWD:
			menuView.menu_bosspw_mark.setVisibility(View.VISIBLE);
			break;
		case MENU_SETTING:
			menuView.menu_setting_mark.setVisibility(View.VISIBLE);
			break;
		case MENU_ABOUT:
			menuView.menu_about_mark.setVisibility(View.VISIBLE);
			break;
		case MNEU_PLAYMANEY:
			menuView.menu_pmmanager_mark.setVisibility(View.VISIBLE);
			break;
		}
	}

}
