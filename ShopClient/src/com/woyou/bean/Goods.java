package com.woyou.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * 商品model
 * 
 * @author 荣
 * 
 */
public class Goods extends SuperModel {
	/**
	 * 商品ID
	 */
	private String gId = "";
	/**
	 * 商品名称
	 */
	private String gName = "";
	/**
	 * 商品单价
	 */
	private float price;
	/**
	 * 商品单位
	 */
	private String unit = "";
	/**
	 * 商品数量
	 */
	private int saleNum;
	/**
	 * 商品总价
	 */
	private float saleFee;
	/**
	 * 商品条码
	 */
	private String pluCode;
	/**
	 * 商品属性简介
	 */
	private String props = "";
	/**
	 * 子商品列表
	 */
	private List<SubGoods> subGoodsList = new ArrayList<SubGoods>();

	public String getgId() {
		return gId;
	}

	public void setgId(String gId) {
		this.gId = gId;
	}

	public String getgName() {
		return gName;
	}

	public void setgName(String gName) {
		this.gName = gName;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public int getSaleNum() {
		return saleNum;
	}

	public void setSaleNum(int saleNum) {
		this.saleNum = saleNum;
	}

	public float getSaleFee() {
		return saleFee;
	}

	public void setSaleFee(float saleFee) {
		this.saleFee = saleFee;
	}

	public String getPluCode() {
		return pluCode;
	}

	public void setPluCode(String pluCode) {
		this.pluCode = pluCode;
	}

	public String getProps() {
		return props;
	}

	public void setProps(String props) {
		this.props = props;
	}

	public List<SubGoods> getSubGoodsList() {
		return subGoodsList;
	}

	public void setSubGoodsList(List<SubGoods> subGoodsList) {
		this.subGoodsList = subGoodsList;
	}

}
