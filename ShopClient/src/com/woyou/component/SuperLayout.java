/** 
 * @Title:  MenuView.java 
 * @author:  xuron
 * @data:  2015年11月20日 下午4:31:40 <创建时间>
 * 
 * @history：<以下是历史记录>
 *
 * @modifier: <修改人>
 * @modify date: 2015年11月20日 下午4:31:40 <修改时间>
 * @log: <修改内容>
 *
 * @modifier: <修改人>
 * @modify date: 2015年11月20日 下午4:31:40 <修改时间>
 * @log: <修改内容>
 */
package com.woyou.component;

import com.woyou.utils.ACache;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

/**
 * 超级layout
 * 
 * @author xuron
 * @versionCode 1 <每次修改提交前+1>
 */
public abstract class SuperLayout extends FrameLayout {
	/**
	 * 上下文对象
	 */
	public Context mContext;
	/**
	 * 缓存对象
	 */
	public ACache aCache;

	public SuperLayout(Context context) {
		super(context);
		mContext = context;
		aCache = ACache.get(mContext);
	}

	public SuperLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		mContext = context;
		aCache = ACache.get(mContext);
	}

	public SuperLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		mContext = context;
		aCache = ACache.get(mContext);
	}

	/**
	 * 初始化控件
	 */
	protected abstract void initView();
}
