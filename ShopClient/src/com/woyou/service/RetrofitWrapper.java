package com.woyou.service;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RestAdapter.LogLevel;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

import com.google.gson.Gson;
import com.squareup.okhttp.OkHttpClient;
import com.woyou.Constants;

/**
 * 网络接口服务(Service)的包装类
 * 
 * @author longtao.li
 */
public class RetrofitWrapper {
	OkHttpClient okHttpClient=new OkHttpClient();
	
	RequestInterceptor requestInterceptor = new RequestInterceptor() {
		@Override
		public void intercept(RequestFacade request) {
			request.addHeader("version", Constants.versionName);
		}
	};

	public <T> T getNetService(Class<T> clazz) {
		String endpoint = Constants.SERVER2;
		return getNetService(clazz, endpoint);
	}
	
	public <T> T getNetService(Class<T> clazz, String endPoiont) {
		RestAdapter ra = new RestAdapter.Builder().setEndpoint(endPoiont)
				.setLogLevel(LogLevel.FULL)
				.setConverter(new GsonConverter(new Gson()))
//				 .setErrorHandler(new RetrofitErrorHandler())
				 .setRequestInterceptor(requestInterceptor)
				 .setClient(new OkClient(okHttpClient))
				.build();
		return ra.create(clazz);
	}
}
