package com.woyou.bean.rpc;

import com.woyou.bean.SuperModel;

/**
 * 发送订单的返回Bean
 * 
 * @author 荣
 * 
 */
public class SendOrderRes extends SuperModel {
	/**
	 * 订单ID
	 */
	private String oId = "";
	/**
	 * 送出时间（unix时间戳）
	 */
	private long sendTime;
	/**
	 * 订单状态（9:已送出）
	 */
	private String status;

	public String getoId() {
		return oId;
	}

	public void setoId(String oId) {
		this.oId = oId;
	}

	public long getSendTime() {
		return sendTime;
	}

	public void setSendTime(long sendTime) {
		this.sendTime = sendTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
