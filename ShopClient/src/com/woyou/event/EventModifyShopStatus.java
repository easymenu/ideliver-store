package com.woyou.event;

/**
 * 修改营业状态的事件
 * 
 * @author zhou.ni
 * 
 */
public class EventModifyShopStatus {

	private String status;

	public String getId() {
		return status;
	}

	public void setId(String status) {
		this.status = status;
	}

	public EventModifyShopStatus(String status) {
		super();
		this.status = status;
	}

}
