package com.woyou.adapter;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.jing.biandang.R;

public class MultiSpinnerPopAdapter extends BaseAdapter {
	private ArrayList<HashMap<String,Object>> arrayList;
	private LayoutInflater layoutInflater;
	private TextView item_taskmanager_multispinner_listview_tv;
	private CheckBox item_taskmanager_multispinner_listview_cb;
	
	public MultiSpinnerPopAdapter(Context context, ArrayList<HashMap<String,Object>> arrayList) {
		this.arrayList = arrayList;
		layoutInflater=LayoutInflater.from(context);
	}
	@Override
	public int getCount() {
		return arrayList.size();
	}
	@Override
	public Object getItem(int position) {
		return position;
	}
	@Override
	public long getItemId(int position) {
		return position;
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		HashMap<String,Object> hashmap=arrayList.get(position);
		if (convertView == null) {
			convertView = layoutInflater.inflate(R.layout.item_multispinner_listview,null);
			item_taskmanager_multispinner_listview_tv= (TextView) convertView.findViewById(R.id.item_taskmanager_multispinner_listview_tv);
			item_taskmanager_multispinner_listview_cb=(CheckBox)convertView.findViewById(R.id.item_taskmanager_multispinner_listview_cb);	
		}
		item_taskmanager_multispinner_listview_tv.setText(hashmap.get("name").toString());
		
		if(Boolean.parseBoolean(hashmap.get("state").toString())){
			item_taskmanager_multispinner_listview_cb.setChecked(true);
		}else{
			item_taskmanager_multispinner_listview_cb.setChecked(false);
		}
		return convertView;
	}

}
