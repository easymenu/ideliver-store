package com.woyou.component;

import java.util.Timer;
import java.util.TimerTask;

import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jing.biandang.R;
import com.woyou.Constants;
import com.woyou.activity.HomeActivity;
import com.woyou.bean.UserInfo;
import com.woyou.bean.rpc.Result;
import com.woyou.bean.rpc.SetShopStatusReq;
import com.woyou.event.EventModifyShopStatus;
import com.woyou.model.rpc.ShopModel;
import com.woyou.utils.LoginUtils;
import com.woyou.utils.ThreadPoolManager;

import de.greenrobot.event.EventBus;

public class StateChoicePopup implements OnClickListener {
	private HomeActivity homeActivity;
	private RelativeLayout positionView;
	private TextView layout_home_actionbar_choice_tv;
	private PopupWindow popupWindow;
	private LinearLayout pop_home_bg;
	private RelativeLayout pop_home_business;
	private RelativeLayout pop_home_busyonehour;
	private RelativeLayout pop_home_busytwohour;
	private RelativeLayout pop_home_rest;
	
	//定时器定时改变营业状态
	private Timer timer;
	private TimerTask timerTask;
	private SetShopStatusReq setShopStatusReq = new SetShopStatusReq();				//设置店铺营业状态
	
	public StateChoicePopup(HomeActivity activity, RelativeLayout positionView,TextView tv) {
		this.homeActivity = activity;
		this.positionView = positionView;
		this.layout_home_actionbar_choice_tv = tv;
	}

	// 弹出泡泡窗体
	public void showPop() {
		// 实例化泡泡窗台的布局文件
		View contentView = LayoutInflater.from(homeActivity).inflate(R.layout.pop_home_choice, null);
		pop_home_bg = (LinearLayout) contentView.findViewById(R.id.pop_home_bg);
		pop_home_business = (RelativeLayout) contentView.findViewById(R.id.pop_home_business);
		pop_home_busyonehour = (RelativeLayout) contentView.findViewById(R.id.pop_home_busyonehour);
		pop_home_busytwohour = (RelativeLayout) contentView.findViewById(R.id.pop_home_busytwohour);
		pop_home_rest = (RelativeLayout) contentView.findViewById(R.id.pop_home_rest);

		pop_home_bg.setOnClickListener(this);
		pop_home_business.setOnClickListener(this);
		pop_home_busyonehour.setOnClickListener(this);
		pop_home_busytwohour.setOnClickListener(this);
		pop_home_rest.setOnClickListener(this);

		// 实例化泡泡窗体
		popupWindow = new PopupWindow(contentView, 200,ViewGroup.LayoutParams.WRAP_CONTENT);
		popupWindow.setFocusable(true);
		popupWindow.setBackgroundDrawable(new BitmapDrawable());

		// 显示泡泡窗体
		popupWindow.showAsDropDown(positionView, 0, -10);
		
		UserInfo userInfo = LoginUtils.getUserInfo(homeActivity);
		if ( userInfo!=null ){
			setShopStatusReq.setsId(userInfo.getsId());
			setShopStatusReq.setPwd(userInfo.getPwd());
		}
	}
	/**
	 * 繁忙定时转到正常营业
	 * @param type
	 * 1:繁忙一小时
	 * 2:繁忙两小时
	 */
	public void busy2Business(int type){
		if(timer!=null){
			timer.cancel();
			timer=null;
		}
		if(timerTask!=null){
			timerTask.cancel();
			timerTask=null;
		}
		timer=new Timer();
		timerTask=new TimerTask() {
			@Override
			public void run() {
				//重置繁忙中
				homeActivity.actionbarView.busyStatus=0;
				homeActivity.menuView.init();
			}
		};
		switch(type){
			case 1:
				timer.schedule(timerTask, 1*60*60*1000);
				break;
			case 2:
				timer.schedule(timerTask, 2*60*60*1000);
				break;
		}
	}
	
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.pop_home_bg:
			break;
		case R.id.pop_home_business:
			homeActivity.menuView.isRefreshAtTime=true;
			Constants.refuseOrderNum=0;
			//重置繁忙中
			homeActivity.actionbarView.busyStatus=0;
			homeActivity.actionbarView.isSettingShopStatus=false;			//取消连接中
			ThreadPoolManager.getInstance().executeTask(new Runnable() {
				@Override
				public void run() {
					try {
						homeActivity.showLoadingHint();
						setShopStatusReq.setStatus("S");
						Result result = ShopModel.getInstance(homeActivity).setShopStatus(setShopStatusReq);
						if ( result!=null && result.code==1 ) {
							homeActivity.runOnUiThread(new Runnable() {
								@Override
								public void run() {
									cancelTimer();
									// 设置营业状态
									Constants.shopStatus = "S";
									positionView.setBackgroundResource(R.raw.actionbar_state_green);
									layout_home_actionbar_choice_tv.setText("营业中");
									homeActivity.menuView.menuStatus.setImageResource(R.raw.icon_menu_status_business);
									EventModifyShopStatus eventModifyShopStatus = new EventModifyShopStatus("S");
									EventBus.getDefault().post(eventModifyShopStatus);
								}
							});
						} else if( result != null && result.code==-3  ){
							homeActivity.homeController.exitLogin();
						} 
						else {
							homeActivity.runOnUiThread(new Runnable() {
								@Override
								public void run() {
									SuperUI.openToast(homeActivity, "设置失败");
								}
							});
						}
					} catch (Exception e) {
						e.printStackTrace();
					} finally {
						homeActivity.hideLoadingHint();
					}
				}
			});
			break;
		case R.id.pop_home_busyonehour:
			homeActivity.menuView.isRefreshAtTime=false;
			Constants.refuseOrderNum=0;
			homeActivity.actionbarView.isSettingShopStatus=false;				//取消连接中
			ThreadPoolManager.getInstance().executeTask(new Runnable() {
				@Override
				public void run() {
					try {
						homeActivity.showLoadingHint();
						setShopStatusReq.setStatus("B");
						Result result = ShopModel.getInstance(homeActivity).setShopStatus(setShopStatusReq);
						if ( result!=null && result.code==1 ) {
							homeActivity.runOnUiThread(new Runnable() {
								@Override
								public void run() {
									// 设置营业状态
									Constants.shopStatus = "B";
									//标记繁忙状态1小时
									homeActivity.actionbarView.busyStatus=1;
									positionView.setBackgroundResource(R.raw.actionbar_state_yellow);
									layout_home_actionbar_choice_tv.setText("繁忙1小时");
									homeActivity.menuView.menuStatus.setImageResource(R.raw.icon_menu_status_busy);
									EventModifyShopStatus eventModifyShopStatus = new EventModifyShopStatus("B");
									EventBus.getDefault().post(eventModifyShopStatus);
									busy2Business(1);
								}
							});
						} else if( result != null && result.code==-3  ){
							homeActivity.homeController.exitLogin();
						} 
					} catch (Exception e) {
						e.printStackTrace();
					} finally {
						homeActivity.hideLoadingHint();
					}
				}
			});
			break;
		case R.id.pop_home_busytwohour:
			homeActivity.menuView.isRefreshAtTime=false;
			Constants.refuseOrderNum=0;
			homeActivity.actionbarView.isSettingShopStatus=false;				//取消连接中
			ThreadPoolManager.getInstance().executeTask(new Runnable() {
				@Override
				public void run() {
					try {
						homeActivity.showLoadingHint();
						setShopStatusReq.setStatus("B");
						Result result = ShopModel.getInstance(homeActivity).setShopStatus(setShopStatusReq);
						if ( result!=null && result.code==1 ) {
							homeActivity.runOnUiThread(new Runnable() {
								@Override
								public void run() {
									// 设置营业状态
									Constants.shopStatus = "B";
									//标记繁忙状态2小时
									homeActivity.actionbarView.busyStatus=2;
									positionView.setBackgroundResource(R.raw.actionbar_state_orange);
									layout_home_actionbar_choice_tv.setText("繁忙2小时");
									homeActivity.menuView.menuStatus.setImageResource(R.raw.icon_menu_status_busy);
									EventModifyShopStatus eventModifyShopStatus = new EventModifyShopStatus("B");
									EventBus.getDefault().post(eventModifyShopStatus);
									busy2Business(2);
								}
							});
						} else if( result != null && result.code==-3  ){
							homeActivity.homeController.exitLogin();
						} 
					} catch (Exception e) {
						e.printStackTrace();
					} finally {
						homeActivity.hideLoadingHint();
					}
				}
			});
			break;
		case R.id.pop_home_rest:
			homeActivity.menuView.isRefreshAtTime=false;
			Constants.refuseOrderNum=0;
			//重置繁忙中
			homeActivity.actionbarView.busyStatus=0;
			homeActivity.actionbarView.isSettingShopStatus=false;   //取消连接中
			ThreadPoolManager.getInstance().executeTask(new Runnable() {
				@Override
				public void run() {
					try {
						homeActivity.showLoadingHint();
						setShopStatusReq.setStatus("C");
						Result result = ShopModel.getInstance(homeActivity).setShopStatus(setShopStatusReq);
						if ( result!=null && result.code==1 ) {
							homeActivity.runOnUiThread(new Runnable() {
								@Override
								public void run() {
									cancelTimer();
									// 设置营业状态
									Constants.shopStatus = "C";
									positionView.setBackgroundResource(R.raw.actionbar_state_red);
									layout_home_actionbar_choice_tv.setText("休息");
									homeActivity.menuView.menuStatus.setImageResource(R.raw.icon_menu_status_rest);
									EventModifyShopStatus eventModifyShopStatus = new EventModifyShopStatus("C");
									EventBus.getDefault().post(eventModifyShopStatus);
								}
							});
						} else if( result != null && result.code==-3  ){
							homeActivity.homeController.exitLogin();
						} 
					} catch (Exception e) {
						e.printStackTrace();
					} finally {
						homeActivity.hideLoadingHint();
					}
				}
			});
			break;
		}
		popupWindow.dismiss();
	}

	/**
	 * 取消定时Timer
	 */
	private void cancelTimer(){
		if(timer!=null){
			timer.cancel();
			timer=null;
		}
		if(timerTask!=null){
			timerTask.cancel();
			timerTask=null;
		}
	}
}
