package com.woyou.bean.rpc;

import com.woyou.bean.SuperModel;

/**
 * 获取昨日信息简介请求
 * @author tlc
 *
 */
public class QueryYesterdayInfoReq extends SuperModel {

	/**
	 * 店铺Id
	 */
	private String sId = "";
	/**
	 * 密码
	 */
	private String pwd = "";
	
	public String getsId() {
		return sId;
	}
	public void setsId(String sId) {
		this.sId = sId;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
}
