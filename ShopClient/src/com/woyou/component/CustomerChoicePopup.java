package com.woyou.component;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jing.biandang.R;

public class CustomerChoicePopup implements OnClickListener {
	private Context context;
	private RelativeLayout positionView;
	private TextView valueView;
	private PopupWindow popupWindow;
	private RelativeLayout pop_customer_orders;
	private RelativeLayout pop_customer_consume;
	
	private CustomerOrderListener mListener;
	
	public void setmListener(CustomerOrderListener mListener) {
		this.mListener = mListener;
	}

	public CustomerChoicePopup(Context context, RelativeLayout positionView,
			TextView valueView) {
		this.context = context;
		this.positionView = positionView;
		this.valueView = valueView;
	}

	// 弹出泡泡窗体
	public void showPopup() {
		// 实例化泡泡窗台的布局文件
		View contentView = LayoutInflater.from(context).inflate(R.layout.pop_customer_choice, null);
		pop_customer_orders = (RelativeLayout) contentView.findViewById(R.id.pop_customer_orders);
		pop_customer_consume = (RelativeLayout) contentView.findViewById(R.id.pop_customer_consume);

		pop_customer_orders.setOnClickListener(this);
		pop_customer_consume.setOnClickListener(this);

		// 实例化泡泡窗体
		if (popupWindow == null) {
			popupWindow = new PopupWindow(contentView, positionView.getWidth(), ViewGroup.LayoutParams.WRAP_CONTENT);
		}
		popupWindow.setFocusable(true);
		popupWindow.setBackgroundDrawable(new BitmapDrawable());

		// 显示泡泡窗体
		popupWindow.showAsDropDown(positionView, 0, 0);
	}

	/**
	 * 隐藏pop窗体
	 */
	public void hidePopup() {
		if (popupWindow != null && popupWindow.isShowing()) {
			popupWindow.dismiss();
		}
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.pop_customer_orders:
			valueView.setText("下单最多");
			mListener.refreshCustomerOrder("1");
			break;
		case R.id.pop_customer_consume:
			valueView.setText("消费最多");
			mListener.refreshCustomerOrder("2");
			break;
		}
		popupWindow.dismiss();
	}
	
	/**
	 * 刷新顾客分析数据
	 * @author zhou.ni
	 *
	 */
	public interface CustomerOrderListener{
		void refreshCustomerOrder(String sortType);
	}
	
}

