package com.woyou.bean;

/**
 * 外送费
 * 
 * @author 荣
 * 
 */
public class DeliverFee {
	private float price; // 外送费

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

}
