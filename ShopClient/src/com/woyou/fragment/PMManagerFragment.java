package com.woyou.fragment;

import java.util.ArrayList;
import java.util.List;

import com.jing.biandang.R;
import com.woyou.activity.HomeActivity;
import com.woyou.adapter.PlayMoneyAdapter;
import com.woyou.bean.rpc.QueryPlayMoneyDetailsReq;
import com.woyou.bean.rpc.QueryPlayMoneyDetailsRes;
import com.woyou.bean.rpc.Result;
import com.woyou.component.SuperUI;
import com.woyou.model.rpc.OrderModel;
import com.woyou.utils.LoginUtils;
import com.woyou.utils.ThreadPoolManager;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import retrofit.RetrofitError;

public class PMManagerFragment extends SuperFragment implements OnClickListener {
	private HomeActivity homeActivity;
	private View layout_pmmanager;
	private PlayMoneyAdapter mAdapter;
	private List<Object> mData = new ArrayList<Object>();// 数据源

	// 非连锁店已开通有记录
	private LinearLayout pm_panel;
	private ListView mListView;
	private RelativeLayout bindcard_layout;
	private TextView bindcard;
	//非连锁店已开通没有记录
	private RelativeLayout nodata_layout;
	private TextView nodata_bindcard;
	//非连锁店未开通
	private RelativeLayout chainno_close_panel;
	private OrderModel orderModel;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		layout_pmmanager = inflater.inflate(R.layout.layout_pmmanager, container, false);
		homeActivity=(HomeActivity)getActivity();
		orderModel = OrderModel.getInstance(homeActivity);
		return layout_pmmanager;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		initView();
		initData();
	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		super.onHiddenChanged(hidden);
		if (!hidden) {
			mData.clear();// 数据源清理
			initData();
		}
	}

	private void initView() {
		//
		nodata_layout=(RelativeLayout)layout_pmmanager.findViewById(R.id.unchain_nodata_layout);
		nodata_bindcard=(TextView)layout_pmmanager.findViewById(R.id.unchain_nodata_bindcard);
		nodata_layout.setOnClickListener(this);
		//
		chainno_close_panel = (RelativeLayout) layout_pmmanager.findViewById(R.id.chainno_close_panel);
		//
		pm_panel = (LinearLayout) layout_pmmanager.findViewById(R.id.pm_panel);
		bindcard_layout = (RelativeLayout) layout_pmmanager.findViewById(R.id.bindcard_layout);
		bindcard_layout.setOnClickListener(this);
		bindcard = (TextView) layout_pmmanager.findViewById(R.id.bindcard);
		mListView = (ListView) layout_pmmanager.findViewById(R.id.lvPlayManeger);
		mAdapter = new PlayMoneyAdapter(getActivity(), mData);
		mListView.setAdapter(mAdapter);
	}

	private void initData() {
		ThreadPoolManager.getInstance().executeTask(new Runnable() {
			@Override
			public void run() {
				try {
					homeActivity.showLoadingHint();
					QueryPlayMoneyDetailsReq queryPlayMoneyDetailsReq = new QueryPlayMoneyDetailsReq();
					queryPlayMoneyDetailsReq.setsId(LoginUtils.getUserInfo(homeActivity).getsId());
					queryPlayMoneyDetailsReq.setPwd(LoginUtils.getUserInfo(homeActivity).getPwd());
					final Result<QueryPlayMoneyDetailsRes> result = orderModel.v2_3queryPlayMoneyDetails(queryPlayMoneyDetailsReq);
					if (result != null && result.code == 1) {
						if (result.getData() != null) {
							homeActivity.runOnUiThread(new Runnable() {

								@Override
								public void run() {
									QueryPlayMoneyDetailsRes queryPlayMoneyDetailsRes = result.getData();
									if (queryPlayMoneyDetailsRes.getIsOnlinePay() == 1) {
										chainno_close_panel.setVisibility(View.GONE);
										if((queryPlayMoneyDetailsRes.getPlayMoneys()!=null&&!queryPlayMoneyDetailsRes.getPlayMoneys().isEmpty())||(queryPlayMoneyDetailsRes.getStayPays()!=null&&!queryPlayMoneyDetailsRes.getStayPays().isEmpty())){
											pm_panel.setVisibility(View.VISIBLE);
											nodata_layout.setVisibility(View.GONE);
											if(queryPlayMoneyDetailsRes.getStayPays()!=null){
												mData.addAll(queryPlayMoneyDetailsRes.getStayPays());
											}
											mData.add("打款记录");
											if(queryPlayMoneyDetailsRes.getPlayMoneys()!=null){
												mData.addAll(queryPlayMoneyDetailsRes.getPlayMoneys());
											}else{
												mData.add("最近3个月内尚无打款记录");
											}
											mAdapter.notifyDataSetChanged();
											bindcard.setText(queryPlayMoneyDetailsRes.getBindCard() + ">");
										}else{
											pm_panel.setVisibility(View.GONE);
											nodata_layout.setVisibility(View.VISIBLE);
											nodata_bindcard.setText(queryPlayMoneyDetailsRes.getBindCard() + ">");
										}
									} else {
										nodata_layout.setVisibility(View.GONE);
										pm_panel.setVisibility(View.GONE);
										chainno_close_panel.setVisibility(View.VISIBLE);
									}
								}
							});
						}
					} else if (result != null && result.getCode() == -3) {
						homeActivity.homeController.exitLogin();
					}
				} catch (RetrofitError error) {
					error.printStackTrace();
				} finally {
					homeActivity.hideLoadingHint();
				}
			}
		});

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.unchain_nodata_layout:
		case R.id.bindcard_layout:
			SuperUI.showBindCordDialog(homeActivity);
			break;
		}
	}
}
