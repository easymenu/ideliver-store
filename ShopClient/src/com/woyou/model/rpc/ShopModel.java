package com.woyou.model.rpc;

import java.util.ArrayList;
import java.util.List;

import retrofit.RetrofitError;
import android.content.Context;

import com.woyou.bean.CommentItem;
import com.woyou.bean.DeliveryTime;
import com.woyou.bean.GoodsInfo;
import com.woyou.bean.GoodsTypeInfo;
import com.woyou.bean.MarkUp;
import com.woyou.bean.ReportItem;
import com.woyou.bean.ShopInfoItem;
import com.woyou.bean.UserReportItem;
import com.woyou.bean.rpc.MD5Req;
import com.woyou.bean.rpc.QueryCommentListReq;
import com.woyou.bean.rpc.QueryGoodsListReq;
import com.woyou.bean.rpc.QueryReportListReq;
import com.woyou.bean.rpc.QueryShopInfoReq;
import com.woyou.bean.rpc.QueryTypeListReq;
import com.woyou.bean.rpc.QueryUserReportListReq;
import com.woyou.bean.rpc.ReplyCommentReq;
import com.woyou.bean.rpc.Result;
import com.woyou.bean.rpc.ServerTimeRes;
import com.woyou.bean.rpc.SetShopStatusReq;
import com.woyou.bean.rpc.UpdateBoxFeeReq;
import com.woyou.bean.rpc.UpdateDeliveryTimesReq;
import com.woyou.bean.rpc.UpdateGoodsInfoReq;
import com.woyou.bean.rpc.UpdateLadderFeeReq;
import com.woyou.bean.rpc.UpdateShopInfoReq;
import com.woyou.service.RetrofitWrapper;
import com.woyou.service.ShopService;

/**
 * 商品业务逻辑控制类
 * 
 * @author 荣
 * 
 */
public class ShopModel extends SuperModel {
	private static Context mContext;
	private static ShopService shopService;
	private static ShopModel shopModel = new ShopModel();

	private ShopModel() {
	}

	/**
	 * 单例模式
	 * 
	 * @return
	 */
	public static ShopModel getInstance(Context context) {
		mContext=context;
		if (shopModel == null) {
			shopModel = new ShopModel();
		}
		if (mNeWrapper == null) {
			mNeWrapper = new RetrofitWrapper();
		}
		if (shopService == null) {
			shopService = mNeWrapper.getNetService(ShopService.class);
		}
		return shopModel;
	}

	/**
	 * 查询服务器时间
	 * 
	 * @param orderList
	 * @return
	 */
	public Result<ServerTimeRes> queryServerTime() throws RetrofitError {
		MD5Req md5Req = new MD5Req(mContext);
		Result<ServerTimeRes> result = shopService.queryServerTime(
				md5Req.jsonParams, md5Req.isEncrypted, md5Req.timeStamp,
				md5Req.randomNum, md5Req.sign);
		return result;
	}
	
	/**
	 * 2.18	设置店铺营业状态（http://xxxx/SetShopStatus）
	 * @return
	 * @throws RetrofitError
	 */
	public Result setShopStatus(SetShopStatusReq setShopStatusReq) throws RetrofitError {
		MD5Req<SetShopStatusReq> md5Req = new MD5Req<SetShopStatusReq>(mContext,setShopStatusReq);
		Result result = shopService.setShopStatus (
				md5Req.jsonParams, md5Req.isEncrypted, md5Req.timeStamp,
				md5Req.randomNum, md5Req.sign);
		return result;
	}
	
	/**
	 * 2.19	获取用户评论（http://xxxx/queryCommentList）
	 * @return
	 * @throws RetrofitError
	 */
	public Result<List<CommentItem>> queryCommentList(QueryCommentListReq queryCommentListReq) throws RetrofitError {
		MD5Req<QueryCommentListReq> md5Req = new MD5Req<QueryCommentListReq>(mContext,queryCommentListReq);
		Result<List<CommentItem>> result = shopService.queryCommentList (
				md5Req.jsonParams, md5Req.isEncrypted, md5Req.timeStamp,
				md5Req.randomNum, md5Req.sign);
		return result;
	}
	
	/**
	 * 2.20	回复评论（http://xxxx/replyComment）
	 * @return
	 * @throws RetrofitError
	 */
	public Result replyComment(ReplyCommentReq commentReq) throws RetrofitError {
		MD5Req<ReplyCommentReq> md5Req = new MD5Req<ReplyCommentReq>(mContext,commentReq);
		Result result = shopService.replyComment (
				md5Req.jsonParams, md5Req.isEncrypted, md5Req.timeStamp,
				md5Req.randomNum, md5Req.sign);
		return result;
	}
	
	/**
	 * 2.21	获取商铺信息（http://xxxx/queryShopInfo）
	 * @return
	 * @throws RetrofitError
	 */
	public Result<ShopInfoItem> queryShopInfo(QueryShopInfoReq shopInfoReq) throws RetrofitError {
		MD5Req<QueryShopInfoReq> md5Req = new MD5Req<QueryShopInfoReq>(mContext,shopInfoReq);
		Result<ShopInfoItem> result = shopService.queryShopInfo(
				md5Req.jsonParams, md5Req.isEncrypted, md5Req.timeStamp,
				md5Req.randomNum, md5Req.sign);
//		Result<ShopInfoItem> result = mockQueryShopInfo();
		return result;
	}
	
	/**
	 * 2.22	更改店铺信息（http://xxxx/updateShopInfo）
	 * @return
	 * @throws RetrofitError
	 */
	public Result updateShopInfo(UpdateShopInfoReq updateShopInfoReq) throws RetrofitError {
		MD5Req<UpdateShopInfoReq> md5Req = new MD5Req<UpdateShopInfoReq>(mContext,updateShopInfoReq);
		Result result = shopService.updateShopInfo(
				md5Req.jsonParams, md5Req.isEncrypted, md5Req.timeStamp,
				md5Req.randomNum, md5Req.sign);
		return result;
	}
	
	/**
	 * 2.23	更新外卖时段（http://xxxx/updateDeliveryTimes）
	 * @return
	 * @throws RetrofitError
	 */
	public Result updateDeliveryTimes(UpdateDeliveryTimesReq updateDeliveryTimesReq) throws RetrofitError {
		MD5Req<UpdateDeliveryTimesReq> md5Req = new MD5Req<UpdateDeliveryTimesReq>(mContext,updateDeliveryTimesReq);
		Result result = shopService.updateDeliveryTimes(
				md5Req.jsonParams, md5Req.isEncrypted, md5Req.timeStamp,
				md5Req.randomNum, md5Req.sign);
		return result;
	}
	
	/**
	 * 2.24	更新商铺阶梯外送费（http://xxxx/updateLadderFee）
	 * @return
	 * @throws RetrofitError
	 */
	public Result<List<MarkUp>> updateLadderFee(UpdateLadderFeeReq updateLadderFeeReq) throws RetrofitError {
		MD5Req<UpdateLadderFeeReq> md5Req = new MD5Req<UpdateLadderFeeReq>(mContext,updateLadderFeeReq);
		Result<List<MarkUp>> result = shopService.updateLadderFee(
				md5Req.jsonParams, md5Req.isEncrypted, md5Req.timeStamp,
				md5Req.randomNum, md5Req.sign);
		return result;
	}
	
	/**
	 * 2.25	获取日或月营业报表（http://xxxx/queryReportList）
	 * @return
	 * @throws RetrofitError
	 */
	public Result<ReportItem> queryReportList(QueryReportListReq queryReportListReq) throws RetrofitError {
		MD5Req<QueryReportListReq> md5Req = new MD5Req<QueryReportListReq>(mContext,queryReportListReq);
		Result<ReportItem> result = shopService.queryReportList(
				md5Req.jsonParams, md5Req.isEncrypted, md5Req.timeStamp,
				md5Req.randomNum, md5Req.sign);
		return result;
	}
	
	/**
	 * 2.26	用户排名报表（http://xxxx/queryUserReportList）
	 * @return
	 * @throws RetrofitError
	 */
	public Result<List<UserReportItem>> queryUserReportList(QueryUserReportListReq queryUserReportListReq) throws RetrofitError {
		MD5Req<QueryUserReportListReq> md5Req = new MD5Req<QueryUserReportListReq>(mContext,queryUserReportListReq);
		Result<List<UserReportItem>> result = shopService.queryUserReportList(
				md5Req.jsonParams, md5Req.isEncrypted, md5Req.timeStamp,
				md5Req.randomNum, md5Req.sign);
		return result;
	}
	
	/**
	 * 2.9	获取商品分类列表（http://xxxx/queryTypeList）
	 * @return
	 * @throws RetrofitError
	 */
	public Result<List<GoodsTypeInfo>> queryTypeList(QueryTypeListReq queryTypeListReq) throws RetrofitError {
		MD5Req<QueryTypeListReq> md5Req = new MD5Req<QueryTypeListReq>(mContext,queryTypeListReq);
		Result<List<GoodsTypeInfo>> result = shopService.queryTypeList(
				md5Req.jsonParams, md5Req.isEncrypted, md5Req.timeStamp,
				md5Req.randomNum, md5Req.sign);
		return result;
	}
	
	/**
	 * 2.10	获取商品列表（http://xxxx/queryGoodsList）
	 * @return
	 * @throws RetrofitError
	 */
	public Result<List<GoodsInfo>> queryGoodsList(QueryGoodsListReq queryGoodsListReq) throws RetrofitError {
		MD5Req<QueryGoodsListReq> md5Req = new MD5Req<QueryGoodsListReq>(mContext,queryGoodsListReq);
		Result<List<GoodsInfo>> result = shopService.queryGoodsList(
				md5Req.jsonParams, md5Req.isEncrypted, md5Req.timeStamp,
				md5Req.randomNum, md5Req.sign);
		return result;
	}
	
	/**
	 * 2.11	修改商品信息（http://xxxx/updateGoodsInfo）
	 * @return
	 * @throws RetrofitError
	 */
	public Result updateGoodsInfo(UpdateGoodsInfoReq updateGoodsInfoReq) throws RetrofitError {
		MD5Req<UpdateGoodsInfoReq> md5Req = new MD5Req<UpdateGoodsInfoReq>(mContext,updateGoodsInfoReq);
		Result result = shopService.updateGoodsInfo(
				md5Req.jsonParams, md5Req.isEncrypted, md5Req.timeStamp,
				md5Req.randomNum, md5Req.sign);
		return result;
	}
	
	//queryShopInfo mock数据
	private Result<ShopInfoItem> mockQueryShopInfo(){
		Result<ShopInfoItem> result = new Result<ShopInfoItem>();
		result.code = 1;
		result.msg = "查询成功";
		
		ShopInfoItem shopInfoItem = new ShopInfoItem();
		shopInfoItem.setoNum("123");
		shopInfoItem.setcNum("2365");
		shopInfoItem.setScore(9);
		shopInfoItem.setGrade("6");
		shopInfoItem.setRate("62.3");
		shopInfoItem.setsName("小倪酒家");
		shopInfoItem.setShopLogo("http://preview.quanjing.com/chineseview112/214-10723.jpg");
		shopInfoItem.setSalesNum(5685);
		shopInfoItem.setState("C");
		shopInfoItem.setRegister("feidao");
		shopInfoItem.setRdate(1425648835);
		shopInfoItem.setsType("烧烤麻辣烫、饮料店、中西餐、小吃店");
		shopInfoItem.setrPhone("18616742809");
		shopInfoItem.setNotice("有人的地方就有江湖 有江湖的地方就有酒");
		shopInfoItem.setBrief("红尘多可笑 痴情最无聊 目空一切也好 此生未了 心却已无所扰 只想换得半世逍遥 醒时对人笑 梦中全忘掉 叹天黑得太早 来生难料 爱恨一笔勾销 对酒当歌我只愿开心到老 风再冷不想逃 花再美也不想要任我飘摇 天越高心越小 不问因果有多少 独自醉倒 今天哭明天笑 不求有人能明了 一身骄傲 歌在唱舞在跳 长夜漫漫不觉晓 将快乐寻找");
		shopInfoItem.setDeliveryTime("7:00-21:00 6个营业时段");
		
		MarkUp markUp1 = new MarkUp();
		markUp1.setfId("001");
		markUp1.setRange(10);
		markUp1.setDeliverFee(5);
		markUp1.setSendUpFee(0);
		markUp1.setType(0);
		MarkUp markUp2 = new MarkUp();
		markUp2.setfId("002");
		markUp2.setRange(15);
		markUp2.setDeliverFee(10);
		markUp2.setSendUpFee(5);
		markUp2.setType(0);
		MarkUp markUp3 = new MarkUp();
		markUp3.setfId("003");
		markUp3.setRange(20);
		markUp3.setDeliverFee(15);
		markUp3.setSendUpFee(10);
		markUp3.setType(0);
		MarkUp markUp4 = new MarkUp();
		markUp4.setfId("004");
		markUp4.setRange(25);
		markUp4.setDeliverFee(18);
		markUp4.setSendUpFee(10);
		markUp4.setType(0);
		MarkUp markUp5 = new MarkUp();
		markUp5.setfId("005");
		markUp5.setRange(30);
		markUp5.setDeliverFee(15);
		markUp5.setSendUpFee(10);
		markUp5.setType(0);
		List<MarkUp> markUpList = new ArrayList<MarkUp>();
		markUpList.add(markUp1);
		markUpList.add(markUp2);
		markUpList.add(markUp3);
		markUpList.add(markUp4);
		markUpList.add(markUp5);
		shopInfoItem.setMarkUpList(markUpList);
		
		DeliveryTime deliveryTime1 = new DeliveryTime();
		deliveryTime1.settId("001");
		deliveryTime1.setFlag("0");
		deliveryTime1.setContent("09:00~09:30");
		DeliveryTime deliveryTime2 = new DeliveryTime();
		deliveryTime2.settId("002");
		deliveryTime2.setFlag("0");
		deliveryTime2.setContent("13:00~13:30");
		DeliveryTime deliveryTime3 = new DeliveryTime();
		deliveryTime3.settId("003");
		deliveryTime3.setFlag("0");
		deliveryTime3.setContent("15:00~15:30");
		DeliveryTime deliveryTime4 = new DeliveryTime();
		deliveryTime4.settId("004");
		deliveryTime4.setFlag("0");
		deliveryTime4.setContent("16:30~17:00");
		List<DeliveryTime> deliveryTimeList = new ArrayList<DeliveryTime>();
		deliveryTimeList.add(deliveryTime1);
		deliveryTimeList.add(deliveryTime2);
		deliveryTimeList.add(deliveryTime3);
		deliveryTimeList.add(deliveryTime4);
		shopInfoItem.setDeliveryTimeList(deliveryTimeList);
		
		shopInfoItem.setBoxFee(2);
		shopInfoItem.setBoxType(1);
		
		result.data = shopInfoItem;
		return result;
	}
	
	//mock 一般的返回数据
	private Result mockResult(){
		Result result = new Result();
		result.code=1;
		result.msg="成功";
		return result;
	}
	
	/**
	 * 2.28	更新餐盒费（http://xxxx/updateBoxFee）
	 * @return
	 * @throws RetrofitError
	 */
	public Result updateBoxFee(UpdateBoxFeeReq parame) throws RetrofitError {
		MD5Req<UpdateBoxFeeReq> md5Req = new MD5Req<UpdateBoxFeeReq>(mContext,parame);
		Result result = shopService.updateBoxFee(
				md5Req.jsonParams, md5Req.isEncrypted, md5Req.timeStamp,
				md5Req.randomNum, md5Req.sign);
		return result;
	}
	
}
