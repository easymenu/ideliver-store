package com.woyou.bean.rpc;

import com.woyou.bean.SuperModel;

/**
 * 同意或拒绝取消订单返回bean
 * 
 * @author 荣
 * 
 */
public class HandleCancelRes extends SuperModel {
	/**
	 * 订单ID
	 */
	private String oId = "";
	/**
	 * 订单状态
	 */
	private String status = "";

	public String getoId() {
		return oId;
	}

	public void setoId(String oId) {
		this.oId = oId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
