package com.woyou.component;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jing.biandang.R;
import com.woyou.fragment.shop.DeliveryFeeFragment;

public class DeliveryFeePopup implements OnClickListener {
	private Context context;
	private DeliveryFeeFragment fragment;
	private TextView positionView;
	private PopupWindow popupWindow;
	private RelativeLayout pop_deliveryfee_one;
	private RelativeLayout pop_deliveryfee_two;
	private RelativeLayout pop_deliveryfee_three;

	public DeliveryFeePopup(Context context, DeliveryFeeFragment fragment, TextView positionView) {
		this.context = context;
		this.fragment = fragment;
		this.positionView = positionView;
	}

	// 弹出泡泡窗体
	public void showPopup() {
		// 实例化泡泡窗台的布局文件
		View contentView = LayoutInflater.from(context).inflate(R.layout.pop_deliveryfee_choice, null);
		pop_deliveryfee_one = (RelativeLayout) contentView.findViewById(R.id.pop_deliveryfee_one);
		pop_deliveryfee_two = (RelativeLayout) contentView.findViewById(R.id.pop_deliveryfee_two);
		pop_deliveryfee_three = (RelativeLayout) contentView.findViewById(R.id.pop_deliveryfee_three);

		pop_deliveryfee_one.setOnClickListener(this);
		pop_deliveryfee_two.setOnClickListener(this);
		pop_deliveryfee_three.setOnClickListener(this);

		// 实例化泡泡窗体
		if (popupWindow == null) {
			popupWindow = new PopupWindow(contentView, 220, ViewGroup.LayoutParams.WRAP_CONTENT);
		}
		popupWindow.setFocusable(true);
		popupWindow.setBackgroundDrawable(new BitmapDrawable());

		// 显示泡泡窗体
		popupWindow.showAsDropDown(positionView, -20, -10);
	}

	/**
	 * 隐藏pop窗体
	 */
	public void hidePopup() {
		if (popupWindow != null && popupWindow.isShowing()) {
			popupWindow.dismiss();
		}
	}

	@Override
	public void onClick(View v) {
//		switch (v.getId()) {
//		case R.id.pop_deliveryfee_one:
//			positionView.setText("分段起送价");
//			fragment.model = "2";
//			fragment.switchPanel(2);
//			break;
//		case R.id.pop_deliveryfee_two:
//			fragment.model = "1";
//			positionView.setText("分段外送费");
//			fragment.switchPanel(1);
//			break;
//		case R.id.pop_deliveryfee_three:
//			fragment.model = "3";
//			positionView.setText("单一起送价");
//			fragment.switchPanel(3);
//			break;
//		}
//		popupWindow.dismiss();
	}
}