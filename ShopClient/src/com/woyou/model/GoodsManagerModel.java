package com.woyou.model;

import java.util.ArrayList;
import java.util.List;

import com.woyou.bean.GoodsItemInfo;
import com.woyou.bean.GoodsState;
import com.woyou.bean.OptionInfo;
import com.woyou.bean.OptionState;

/**
 * 商品状态管理的控制类
 * 
 * @author 荣
 * 
 */
public class GoodsManagerModel {
	/**
	 * 商品的数据源
	 */
	public List<GoodsState> dataSource = new ArrayList<GoodsState>();
	/**
	 * 暂缺商品数据源
	 */
	public List<GoodsState> soldoutData = new ArrayList<GoodsState>();

	private static GoodsManagerModel gmModel = new GoodsManagerModel();

	private GoodsManagerModel() {
	}

	/**
	 * 单例模式
	 * 
	 * @return
	 */
	public static GoodsManagerModel getInstance() {
		if (gmModel == null) {
			gmModel = new GoodsManagerModel();
		}
		return gmModel;
	}

	/**
	 * 获取修改改变过的商品列表
	 * 
	 * @return
	 */
	public List<GoodsState> getChangeData() {
		List<GoodsState> changeData = new ArrayList<GoodsState>();
		for(int i=0;i<dataSource.size();i++){
			//判断商品的状态发生改变后
			if(dataSource.get(i).getOrigin()!=dataSource.get(i).getState()){
				changeData.add(dataSource.get(i));
			}else{
				//判断商品属性是否发生改变
				List<OptionState> optionStates=dataSource.get(i).getOptList();
				if(optionStates!=null&&optionStates.size()>0){
					for(int j=0;j<optionStates.size();j++){
						if(optionStates.get(j).getOrigin()!=optionStates.get(j).getState()){
							changeData.add(dataSource.get(i));
							break;
						}
					}
				}
			}
		}
		return changeData;
	}
	
	/**
	 * 设置提交商品列表
	 * @return
	 */
	public List<GoodsItemInfo> getGoodsItemInfoData(){
		List<GoodsItemInfo> goodsItemInfos=new ArrayList<GoodsItemInfo>();
		List<GoodsState> goodsStates=getChangeData();
		
		for(int i=0;i<goodsStates.size();i++){
			GoodsState goodsState=goodsStates.get(i);
			GoodsItemInfo goodsItemInfo=new GoodsItemInfo();
			goodsItemInfo.setgId(goodsState.getgId());
			goodsItemInfo.setIsChange(1);
			goodsItemInfo.setState(goodsState.getState());

			List<OptionState> optionStates=goodsState.getOptList();
			List<OptionInfo> optionInfos=new ArrayList<OptionInfo>();
			if(optionStates!=null){
				for(int j=0;j<optionStates.size();j++){
					OptionState optionState=optionStates.get(j);
					OptionInfo optionInfo=new OptionInfo();
					optionInfo.setOptId(optionState.getOptId());
					optionInfo.setState(optionState.getState());
					optionInfos.add(optionInfo);
				}
				goodsItemInfo.setPropList(optionInfos);
			}
			goodsItemInfos.add(goodsItemInfo);
		}
		
		return goodsItemInfos;
	}

	/**
	 * 重置ChangeData
	 */
	public void resetChangeData(){
		for(int i=0;i<dataSource.size();i++){
			//还原商品状态ChangeData
			if(dataSource.get(i).getOrigin()!=dataSource.get(i).getState()){
				dataSource.get(i).setState(dataSource.get(i).getOrigin());
			}
			//还原属性状态
			List<OptionState> optionStates=dataSource.get(i).getOptList();
			if(optionStates!=null&&optionStates.size()>0){
				for(int j=0;j<optionStates.size();j++){
					optionStates.get(j).setState(optionStates.get(j).getOrigin());
				}
			}
		}
	}

	/**
	 * 提交ChangeData
	 */
	public void submitChangeData(){
		for(int i=0;i<dataSource.size();i++){
			//覆盖原始状态ChangeData
			if(dataSource.get(i).getOrigin()!=dataSource.get(i).getState()){
				dataSource.get(i).setOrigin(dataSource.get(i).getState());
			}
			//属性列表
			List<OptionState> optionStates=dataSource.get(i).getOptList();
			if(optionStates!=null){
				for(int j=0;j<optionStates.size();j++){
					OptionState optionState=optionStates.get(j);
					//保存属性原始状态
					optionState.setOrigin(optionState.getState());
				}
			}
		}
	}

	/**
	 * 过滤数据源获取其中的暂缺商品
	 */
	public List<GoodsState> getSoldoutData() {
		soldoutData.clear();
		for (int i = 0; i < dataSource.size(); i++) {
			GoodsState goodsState = dataSource.get(i);
			//商品暂缺
			if (goodsState.getState() == 2) {
				if (!soldoutData.contains(goodsState)) {
					soldoutData.add(goodsState);
				}
			}else{
				List<OptionState> optionStates=goodsState.getOptList();
				if(optionStates!=null){
					for(int j=0;j<optionStates.size();j++){
						//属性暂缺
						if(optionStates.get(j).getState()==2){
							if (!soldoutData.contains(goodsState)) {
								soldoutData.add(goodsState);
							}
						}
					}
				}
			}
		}
		return soldoutData;
	}

	/**
	 * 添加商品到暂缺商品
	 */
	public void addGoodsList2SoldoutData(List<GoodsState> list) {
		soldoutData.clear();
		soldoutData.addAll(list);
		addGoodsList2DataSource(list);
	}

	/**
	 * 根据类别来获取数据源中的商品列表
	 * @return
	 */
	public List<GoodsState> getGoodsListByTid(String tid){
		List<GoodsState> goodsStates=new ArrayList<GoodsState>();
		for(int i=0;i<dataSource.size();i++){
			if(dataSource.get(i).gettId().equals(tid)){
				goodsStates.add(dataSource.get(i));
			}
		}
		return goodsStates;
	}
	
	/**
	 * 添加商品列表到数据源
	 */
	public void addGoodsList2DataSource(List<GoodsState> list) {
		for (int i = 0; i < list.size(); i++) {
			GoodsState newGoodsState = list.get(i);
			if (!isInclude(newGoodsState)) {
				//保存商品的原始状态
				newGoodsState.setOrigin(newGoodsState.getState());
				List<OptionState> optionStates=newGoodsState.getOptList();
				if(optionStates!=null){
					for(int j=0;j<optionStates.size();j++){
						OptionState optionState=optionStates.get(j);
						//保存属性原始状态
						optionState.setOrigin(optionState.getState());
					}
				}
				dataSource.add(newGoodsState);
			}
		}
	}

	/**
	 * 是否数据源是否包含该对象
	 * 
	 * @param goodsState
	 * @return
	 */
	private boolean isInclude(GoodsState goodsState) {
		for (int i = 0; i < dataSource.size(); i++) {
			if (dataSource.get(i).getgId().equals(goodsState.getgId())) {
				dataSource.get(i).setgName(goodsState.getgName());
				return true;
			}
		}
		return false;
	}

	/**
	 * 获取商品列的数据源
	 * 
	 * @return
	 */
	public List<GoodsState> getDataSource() {
		return dataSource;
	}
	/**
	 * 清除所有数
	 */
	public void clearAllData(){
		dataSource.clear();
		soldoutData.clear();
	}
}
