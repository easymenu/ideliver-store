package com.woyou.bean.rpc;


/**
 * 回复评论的请求bean
 * @author zhou.ni
 *
 */
public class ReplyCommentReq {
	
	private String sId;			//店铺id
	private String pwd;			//密码
	private String cId;			//评论id
	private String content;		//回复内容
	
	public String getsId() {
		return sId;
	}
	public void setsId(String sId) {
		this.sId = sId;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public String getcId() {
		return cId;
	}
	public void setcId(String cId) {
		this.cId = cId;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
}
