package com.woyou.listener;

import com.woyou.activity.HomeActivity;

import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;

/**
 * 监听Viewpage切换事件
 * 
 * @author lenovo
 *
 */
public class OrderPageChangeListener implements OnPageChangeListener {
    //当前焦点视图

    private int currIndex=0;
    private int position_one;
    private int position_two;
    private int position_three;
	//滚动条
	private ImageView layout_track_iv;
	private HomeActivity homeActivity;
	private InputMethodManager imm;
	private EditText et;

	/**
	 * 
	 * @param homeActivity
	 * @param imm
	 * @param screenWidth
	 * @param iv
	 */
	public OrderPageChangeListener(HomeActivity homeActivity,InputMethodManager imm,EditText et,int screenWidth,ImageView iv){
		this.homeActivity=homeActivity;
		this.imm=imm;
		this.et=et;
        position_one=screenWidth/4;
        position_two=position_one*2;
        position_three=position_one*3;
        layout_track_iv=iv;
	}
	@Override
	public void onPageSelected(int arg0) {
		//操纵输入法
		if(arg0==3){
			et.requestFocus();
			imm.toggleSoftInputFromWindow(et.getWindowToken(), InputMethodManager.SHOW_IMPLICIT, InputMethodManager.HIDE_NOT_ALWAYS);
		}else{
			if(homeActivity.getCurrentFocus()!=null&&homeActivity.getCurrentFocus().getWindowToken()!=null){
				imm.hideSoftInputFromWindow(homeActivity.getCurrentFocus().getWindowToken(),InputMethodManager.HIDE_NOT_ALWAYS);
			}
		}
		
		Animation animation = null;
		switch (arg0) {
		case 0:
			// 设置滚动条
			switch (currIndex) {
			case 1:
				animation = new TranslateAnimation(position_one, 0, 0, 0);
				break;
			case 2:
				animation = new TranslateAnimation(position_two, 0, 0, 0);
				break;
			case 3:
				animation = new TranslateAnimation(position_three, 0, 0, 0);
				break;
			}
			break;
		case 1:
			// 设置滚动条
			switch (currIndex) {
			case 0:
				animation = new TranslateAnimation(0, position_one, 0, 0);
				break;
			case 2:
				animation = new TranslateAnimation(position_two, position_one,
						0, 0);
				break;
			case 3:
				animation = new TranslateAnimation(position_three,
						position_one, 0, 0);
				break;
			}
			break;
		case 2:
			// 设置滚动条
			switch (currIndex) {
			case 0:
				animation = new TranslateAnimation(0, position_two, 0, 0);
				break;
			case 1:
				animation = new TranslateAnimation(position_one, position_two,
						0, 0);
				break;
			case 3:
				animation = new TranslateAnimation(position_three,
						position_two, 0, 0);
				break;
			}
			break;
		case 3:
			// 设置滚动条
			switch (currIndex) {
			case 0:
				animation = new TranslateAnimation(0, position_three, 0, 0);
				break;
			case 1:
				animation = new TranslateAnimation(position_one,
						position_three, 0, 0);
				break;
			case 2:
				animation = new TranslateAnimation(position_two,
						position_three, 0, 0);
				break;
			}
			break;
		}
		currIndex = arg0;
		animation.setFillAfter(true);
		animation.setDuration(300);
		layout_track_iv.startAnimation(animation);
	}

	@Override
	public void onPageScrollStateChanged(int arg0) {

	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {

	}

}
