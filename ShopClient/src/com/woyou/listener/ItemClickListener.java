package com.woyou.listener;

import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.View.OnClickListener;

/**
 *  监听导航条的点击事件
 * @author lenovo
 *
 */
public class ItemClickListener implements OnClickListener {
	private int index = 0;
	private ViewPager viewPager;
	
	public ItemClickListener(int index,ViewPager viewPager) {
		this.index = index;
		this.viewPager=viewPager;
	}

	@Override
	public void onClick(View v) {
		if(viewPager!=null){
			viewPager.setCurrentItem(index);
		}
		
	}

}