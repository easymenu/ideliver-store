package com.woyou.component;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.jing.biandang.R;
import com.woyou.adapter.SpinnerPopAdapter;

public class SpinnerPopup {
	private Context c;
	private TextView positionView;
	private PopupWindow popupWindow;

	public SpinnerPopup(Context context, TextView positionView,ArrayList<String> data) {
		this.c = context;
		this.positionView = positionView;
		showPaoWindow(data);
	}
	
	// 弹出泡泡窗体
	public void showPaoWindow(final ArrayList<String> arrayList) {
		// 实例化泡泡窗台的布局文件
		View contentView =LayoutInflater.from(c).inflate(R.layout.layout_spinner, null);
		ListView layout_taskmanager_spinner_listview = (ListView) contentView.findViewById(R.id.layout_taskmanager_spinner_listview);
		SpinnerPopAdapter adapterSp = new SpinnerPopAdapter(c, arrayList);
		layout_taskmanager_spinner_listview.setAdapter(adapterSp);
		// ListView控件点击事件
		layout_taskmanager_spinner_listview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
				popupWindow.dismiss();
				positionView.setText(arrayList.get(position));
			}
		});

		// 实例化泡泡窗体
		popupWindow = new PopupWindow(contentView,600,ViewGroup.LayoutParams.WRAP_CONTENT);

		popupWindow.setFocusable(true);
		popupWindow.setBackgroundDrawable(new BitmapDrawable());

		// 显示泡泡窗体
		popupWindow.showAsDropDown(positionView, 0, 0);

	}

}
