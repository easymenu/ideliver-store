package com.woyou.bean.rpc;

import java.util.ArrayList;
import java.util.List;

import com.woyou.bean.OptionInfo;
import com.woyou.bean.SuperModel;

/**
 * 修改商品信息请求
 * @author tlc
 *
 */
public class UpdateGoodsInfoReq extends SuperModel {
	
	/**
	 * 店铺ID
	 */
	private String sId = "";
	/**
	 * 密码
	 */
	private String pwd = "";
	/**
	 * 商品ID
	 */
	private String gId = "";
	/**
	 * 商品名称
	 */
	private String gName = "";
	/**
	 * 商品单价
	 */
	private float price;
	/**
	 * 商品单位
	 */
	private String unit = "";
	/**
	 * 1：商品单价和单位有修改，2没有修改
	 */
	private int isChange;
	/**
	 * 商品属性列表
	 */
	private List<OptionInfo> propList = new ArrayList<OptionInfo>();
	
	public String getsId() {
		return sId;
	}
	public void setsId(String sId) {
		this.sId = sId;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public String getgId() {
		return gId;
	}
	public void setgId(String gId) {
		this.gId = gId;
	}
	public String getgName() {
		return gName;
	}
	public void setgName(String gName) {
		this.gName = gName;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public int getIsChange() {
		return isChange;
	}
	public void setIsChange(int isChange) {
		this.isChange = isChange;
	}
	public List<OptionInfo> getPropList() {
		return propList;
	}
	public void setPropList(List<OptionInfo> propList) {
		this.propList = propList;
	}
}
