package com.woyou.bean;


/**
 * 备注
 * 
 * @author 荣
 * 
 */
public class Remark extends SuperModel {
	/**
	 * 备注信息
	 */
	private String remark;

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

}
