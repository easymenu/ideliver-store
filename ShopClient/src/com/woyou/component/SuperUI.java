package com.woyou.component;

import com.jing.biandang.R;
import com.woyou.activity.HomeActivity;
import com.woyou.bean.rpc.UpdateVersionRes;
import com.woyou.utils.DownloadeManager;
import com.woyou.utils.ThreadPoolManager;
import com.woyou.utils.TypeJudgeTools;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * * UI 工具组件
 * 
 * @author 荣
 * 
 */
public class SuperUI {
	private static int num = 3;
	private AlertDialog customAlert;

	/**
	 * 弹出一个Toast
	 * 
	 * @param context
	 * @param msg
	 */
	public static void openToast(Context context, String msg) {
		LayoutInflater inflater = LayoutInflater.from(context);
		View view = inflater.inflate(R.layout.layout_toast, null);
		TextView tv = (TextView) view.findViewById(R.id.layout_toast_tv);
		tv.setText(msg);
		Toast toast = new Toast(context);
		toast.setView(view);
		toast.setGravity(Gravity.CENTER_VERTICAL, 0, 200);
		toast.setDuration(2000);
		toast.show();
	}

	/**
	 * 自定义一个不打印的提示Dialog
	 * 
	 * @param context
	 * @return
	 */
	public static Dialog openNotPrintHint(final HomeActivity homeActivity) {
		final Dialog dialog = new Dialog(homeActivity, R.style.printingDialog);
		View view = LayoutInflater.from(homeActivity).inflate(R.layout.layout_notprint, null);
		final Button notprint_know = (Button) view.findViewById(R.id.layout_notprint_know);
		dialog.setContentView(view);
		dialog.setCanceledOnTouchOutside(false);
		notprint_know.setOnClickListener(new android.view.View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		// 计时三秒自动隐藏Dialog
		ThreadPoolManager.getInstance().executeTask(new Runnable() {
			@Override
			public void run() {
				try {
					for (int i = 0; i < 3; i++) {
						Thread.sleep(1000);
						homeActivity.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								notprint_know.setText("知道了 ( " + (num--) + " )");
							}
						});
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					homeActivity.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							if (dialog.isShowing()) {
								dialog.dismiss();
							}
							num = 3;
						}
					});
				}
			}
		});

		return dialog;
	}

	/**
	 * 自定义一个正在打印的提示Dialog
	 * 
	 * @param context
	 * @return
	 */
	public static Dialog openPrintingHint(final HomeActivity homeActivity) {
		final Dialog dialog = new Dialog(homeActivity, R.style.printingDialog);
		View view = LayoutInflater.from(homeActivity).inflate(R.layout.layout_printing, null);
		final Button printing_know = (Button) view.findViewById(R.id.layout_printing_know);
		dialog.setContentView(view);
		dialog.setCanceledOnTouchOutside(false);
		printing_know.setOnClickListener(new android.view.View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		// 计时三秒自动隐藏Dialog
		ThreadPoolManager.getInstance().executeTask(new Runnable() {
			@Override
			public void run() {
				try {
					for (int i = 0; i < 3; i++) {
						Thread.sleep(1000);
						homeActivity.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								printing_know.setText("知道了 ( " + (num--) + " )");
							}
						});
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					homeActivity.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							if (dialog.isShowing()) {
								dialog.dismiss();
							}
							num = 3;
						}
					});
				}
			}
		});

		return dialog;
	}

	/**
	 * 弹出一个等待dialog
	 * 
	 * @param context
	 * @return Alertdialog
	 */
	public static AlertDialog openWaitDialog(Context context, String title, String msg) {
		if (TypeJudgeTools.isNull(msg)) {
			msg = "正在加载数据...";
		}
		Builder builder = new Builder(context);
		LayoutInflater inflater = LayoutInflater.from(context);
		View view = inflater.inflate(R.layout.layout_openwaitdialog, null);
		TextView textview = (TextView) view.findViewById(R.id.item_openwaitdialog_text);
		textview.setText(msg);
		builder.setTitle(title);
		builder.setView(view);
		AlertDialog loadWaitDialog = builder.create();
		loadWaitDialog.setCanceledOnTouchOutside(false);
		loadWaitDialog.show();
		return loadWaitDialog;

	}

	/**
	 * 提示更新的dialog
	 * 
	 * @param context
	 */
	public static Dialog openUpdateDialog(boolean isForce, final Activity activity,
			final UpdateVersionRes updateVersionRes) {
		final Dialog dialog = new Dialog(activity, R.style.defaultDialogStyle);
		View view = LayoutInflater.from(activity).inflate(R.layout.layout_checkupdate, null);
		TextView checkupdate_info = (TextView) view.findViewById(R.id.layout_checkupdate_info);
		RelativeLayout checkupdate_cancel = (RelativeLayout) view.findViewById(R.id.layout_checkupdate_cancel);
		RelativeLayout checkupdate_down = (RelativeLayout) view.findViewById(R.id.layout_checkupdate_down);
		// 根据标志位判断是否显示取消更新按钮
		if (isForce) {
			checkupdate_cancel.setVisibility(View.GONE);
		} else {
			checkupdate_cancel.setVisibility(View.VISIBLE);
		}

		dialog.setContentView(view);
		dialog.setCanceledOnTouchOutside(false);
		dialog.show();
		checkupdate_info.setText(updateVersionRes.getInfo().replace("<LF>", "\n"));
		checkupdate_cancel.setOnClickListener(new android.view.View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		checkupdate_down.setOnClickListener(new android.view.View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
				new DownloadeManager(activity, "" + updateVersionRes.getUpdateUrl()).showDownloadDialog(false);
			}
		});
		return dialog;
	}

	/**
	 * 弹出一个确认退出的dialog
	 */
	public static Dialog openExitDialog(final HomeActivity homeActivity, String msg, boolean iscancel) {
		final Dialog dialog = new Dialog(homeActivity, R.style.defaultDialogStyle);
		View view = LayoutInflater.from(homeActivity).inflate(R.layout.layout_exit, null);
		TextView exit_msg = (TextView) view.findViewById(R.id.exit_msg);
		RelativeLayout exit_ok = (RelativeLayout) view.findViewById(R.id.layout_exit_ok);
		RelativeLayout exit_cancel = (RelativeLayout) view.findViewById(R.id.layout_exit_cancel);
		exit_msg.setText(msg);
		if (iscancel) {
			exit_cancel.setVisibility(View.GONE);
		}
		dialog.setContentView(view);
		dialog.setCanceledOnTouchOutside(false);
		dialog.show();
		exit_ok.setOnClickListener(new android.view.View.OnClickListener() {
			@Override
			public void onClick(View v) {
				homeActivity.homeController.exit2Login();
				dialog.dismiss();
			}
		});
		exit_cancel.setOnClickListener(new android.view.View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		return dialog;
	}

	/**
	 * 自定义对话框
	 * 
	 * @param context
	 * @return
	 */
	public Dialog customDialog(Context context) {
		Dialog dialog = new Dialog(context, R.style.progressDialog);
		View view = LayoutInflater.from(context).inflate(R.layout.layout_openwaitdialog, null);
		dialog.setContentView(view);
		dialog.setCanceledOnTouchOutside(false);
		final ImageView iv = (ImageView) view.findViewById(R.id.layout_progressdialog_anim_iv);
		ImageView layout_progressdialog_cancel_iv = (ImageView) view.findViewById(R.id.layout_progressdialog_cancel_iv);
		// 启动 动画，因为如果没有启动方法，它没办法自己启动
		iv.post(new Runnable() {
			@Override
			public void run() {
				AnimationDrawable animationDrawable = (AnimationDrawable) iv.getDrawable();// 获取imageview动画列表
				animationDrawable.start();// 开启动画
			}
		});
		layout_progressdialog_cancel_iv.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				customAlert.cancel();
			}
		});
		return dialog;
	}

	/**
	 * 显示提示绑定银行卡的对话框
	 */
	public static void showBindCordDialog(Context mContext) {

		final Dialog dialog = new Dialog(mContext, R.style.printingDialog);
		View view = LayoutInflater.from(mContext).inflate(R.layout.layout_bindcord, null);
		Button layout_bindcord_know = (Button) view.findViewById(R.id.layout_bindcord_know);
		layout_bindcord_know.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.cancel();
			}
		});
		dialog.setContentView(view);
		dialog.setCanceledOnTouchOutside(false);
		dialog.show();
	}
}
