/** 
 * @Title:  FmInfo.java 
 * @author:  xuron
 * @data:  2015年11月30日 上午11:21:22 <创建时间>
 * 
 * @history：<以下是历史记录>
 *
 * @modifier: <修改人>
 * @modify date: 2015年11月30日 上午11:21:22 <修改时间>
 * @log: <修改内容>
 *
 * @modifier: <修改人>
 * @modify date: 2015年11月30日 上午11:21:22 <修改时间>
 * @log: <修改内容>
 */
package com.woyou.bean;

import android.support.v4.app.Fragment;

/**
 * FM与FM传递参数用的值
 * 
 * @author xuron
 * @versionCode 1
 */
public class FmInfo extends SuperModel {
	/**
	 * 跳转的Fragment
	 */
	private Class<Fragment> clazzFM;
	/**
	 * fm的title名字
	 */
	private String titleName;
	/**
	 * 是否加入动画
	 */
	private boolean isAnim = false;
	/**
	 * 
	 * @param clazzFM跳转的Fragment
	 * @param titleName标题名字
	 * @param isAnim是否加入切换动画
	 */
	public FmInfo(Class clazzFM, String titleName, boolean isAnim) {
		super();
		this.clazzFM = clazzFM;
		this.titleName = titleName;
		this.isAnim = isAnim;
	}

	public String getTitleName() {
		return titleName;
	}

	public void setTitleName(String titleName) {
		this.titleName = titleName;
	}

	public Class<Fragment> getClazzFM() {
		return clazzFM;
	}

	public void setClazzFM(Class<Fragment> clazzFM) {
		this.clazzFM = clazzFM;
	}

	public boolean isAnim() {
		return isAnim;
	}

	public void setAnim(boolean isAnim) {
		this.isAnim = isAnim;
	}

	@Override
	public String toString() {
		return "FmInfo [clazzFM=" + clazzFM + ", titleName=" + titleName + ", isAnim=" + isAnim + "]";
	}

}
