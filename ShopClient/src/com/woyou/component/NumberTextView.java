package com.woyou.component;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;
/**
 * 自定义数字控件设置字体样式
 */
public class NumberTextView extends TextView {
	private Context c;
	public NumberTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		c=context;
		init();
	}
	
	public void init(){
		Typeface face = Typeface.createFromAsset (c.getAssets() , "fonts/kalinga.ttf");
		this.setTypeface (face);
	}
	
}
