package com.woyou.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * 商品属性组Model
 * @author tlc
 *
 */
public class OptionGroupInfo extends SuperModel{
	
	/**
	 * 属性组Id
	 */
	private String optGId = "";
	/**
	 * 属性组名称
	 */
	private String optGName = "";
	/**
	 * 属性Option列表
	 */
	private List<OptionInfo> optList = new ArrayList<OptionInfo>();
	/**
	 * 是否可修改
	 */
	private int isChange;
	
	public String getOptGId() {
		return optGId;
	}
	public void setOptGId(String optGId) {
		this.optGId = optGId;
	}
	public String getOptGName() {
		return optGName;
	}
	public void setOptGName(String optGName) {
		this.optGName = optGName;
	}
	public List<OptionInfo> getOptList() {
		return optList;
	}
	public void setOptList(List<OptionInfo> optList) {
		this.optList = optList;
	}
	public int getIsChange() {
		return isChange;
	}
	public void setIsChange(int isChange) {
		this.isChange = isChange;
	}
}
