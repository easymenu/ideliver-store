package com.woyou.bean.rpc;

import com.woyou.bean.SuperModel;

/**
 * 便当机用户登录请求
 * @author tlc
 *
 */
public class UserLoginReq extends SuperModel {

	/**
	 * 店铺编号
	 */
	private String bId = "";
	/**
	 * 用户编号
	 */
	private String uCode = "";
	/**
	 * 验证码
	 */
	private String code = "";
	/**
	 * 密码（随机自动生成6位数字）
	 */
	private String pwd = "";
	/**
	 * 0:初次登录；1：再次登录
	 */
	private int loginType;
	/**
	 * 设备编号
	 */
	private String deviceId = "";
	/**
	 * 外卖机版本
	 */
	private String version = "";
	
	public String getbId() {
		return bId;
	}
	public void setbId(String bId) {
		this.bId = bId;
	}
	public String getuCode() {
		return uCode;
	}
	public void setuCode(String uCode) {
		this.uCode = uCode;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public int getLoginType() {
		return loginType;
	}
	public void setLoginType(int loginType) {
		this.loginType = loginType;
	}
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
}
