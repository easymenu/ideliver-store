package com.woyou.bean.rpc;

import com.woyou.bean.SuperModel;

/**
 * 获取今日信息简介的返回bean
 * 
 * @author 荣
 * 
 */
public class QueryTodayRes extends SuperModel {
	/**
	 * 暂缺商品数量
	 */
	private int NAnum;
	/**
	 * 未读评论数量
	 */
	private int newCommentNum;
	/**
	 * 今日订单数量（成功接单的数量）
	 */
	private int orderNum;
	/**
	 * 今日售出商品的数量
	 */
	private int soldNum;
	/**
	 * 今日收入金额
	 */
	private float income;

	public int getNAnum() {
		return NAnum;
	}

	public void setNAnum(int nAnum) {
		NAnum = nAnum;
	}

	public int getNewCommentNum() {
		return newCommentNum;
	}

	public void setNewCommentNum(int newCommentNum) {
		this.newCommentNum = newCommentNum;
	}

	public int getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(int orderNum) {
		this.orderNum = orderNum;
	}

	public int getSoldNum() {
		return soldNum;
	}

	public void setSoldNum(int soldNum) {
		this.soldNum = soldNum;
	}

	public float getIncome() {
		return income;
	}

	public void setIncome(float income) {
		this.income = income;
	}

}
