package com.woyou.bean.rpc;

import java.util.List;

import com.woyou.bean.Time;

/**
 * 更新外卖时段的请求bean
 * @author zhou.ni
 *
 */
public class UpdateDeliveryTimesReq {
	
	private String sId;				//商铺Id
	private String pwd;				//密码
	private List<Time> timeList;	//时段列表
	
	public String getsId() {
		return sId;
	}
	public void setsId(String sId) {
		this.sId = sId;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public List<Time> getTimeList() {
		return timeList;
	}
	public void setTimeList(List<Time> timeList) {
		this.timeList = timeList;
	}
	
	
	
}
