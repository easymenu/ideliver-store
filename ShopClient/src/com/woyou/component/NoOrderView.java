/** 
 * @Title:  NoOrderView.java 
 * @author:  xuron
 * @data:  2015年11月27日 上午9:59:19 <创建时间>
 * 
 * @history：<以下是历史记录>
 *
 * @modifier: <修改人>
 * @modify date: 2015年11月27日 上午9:59:19 <修改时间>
 * @log: <修改内容>
 *
 * @modifier: <修改人>
 * @modify date: 2015年11月27日 上午9:59:19 <修改时间>
 * @log: <修改内容>
 */
package com.woyou.component;

import java.util.List;

import com.jing.biandang.R;
import com.woyou.activity.HomeActivity;
import com.woyou.bean.NAGoodsInfo;
import com.woyou.bean.rpc.QueryYesterdayInfoReq;
import com.woyou.bean.rpc.QueryYesterdayInfoRes;
import com.woyou.bean.rpc.Result;
import com.woyou.fragment.order.OrderFragment;
import com.woyou.model.rpc.OrderModel;
import com.woyou.utils.LoginUtils;
import com.woyou.utils.ThreadPoolManager;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import retrofit.RetrofitError;

/**
 * 自定义无订单的View
 * 
 * @author xuron
 * @versionCode 1 <每次修改提交前+1>
 */
public class NoOrderView extends SuperLayout implements OnClickListener {
	private HomeActivity homeActivity;
	private OrderFragment orderFragment;
	private OrderModel orderModel;
	/**
	 * 昨日订单详情
	 */
	private QueryYesterdayInfoRes queryYesterdayInfoRes;

	// 今日无订单页面
	private RelativeLayout layout_noorder_view;
	private RelativeLayout layout_noorder_nagoods;
	private TextView layout_noorder_nagoods_tv;
	private RelativeLayout layout_noorder_soldout;
	private TextView layout_noorder_soldout_tv;
	private RelativeLayout layout_noorder_noread;
	private TextView layout_noorder_noread_tv;
	private RelativeLayout layout_noorder_comment;
	private TextView layout_noorder_comment_tv;

	private RelativeLayout layout_noorder_order;
	private TextView layout_noorder_order_tv;
	private RelativeLayout layout_noorder_soldnum;
	private TextView layout_noorder_soldnum_tv;
	private RelativeLayout layout_noorder_income;
	private TextView layout_noorder_income_tv;

	public NoOrderView(Context context) {
		super(context);
		initView();
	}

	public NoOrderView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initView();
	}

	public NoOrderView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initView();
	}

	@Override
	protected void initView() {
		orderModel = OrderModel.getInstance(homeActivity);
		View.inflate(mContext, R.layout.layout_noorder, this);
		layout_noorder_view = (RelativeLayout) findViewById(R.id.layout_noorder_view);
		layout_noorder_nagoods = (RelativeLayout) findViewById(R.id.layout_noorder_nagoods);
		layout_noorder_nagoods_tv = (TextView) findViewById(R.id.layout_noorder_nagoods_tv);
		layout_noorder_soldout = (RelativeLayout) findViewById(R.id.layout_noorder_soldout);
		layout_noorder_soldout_tv = (TextView) findViewById(R.id.layout_noorder_soldout_tv);
		layout_noorder_noread = (RelativeLayout) findViewById(R.id.layout_noorder_noread);
		layout_noorder_noread_tv = (TextView) findViewById(R.id.layout_noorder_noread_tv);
		layout_noorder_comment = (RelativeLayout) findViewById(R.id.layout_noorder_comment);
		layout_noorder_comment_tv = (TextView) findViewById(R.id.layout_noorder_comment_tv);

		layout_noorder_order = (RelativeLayout) findViewById(R.id.layout_noorder_order);
		layout_noorder_order_tv = (TextView) findViewById(R.id.layout_noorder_order_tv);
		layout_noorder_soldnum = (RelativeLayout) findViewById(R.id.layout_noorder_soldnum);
		layout_noorder_soldnum_tv = (TextView) findViewById(R.id.layout_noorder_soldnum_tv);
		layout_noorder_income = (RelativeLayout) findViewById(R.id.layout_noorder_income);
		layout_noorder_income_tv = (TextView) findViewById(R.id.layout_noorder_income_tv);

		layout_noorder_soldout.setOnClickListener(this);
		layout_noorder_noread.setOnClickListener(this);
		layout_noorder_soldnum.setOnClickListener(this);
		layout_noorder_order.setOnClickListener(this);
		layout_noorder_income.setOnClickListener(this);
	}

	/**
	 * 设置参数
	 */
	public void setParams(HomeActivity homeActivity, OrderFragment orderFragment, OrderModel orderModel) {
		this.homeActivity = homeActivity;
		this.orderFragment = orderFragment;
		this.orderModel = orderModel;
	}

	/**
	 * 显示今日无订单页面，并加载数据
	 */
	public void showNoOrderView() {

		ThreadPoolManager.getInstance().executeTask(new Runnable() {
			@Override
			public void run() {
				try {
					homeActivity.showLoadingHint();
					QueryYesterdayInfoReq queryYesterdayInfoReq = new QueryYesterdayInfoReq();
					queryYesterdayInfoReq.setsId(LoginUtils.getUserInfo(homeActivity).getsId());
					queryYesterdayInfoReq.setPwd(LoginUtils.getUserInfo(homeActivity).getPwd());
					Result<QueryYesterdayInfoRes> result = orderModel.queryYesterdayInfo(queryYesterdayInfoReq);
					if (result != null && result.getCode() == 1) {
						queryYesterdayInfoRes = result.getData();
						homeActivity.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								if (queryYesterdayInfoRes.getNAGoods() != null
										&& queryYesterdayInfoRes.getNAGoods().size() > 0) {
									String naInfo = "";
									List<NAGoodsInfo> naGoodsInfo = queryYesterdayInfoRes.getNAGoods();
									for (int i = 0; i < naGoodsInfo.size(); i++) {
										naInfo = naInfo + naGoodsInfo.get(i).getGoods() + "\n";
									}
									layout_noorder_nagoods_tv.setText("" + naInfo);
								} else {
									layout_noorder_nagoods_tv.setText("");
								}
								layout_noorder_soldout_tv.setText("" + queryYesterdayInfoRes.getNAnum());
								layout_noorder_noread_tv.setText("" + queryYesterdayInfoRes.getNewCommentNum());
								layout_noorder_comment_tv.setText("" + queryYesterdayInfoRes.getNewComment());
								layout_noorder_order_tv.setText("" + queryYesterdayInfoRes.getOrderNum());
								layout_noorder_soldnum_tv.setText("" + queryYesterdayInfoRes.getSoldNum());
								layout_noorder_income_tv.setText("" + queryYesterdayInfoRes.getIncome());
							}
						});
					} else if (result != null && result.getCode() == -3) {
						homeActivity.homeController.exitLogin();
					}
				} catch (RetrofitError error) {

				} finally {
					homeActivity.hideLoadingHint();
				}
			}
		});

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.layout_noorder_soldout:
			orderFragment.orderController.jump2SoldoutFragment();
			break;
		case R.id.layout_noorder_noread:
			orderFragment.orderController.jump2MessageFragment();
			break;
		case R.id.layout_noorder_soldnum:
		case R.id.layout_noorder_order:
		case R.id.layout_noorder_income:
			orderFragment.orderController.jump2ReportFragment();
			break;
		}
	}

}
