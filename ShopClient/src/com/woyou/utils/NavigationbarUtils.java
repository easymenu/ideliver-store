package com.woyou.utils;

import com.woyou.Constants;

import android.content.Context;
import android.content.Intent;

public class NavigationbarUtils {
	private static Context context;
	public static NavigationbarUtils navigationbarUtils;

	private NavigationbarUtils() {

	}

	public static NavigationbarUtils getInstance(Context c) {
		context = c;
		if (navigationbarUtils == null) {
			navigationbarUtils = new NavigationbarUtils();
		}
		return navigationbarUtils;
	}

	/**
	 * 显示导航栏
	 */
	public void show() {
		Intent b = null;
		if (Constants.curMainBoard == Constants.MAIN_BOARD_C500) {
			b = new Intent();
			b.setAction("android.intent.action.NAVIGATION");
			b.putExtra("ctrl", "show");
			context.sendBroadcast(b, null);
		}
		if (Constants.curMainBoard == Constants.MAIN_BOARD_RK30) {
			b = new Intent("com.android.action.display_navigationbar");
			context.sendBroadcast(b);
		}
	}

	/**
	 * 隐藏导航栏
	 */
	public void hide() {
		String model = MainBoardUtil.getModel();
		Intent b = null;
		if (model.contains(Constants.MAIN_BOARD_SMDKV210)) {
			Constants.curMainBoard = Constants.MAIN_BOARD_SMDKV210;
		} else if (model.contains(Constants.MAIN_BOARD_C500)) {
			Constants.curMainBoard = Constants.MAIN_BOARD_C500;
			b = new Intent();
			b.setAction("android.intent.action.NAVIGATION");
			b.putExtra("ctrl", "hide");
			context.sendBroadcast(b, null);
		} else if (model.contains(Constants.MAIN_BOARD_RK30)) {
			Constants.curMainBoard = Constants.MAIN_BOARD_RK30;
			b = new Intent("com.android.action.hide_navigationbar");
			context.sendBroadcast(b);
		} else {
			Constants.curMainBoard = Constants.MAIN_BOARD_UNKNOW;
		}
	}
}
