package com.woyou.event;

import com.woyou.bean.rpc.SendOrderRes;

/**
 * 送出订单
 * 
 * @author 荣
 * 
 */
public class EventSendOrder {
	/**
	 * 发送订单的返回数据
	 */
	private SendOrderRes sendOrderRes;

	public EventSendOrder(SendOrderRes sendOrderRes) {
		super();
		this.sendOrderRes = sendOrderRes;
	}

	public SendOrderRes getSendOrderRes() {
		return sendOrderRes;
	}

	public void setSendOrderRes(SendOrderRes sendOrderRes) {
		this.sendOrderRes = sendOrderRes;
	}
	
}
