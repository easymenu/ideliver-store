	package com.woyou.fragment.shop;

import retrofit.RetrofitError;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.jing.biandang.R;
import com.woyou.activity.HomeActivity;
import com.woyou.bean.BoxFee;
import com.woyou.bean.UserInfo;
import com.woyou.bean.rpc.Result;
import com.woyou.bean.rpc.UpdateBoxFeeReq;
import com.woyou.component.SuperUI;
import com.woyou.fragment.SuperFragment;
import com.woyou.model.rpc.ShopModel;
import com.woyou.utils.FormatTools;
import com.woyou.utils.LoginUtils;
import com.woyou.utils.ThreadPoolManager;

/**
 * 餐盒费界面
 * 
 * @author zhou.ni
 * 
 */
public class BoxFeeFragment extends SuperFragment implements OnClickListener {
	private HomeActivity homeActivity;
	private View view;
	private RelativeLayout noBoxFee;
	private ImageView noBoxFeeImage;
	private RelativeLayout singeBoxFee;
	private ImageView singeBoxFeeImage;
	private EditText singeBoxFeeMpdify;
	private RelativeLayout eachBoxFee;
	private ImageView eachBoxFeeImage;
	private EditText eachBoxFeeMpdify;
	//更新操作面板
	private LinearLayout panel;
	
	private BoxFee boxFee;
	private UpdateBoxFeeReq boxFeeReq = new UpdateBoxFeeReq();
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.fm_boxfee, container, false);
		homeActivity=(HomeActivity)getActivity();
		return view;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		initView();
	}
	
	@Override
	public void onHiddenChanged(boolean hidden) {
		super.onHiddenChanged(hidden);
		if ( !hidden ) {
			fullBoxFee();
		}
	}

	private void initView() {
		noBoxFee = (RelativeLayout) view.findViewById(R.id.no_boxfee);
		noBoxFeeImage = (ImageView) view.findViewById(R.id.no_boxfee_image);
		noBoxFee.setOnClickListener(this);
		singeBoxFee = (RelativeLayout) view.findViewById(R.id.singe_boxfee);
		singeBoxFeeImage = (ImageView) view.findViewById(R.id.singe_boxfee_image);
		singeBoxFeeMpdify = (EditText) view.findViewById(R.id.singe_boxfee_modify);
		singeBoxFee.setOnClickListener(this);
		eachBoxFee = (RelativeLayout) view.findViewById(R.id.each_boxfee);
		eachBoxFeeImage = (ImageView) view.findViewById(R.id.each_boxfee_image);
		eachBoxFeeMpdify = (EditText) view.findViewById(R.id.each_boxfee_modify);
		eachBoxFee.setOnClickListener(this);
		panel = (LinearLayout) view.findViewById(R.id.boxfee_panel);
		RelativeLayout cancel = (RelativeLayout) view.findViewById(R.id.boxfee_cancel);
		RelativeLayout ok = (RelativeLayout) view.findViewById(R.id.boxfee_submit);
		cancel.setOnClickListener(this);
		ok.setOnClickListener(this);
		
		singeBoxFeeMpdify.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}
			@Override
			public void afterTextChanged(Editable s) {
				singeBoxFeeMpdify.setSelection( s.toString().length());
				
				if( boxFee.getType()==1 ) {
					if ( !s.toString().equals(FormatTools.String2Money(boxFee.getPrice()+"")) ) {
						panel.setVisibility(View.VISIBLE);
					} else {
						panel.setVisibility(View.GONE);
					}
				}
				
				if ( s.toString().length()>6 ) {
					singeBoxFeeMpdify.setText( s.toString().substring(0, 6) );
				}
			}
		});
		
		eachBoxFeeMpdify.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}
			@Override
			public void afterTextChanged(Editable s) {
				eachBoxFeeMpdify.setSelection( s.toString().length());
				
				if( boxFee.getType()==2 ) {
					if ( !s.toString().equals(FormatTools.String2Money(boxFee.getPrice()+"")) ) {
						panel.setVisibility(View.VISIBLE);
					} else {
						panel.setVisibility(View.GONE);
					}
				}
				
				if ( s.toString().length()>6 ) {
					eachBoxFeeMpdify.setText( s.toString().substring(0, 6) );
				}
				
			}
		});
		
		fullBoxFee();
	}
	
	/**
	 * 刷新餐盒费
	 */
	private void fullBoxFee(){
		boxFee = (BoxFee) aCache.getAsObject("boxfee");
		if ( boxFee!=null ) {
			if ( boxFee.getType()==0 ) {
				changeSelectState(noBoxFee, noBoxFeeImage, singeBoxFee, singeBoxFeeImage, eachBoxFee, eachBoxFeeImage);
				singeBoxFeeMpdify.setText("");
				eachBoxFeeMpdify.setText("");
				boxFeeReq.setType(0);
				singeBoxFeeMpdify.setEnabled(false);
				eachBoxFeeMpdify.setEnabled(false);
			} else if ( boxFee.getType()==1 ) {
				changeSelectState(singeBoxFee, singeBoxFeeImage, noBoxFee, noBoxFeeImage, eachBoxFee, eachBoxFeeImage);
				singeBoxFeeMpdify.setText(FormatTools.String2Money(boxFee.getPrice()+""));
				eachBoxFeeMpdify.setText("");
				boxFeeReq.setType(1);
				singeBoxFeeMpdify.setEnabled(true);
				eachBoxFeeMpdify.setEnabled(false);
			} else {
				changeSelectState(eachBoxFee, eachBoxFeeImage, noBoxFee, noBoxFeeImage, singeBoxFee, singeBoxFeeImage);
				eachBoxFeeMpdify.setText(FormatTools.String2Money(boxFee.getPrice()+""));
				singeBoxFeeMpdify.setText("");
				boxFeeReq.setType(2);
				singeBoxFeeMpdify.setEnabled(false);
				eachBoxFeeMpdify.setEnabled(true);
			}
		}
		panel.setVisibility(View.GONE);
	}
	
	/**
	 * 改变选择的状态
	 * @param selectBg
	 * @param selectImage
	 * @param b1
	 * @param i1
	 * @param b2
	 * @param i2
	 */
	private void changeSelectState(RelativeLayout selectBg,
			ImageView selectImage, RelativeLayout b1, ImageView i1,
			RelativeLayout b2, ImageView i2) {
		selectBg.setBackgroundResource(R.drawable.boxfee_pressed_bg);
		selectImage.setImageResource(R.raw.radio_2);
		b1.setBackgroundResource(R.drawable.boxfee_normal_bg);
		i1.setImageResource(R.raw.radio_1);
		b2.setBackgroundResource(R.drawable.boxfee_normal_bg);
		i2.setImageResource(R.raw.radio_1);
	}
	
	/**
	 * 更新餐盒费
	 */
	private void updateBoxFee(){
		UserInfo userInfo = LoginUtils.getUserInfo(homeActivity);
		if (userInfo!=null ) {
			boxFeeReq.setsId(userInfo.getsId());
			boxFeeReq.setPwd(userInfo.getPwd());
		}
		ThreadPoolManager.getInstance().executeTask(new Runnable() {
			
			@Override
			public void run() {
				try {
					final Result result = ShopModel.getInstance(getActivity()).updateBoxFee(boxFeeReq);
					if ( result!=null && result.code==1 ) {
						homeActivity.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								SuperUI.openToast(homeActivity, result.msg);
								BoxFee fee = new BoxFee();
								fee.setPrice(boxFeeReq.getPrice());
								fee.setType(boxFeeReq.getType());
								aCache.put("boxfee", fee);
								fullBoxFee();
							}
						});
					} else if( result != null && result.code==-3 ) {
						homeActivity.homeController.exitLogin();
					} else {
						homeActivity.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								SuperUI.openToast(homeActivity, "保存失败,请重试");
							}
						});
					}
				} catch (RetrofitError e) {
					homeActivity.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							SuperUI.openToast(homeActivity, "保存失败,请重试");
						}
					});
				}
				
			}
		});
	}


	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.no_boxfee:	
			hideSoftInput(homeActivity, singeBoxFeeMpdify);
			changeSelectState(noBoxFee, noBoxFeeImage, singeBoxFee, singeBoxFeeImage, eachBoxFee, eachBoxFeeImage);
			singeBoxFeeMpdify.setText("");
			eachBoxFeeMpdify.setText("");
			boxFeeReq.setType(0);
			singeBoxFeeMpdify.setEnabled(false);
			eachBoxFeeMpdify.setEnabled(false);
			if( boxFee.getType()!=0 ) {
				panel.setVisibility(View.VISIBLE);
			}
			break;
		case R.id.singe_boxfee:
			hideSoftInput(homeActivity, singeBoxFeeMpdify);
			changeSelectState(singeBoxFee, singeBoxFeeImage, noBoxFee, noBoxFeeImage, eachBoxFee, eachBoxFeeImage);
			eachBoxFeeMpdify.setText("");
			boxFeeReq.setType(1);
			singeBoxFeeMpdify.setEnabled(true);
			eachBoxFeeMpdify.setEnabled(false);
			if( boxFee.getType()!=1 ) {
				panel.setVisibility(View.VISIBLE);
			}
			break;
		case R.id.each_boxfee:
			hideSoftInput(homeActivity, singeBoxFeeMpdify);
			changeSelectState(eachBoxFee, eachBoxFeeImage, noBoxFee, noBoxFeeImage, singeBoxFee, singeBoxFeeImage);
			singeBoxFeeMpdify.setText("");
			boxFeeReq.setType(2);
			singeBoxFeeMpdify.setEnabled(false);
			eachBoxFeeMpdify.setEnabled(true);
			if( boxFee.getType()!=2 ) {
				panel.setVisibility(View.VISIBLE);
			}
			break;
		case R.id.boxfee_cancel:
			fullBoxFee();
			break;
		case R.id.boxfee_submit:
			if ( judgeCurrent() ) {
				updateBoxFee();
			}
			break;
		default:
			break;
		}

	}
	
	/**
	 * 判断输入是否合法
	 * @return
	 */
	private boolean judgeCurrent(){
		String singe = singeBoxFeeMpdify.getText().toString();
		String each = eachBoxFeeMpdify.getText().toString();
		
		if ( boxFeeReq.getType()==0 ) {
			boxFeeReq.setPrice(0);
		}
		
		if ( boxFeeReq.getType()==1 ) {
			if ( !TextUtils.isEmpty(singe) ) {
				if (  !"0".equals(FormatTools.String2Money(singe)) ) {
					try {
						boxFeeReq.setPrice(Float.parseFloat(singe));
					} catch (Exception e) {
						return false;
					}
				} else {
					SuperUI.openToast(homeActivity, "餐盒费不能为空,为0");
					return false;
				}
			} else {
				SuperUI.openToast(homeActivity, "餐盒费不能为空,为0");
				return false;
			}
		}
		
		if ( boxFeeReq.getType()==2 ) {
			if ( !TextUtils.isEmpty(each) ) {
				if (!"0".equals(FormatTools.String2Money(each)) ) {
					try {
						boxFeeReq.setPrice(Float.parseFloat(each));
					} catch (Exception e) {
						return false;
					}
				} else {
					SuperUI.openToast(homeActivity, "餐盒费不能为空,为0");
					return false;
				}
			} else {
				SuperUI.openToast(homeActivity, "餐盒费不能为空,为0");
				return false;
			}
		}
		return true;
	}

}
