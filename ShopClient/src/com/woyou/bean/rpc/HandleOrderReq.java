package com.woyou.bean.rpc;

import com.woyou.bean.SuperModel;

/**
 * 接受或拒绝订单的请求bean
 * 
 * @author 荣
 * 
 */
public class HandleOrderReq extends SuperModel {
	/**
	 * 商铺Id
	 */
	private String sId = "";
	/**
	 * 密码
	 */
	private String pwd = "";
	/**
	 * 订单ID
	 */
	private String oId = "";
	/**
	 * 订单结果（1：接受订单2：拒绝订单）
	 */
	private int handle;
	/**
	 * 拒绝理由（接受订单为空）
	 */
	private int refuse;

	public String getsId() {
		return sId;
	}

	public void setsId(String sId) {
		this.sId = sId;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getoId() {
		return oId;
	}

	public void setoId(String oId) {
		this.oId = oId;
	}

	public int getHandle() {
		return handle;
	}

	public void setHandle(int handle) {
		this.handle = handle;
	}

	public int getRefuse() {
		return refuse;
	}

	public void setRefuse(int refuse) {
		this.refuse = refuse;
	}

}
