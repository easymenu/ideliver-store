package com.woyou.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * 商品Model
 * @author tlc
 *
 */
public class GoodsInfo extends SuperModel {
	
	/**
	 * 商品ID
	 */
	private String gId = "";
	/**
	 * 商品名称
	 */
	private String gName = "";
	/**
	 * 商品单价
	 */
	private float price;
	/**
	 * 商品单位
	 */
	private String unit = "";
	/**
	 * 是否暂缺（1正常，2暂缺）
	 */
	private int state;
	/**
	 * 商品图片Url
	 */
	private String prcUrl = "";
	/**
	 * 商品属性列表
	 */
	private List<OptionGroupInfo> optionGroup = new ArrayList<OptionGroupInfo>();
	
	public String getgId() {
		return gId;
	}
	public void setgId(String gId) {
		this.gId = gId;
	}
	public String getgName() {
		return gName;
	}
	public void setgName(String gName) {
		this.gName = gName;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}
	public String getPrcUrl() {
		return prcUrl;
	}
	public void setPrcUrl(String prcUrl) {
		this.prcUrl = prcUrl;
	}
	public List<OptionGroupInfo> getOptionGroup() {
		return optionGroup;
	}
	public void setOptionGroup(List<OptionGroupInfo> optionGroup) {
		this.optionGroup = optionGroup;
	}
}
