package com.woyou.utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidParameterException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.text.TextUtils;
import android_serialport_api.SerialPort;

import com.woyou.Constants;
import com.woyou.bean.BoxCount;
import com.woyou.bean.Coupon;
import com.woyou.bean.CouponCount;
import com.woyou.bean.Gift;
import com.woyou.bean.GiftCount;
import com.woyou.bean.Goods;
import com.woyou.bean.GoodsCount;
import com.woyou.bean.ReportData;
import com.woyou.bean.ReportItem;
import com.woyou.bean.rpc.QueryOrderDetailsRes;
import com.woyou.bean.rpc.UserLoginRes;

public class PrintUtil {
	public static ACache aCache;
	final static int BUFFER_SIZE = 4096;

	// add by yidie
	public static boolean printBytes(byte[] printText) {
		boolean returnValue = true;
		try {
			OutputStream mOutputStream = getSerialPort().getOutputStream();
			mOutputStream.write(printText);
		} catch (Exception ex) {
			returnValue = false;
		}
		return returnValue;
	}

	// add by yidie
	public static boolean printString(String paramString) {
		return printBytes(getGbk(paramString));
	}

	/***************************************************************************
	 * add by yidie 2012-01-10 功能：设置打印绝对位置 参数： int 在当前行，定位光标位置，取值范围0至576点 说明：
	 * 在字体常规大小下，每汉字24点，英文字符12点 如位于第n个汉字后，则position=24*n
	 * 如位于第n个半角字符后，则position=12*n
	 ****************************************************************************/

	public static byte[] setCusorPosition(int position) {
		byte[] returnText = new byte[4]; // 当前行，设置绝对打印位置 ESC $ bL bH
		returnText[0] = 0x1B;
		returnText[1] = 0x24;
		returnText[2] = (byte) (position % 256);
		returnText[3] = (byte) (position / 256);
		return returnText;
	}

	public static byte[] setLineHeight(byte h) {
		byte[] returnText = new byte[] { 0x1B, 0x33, h }; // 切纸； 1B 33 n
		return returnText;
	}

	public static byte[] setDefaultLineHeight() {
		byte[] returnText = new byte[] { 0x1B, 0x32 }; // 切纸； 1B 32
		return returnText;
	}

	public static byte[] InputStreamTOByte(InputStream in) throws IOException {

		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		byte[] data = new byte[BUFFER_SIZE];
		int count = -1;
		while ((count = in.read(data, 0, BUFFER_SIZE)) != -1)
			outStream.write(data, 0, count);

		data = null;
		return outStream.toByteArray();
	}

	public static void printLogo(Context c) {
		PrintUtil.printBytes(PrintUtil.setLineHeight((byte) 0));
		InputStream is = c.getClass().getResourceAsStream("/assets/bill.bin");
		byte[] b;
		try {
			b = InputStreamTOByte(is);
			PrintUtil.printBytes(b);
			PrintUtil.printBytes(PrintUtil.setDefaultLineHeight());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static byte[] getLogo(Context c) {
		InputStream is = c.getClass().getResourceAsStream("/assets/bill.bin");
		byte[] b;
		try {
			b = InputStreamTOByte(is);
			return b;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/***************************************************************************
	 * add by yidie 2012-01-10 功能：订单打印 参数： String 订单短号 OrderDetail 打印内容，包含
	 * GoodsInfo[] String 打印标题
	 ****************************************************************************/

	public static boolean printOrder(Context c, QueryOrderDetailsRes detail) throws InvalidParameterException, SecurityException,IOException {
		DecimalFormat dcmFmt = new DecimalFormat("0.00");
		int iNum = 0, i;

		byte[] tempBuffer = new byte[8000];
		String stTmp = "";

		byte[] oldText = setAlignCenter('2');
		System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
		iNum += oldText.length;

		oldText = getLogo(c);
		System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
		iNum += oldText.length;

		oldText = setWH('1');
		System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
		iNum += oldText.length;

		oldText = getGbk("\n");
		System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
		iNum += oldText.length;

		oldText = setAlignCenter('2');
		System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
		iNum += oldText.length;

		oldText = setWH('4');
		System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
		iNum += oldText.length;

		oldText = setBold(true);
		System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
		iNum += oldText.length;

		oldText = getGbk(LoginUtils.getUserInfo(c).getsName());
		System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
		iNum += oldText.length;

		oldText = setWH('1');
		System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
		iNum += oldText.length;

		oldText = getGbk("\n");
		System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
		iNum += oldText.length;

		oldText = setAlignCenter('1');
		System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
		iNum += oldText.length;

		oldText = getGbk("\n" + detail.getOpStatus() + ": ");
		System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
		iNum += oldText.length;

		oldText = getGbk(detail.getShortNo());
		System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
		iNum += oldText.length;

		oldText = setCusorPosition(324);
		System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
		iNum += oldText.length;
		if (aCache == null) {
			aCache = ACache.get(c);
		}

		String ServerTime = aCache.getAsString("ServerTime");
		if (!TypeJudgeTools.isNull(ServerTime)) {
			long stime = Long.valueOf(ServerTime);
			String strTime = FormatTools.convertTime(stime, "yyyy-MM-dd HH:mm");
			oldText = getGbk(strTime + "打印\n");
		}

		System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
		iNum += oldText.length;

		oldText = setBold(false);
		System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
		iNum += oldText.length;

		oldText = getGbk("----------------------------------------------\n");
		System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
		iNum += oldText.length;

		oldText = setWH('3');
		System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
		iNum += oldText.length;

		
		if(detail.getIsReserve()==1){
			String date=FormatTools.convertTime(detail.getReserveTime()*1000, "yyyy-MM-dd");
			oldText = getGbk("外送时间：" + date);
			System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
			iNum += oldText.length;
			
			oldText = setBold(true);
			System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
			iNum += oldText.length;
			
			oldText = getGbk("  尽快送达"+"\n");
			System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
			iNum += oldText.length;
		}else{
			String date=FormatTools.convertTime(detail.getReserveTime()*1000, "yyyy-MM-dd");
			oldText = getGbk("外送时间：" + date);
			System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
			iNum += oldText.length;
			
			oldText = setBold(true);
			System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
			iNum += oldText.length;
			
			String time=FormatTools.convertTime(detail.getReserveTime()*1000, "HH:mm");
			oldText = getGbk("  "+time+"( 预约 )\n");
			System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
			iNum += oldText.length;
		}

		oldText = setBold(false);
		System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
		iNum += oldText.length;
		
		oldText = getGbk("外送地址：" + detail.getAddr() + "\n");
		System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
		iNum += oldText.length;
		String tele="";
		if(detail.getSex()==1){
			tele = "收货人：" + detail.getContact() +"女士  " + detail.getPhone();
		}else if(detail.getSex()==2){
			tele = "收货人：" + detail.getContact() +"先生  " + detail.getPhone();
		}else{
			tele = "收货人：" + detail.getContact() +" " + detail.getPhone();
		}
		
		if(detail.getOpStatus().equals("已拒绝") || detail.getOpStatus().equals("已取消") || detail.getOpStatus().equals("未应答") || detail.getOpStatus().equals("接通后未应答")){
			if(detail.getSex()==1){
				tele = "收货人：" + detail.getContact() +"女士  ";
			}else if(detail.getSex()==2){
				tele = "收货人：" + detail.getContact() +"先生  ";
			}else{
				tele = "收货人：" + detail.getContact();
			}
		}

		oldText = getGbk(tele + "\n");
		System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
		iNum += oldText.length;
		if(detail.getOpStatus().equals("已拒绝") || detail.getOpStatus().equals("已取消") || detail.getOpStatus().equals("未应答") || detail.getOpStatus().equals("接通后未应答")){

		}else{
			if(!detail.getPhone().equals(detail.getUserPhone())){
				String person = "下单人：" + detail.getUserPhone();
				oldText = getGbk(person + "\n");
				System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
				iNum += oldText.length;
			}
		}

		oldText = setWH('1');
		System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
		iNum += oldText.length;

		oldText = getGbk("   商品名称              单价    数量    金额\n");
		System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
		iNum += oldText.length;

		oldText = getGbk("----------------------------------------------\n");
		System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
		iNum += oldText.length;

		if (detail != null) {
			// 菜品名最多10个汉字（20个字符）；单价最多8个字符；数量最多4个字符；金额最多8个字符；中间分隔各2个空格
			float realPrice = 0;
			for (Goods goods : detail.getGoodsList()) {

				oldText = setWH('3');
				System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
				iNum += oldText.length;

				stTmp = goods.getgName()+goods.getProps();

				if (stTmp.length() > 12) {
					stTmp += "\n";
				}

				oldText = getGbk(stTmp);
				System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
				iNum += oldText.length;

				oldText = setWH('1');
				System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
				iNum += oldText.length;

				realPrice = goods.getPrice();
				stTmp = dcmFmt.format(realPrice);

				oldText = setCusorPosition(360 - 12 * stTmp.length());
				System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
				iNum += oldText.length;

				oldText = getGbk(stTmp);
				System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
				iNum += oldText.length;

				// 数量
				stTmp = goods.getSaleNum() + "";

				oldText = setCusorPosition(432 - 12 * stTmp.length());
				System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
				iNum += oldText.length;

				oldText = getGbk(stTmp);
				System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
				iNum += oldText.length;

				// 总价
				stTmp = dcmFmt.format(goods.getSaleFee()) + "";
				oldText = setCusorPosition(552 - 12 * stTmp.length());
				System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
				iNum += oldText.length;

				stTmp += "\n";
				oldText = getGbk(stTmp);
				System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
				iNum += oldText.length;
				
				if(!TextUtils.isEmpty(goods.getPluCode())){

					oldText = setAlignCenter('2');
					System.arraycopy(oldText, 0, tempBuffer,  iNum,  oldText.length);
					iNum += oldText.length;

					oldText = PrintBarcode(goods.getPluCode());
					System.arraycopy(oldText, 0,  tempBuffer,  iNum,  oldText.length);
					iNum += oldText.length;						
					
					oldText = setAlignCenter('1');
					System.arraycopy(oldText, 0, tempBuffer,  iNum,  oldText.length);
					iNum += oldText.length;
				}
			}

			if(detail.getDeliverFee()>0||detail.getGiftList()!=null||detail.getCouponList()!=null){
				oldText = getGbk("----------------------------------------------\n");
				System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
				iNum += oldText.length;
			}

			//外送费
			if (detail.getDeliverFee() > 0) {
				oldText = setWH('3');
				System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
				iNum += oldText.length;
				
				oldText = getGbk("外送费");
				System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
				iNum += oldText.length;

				oldText = setWH('1');
				System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
				iNum += oldText.length;
				
				oldText = setBold(false);
				System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
				iNum += oldText.length;

				stTmp = dcmFmt.format(detail.getDeliverFee()) + "";
				oldText = setCusorPosition(552 - 12 * stTmp.length());
				System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
				iNum += oldText.length;

				stTmp += "\n";
				oldText = getGbk(stTmp);
				System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
				iNum += oldText.length;
			}

			//餐盒费
			if (detail.getBoxType()!=0&&detail.getBoxFee()>0) {
				oldText = setWH('3');
				System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
				iNum += oldText.length;
				
				oldText = getGbk("餐盒费");
				System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
				iNum += oldText.length;

				oldText = setWH('1');
				System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
				iNum += oldText.length;
				
				oldText = setBold(false);
				System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
				iNum += oldText.length;
				
				// 总价
				stTmp = dcmFmt.format(detail.getBoxFee()) + "";
				oldText = setCusorPosition(552 - 12 * stTmp.length());
				System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
				iNum += oldText.length;

				stTmp += "\n";
				oldText = getGbk(stTmp);
				System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
				iNum += oldText.length;
			}
			
			//优惠活动
			List<Gift> gifts=detail.getGiftList();
			//gift数量
			int giftNum=0;
			if (gifts!=null) {
				for(int j=0;j<gifts.size();j++){
					Gift gift=gifts.get(j);
					if(gift.getSaleNum()>0){
						if(gift.getPrice()>=0){
							//统计赠品数量并且赠品价格大于0
							giftNum=giftNum+gift.getSaleNum();
							oldText = setWH('3');
							System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
							iNum += oldText.length;
							oldText = setBold(true);
							System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
							iNum += oldText.length;
						}else{
							oldText = setWH('3');
							System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
							iNum += oldText.length;
							
							oldText = setBold(false);
							System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
							iNum += oldText.length;
						}
						
						
						//活动名
						stTmp=gift.getgName();
						if (stTmp.length() > 12) {
							stTmp += "\n";
						}
						oldText = getGbk(stTmp);
						System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
						iNum += oldText.length;
						
						oldText = setWH('1');
						System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
						iNum += oldText.length;
						
						oldText = setBold(false);
						System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
						iNum += oldText.length;
						
						//单价
						stTmp = dcmFmt.format(gift.getPrice())+ "";
						oldText = setCusorPosition(360 - 12 * stTmp.length());
						System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
						iNum += oldText.length;
						
						// 数量
						stTmp = gift.getSaleNum() + "";
						oldText = setCusorPosition(432 - 12 * stTmp.length());
						System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
						iNum += oldText.length;

						oldText = getGbk(stTmp);
						System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
						iNum += oldText.length;

						// 总价
						float money=gift.getPrice();
						if(money==0){
							stTmp = "赠送";
							oldText = setCusorPosition(552 - 24 * stTmp.length());
							System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
							iNum += oldText.length;
						}else{
							stTmp = dcmFmt.format(gift.getPrice()) + "";
							oldText = setCusorPosition(552 - 12 * stTmp.length());
							System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
							iNum += oldText.length;
						}

						stTmp += "\n";
						oldText = getGbk(stTmp);
						System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
						iNum += oldText.length;
					}
				}
			}

			//优惠券
			List<Coupon> coupons=detail.getCouponList();
			if (coupons!=null) {
				for(int j=0;j<coupons.size();j++){
					oldText = setWH('3');
					System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
					iNum += oldText.length;

					oldText = setBold(false);
					System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
					iNum += oldText.length;
					
					Coupon coupon=coupons.get(j);
					//活动名
					stTmp=coupon.getcName();
					if (stTmp.length() > 12) {
						stTmp += "\n";
					}
					oldText = getGbk(stTmp);
					System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
					iNum += oldText.length;
					
					oldText = setWH('1');
					System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
					iNum += oldText.length;
					
					oldText = setBold(false);
					System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
					iNum += oldText.length;
					
					//单价
					stTmp = coupon.getcType()+ "";
					oldText = setCusorPosition(360 - 12 * stTmp.length());
					System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
					iNum += oldText.length;

					// 总价
					stTmp = "-"+coupon.getValue() ;
					oldText = setCusorPosition(552 - 12 * stTmp.length());
					System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
					iNum += oldText.length;

					stTmp += "\n";
					oldText = getGbk(stTmp);
					System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
					iNum += oldText.length;
				}
			}
			
			oldText = setBold(false);
			System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
			iNum += oldText.length;

			oldText = getGbk("----------------------------------------------\n");
			System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
			iNum += oldText.length;

			oldText = setWH('3');
			System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
			iNum += oldText.length;
			oldText = getGbk("总计： ");
			System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
			iNum += oldText.length;

			stTmp ="共"+ (detail.getSumNum()+giftNum) + "份";
			if(detail.getIsPay()==0){
				
				stTmp =stTmp+"   "+ dcmFmt.format(detail.getSumFee());
				oldText = setCusorPosition(408 - 12 * stTmp.length());
				System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
				iNum += oldText.length;
				
				oldText = getGbk(stTmp);
				System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
				iNum += oldText.length;
				
				oldText=setBold(true);
				System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
				iNum += oldText.length;
				
				stTmp ="( 已支付 )";
				oldText = setCusorPosition(552-120);
				System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
				iNum += oldText.length;
			}else{
				stTmp =stTmp+"   "+ dcmFmt.format(detail.getSumFee());
				oldText = setCusorPosition(552 - 12 * stTmp.length());
				System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
				iNum += oldText.length;
			}

			stTmp += "\n";
			oldText = getGbk(stTmp);
			System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
			iNum += oldText.length;

			if (!TypeJudgeTools.isNull(detail.getRemark())) {
				oldText = setBold(true);
				System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
				iNum += oldText.length;
				
				oldText = getGbk("备注：" + detail.getRemark() + "\n");
				System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
				iNum += oldText.length;
			}

			// 条码

			/*
			 * 
			 * oldText = setWH('1'); System.arraycopy(oldText, 0, tempBuffer,
			 * iNum, oldText.length); iNum += oldText.length;
			 * 
			 * 
			 * oldText = getGbk("\n"); System.arraycopy(oldText, 0, tempBuffer,
			 * iNum, oldText.length); iNum += oldText.length;
			 * 
			 * oldText = PrintBarcode(detail.No.substring(9));
			 * System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
			 * iNum += oldText.length;
			 * 
			 * oldText = setAlignCenter('1'); System.arraycopy(oldText, 0,
			 * tempBuffer, iNum, oldText.length); iNum += oldText.length;
			 * 
			 * oldText = getGbk("\n"); System.arraycopy(oldText, 0, tempBuffer,
			 * iNum, oldText.length); iNum += oldText.length;
			 */
		}
		oldText = setAlignCenter('2');
		System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
		iNum += oldText.length;

		oldText = setWH('1');
		System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
		iNum += oldText.length;

		oldText = getGbk("\n感谢使用[我有外卖]订餐,24小时服务热线 4008519517\n");
		System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
		iNum += oldText.length;
		
		//打印店铺公告
		UserLoginRes userLoginRes=(UserLoginRes)ACache.get(c).getAsObject("loginUserInfo");
		if(userLoginRes!=null&&!TypeJudgeTools.isNull(userLoginRes.getNotice())){
			oldText = setAlignCenter('2');
			System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
			iNum += oldText.length;

			oldText = setWH('4');
			System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
			iNum += oldText.length;

			oldText = setBold(true);
			System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
			iNum += oldText.length;
			
			oldText = getGbk("\n"+userLoginRes.getNotice()+"\n\n\n");
			System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
			iNum += oldText.length;
		}
		
		oldText = CutePaper();
		System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
		iNum += oldText.length;
		SerialPort mSerialPort = getSerialPort();
		OutputStream mOutputStream = mSerialPort.getOutputStream();
		try {
			mOutputStream.write(tempBuffer);
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	 
	/***************************************************************************
	 * add by yidie 2012-01-12 功能：报表打印 参数： String 打印标题，如“月报表：2013-01” ReportSale
	 * 打印内容，包含 SaleInfo[]
	 ****************************************************************************/

	public static boolean printReport(String title, ReportItem reportItem, String date)
			throws InvalidParameterException, SecurityException, IOException {
		int iNum = 0;
		String stTmp = "";

		byte[] tempBuffer = new byte[8000];
		SerialPort mSerialPort = getSerialPort();
		OutputStream mOutputStream = mSerialPort.getOutputStream();

		byte[] oldText = setAlignCenter('1');
		System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
		iNum += oldText.length;

		oldText = setWH('3');
		System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
		iNum += oldText.length;

		oldText = getGbk(title);
		System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
		iNum += oldText.length;
		
		oldText = getGbk("  " + date);
		System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
		iNum += oldText.length;

		oldText = setCusorPosition(324);
		System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
		iNum += oldText.length;

		String strTime = new SimpleDateFormat("yyyy-MM-dd HH:mm",
				Locale.SIMPLIFIED_CHINESE).format(new Date());
		oldText = getGbk(strTime + "打印\n\n");
		System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
		iNum += oldText.length;

		oldText = setAlignCenter('2');
		System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
		iNum += oldText.length;

		oldText = setWH('4');
		System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
		iNum += oldText.length;

		oldText = setBold(true);
		System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
		iNum += oldText.length;

		oldText = getGbk(Constants.shopName);
		System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
		iNum += oldText.length;

		oldText = setWH('1');
		System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
		iNum += oldText.length;

		oldText = getGbk("\n\n");
		System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
		iNum += oldText.length;

		oldText = setAlignCenter('1');
		System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
		iNum += oldText.length;

		oldText = setBold(false);
		System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
		iNum += oldText.length;

		oldText = setWH('1');
		System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
		iNum += oldText.length;

		oldText = getGbk("　　商品名称　　　　　　售出数量　　　售出金额\n");
		System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
		iNum += oldText.length;

		oldText = getGbk("----------------------------------------------\n");
		System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
		iNum += oldText.length;

		if (reportItem != null) {
			
			//商品列表
			List<GoodsCount> goodsList = reportItem.getGoodsCount();
			List<ReportData> reportList = new ArrayList<ReportData>();
			if (goodsList != null && goodsList.size() > 0) {
				for (GoodsCount goodsCount : goodsList) {
					ReportData item = new ReportData();
					item.setName(goodsCount.getgName());
					item.setNum(goodsCount.getgNum() + "");
					item.setPrice(goodsCount.getgPrice() + "");
					item.setType(0);
					reportList.add(item);
				}
			}
			//餐盒费
			List<BoxCount> countList = reportItem.getBoxCount();
			if ( countList!=null && countList.size()>0 ) {
				for (BoxCount boxCount : countList) {
					ReportData item = new ReportData();
					item.setName(boxCount.getbName());
					item.setNum(boxCount.getbNum() + "");
					item.setPrice(boxCount.getbPrice() + "");
					item.setType(3);
					reportList.add(item);
				}
			}
			if (reportList.size() > 0) {
				for (int i = 0; i < reportList.size(); i++) {
					ReportData item = reportList.get(i);
					stTmp = item.getName();

					if (stTmp.length() > 12) {
//						stTmp = stTmp.substring(0, 12);    //截取12个字符打印
						stTmp += "\n";
					}

					oldText = getGbk(stTmp);
					System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
					iNum += oldText.length;

					oldText = setWH('1');
					System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
					iNum += oldText.length;
					
					stTmp = item.getNum();
					if (stTmp.length() > 4) {
						stTmp = stTmp.substring(0, 4);
					}
					
					oldText = setCusorPosition(384 - 12 * stTmp.length());
					System.arraycopy(oldText, 0, tempBuffer, iNum,
							oldText.length);
					iNum += oldText.length;

					oldText = getGbk(stTmp);
					System.arraycopy(oldText, 0, tempBuffer, iNum,
							oldText.length);
					iNum += oldText.length;

					stTmp = item.getPrice();
					if ( TextUtils.equals("免费", item.getPrice()) ) {
						oldText = setCusorPosition(552 - 24*stTmp.length());
					} else {
						if (stTmp.length() > 10) {
							stTmp = stTmp.substring(0, 10);
						}
						oldText = setCusorPosition(552 - 12 * stTmp.length());
					}
					
					System.arraycopy(oldText, 0, tempBuffer, iNum,
							oldText.length);
					iNum += oldText.length;
					
					stTmp += "\n";

					oldText = getGbk(stTmp);
					System.arraycopy(oldText, 0, tempBuffer, iNum,
							oldText.length);
					iNum += oldText.length;

					if (iNum > 7500) {
						try {
							mOutputStream.write(tempBuffer);
						} catch (IOException e) {
							e.printStackTrace();
							return false;
						}
						iNum = 0;
						tempBuffer = new byte[8000];
					}
				}
			}
			
			//赠品
			List<GiftCount> giftList = reportItem.getGiftCount();
			if ( giftList!=null && giftList.size()>0 ) {
				
				oldText = getGbk("----------------------------------------------\n");
				System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
				iNum += oldText.length;
				
				List<ReportData> giftDatas = new ArrayList<ReportData>();
				if (giftList != null && giftList.size() > 0) {
					for (GiftCount giftCount : giftList) {
						ReportData item = new ReportData();
						item.setName(giftCount.getGiftName());
						item.setNum(giftCount.getGiftNum() + "");
						String money = FormatTools.String2Money(giftCount.getGiftPrice() + "");
						if ( TextUtils.equals(money, "0") ){
							item.setPrice("免费");
						} else {
							item.setPrice(giftCount.getGiftPrice() + "");
						}
						item.setType(1);
						giftDatas.add(item);
					}
				}
				if (giftDatas.size() > 0) {
					for (int i = 0; i < giftDatas.size(); i++) {
						ReportData item = giftDatas.get(i);
						stTmp = item.getName();

						if (stTmp.length() > 12) {
							stTmp += "\n";
						}

						oldText = getGbk(stTmp);
						System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
						iNum += oldText.length;

						oldText = setWH('1');
						System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
						iNum += oldText.length;
						
						stTmp = item.getNum();
						if (stTmp.length() > 4) {
							stTmp = stTmp.substring(0, 4);
						}
						
						oldText = setCusorPosition(384 - 12 * stTmp.length());
						System.arraycopy(oldText, 0, tempBuffer, iNum,
								oldText.length);
						iNum += oldText.length;

						oldText = getGbk(stTmp);
						System.arraycopy(oldText, 0, tempBuffer, iNum,
								oldText.length);
						iNum += oldText.length;

						stTmp = item.getPrice();
						if ( TextUtils.equals("免费", item.getPrice()) ) {
							oldText = setCusorPosition(552 - 24*stTmp.length());
						} else {
							if (stTmp.length() > 10) {
								stTmp = stTmp.substring(0, 10);
							}
							oldText = setCusorPosition(552 - 12 * stTmp.length());
						}
						
						System.arraycopy(oldText, 0, tempBuffer, iNum,
								oldText.length);
						iNum += oldText.length;
						
						stTmp += "\n";

						oldText = getGbk(stTmp);
						System.arraycopy(oldText, 0, tempBuffer, iNum,
								oldText.length);
						iNum += oldText.length;

						if (iNum > 7500) {
							try {
								mOutputStream.write(tempBuffer);
							} catch (IOException e) {
								e.printStackTrace();
								return false;
							}
							iNum = 0;
							tempBuffer = new byte[8000];
						}
					}
				}
				
			}
			
			//优惠券
			List<CouponCount> couponList = reportItem.getCouponCount();
			if ( couponList!=null && couponList.size()>0 ) {
				
				oldText = getGbk("----------------------------------------------\n");
				System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
				iNum += oldText.length;
				
				List<ReportData> couponDatas = new ArrayList<ReportData>();
				if (couponList != null && couponList.size() > 0) {
					for (CouponCount couponCount : couponList) {
						ReportData item = new ReportData();
						item.setName(couponCount.getcName());
						item.setNum(couponCount.getcNum() + "");
						item.setPrice(couponCount.getcPrice() + "");
						item.setType(2);
						couponDatas.add(item);
					}
				}
				if (couponDatas.size() > 0) {
					for (int i = 0; i < couponDatas.size(); i++) {
						ReportData item = couponDatas.get(i);
						stTmp = item.getName();

						if (stTmp.length() > 12) {
							stTmp += "\n";
						}

						oldText = getGbk(stTmp);
						System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
						iNum += oldText.length;

						oldText = setWH('1');
						System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
						iNum += oldText.length;
						
						stTmp = item.getNum();
						if (stTmp.length() > 4) {
							stTmp = stTmp.substring(0, 4);
						}
						
						oldText = setCusorPosition(384 - 12 * stTmp.length());
						System.arraycopy(oldText, 0, tempBuffer, iNum,
								oldText.length);
						iNum += oldText.length;

						oldText = getGbk(stTmp);
						System.arraycopy(oldText, 0, tempBuffer, iNum,
								oldText.length);
						iNum += oldText.length;

						stTmp = item.getPrice();
						if ( TextUtils.equals("免费", item.getPrice()) ) {
							oldText = setCusorPosition(552 - 24*stTmp.length());
						} else {
							if (stTmp.length() > 10) {
								stTmp = stTmp.substring(0, 10);
							}
							oldText = setCusorPosition(552 - 12 * stTmp.length());
						}
						
						System.arraycopy(oldText, 0, tempBuffer, iNum,
								oldText.length);
						iNum += oldText.length;
						
						stTmp += "\n";

						oldText = getGbk(stTmp);
						System.arraycopy(oldText, 0, tempBuffer, iNum,
								oldText.length);
						iNum += oldText.length;

						if (iNum > 7500) {
							try {
								mOutputStream.write(tempBuffer);
							} catch (IOException e) {
								e.printStackTrace();
								return false;
							}
							iNum = 0;
							tempBuffer = new byte[8000];
						}
					}
				}
				
			}
			
			oldText = getGbk("----------------------------------------------\n");
			System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
			iNum += oldText.length;

			oldText = setAlignCenter('1');
			System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
			iNum += oldText.length;

			oldText = setWH('3');
			System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
			iNum += oldText.length;
			
			oldText = getGbk("总计: " + reportItem.getSumNum() + "单   \n");
			System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
			iNum += oldText.length;
			
			oldText = getGbk("收款总计: " + reportItem.getSumFee() + "元    \n");
			System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
			iNum += oldText.length;
			
			oldText = getGbk("优惠券补贴: " + reportItem.getCouponFee() + "元  \n");
			System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
			iNum += oldText.length;
			
			oldText = getGbk("总营业额: " + reportItem.getBusinessFee() + "元  \n");
			System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
			iNum += oldText.length;
			
		}

		oldText = CutePaper();
		System.arraycopy(oldText, 0, tempBuffer, iNum, oldText.length);
		iNum += oldText.length;
		try {
			mOutputStream.write(tempBuffer);
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	private static SerialPort mSerialPort = null;

	public static SerialPort getSerialPort() throws SecurityException,
			IOException, InvalidParameterException {
		if (mSerialPort == null) {
			String spFile = null;
			String model = MainBoardUtil.getModel(); // android.os.Build.MODEL.toLowerCase();
			if (model.contains(Constants.MAIN_BOARD_SMDKV210)) {
				spFile = "/dev/s3c2410_serial0";
			} else if (model.contains(Constants.MAIN_BOARD_RK30)) {
				spFile = "/dev/ttyS1";
			} else if (model.contains(Constants.MAIN_BOARD_C500)) {
				spFile = "/dev/ttyS1";
			} else {
				throw new IOException("unknow hardware!");
			}

			int baudrate = 115200;
			boolean flagCon = true;

			File myFile = new File(spFile);

			/* Open the serial port */
			mSerialPort = new SerialPort(myFile, baudrate, 0, flagCon);
		}
		return mSerialPort;
	}

	public static void closeSerialPort() {
		if (mSerialPort != null) {
			mSerialPort.close();
			mSerialPort = null;
		}
	}

	public static byte[] getGbk(String stText) {
		byte[] returnText = null;
		try {
			returnText = stText.getBytes("GBK"); // 必须放在try内才可以
		} catch (Exception ex) {
			;
		}
		return returnText;
	}

	public static byte[] setWH(char dist) {
		byte[] returnText = new byte[3]; // GS ! 11H 倍宽倍高
		returnText[0] = 0x1D;
		returnText[1] = 0x21;

		switch (dist) // 1-无；2-倍宽；3-倍高； 4-倍宽倍高
		{
		case '2':
			returnText[2] = 0x10;
			break;
		case '3':
			returnText[2] = 0x01;
			break;
		case '4':
			returnText[2] = 0x11;
			break;
		default:
			returnText[2] = 0x00;
			break;
		}

		return returnText;
	}

	public static byte[] setAlignCenter(char dist) {
		byte[] returnText = new byte[3]; // 对齐 ESC a
		returnText[0] = 0x1B;
		returnText[1] = 0x61;

		switch (dist) // 1-左对齐；2-居中对齐；3-右对齐
		{
		case '2':
			returnText[2] = 0x01;
			break;
		case '3':
			returnText[2] = 0x02;
			break;
		default:
			returnText[2] = 0x00;
			break;
		}
		return returnText;
	}

	public static byte[] setBold(boolean dist) {
		byte[] returnText = new byte[3]; // 加粗 ESC E
		returnText[0] = 0x1B;
		returnText[1] = 0x45;

		if (dist) {
			returnText[2] = 0x01; // 表示加粗
		} else {
			returnText[2] = 0x00;
		}
		return returnText;
	}

	public static byte[] PrintBarcode(String stBarcode) {
		int iLength = stBarcode.length() + 4;
		byte[] returnText = new byte[iLength];

		returnText[0] = 0x1D;
		returnText[1] = 'k';
		returnText[2] = 0x45;
		returnText[3] = (byte) stBarcode.length(); // 条码长度；

		System.arraycopy(stBarcode.getBytes(), 0, returnText, 4,
				stBarcode.getBytes().length);

		return returnText;
	}

	public static byte[] CutePaper() {
		byte[] returnText = new byte[] { 0x1D, 0x56, 0x42, 0x00 }; // 切纸； GS V
																	// 66D 0D
		return returnText;
	}
}
