/** 
 * @Title:  java 
 * @author:  xuron
 * @data:  2015年11月24日 上午10:03:37 <创建时间>
 * 
 * @history：<以下是历史记录>
 *
 * @modifier: <修改人>
 * @modify date: 2015年11月24日 上午10:03:37 <修改时间>
 * @log: <修改内容>
 *
 * @modifier: <修改人>
 * @modify date: 2015年11月24日 上午10:03:37 <修改时间>
 * @log: <修改内容>
 */
package com.woyou.component;

import java.util.Timer;
import java.util.TimerTask;

import com.jing.biandang.R;
import com.woyou.Constants;
import com.woyou.activity.HomeActivity;
import com.woyou.bean.FmInfo;
import com.woyou.bean.UserInfo;
import com.woyou.bean.rpc.UserLoginRes;
import com.woyou.controller.ActionBarController;
import com.woyou.fragment.MessageFragment;
import com.woyou.fragment.order.OrderFragment;
import com.woyou.model.OrderManageModel;
import com.woyou.service.MessageConnection;
import com.woyou.utils.FormatTools;
import com.woyou.utils.LightManager;
import com.woyou.utils.LoginUtils;
import com.woyou.utils.NavigationbarUtils;
import com.woyou.utils.SoundManager;
import com.woyou.utils.StateJudgeTools;
import com.woyou.utils.TypeJudgeTools;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import net.simonvt.menudrawer.MenuDrawer;

/**
 * 自定义ActionBar
 * 
 * @author xuron
 * @versionCode 1 <每次修改提交前+1>
 */
public class ActionbarView extends SuperLayout {
	public ActionBarController actionBarController;
	/**
	 * HomeActivity
	 */
	private HomeActivity homeActivity;
	public MenuDrawer menuDrawer;
	private MenuView menuView;
	// 上边Actionbar
	public RelativeLayout layout_actionbar_mask;
	public TextView layout_actionbar_time;
	public TextView layout_actionbar_nettype;
	public ImageView layout_actionbar_neticon;
	public RelativeLayout layout_actionbar_choice;
	public TextView layout_actionbar_choice_tv;
	public LinearLayout layout_actionbar_switch;
	public LinearLayout layout_actionbar_back;
	public TextView layout_actionbar_back_tv;
	public RelativeLayout layout_actionbar_home;
	public FrameLayout layout_actionbar_message;
	public TextView layout_actionbar_shopname;
	/**
	 * 是否正在设置店铺状态
	 */
	public boolean isSettingShopStatus = true;
	/**
	 * 设置繁忙状态0:繁忙中，1：一小时，2:两小时
	 */
	public int busyStatus = 0;
	/**
	 * 营业状态
	 */
	public String shopStatus;
	/**
	 * pop
	 */
	private StateChoicePopup stateChoicePopup;
	/**
	 * 定义一个全局的计时器与服务器同步时间
	 */
	public Timer serverTimer = null;
	public TimerTask serverTimerTask = null;
	/**
	 * 刷新当前时间和当前网络类型
	 */
	public Timer refreshTimer = null;
	public TimerTask refreshTimerTask = null;

	public ActionbarView(Context context,MenuDrawer menuDrawer) {
		super(context);
		homeActivity = (HomeActivity) context;
		this.menuDrawer = menuDrawer;
		initView();
		actionBarController=new ActionBarController(context,this);
	}

	@Override
	protected void initView() {

		LayoutInflater.from(getContext()).inflate(R.layout.layout_home_actionbar, this);
		layout_actionbar_time = (TextView) findViewById(R.id.layout_actionbar_time);
		layout_actionbar_nettype = (TextView) findViewById(R.id.layout_actionbar_nettype);
		layout_actionbar_neticon = (ImageView) findViewById(R.id.layout_actionbar_neticon);
		layout_actionbar_mask = (RelativeLayout) findViewById(R.id.layout_actionbar_mask);
		layout_actionbar_switch = (LinearLayout) findViewById(R.id.layout_actionbar_switch);
		layout_actionbar_shopname = (TextView) findViewById(R.id.layout_actionbar_shopname);
		// 设置店铺名称
		if (LoginUtils.getUserInfo(mContext) != null) {
			UserInfo userInfo = LoginUtils.getUserInfo(mContext);
			layout_actionbar_shopname.setText(userInfo.getsName());
		}
		layout_actionbar_back = (LinearLayout) findViewById(R.id.layout_actionbar_back);
		layout_actionbar_back_tv = (TextView) findViewById(R.id.layout_actionbar_back_tv);
		layout_actionbar_home = (RelativeLayout) findViewById(R.id.layout_actionbar_home);
		layout_actionbar_message = (FrameLayout) findViewById(R.id.layout_actionbar_message);
		layout_actionbar_choice = (RelativeLayout) findViewById(R.id.layout_actionbar_choice);
		layout_actionbar_choice_tv = (TextView) findViewById(R.id.layout_actionbar_choice_tv);
		// 初始化titlebar
		getNetTypeAndTime();

		// 操作左菜单
		layout_actionbar_switch.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				hideInputMethod();
				menuDrawer.toggleMenu();
			}
		});
		// 返回到上一个Fragment
		layout_actionbar_back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				homeActivity.backBeforeFM();
			}
		});
		/**
		 * 用蒙版 屏蔽actionbar上的点击事件
		 */
		layout_actionbar_mask.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				return true;
			}
		});
		/**
		 * 跳转到营业订单页面
		 */
		layout_actionbar_home.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				hideInputMethod();
				menuView.menuViewController.showMark(menuView, 0);
				if (aCache.getAsObject("loginUserInfo") != null) {
					UserLoginRes userLoginRes = (UserLoginRes) aCache.getAsObject("loginUserInfo");
					homeActivity.openFM(new FmInfo(OrderFragment.class,userLoginRes.getsName(),false));
				}
			}
		});
		/**
		 * 跳转到消息中心页面
		 */
		layout_actionbar_message.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				hideInputMethod();
				menuView.menuViewController.showMark(menuView, 2);
				homeActivity.openFM(new FmInfo(MessageFragment.class,"消息中心",false));
			}
		});
		// 操作右边营业状态下拉列表
		layout_actionbar_choice.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (stateChoicePopup == null) {
					stateChoicePopup = new StateChoicePopup(homeActivity, layout_actionbar_choice,layout_actionbar_choice_tv);
				}
				stateChoicePopup.showPop();
			}
		});

	}

	/**
	 * 设置MenuView
	 * 
	 * @param menuView
	 */
	public void setMenuView(MenuView menuView) {
		this.menuView = menuView;
	}

	/**
	 * 获取网络类型和服务器时间 -1：没有网络 1：WIFI网络 2：wap网络 3：net网络
	 */
	public void getNetTypeAndTime() {
		if (refreshTimer != null) {
			refreshTimer.cancel();
			refreshTimer = null;
		}
		if (refreshTimerTask != null) {
			refreshTimerTask.cancel();
			refreshTimerTask = null;
		}
		refreshTimer = new Timer();
		refreshTimerTask = new TimerTask() {

			@Override
			public void run() {
				post(new Runnable() {
					public void run() {
						String ServerTime = aCache.getAsString("ServerTime");
						if (!TypeJudgeTools.isNull(ServerTime)) {
							String time = FormatTools.convertTime(Long.valueOf(ServerTime), "HH:mm");
							layout_actionbar_time.setText(time);
						}
						// 更改网络
						int netType = StateJudgeTools.getNetworkState(mContext);
						switch (netType) {
						case 1:
							layout_actionbar_nettype.setText("Wifi");
							layout_actionbar_neticon.setImageResource(R.raw.icon_net_wifi_28);
							break;
						case 3:
							layout_actionbar_nettype.setText("3G");
							layout_actionbar_neticon.setImageResource(R.raw.icon_net_3g_28);
							break;
						case 5:
							layout_actionbar_nettype.setText("有线网络");
							layout_actionbar_neticon.setImageResource(R.raw.icon_net_line_28);
							break;
						default:
							layout_actionbar_nettype.setText("无网络");
							layout_actionbar_neticon.setImageResource(R.raw.icon_net_no_28);
							break;
						}
					}
				});
			}
		};
		refreshTimer.schedule(refreshTimerTask, 1 * 1000, 10 * 1000);
	}

	/**
	 * 隐藏输入法
	 */
	public void hideInputMethod() {
		if (homeActivity.getCurrentFocus() == null) {
			return;
		}
		homeActivity.hideInputMethod();
	}

	/**
	 * onDestroy
	 */
	public void onDestroy() {

		// 关闭同步服务器时间
		if (refreshTimer != null) {
			refreshTimer.cancel();
			refreshTimer = null;
		}
		if (refreshTimerTask != null) {
			refreshTimerTask.cancel();
			refreshTimerTask = null;
		}
		// 关闭同步服务器时间的Timer
		if (serverTimer != null) {
			serverTimer.cancel();
			serverTimer = null;
		}
		if (serverTimerTask != null) {
			serverTimerTask.cancel();
			serverTimerTask = null;
		}
		// 根据时间段每半个小时刷新一下营业状态
		menuView.isRefreshAtTime = true;
		// 设置营业状态
		isSettingShopStatus = false;
		NavigationbarUtils.getInstance(homeActivity).show();
		// 关闭闪烁灯
		LightManager.turnOnRedLight(false);
		LightManager.turnOnBlueLight(false);
		LightManager.turnOnRedBlueLight(false);
		// 关闭声音
		SoundManager.getInstance(homeActivity).stopSound();
		// 关闭心跳线程
		MessageConnection.getInstance().stopSocket();
	}

	/**
	 * 同步服务器时间
	 */
	public void syncServerTime() {
		if (serverTimer != null) {
			serverTimer.cancel();
			serverTimer = null;
		}
		if (serverTimerTask != null) {
			serverTimerTask.cancel();
			serverTimerTask = null;
		}
		serverTimer = new Timer();
		serverTimerTask = new TimerTask() {
			@Override
			public void run() {
				String ServerTime = aCache.getAsString("ServerTime");
				if (!TypeJudgeTools.isNull(ServerTime)) {
					long stime = Long.valueOf(ServerTime);
					stime = stime + (60 * 1000);
					ServerTime = stime + "";
					aCache.put("ServerTime", ServerTime);
					// 刷新全部送出订单和今日无订单页面
					if (FormatTools.convertTime(stime, "HH:mm").equals("00:10")) {
						if (homeActivity.getOrderFm() != null) {
							OrderManageModel.getInstance().clearAllData();
							homeActivity.getOrderFm().orderController.loadAllOrderList();
						}
					}
					// 根据时间段每半个小时刷新一下营业状态且当前状态不为休息
					if (menuView.isRefreshAtTime && !Constants.shopStatus.equals("C")) {
						String atTime = FormatTools.convertTime(stime, "HH:mm");
						actionBarController.setShopStatusByTimerList(atTime);
					}
				}
			}
		};
		// 每分钟走一次
		serverTimer.schedule(serverTimerTask, 60 * 1000, 60 * 1000);
	}
}
