package com.woyou.fragment.shop;

import java.util.ArrayList;
import java.util.List;

import com.jing.biandang.R;
import com.squareup.picasso.Picasso;
import com.woyou.Constants;
import com.woyou.activity.HomeActivity;
import com.woyou.bean.BoxFee;
import com.woyou.bean.DeliveryTime;
import com.woyou.bean.FmInfo;
import com.woyou.bean.MarkUp;
import com.woyou.bean.ShopInfoItem;
import com.woyou.bean.UserInfo;
import com.woyou.bean.WYList;
import com.woyou.bean.rpc.QueryShopInfoReq;
import com.woyou.bean.rpc.Result;
import com.woyou.bean.rpc.UpdateShopInfoReq;
import com.woyou.bean.rpc.UserLoginRes;
import com.woyou.component.SuperUI;
import com.woyou.event.EventModifyShopStatus;
import com.woyou.fragment.SuperFragment;
import com.woyou.model.rpc.ShopModel;
import com.woyou.utils.DateUtils;
import com.woyou.utils.FormatTools;
import com.woyou.utils.LoginUtils;
import com.woyou.utils.ThreadPoolManager;
import com.woyou.utils.TypeJudgeTools;

import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import de.greenrobot.event.EventBus;
import retrofit.RetrofitError;

/**
 * 店铺信息界面
 * 
 * @author zhou.ni
 *
 */
public class ShopFragment extends SuperFragment implements OnClickListener {

	private HomeActivity homeActivity;
	private View view;

	private LinearLayout layout_shop_linear;
	// 统计及评分
	private TextView layout_shop_order;
	private TextView layout_shop_customer;
	private TextView layout_shop_score;
	private TextView layout_shop_rate;
	
	// 店铺图片和名字
	private ImageView layout_menu_image;
	private TextView layout_menu_name;
	private TextView layout_menu_info;
	public ImageView menuStart;			//星级
	private ImageView menuPay;			//是否支持在线支付
	public TextView menuScorenum;		//评分人数
	public ImageView menuStatus;		//店铺状态
	public TextView menuSendup;			//起送价
	public TextView menuSalesnum;		//月售

	// 注册信息
	private TextView layout_shop_type;
	private TextView layout_shop_register;
	private TextView layout_shop_person;
	private TextView layout_shop_phone;
	// 服务电话
	private String sPhoneNum;
	private EditText layout_shop_sphone_et;
	private TextView layout_shop_sphone_modify;
	private TextView layout_shop_sphone_save;
	private TextView layout_shop_sphone_cancel;
	// 店铺公告
	private String sShopNotice;
	private EditText layout_shop_notice_et;
	private TextView layout_shop_notice_modify;
	private TextView layout_shop_notice_save;
	private TextView layout_shop_notice_cancel;
	// 外送加价
	private TextView layout_shop_markup_tv;
	private TextView layout_shop_markup_modify;
	// 外卖时段
	private TextView layout_shop_bh_tv;
	private TextView layout_shop_bh_modify;
	// 店铺简介
	private String sShopInfo;
	private EditText layout_shop_brief_et;
	private TextView layout_shop_brief_modify;
	private TextView layout_shop_brief_save;
	private TextView layout_shop_brief_cancel;
	private TextView layout_shop_replymessage;
	//餐盒费
	private TextView boxFeeTv;
	private TextView modifyBoxFee;
	
	private ShopModel mShopController;
	private ShopInfoItem shopInfoItem;					//店铺详情

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		EventBus.getDefault().register(this);
		homeActivity=(HomeActivity)getActivity();
		view = inflater.inflate(R.layout.layout_shop, container, false);
		mShopController = ShopModel.getInstance(homeActivity);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		initView();
		loadShopData();
	}
	
	@Override
	public void onHiddenChanged(boolean hidden) {
		super.onHiddenChanged(hidden);
		if( !hidden ) {
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					loadShopData();
					modifyShopStatus();
					refreshDeliverFee();
					refreshBoxFee();
					refreshShopHours();
				}
			}, 500);
		} else {
			isShowModify(false);
			hideSaveButton();
			homeActivity.isCanShopSetting=false;
		}
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		EventBus.getDefault().unregister(this);
	}
	
	/**
	 * 初始化页面
	 */
	private void initView() {
		layout_shop_linear = (LinearLayout) view.findViewById(R.id.layout_shop_linear);
		layout_shop_linear.setOnClickListener(this);
		
		// 统计及评分
		layout_shop_order = (TextView) view.findViewById(R.id.layout_shop_order);
		layout_shop_customer = (TextView) view.findViewById(R.id.layout_shop_customer);
		layout_shop_score = (TextView) view.findViewById(R.id.layout_shop_score);
		layout_shop_rate = (TextView) view.findViewById(R.id.layout_shop_rate);
		
		// 店铺图片和名字
		layout_menu_image = (ImageView) view.findViewById(R.id.layout_menu_image);
		layout_menu_name = 	(TextView) view.findViewById(R.id.layout_menu_name);
		layout_menu_info = 	(TextView) view.findViewById(R.id.layout_menu_info);
		menuStart = (ImageView) view.findViewById(R.id.layout_menu_start);
		menuScorenum = (TextView) view.findViewById(R.id.layout_menu_scorenum);
		menuStatus = (ImageView) view.findViewById(R.id.layout_menu_status);
		menuPay=(ImageView)view.findViewById(R.id.layout_menu_pay);
		menuSalesnum = (TextView) view.findViewById(R.id.layout_menu_salesnum);
		menuSendup = (TextView) view.findViewById(R.id.layout_menu_sendup);
		
		// 注册信息
		layout_shop_type = (TextView) view.findViewById(R.id.layout_shop_type);
		layout_shop_register = (TextView) view.findViewById(R.id.layout_shop_register);
		layout_shop_person = (TextView) view.findViewById(R.id.layout_shop_person);
		layout_shop_phone = (TextView) view.findViewById(R.id.layout_shop_phone);

		// 服务电话
		layout_shop_sphone_et = (EditText) view.findViewById(R.id.layout_shop_sphone_et);
		layout_shop_sphone_modify = (TextView) view.findViewById(R.id.layout_shop_sphone_modify);
		layout_shop_sphone_save = (TextView) view.findViewById(R.id.layout_shop_sphone_save);
		layout_shop_sphone_cancel = (TextView) view.findViewById(R.id.layout_shop_sphone_cancel);
		layout_shop_sphone_modify.setOnClickListener(this);
		layout_shop_sphone_save.setOnClickListener(this);
		layout_shop_sphone_cancel.setOnClickListener(this);

		// 店铺公告
		layout_shop_notice_et = (EditText) view.findViewById(R.id.layout_shop_notice_et);
		layout_shop_notice_modify = (TextView) view.findViewById(R.id.layout_shop_notice_modify);
		layout_shop_notice_save = (TextView) view.findViewById(R.id.layout_shop_notice_save);
		layout_shop_notice_cancel = (TextView) view.findViewById(R.id.layout_shop_notice_cancel);
		layout_shop_notice_modify.setOnClickListener(this);
		layout_shop_notice_save.setOnClickListener(this);
		layout_shop_notice_cancel.setOnClickListener(this);
		// 外送加价
		layout_shop_markup_tv = (TextView) view.findViewById(R.id.layout_shop_markup_tv);
		layout_shop_markup_modify = (TextView) view.findViewById(R.id.layout_shop_markup_modify);
		layout_shop_markup_modify.setOnClickListener(this);
		// 外卖时段
		layout_shop_bh_tv = (TextView) view.findViewById(R.id.layout_shop_bh_tv);
		layout_shop_bh_modify = (TextView) view.findViewById(R.id.layout_shop_bh_modify);
		layout_shop_bh_modify.setOnClickListener(this);
		// 店铺简介
		layout_shop_brief_et = (EditText) view.findViewById(R.id.layout_shop_brief_et);
		layout_shop_replymessage = (TextView) view.findViewById(R.id.layout_shop_replymessage);
		layout_shop_brief_modify = (TextView) view.findViewById(R.id.layout_shop_brief_modify);
		layout_shop_brief_save = (TextView) view.findViewById(R.id.layout_shop_brief_save);
		layout_shop_brief_cancel = (TextView) view.findViewById(R.id.layout_shop_brief_cancel);
		layout_shop_brief_modify.setOnClickListener(this);
		layout_shop_brief_save.setOnClickListener(this);
		layout_shop_brief_cancel.setOnClickListener(this);
		//餐盒费
		boxFeeTv = (TextView) view.findViewById(R.id.layout_shop_box_tv);
		modifyBoxFee = (TextView) view.findViewById(R.id.layout_shop_box_modify);  
		modifyBoxFee.setOnClickListener(this);
		//监听输入内容长度是否超出范围
		layout_shop_brief_et.addTextChangedListener(mTextWatcher);
		
	}

	/**
	 * 获取店铺详情
	 */
	public void loadShopData() {
		ThreadPoolManager.getInstance().executeTask(new Runnable() {
			@Override
			public void run() {
				try {
					homeActivity.showLoadingHint();
					QueryShopInfoReq shopInfoReq = new QueryShopInfoReq();
					UserInfo userInfo = LoginUtils.getUserInfo(homeActivity);
					if ( userInfo!=null ){
						shopInfoReq.setsId(userInfo.getsId());
						shopInfoReq.setPwd(userInfo.getPwd());
					}
					final Result<ShopInfoItem> result = mShopController.queryShopInfo(shopInfoReq);
					if ( result!=null && result.code==1 ) {
						homeActivity.runOnUiThread(new Runnable() {
							
							@Override
							public void run() {
								refreshUI(result);
							}
						});
					} else if( result != null && result.code==-3 ) {
						homeActivity.homeController.exitLogin();
					} else {
						homeActivity.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								SuperUI.openToast(homeActivity, "很抱歉,服务器又任性了");
							}
						});
					}
				} catch (RetrofitError e) {
					homeActivity.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							SuperUI.openToast(homeActivity, "很抱歉,服务器又任性了");
						}
					});
				} finally {
					homeActivity.hideLoadingHint();
				}
			}
		});
	}
	
	/**
	 * 刷新店铺信息
	 * @param result
	 */
	private void refreshUI(Result<ShopInfoItem> result){
		shopInfoItem = result.getData();
		if( shopInfoItem!=null ) {
			// 设置左边抽屉的内容
			String[] sNameArray = shopInfoItem.getsName().split("\\(");
			homeActivity.menuView.menuName.setText(sNameArray[0]);
			layout_menu_name.setText(sNameArray[0]);
			if ( !TextUtils.isEmpty(shopInfoItem.getNotice())) {
				homeActivity.menuView.menuInfo.setText(shopInfoItem.getNotice());
				layout_menu_info.setText(shopInfoItem.getNotice());
				layout_shop_notice_et.setText(shopInfoItem.getNotice());
			} else if (sNameArray.length > 1) {
				homeActivity.menuView.menuInfo.setText("(" + sNameArray[1]);
				layout_menu_info.setText("(" + sNameArray[1]);
			} else {
				homeActivity.menuView.menuInfo.setText("");
				layout_menu_info.setText("");
			}
			
			if( !TextUtils.isEmpty(shopInfoItem.getShopLogo()) ){
				Picasso.with(homeActivity).load(shopInfoItem.getShopLogo()).placeholder(R.raw.icon_default).error(R.raw.icon_default).into(layout_menu_image);
				Picasso.with(homeActivity).load(shopInfoItem.getShopLogo()).placeholder(R.raw.icon_default).error(R.raw.icon_default).into(homeActivity.menuView.menuImage);
			} else {
				layout_menu_image.setImageResource(R.raw.icon_default);
				homeActivity.menuView.menuImage.setImageResource(R.raw.icon_default);
			}
			
			if ( shopInfoItem.getScoreNum()!=0 ) {
				homeActivity.menuView.menuScorenum.setText("(" + shopInfoItem.getScoreNum() + ")");
			} else {
				homeActivity.menuView.menuScorenum.setText("");
			}
			// 星级
			setPraiseRate(homeActivity.menuView.menuStart, shopInfoItem.getScore());
			setPraiseRate(menuStart, shopInfoItem.getScore());
			modifyShopStatus();
			// 获取店铺信息
			UserLoginRes loginRes = (UserLoginRes) aCache.getAsObject("loginUserInfo");
			if (loginRes != null) {
				//判断是否支持在线支付
				if(loginRes.getIsOnlinePay()==1){
					menuPay.setVisibility(View.VISIBLE);
				}else{
					menuPay.setVisibility(View.GONE);
				}
			}

			//评分人数
			if ( shopInfoItem.getScoreNum()!=0 ) {
				homeActivity.menuView.menuScorenum.setText("(" + shopInfoItem.getScoreNum() + ")");
				menuScorenum.setText("(" + shopInfoItem.getScoreNum() + ")");
			} else {
				homeActivity.menuView.menuScorenum.setText("");
				menuScorenum.setText("");
			}
			// 20元起送 / 月售200份
			List<MarkUp> list = shopInfoItem.getMarkUpList();
			if ( list!=null && list.size()>0 ) {
				String prompt = FormatTools.String2Money(list.get(0).getSendUpFee()+"") + "元起送 / ";
				homeActivity.menuView.menuSendup.setText(prompt);
				menuSendup.setText(prompt);
			} else {
				homeActivity.menuView.menuSendup.setText("0元起送 / ");
				menuSendup.setText("0元起送 / ");
			}
			homeActivity.menuView.menuSalesnum.setText("月售" + shopInfoItem.getSalesNum() + "份");
			menuSalesnum.setText("月售" + shopInfoItem.getSalesNum() + "份");
			
			//评分
			layout_shop_score.setText(shopInfoItem.getScore() + "");
			if( !TextUtils.isEmpty(shopInfoItem.getRate()) ) {
				layout_shop_rate.setText(Html.fromHtml(shopInfoItem.getRate()+ "<small><small>%</small></small>"));
			}
			if( !TextUtils.isEmpty(shopInfoItem.getsType()) ) {
				layout_shop_type.setText(shopInfoItem.getsType());
			}
			
			if ( shopInfoItem.getRdate()==0 ) {
				layout_shop_register.setText("注册时间：");
			} else {
				layout_shop_register.setText("注册时间：" + DateUtils.formatDate(shopInfoItem.getRdate()));
			}
			
			if( !TextUtils.isEmpty(shopInfoItem.getRegister()) ) {
				layout_shop_person.setText("注册人：" + shopInfoItem.getRegister());
			}
			if( !TextUtils.isEmpty(shopInfoItem.getrPhone()) ) {
				layout_shop_phone.setText("注册电话：" + shopInfoItem.getrPhone());
			}
			if ( !TextUtils.isEmpty(shopInfoItem.getsPhone()) ){
				layout_shop_sphone_et.setText(shopInfoItem.getsPhone());
			}
			if( !TextUtils.isEmpty(shopInfoItem.getBrief()) ) {
				layout_shop_brief_et.setText(shopInfoItem.getBrief());
			}
			if( !TextUtils.isEmpty(shopInfoItem.getDeliveryTime()) ) {
				layout_shop_bh_tv.setText(shopInfoItem.getDeliveryTime());
			}

			layout_shop_order.setText(shopInfoItem.getSalesNum()+"");
			layout_shop_customer.setText(shopInfoItem.getScoreNum()+"");
			
			List<MarkUp> upList = shopInfoItem.getMarkUpList();						  //把阶梯价存入缓存
			if ( upList!=null ) {
				WYList<MarkUp> upWyList = new WYList<MarkUp>();
				upWyList.addAll(upList);
				aCache.put("deliverfee", upWyList);     			
			}
			
			List<DeliveryTime> deliveryList = shopInfoItem.getDeliveryTimeList();     //把外送时间段存入缓存
			if ( deliveryList!=null ) {
				WYList<DeliveryTime> deliveryWyList = new WYList<DeliveryTime>();
				deliveryWyList.addAll(deliveryList);
				aCache.put("delivertime", deliveryWyList );	
			}
			
			BoxFee boxFee = new BoxFee();
			boxFee.setType(shopInfoItem.getBoxType());
			boxFee.setPrice(shopInfoItem.getBoxFee());
			aCache.put("boxfee", boxFee);
			
			fullDeliverFee(shopInfoItem.getMarkUpList());
			fullBoxFee(boxFee);
		}
			
	
	}
	
	/**
	 * 修改店铺营业状态
	 * @param event
	 */
	private void modifyShopStatus() {
		String status = Constants.shopStatus;
		if ( "S".equals(status) ) {
			menuStatus.setImageResource(R.raw.icon_menu_status_business);
		} else if ( "B".equals(status) ) {
			menuStatus.setImageResource(R.raw.icon_menu_status_busy);
		} else {
			menuStatus.setImageResource(R.raw.icon_menu_status_rest);
		}
		
	}
	
	/**
	 * 接收修改店铺营业状态的事件
	 */
	public void onEvent(EventModifyShopStatus event) {
		String status = event.getId();
		if ( "S".equals(status) ) {
			menuStatus.setImageResource(R.raw.icon_menu_status_business);
		} else if ( "B".equals(status) ) {
			menuStatus.setImageResource(R.raw.icon_menu_status_busy);
		} else {
			menuStatus.setImageResource(R.raw.icon_menu_status_rest);
		}
	}
	
	/**
	 * 评分星级
	 * @param img
	 * @param score
	 */
	private void setPraiseRate(ImageView img, float score) {
		if (score == 0) {
			img.setImageResource(R.raw.icon_menu_star0);
		} else if (score <= 1) {
			img.setImageResource(R.raw.icon_menu_star05);
		} else if (score <= 2) {
			img.setImageResource(R.raw.icon_menu_star1);
		} else if (score <= 3) {
			img.setImageResource(R.raw.icon_menu_star15);
		} else if (score <= 4) {
			img.setImageResource(R.raw.icon_menu_star2);
		} else if (score <= 5) {
			img.setImageResource(R.raw.icon_menu_star25);
		} else if (score <= 6) {
			img.setImageResource(R.raw.icon_menu_star3);
		} else if (score <= 7) {
			img.setImageResource(R.raw.icon_menu_star35);
		} else if (score <= 8) {
			img.setImageResource(R.raw.icon_menu_star4);
		} else if (score <= 9) {
			img.setImageResource(R.raw.icon_menu_star45);
		} else {
			img.setImageResource(R.raw.icon_menu_star5);
		}
	}
	
	/**
	 * 载入餐盒费
	 * @param boxFee
	 */
	private void fullBoxFee(BoxFee boxFee) {
		if ( boxFee!=null ) {
			if ( boxFee.getType()==0 ) {
				boxFeeTv.setText("不收餐盒费");
			} else if ( boxFee.getType()==1 ) {
				boxFeeTv.setText("统一加收" + FormatTools.String2Money(boxFee.getPrice()+"") + "元餐盒费");
			} else {
				boxFeeTv.setText("每份商品加收" + FormatTools.String2Money(boxFee.getPrice()+"") + "元餐盒费");
			}
		}
	}
	
	/**
	 * 载入阶梯价
	 * @param markUpList
	 */
	private void fullDeliverFee(List<MarkUp> markUpList){
		if ( markUpList!=null && markUpList.size()>0 ) {
			String info = "";		
			for (int i = 0; i < markUpList.size(); i++) {
				info = info + "<span>小于"
						+ FormatTools.String2Money(markUpList.get(i).getRange()/1000+"")
						+ "公里内，订单满￥"
						+ FormatTools.String2Money(markUpList.get(i).getSendUpFee()+"")
						+ "元送货<br>"
						+ "并加收"
						+ FormatTools.String2Money(markUpList.get(i).getDeliverFee()+"")
						+ "元外送费</span><br>";
				
			}
			layout_shop_markup_tv.setText(Html.fromHtml(info));
		}
 	}
	
	/**
	 * 刷新阶梯价
	 */
	private void refreshDeliverFee(){
		WYList<MarkUp> list = (WYList<MarkUp>) aCache.getAsObject("deliverfee");
		String info = "";
		if ( list!=null && list.size()>0 ) {
			//修改左侧抽屉的外送费
			String prompt = FormatTools.String2Money(list.get(0).getSendUpFee()+"") + "元起送 / ";
			homeActivity.menuView.menuSendup.setText(prompt);
			menuSendup.setText(prompt);
			//修改阶梯价
			for (int i = 0; i < list.size(); i++) {
				info = info + "<span>小于"
						+ FormatTools.String2Money(list.get(i).getRange()/1000+"")
						+ "公里内，订单满￥"
						+ FormatTools.String2Money(list.get(i).getSendUpFee()+"")
						+ "元送货<br>"
						+ "并加收"
						+ FormatTools.String2Money(list.get(i).getDeliverFee()+"")
						+ "元外送费</span><br>";
				
			}
			layout_shop_markup_tv.setText(Html.fromHtml(info));
		} else {
			layout_shop_markup_tv.setText("");
		}
	}
	
	/**
	 * 刷新餐盒费
	 */
	private void refreshBoxFee(){
		BoxFee boxFee = (BoxFee) aCache.getAsObject("boxfee");
		if ( boxFee!=null ) {
			if ( boxFee.getType()==0 ) {
				boxFeeTv.setText("不收餐盒费");
			} else if ( boxFee.getType()==1 ) {
				boxFeeTv.setText("统一加收" + FormatTools.String2Money(boxFee.getPrice()+"") + "元餐盒费");
			} else {
				boxFeeTv.setText("每份商品加收" + FormatTools.String2Money(boxFee.getPrice()+"") + "元餐盒费");
			}
		}
	}

	/**
	 * 刷新外卖时段
	 */
	private void refreshShopHours(){
		WYList<DeliveryTime> list = (WYList<DeliveryTime>) aCache.getAsObject("delivertime");
		
		int index = 0;
		if ( list!=null && list.size()>0 ) {
			for (int i = 0; i <list.size(); i++) {
				DeliveryTime time = list.get(i);
				if ( time.getFlag().equals("0") ) {
					index += 1;
				} 
			}
			
			if ( index>2 ) {
				// 取出一个连续外卖的时间段放到subTimeList中然后全部都封装到supTimeList中
				ArrayList<ArrayList<String>> supTimeList = new ArrayList<ArrayList<String>>();
				ArrayList<String> subTimeList = new ArrayList<String>();
				// 是否要重新new一个subTimeList
				boolean isBulid = true;
				for (int i = 0; i < list.size(); i++) {
					if (list.get(i).getFlag().equals("0")) {
						subTimeList.add(list.get(i).getContent());
						isBulid = true;
						if (list.get(i).getContent().equals("23:30~24:00")) {
							supTimeList.add(subTimeList);
						}
					} else {
						if (isBulid) {
							if (subTimeList.size() > 0) {
								supTimeList.add(subTimeList);
								subTimeList = new ArrayList<String>();
							}
						}
						isBulid = false;
					}
				}
				layout_shop_bh_tv.setText("从0:00到24:00分为" + supTimeList.size() + "段营业时间");
			} else {
				String info = "";
				for (int i = 0; i < list.size(); i++) {
					DeliveryTime time = list.get(i);
					if ( time.getFlag().equals("0") ) {
						info = info + time.getContent()+" ";
						info = info.replaceAll("~", "-");
					}
				}
				layout_shop_bh_tv.setText(info);
			}
				
		}
		
	}
	
	/**
	 * 格式化时间09:00 格式化成9:00
	 */
	public String formatTime(String time) {
		if (TypeJudgeTools.isNull(time)) {
			return "";
		}
		if (time.substring(0, 1).equals("0")) {
			time = time.substring(1);
		}
		return time;
	}
	
	/**
	 * 隐藏保存按钮
	 */
	private void hideSaveButton(){
		hideSoftInput(homeActivity, layout_shop_sphone_et);
		layout_shop_sphone_et.setEnabled(false);
		layout_shop_notice_et.setEnabled(false);
		layout_shop_brief_et.setEnabled(false);
		layout_shop_sphone_save.setVisibility(View.GONE);
		layout_shop_sphone_cancel.setVisibility(View.GONE);
		layout_shop_notice_save.setVisibility(View.GONE);
		layout_shop_notice_cancel.setVisibility(View.GONE);
		layout_shop_brief_save.setVisibility(View.GONE);
		layout_shop_brief_cancel.setVisibility(View.GONE);
		layout_shop_replymessage.setVisibility(View.INVISIBLE);
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.layout_shop_linear:
			homeActivity.homeController.hideInputMethod();
			break;
		case R.id.layout_shop_sphone_modify:			// 服务电话

			if(!homeActivity.isCanShopSetting){
				// 判断是否在5分钟内的解锁期内
				if (!homeActivity.isUnlock5Minute) {
					// 判断是否设置了老板锁
					if (aCache.getAsString("isSetPW") != null) {
						if (aCache.getAsString("isSetPW").equals("yes")) {
							// 跳转方向1表示analysisFragment
							aCache.put("jumpDirection", "5");
							homeActivity.checkBossPW();
							return;
						}
					}
				}
			}

			sPhoneNum = layout_shop_sphone_et.getText().toString();
			layout_shop_sphone_et.setEnabled(true);
			layout_shop_sphone_et.setFocusableInTouchMode(true);
			layout_shop_sphone_et.setSelection(layout_shop_sphone_et.length());
			layout_shop_sphone_modify.setVisibility(View.GONE);
			layout_shop_sphone_save.setVisibility(View.VISIBLE);
			layout_shop_sphone_cancel.setVisibility(View.VISIBLE);
			isShowModify(true);
			break;
		case R.id.layout_shop_sphone_save:
			layout_shop_sphone_et.setEnabled(false);
			layout_shop_sphone_modify.setVisibility(View.VISIBLE);
			layout_shop_sphone_save.setVisibility(View.GONE);
			layout_shop_sphone_cancel.setVisibility(View.GONE);
			isShowModify(false);
			homeActivity.hideInputMethod();
			boolean isOk = TypeJudgeTools.isPhoneNum(layout_shop_sphone_et.getText().toString());
			if (isOk) {
				updateShopInfo();
			} else {
				layout_shop_sphone_et.setText(sPhoneNum);
				SuperUI.openToast(homeActivity, "亲,请输入正确的电话号码");
			}
			break;
		case R.id.layout_shop_sphone_cancel:
			layout_shop_sphone_et.setText(sPhoneNum);
			layout_shop_sphone_et.setEnabled(false);
			layout_shop_sphone_modify.setVisibility(View.VISIBLE);
			layout_shop_sphone_save.setVisibility(View.GONE);
			layout_shop_sphone_cancel.setVisibility(View.GONE);
			isShowModify(false);
			homeActivity.hideInputMethod();
			break;
		case R.id.layout_shop_notice_modify:

			if(!homeActivity.isCanShopSetting){
				// 判断是否在5分钟内的解锁期内
				if (!homeActivity.isUnlock5Minute) {
					// 判断是否设置了老板锁
					if (aCache.getAsString("isSetPW") != null) {
						if (aCache.getAsString("isSetPW").equals("yes")) {
							// 跳转方向1表示analysisFragment
							aCache.put("jumpDirection", "5");
							homeActivity.checkBossPW();
							return;
						}
					}
				}
			}

			sShopNotice = layout_shop_notice_et.getText().toString();
			layout_shop_notice_et.setEnabled(true);
			layout_shop_notice_et.setSelection(layout_shop_notice_et.length());
			layout_shop_notice_modify.setVisibility(View.GONE);
			layout_shop_notice_save.setVisibility(View.VISIBLE);
			layout_shop_notice_cancel.setVisibility(View.VISIBLE);
			isShowModify(true);
			break;
		case R.id.layout_shop_notice_save:				// 店铺公告
			layout_shop_notice_et.setEnabled(false);
			layout_shop_notice_modify.setVisibility(View.VISIBLE);
			layout_shop_notice_save.setVisibility(View.GONE);
			layout_shop_notice_cancel.setVisibility(View.GONE);
			isShowModify(false);
			homeActivity.hideInputMethod();
			if (!TypeJudgeTools.isNull(layout_shop_notice_et.getText().toString())) {
				updateShopInfo();
			} else {
				layout_shop_notice_et.setText(sShopNotice);
				SuperUI.openToast(homeActivity, "亲,输入店铺公告内容不能为空");
			}
			break;
		case R.id.layout_shop_notice_cancel:
			layout_shop_notice_et.setText(sShopNotice);
			layout_shop_notice_et.setEnabled(false);
			layout_shop_notice_modify.setVisibility(View.VISIBLE);
			layout_shop_notice_save.setVisibility(View.GONE);
			layout_shop_notice_cancel.setVisibility(View.GONE);
			isShowModify(false);
			homeActivity.hideInputMethod();
			break;
		case R.id.layout_shop_markup_modify:			// 外送加价
			if(!homeActivity.isCanShopSetting){
				// 判断是否在5分钟内的解锁期内
				if (!homeActivity.isUnlock5Minute) {
					// 判断是否设置了老板锁
					if (aCache.getAsString("isSetPW") != null) {
						if (aCache.getAsString("isSetPW").equals("yes")) {
							// 跳转方向1表示analysisFragment
							aCache.put("jumpDirection", "7");
							homeActivity.checkBossPW();
							return;
						}
					}
				}
			}
			jumpToDeliverFee();
			break;
		case R.id.layout_shop_bh_modify:				// 外卖时段
			if(!homeActivity.isCanShopSetting){
				// 判断是否在5分钟内的解锁期内
				if (!homeActivity.isUnlock5Minute) {
					// 判断是否设置了老板锁
					if (aCache.getAsString("isSetPW") != null) {
						if (aCache.getAsString("isSetPW").equals("yes")) {
							// 跳转方向1表示analysisFragment
							aCache.put("jumpDirection", "9");
							homeActivity.checkBossPW();
							return;
						}
					}
				}
			}
			jumpToShopHours();
			break;
		case R.id.layout_shop_box_modify:				// 餐盒费
			if(!homeActivity.isCanShopSetting){
				// 判断是否在5分钟内的解锁期内
				if (!homeActivity.isUnlock5Minute) {
					// 判断是否设置了老板锁
					if (aCache.getAsString("isSetPW") != null) {
						if (aCache.getAsString("isSetPW").equals("yes")) {
							// 跳转方向1表示analysisFragment
							aCache.put("jumpDirection", "8");
							homeActivity.checkBossPW();
							return;
						}
					}
				}
			}
			jumpToBoxFee();
			break;
		case R.id.layout_shop_brief_modify:				// 店铺简介
			if(!homeActivity.isCanShopSetting){
				// 判断是否在5分钟内的解锁期内
				if (!homeActivity.isUnlock5Minute) {
					// 判断是否设置了老板锁
					if (aCache.getAsString("isSetPW") != null) {
						if (aCache.getAsString("isSetPW").equals("yes")) {
							// 跳转方向1表示analysisFragment
							aCache.put("jumpDirection", "5");
							homeActivity.checkBossPW();
							return;
						}
					}
				}
			}
			sShopInfo = layout_shop_brief_et.getText().toString();
			layout_shop_brief_et.setEnabled(true);
			layout_shop_replymessage.setVisibility(View.VISIBLE);
			layout_shop_brief_et.setSelection(layout_shop_brief_et.length());
			layout_shop_brief_modify.setVisibility(View.GONE);
			layout_shop_brief_save.setVisibility(View.VISIBLE);
			layout_shop_brief_cancel.setVisibility(View.VISIBLE);
			isShowModify(true);
			break;
		case R.id.layout_shop_brief_save:
			layout_shop_brief_et.setEnabled(false);
			layout_shop_replymessage.setVisibility(View.INVISIBLE);
			layout_shop_brief_modify.setVisibility(View.VISIBLE);
			layout_shop_brief_save.setVisibility(View.GONE);
			layout_shop_brief_cancel.setVisibility(View.GONE);
			isShowModify(false);
			homeActivity.hideInputMethod();
			if (!TypeJudgeTools.isNull(layout_shop_brief_et.getText().toString())) {
				updateShopInfo();
			} else {
				layout_shop_brief_et.setText(sShopInfo);
				SuperUI.openToast(homeActivity, "亲,输入店铺简介内容不能为空");
			}
			break;
		case R.id.layout_shop_brief_cancel:
			layout_shop_brief_et.setText(sShopInfo);
			layout_shop_brief_et.setEnabled(false);
			layout_shop_replymessage.setVisibility(View.INVISIBLE);
			layout_shop_brief_modify.setVisibility(View.VISIBLE);
			layout_shop_brief_save.setVisibility(View.GONE);
			layout_shop_brief_cancel.setVisibility(View.GONE);
			isShowModify(false);
			homeActivity.hideInputMethod();
			break;
		}
	}

	/**
	 * 跳转到外送费设置页面
	 */
	public void jumpToDeliverFee() {
		homeActivity.openSubFM(this, new FmInfo(DeliveryFeeFragment.class, "起送价&外送费", true));
	}

	/**
	 * 跳转到外送时间设置页面
	 */
	public void jumpToShopHours() {
		homeActivity.openSubFM(this, new FmInfo(ShopHoursFragment.class, "外卖时段", true));
	}
	
	/**
	 * 跳转到餐盒费页面
	 */
	public void jumpToBoxFee() {
		homeActivity.openSubFM(this, new FmInfo(BoxFeeFragment.class, "餐盒费", true));
	}

	/**
	 * 更新店铺信息
	 */
	private void updateShopInfo(){
		ThreadPoolManager.getInstance().executeTask(new Runnable() {
			
			@Override
			public void run() {
				try {
					homeActivity.showLoadingHint();
					UpdateShopInfoReq updateShopInfoReq = new UpdateShopInfoReq();
					final String notice = layout_shop_notice_et.getText().toString();    //店铺公告
					String brief  = layout_shop_brief_et.getText().toString();	   		 //店铺简介	
					String phone  = layout_shop_sphone_et.getText().toString();	   		 //店铺服务电话	
					UserInfo userInfo = LoginUtils.getUserInfo(homeActivity);
					if ( userInfo!=null ){
						updateShopInfoReq.setsId(userInfo.getsId());
						updateShopInfoReq.setPwd(userInfo.getPwd());
					}
					updateShopInfoReq.setNotice(TypeJudgeTools.replaceBlank(notice));
					updateShopInfoReq.setBrief(TypeJudgeTools.replaceBlank(brief));
					updateShopInfoReq.setPhone(TypeJudgeTools.replaceBlank(phone));
					
					final Result result = mShopController.updateShopInfo(updateShopInfoReq);
					if ( result!=null && result.code==1 ) {
						homeActivity.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								SuperUI.openToast(homeActivity, result.msg);
								UserLoginRes obj = (UserLoginRes) aCache.getAsObject("loginUserInfo");
								obj.setNotice(notice);
								Log.i("loginUserInfo", obj.toString());
								// 存储用户登录信息
								aCache.put("loginUserInfo", obj);
							}
						});
					} else if( result != null && result.code==-3 ) {
						homeActivity.homeController.exitLogin();
					} else {
						homeActivity.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								SuperUI.openToast(homeActivity, result.msg);
							}
						});
					}
				} catch (RetrofitError e) {
					switch (e.getKind()) {
					case CONVERSION:
						break;
					case HTTP:
						break;
					case NETWORK:
						break;
					case UNEXPECTED:
						break;
					}
				} finally {
					homeActivity.hideLoadingHint();
				}
			}
		});
	}
	
	TextWatcher mTextWatcher = new TextWatcher() {
		private String temp;
		private boolean flag = false;

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			if (flag) {
				return;
			}
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			if (flag) {
				return;
			}
			temp = s.toString();
		}

		@Override
		public void afterTextChanged(Editable s) {
			if (flag) {
				flag = false;
				return;
			}
			if (layout_shop_brief_et.getText().toString().length() <= 200) {
				layout_shop_replymessage.setText("您还可以输入"+ (200 - layout_shop_brief_et.getText().toString().length()) + "个字");
			} else {
				flag = true;
				layout_shop_brief_et.setText(temp);
				layout_shop_brief_et.setSelection(temp.length());

			}
		}
	};

	/**
	 * 隐藏或者显示修改按钮
	 * 
	 * @param isModify
	 */
	public void isShowModify(boolean isModify) {
		if (isModify) {
			layout_shop_sphone_modify.setVisibility(View.GONE);
			layout_shop_notice_modify.setVisibility(View.GONE);
			layout_shop_markup_modify.setVisibility(View.GONE);
			layout_shop_bh_modify.setVisibility(View.GONE);
			layout_shop_brief_modify.setVisibility(View.GONE);
			modifyBoxFee.setVisibility(View.INVISIBLE);      
		} else {
			layout_shop_sphone_modify.setVisibility(View.VISIBLE);
			layout_shop_notice_modify.setVisibility(View.VISIBLE);
			layout_shop_markup_modify.setVisibility(View.VISIBLE);
			layout_shop_bh_modify.setVisibility(View.VISIBLE);
			layout_shop_brief_modify.setVisibility(View.VISIBLE);
			modifyBoxFee.setVisibility(View.VISIBLE);
		}
	}
	
}
