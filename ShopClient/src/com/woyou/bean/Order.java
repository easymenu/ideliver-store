package com.woyou.bean;


/**
 * 订单
 * 
 * @author 荣
 * 
 */
public class Order extends SuperModel {
	/**
	 * 订单ID
	 */
	private String oId = "";
	/**
	 * 短编号
	 */
	private String shortNo = "";
	/**
	 * 联系人
	 */
	private String contact = "";
	/**
	 * 联系电话
	 */
	private String phone = "";
	/**
	 * 订单金额
	 */
	private float price;
	/**
	 * 商品概要
	 */
	private String goodsSummary = "";
	/**
	 * 送餐地址
	 */
	private String addrName = "";
	/**
	 * 是否预约单 1：及时单，2：预约单 
	 */
	private int isReserve;
	/**
	 * 送餐时间
	 */
	private long reserveTime;
	/**
	 * 下单时间（unix时间戳）
	 */
	private long orderTime;
	/**
	 * 订单状态（1:新订单；2:推送成功；3:未接通；4:接通后未应答；5:拒绝；6:返单(需要返回暂缺商品)；7:已确认；8:已取消；9:已送出,
	 * 10.已送达）
	 */
	private String status = "";

	public String getoId() {
		return oId;
	}

	public void setoId(String oId) {
		this.oId = oId;
	}

	public String getShortNo() {
		return shortNo;
	}

	public void setShortNo(String shortNo) {
		this.shortNo = shortNo;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public String getGoodsSummary() {
		return goodsSummary;
	}

	public void setGoodsSummary(String goodsSummary) {
		this.goodsSummary = goodsSummary;
	}

	public String getAddrName() {
		return addrName;
	}

	public void setAddrName(String addrName) {
		this.addrName = addrName;
	}

	public int getIsReserve() {
		return isReserve;
	}

	public void setIsReserve(int isReserve) {
		this.isReserve = isReserve;
	}

	public long getReserveTime() {
		return reserveTime;
	}

	public void setReserveTime(long reserveTime) {
		this.reserveTime = reserveTime;
	}

	public long getOrderTime() {
		return orderTime;
	}

	public void setOrderTime(long orderTime) {
		this.orderTime = orderTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
