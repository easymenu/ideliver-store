package com.woyou.bean.rpc;

/**
 * 用户排名报表的请求bean
 * @author zhou.ni
 *
 */
public class QueryUserReportListReq {
	
	private String sId;			//店铺Id
	private String pwd;			//密码
	private String days;		//天数，取值（30，90，180，360），时间段选择
	private String sortType;	//排序，1-按订单，2-按金额。默认为1按订单排序	
	private int page;			//分页
	
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public String getsId() {
		return sId;
	}
	public void setsId(String sId) {
		this.sId = sId;
	}
	public String getDays() {
		return days;
	}
	public void setDays(String days) {
		this.days = days;
	}
	public String getSortType() {
		return sortType;
	}
	public void setSortType(String sortType) {
		this.sortType = sortType;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	
	
}
