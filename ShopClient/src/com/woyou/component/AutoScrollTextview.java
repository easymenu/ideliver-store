package com.woyou.component;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.TextView;

/**
 * 
 * TODO 单行文本跑马灯控件
 *
 * @author tianlu
 * @version 1.0
 */
public class AutoScrollTextview extends TextView implements OnClickListener {
    public final static String TAG = AutoScrollTextview.class.getSimpleName();
    
    private float textLength = 0f;//文本长度
    private float step = 0f;//文字的横坐标
    private float y = 0f;//文字的纵坐标
    private float temp_view_plus_text_length = 0.0f;//用于计算的临时变量
    private float temp_view_plus_two_text_length = 0.0f;//用于计算的临时变量
    public boolean isStarting = false;//是否开始滚动
    private Paint paint = null;//绘图样式
    private String text = "";//文本内容
    private int width;//控件宽度
    private int height;//控件高度

    
    public AutoScrollTextview(Context context) {
        super(context);
        initView();
    }

    public AutoScrollTextview(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public AutoScrollTextview(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initView();
    }
    
    /**
     * 初始化控件
     */
    private void initView()
    {
        setOnClickListener(this);
        paint = getPaint();
    }
    
    /**
     * 获取空间宽度
     */
    public int getTextViewWidth(AutoScrollTextview textview)
    {
    	 int w = View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED);
         int h = View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED);
         textview.measure(w, h);
         width =textview.getMeasuredWidth();
         return width;
    }
    
    /**
     * 文本初始化，每次更改文本内容或者文本效果等之后都需要重新初始化一下
     */
    public float init()
    {
    	text = getText().toString();
        textLength = paint.measureText(text);
        step = textLength;
        temp_view_plus_text_length =  textLength;
        temp_view_plus_two_text_length = (float) (textLength * 1.9);
        y = getTextSize() + getPaddingTop();
        return textLength;
    }
    
  //dp转换为像素值
  	public int dip2px(Context context, float dpValue) {
          final float scale = context.getResources().getDisplayMetrics().density;
          return (int) (dpValue * scale + 0.5f);
  	}
    
    @Override
    public Parcelable onSaveInstanceState()
    {
        Parcelable superState = super.onSaveInstanceState();
        SavedState ss = new SavedState(superState);
        
        ss.step = step;
        ss.isStarting = isStarting;
        
        return ss;
        
    }
    
    @Override
    public void onRestoreInstanceState(Parcelable state)
    {
        if (!(state instanceof SavedState)) {
            super.onRestoreInstanceState(state);
            return;
        }
        SavedState ss = (SavedState)state;
        super.onRestoreInstanceState(ss.getSuperState());
        
        step = ss.step;
        isStarting = ss.isStarting;

    }
    
    public static class SavedState extends BaseSavedState {
        public boolean isStarting = false;
        public float step = 0.0f;
        SavedState(Parcelable superState) {
            super(superState);
        }

        @Override
        public void writeToParcel(Parcel out, int flags) {
            super.writeToParcel(out, flags);
            out.writeBooleanArray(new boolean[]{isStarting});
            out.writeFloat(step);
        }


        public static final Parcelable.Creator<SavedState> CREATOR
                = new Parcelable.Creator<SavedState>() {
            
            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }

            @Override
            public SavedState createFromParcel(Parcel in) {
                return new SavedState(in);
            }
        };

        private SavedState(Parcel in) {
            super(in);
            boolean[] b = null;
            in.readBooleanArray(b);
            if(b != null && b.length > 0)
                isStarting = b[0];
            step = in.readFloat();
        }
    }

    /**
     * 开始滚动
     */
    public void startScroll()
    {
        isStarting = true;
        invalidate();
    }
    
    /**
     * 停止滚动
     */
    public void stopScroll()
    {
        isStarting = false;
        invalidate();
    }

    @Override
    public void onDraw(Canvas canvas) {
        canvas.drawText(text, temp_view_plus_text_length - step, y, paint);
        if(!isStarting)
        {
            return;
        }
        step += 1;
        if(step > temp_view_plus_two_text_length)
            step = textLength;
        invalidate();

    }

    @Override
    public void onClick(View v) {
        if(isStarting)
            stopScroll();
        else
            startScroll();
        
    }

}
