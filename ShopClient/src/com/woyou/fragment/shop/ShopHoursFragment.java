package com.woyou.fragment.shop;

import java.util.ArrayList;
import java.util.List;

import com.jing.biandang.R;
import com.woyou.activity.HomeActivity;
import com.woyou.bean.DeliveryTime;
import com.woyou.bean.Time;
import com.woyou.bean.UserInfo;
import com.woyou.bean.WYList;
import com.woyou.bean.rpc.Result;
import com.woyou.bean.rpc.UpdateDeliveryTimesReq;
import com.woyou.component.SuperUI;
import com.woyou.fragment.SuperFragment;
import com.woyou.model.rpc.ShopModel;
import com.woyou.utils.LoginUtils;
import com.woyou.utils.ThreadPoolManager;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import retrofit.RetrofitError;

/**
 * 外卖时段碎片
 * @author zhou.ni
 *
 */
public class ShopHoursFragment extends SuperFragment implements OnClickListener,OnCheckedChangeListener{
	private HomeActivity homeActivity;
	private View layout_shophours;
	//复选框
	private CheckBox shophours_right0;
	private CheckBox shophours_left0;
	private CheckBox shophours_right1;
	private CheckBox shophours_left1;
	private CheckBox shophours_right2;
	private CheckBox shophours_left2;
	private CheckBox shophours_right3;
	private CheckBox shophours_left3;
	private CheckBox shophours_right4;
	private CheckBox shophours_left4;
	private CheckBox shophours_right5;
	private CheckBox shophours_left5;
	private CheckBox shophours_right6;
	private CheckBox shophours_left6;
	private CheckBox shophours_right7;
	private CheckBox shophours_left7;
	private CheckBox shophours_right8;
	private CheckBox shophours_left8;
	private CheckBox shophours_right9;
	private CheckBox shophours_left9;
	private CheckBox shophours_right10;
	private CheckBox shophours_left10;
	private CheckBox shophours_right11;
	private CheckBox shophours_left11;
	private CheckBox shophours_right12;
	private CheckBox shophours_left12;
	private CheckBox shophours_right13;
	private CheckBox shophours_left13;
	private CheckBox shophours_right14;
	private CheckBox shophours_left14;
	private CheckBox shophours_right15;
	private CheckBox shophours_left15;
	private CheckBox shophours_right16;
	private CheckBox shophours_left16;
	private CheckBox shophours_right17;
	private CheckBox shophours_left17;
	private CheckBox shophours_right18;
	private CheckBox shophours_left18;
	private CheckBox shophours_right19;
	private CheckBox shophours_left19;
	private CheckBox shophours_right20;
	private CheckBox shophours_left20;
	private CheckBox shophours_right21;
	private CheckBox shophours_left21;
	private CheckBox shophours_right22;
	private CheckBox shophours_left22;
	private CheckBox shophours_right23;
	private CheckBox shophours_left23;

	//操作面板
	private LinearLayout layout_shophours_panel;
	private RelativeLayout layout_shophours_cancel;
	private RelativeLayout layout_shophours_submit;
	
	private List<CheckBox> checkBoxList = new ArrayList<CheckBox>();   //装载所有Checkbox的列表
	private List<Boolean> statusList = new ArrayList<Boolean>();  	   //保存原始的CheckBox的类型
	private WYList<DeliveryTime> deliveryTimeList;					   //外卖时段列表
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		layout_shophours = inflater.inflate(R.layout.layout_shophours, container, false);
		homeActivity=(HomeActivity)getActivity();
		return layout_shophours;
	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		initView();
		fullDeliverTime();
	}
	/**
	 * 初始化页面
	 */
	public void initView(){
		//复选框
		shophours_left0=(CheckBox)layout_shophours.findViewById(R.id.shophours_left0);
		shophours_right0=(CheckBox)layout_shophours.findViewById(R.id.shophours_right0);
		shophours_left1=(CheckBox)layout_shophours.findViewById(R.id.shophours_left1);
		shophours_right1=(CheckBox)layout_shophours.findViewById(R.id.shophours_right1);
		shophours_left2=(CheckBox)layout_shophours.findViewById(R.id.shophours_left2);
		shophours_right2=(CheckBox)layout_shophours.findViewById(R.id.shophours_right2);
		shophours_left3=(CheckBox)layout_shophours.findViewById(R.id.shophours_left3);
		shophours_right3=(CheckBox)layout_shophours.findViewById(R.id.shophours_right3);
		shophours_left4=(CheckBox)layout_shophours.findViewById(R.id.shophours_left4);
		shophours_right4=(CheckBox)layout_shophours.findViewById(R.id.shophours_right4);
		shophours_left5=(CheckBox)layout_shophours.findViewById(R.id.shophours_left5);
		shophours_right5=(CheckBox)layout_shophours.findViewById(R.id.shophours_right5);
		shophours_left6=(CheckBox)layout_shophours.findViewById(R.id.shophours_left6);
		shophours_right6=(CheckBox)layout_shophours.findViewById(R.id.shophours_right6);
		shophours_left7=(CheckBox)layout_shophours.findViewById(R.id.shophours_left7);
		shophours_right7=(CheckBox)layout_shophours.findViewById(R.id.shophours_right7);
		shophours_left8=(CheckBox)layout_shophours.findViewById(R.id.shophours_left8);
		shophours_right8=(CheckBox)layout_shophours.findViewById(R.id.shophours_right8);
		shophours_left9=(CheckBox)layout_shophours.findViewById(R.id.shophours_left9);
		shophours_right9=(CheckBox)layout_shophours.findViewById(R.id.shophours_right9);
		shophours_left10=(CheckBox)layout_shophours.findViewById(R.id.shophours_left10);
		shophours_right10=(CheckBox)layout_shophours.findViewById(R.id.shophours_right10);
		shophours_left11=(CheckBox)layout_shophours.findViewById(R.id.shophours_left11);
		shophours_right11=(CheckBox)layout_shophours.findViewById(R.id.shophours_right11);
		shophours_left12=(CheckBox)layout_shophours.findViewById(R.id.shophours_left12);
		shophours_right12=(CheckBox)layout_shophours.findViewById(R.id.shophours_right12);
		shophours_left13=(CheckBox)layout_shophours.findViewById(R.id.shophours_left13);
		shophours_right13=(CheckBox)layout_shophours.findViewById(R.id.shophours_right13);
		shophours_left14=(CheckBox)layout_shophours.findViewById(R.id.shophours_left14);
		shophours_right14=(CheckBox)layout_shophours.findViewById(R.id.shophours_right14);
		shophours_left15=(CheckBox)layout_shophours.findViewById(R.id.shophours_left15);
		shophours_right15=(CheckBox)layout_shophours.findViewById(R.id.shophours_right15);
		shophours_left16=(CheckBox)layout_shophours.findViewById(R.id.shophours_left16);
		shophours_right16=(CheckBox)layout_shophours.findViewById(R.id.shophours_right16);
		shophours_left17=(CheckBox)layout_shophours.findViewById(R.id.shophours_left17);
		shophours_right17=(CheckBox)layout_shophours.findViewById(R.id.shophours_right17);
		shophours_left18=(CheckBox)layout_shophours.findViewById(R.id.shophours_left18);
		shophours_right18=(CheckBox)layout_shophours.findViewById(R.id.shophours_right18);
		shophours_left19=(CheckBox)layout_shophours.findViewById(R.id.shophours_left19);
		shophours_right19=(CheckBox)layout_shophours.findViewById(R.id.shophours_right19);
		shophours_left20=(CheckBox)layout_shophours.findViewById(R.id.shophours_left20);
		shophours_right20=(CheckBox)layout_shophours.findViewById(R.id.shophours_right20);
		shophours_left21=(CheckBox)layout_shophours.findViewById(R.id.shophours_left21);
		shophours_right21=(CheckBox)layout_shophours.findViewById(R.id.shophours_right21);
		shophours_left22=(CheckBox)layout_shophours.findViewById(R.id.shophours_left22);
		shophours_right22=(CheckBox)layout_shophours.findViewById(R.id.shophours_right22);
		shophours_left23=(CheckBox)layout_shophours.findViewById(R.id.shophours_left23);
		shophours_right23=(CheckBox)layout_shophours.findViewById(R.id.shophours_right23);
		
		//控制面板
		layout_shophours_panel=(LinearLayout)layout_shophours.findViewById(R.id.layout_shophours_panel);
		layout_shophours_cancel=(RelativeLayout)layout_shophours.findViewById(R.id.layout_shophours_cancel);
		layout_shophours_submit=(RelativeLayout)layout_shophours.findViewById(R.id.layout_shophours_submit);
		layout_shophours_cancel.setOnClickListener(this);
		layout_shophours_submit.setOnClickListener(this);
		
		//将所有的Checkbox添加到List中
		checkBoxList.add(shophours_left0);
		checkBoxList.add(shophours_right0);
		checkBoxList.add(shophours_left1);
		checkBoxList.add(shophours_right1);
		checkBoxList.add(shophours_left2);
		checkBoxList.add(shophours_right2);
		checkBoxList.add(shophours_left3);
		checkBoxList.add(shophours_right3);
		checkBoxList.add(shophours_left4);
		checkBoxList.add(shophours_right4);
		checkBoxList.add(shophours_left5);
		checkBoxList.add(shophours_right5);
		checkBoxList.add(shophours_left6);
		checkBoxList.add(shophours_right6);
		checkBoxList.add(shophours_left7);
		checkBoxList.add(shophours_right7);
		checkBoxList.add(shophours_left8);
		checkBoxList.add(shophours_right8);
		checkBoxList.add(shophours_left9);
		checkBoxList.add(shophours_right9);
		checkBoxList.add(shophours_left10);
		checkBoxList.add(shophours_right10);
		checkBoxList.add(shophours_left11);
		checkBoxList.add(shophours_right11);
		checkBoxList.add(shophours_left12);
		checkBoxList.add(shophours_right12);
		checkBoxList.add(shophours_left13);
		checkBoxList.add(shophours_right13);
		checkBoxList.add(shophours_left14);
		checkBoxList.add(shophours_right14);
		checkBoxList.add(shophours_left15);
		checkBoxList.add(shophours_right15);
		checkBoxList.add(shophours_left16);
		checkBoxList.add(shophours_right16);
		checkBoxList.add(shophours_left17);
		checkBoxList.add(shophours_right17);
		checkBoxList.add(shophours_left18);
		checkBoxList.add(shophours_right18);
		checkBoxList.add(shophours_left19);
		checkBoxList.add(shophours_right19);
		checkBoxList.add(shophours_left20);
		checkBoxList.add(shophours_right20);
		checkBoxList.add(shophours_left21);
		checkBoxList.add(shophours_right21);
		checkBoxList.add(shophours_left22);
		checkBoxList.add(shophours_right22);
		checkBoxList.add(shophours_left23);
		checkBoxList.add(shophours_right23);
	}
	
	@Override
	public void onHiddenChanged(boolean hidden) {
		super.onHiddenChanged(hidden);
		if (!hidden) {
			fullDeliverTime();
		}
	}
	
	/**
	 * 填充外卖时段数据
	 */
	private void fullDeliverTime() {
		deliveryTimeList = (WYList<DeliveryTime>) aCache.getAsObject("delivertime");
		if ( deliveryTimeList!=null && deliveryTimeList.size()>0 ) {
			initCheckBox();			//初始化CheckBox的状态
			storeOriginalData();    //保存Checkbox的原始状态
		}
	}
	
	/**
	 * 保存原始数据
	 */
	private void storeOriginalData() {
		statusList.clear();
		if (deliveryTimeList != null && deliveryTimeList.size() > 0) {
			for (int i = 0; i < deliveryTimeList.size(); i++) {
				if (deliveryTimeList.get(i).getContent()
						.equals("00:00~00:30")) {
					if (checkBoxList.get(i).isChecked()) {
						statusList.add(true);
					} else {
						statusList.add(false);
					}
				} else if (deliveryTimeList.get(i).getContent()
						.equals("00:30~01:00")) {
					if (checkBoxList.get(i).isChecked()) {
						statusList.add(true);
					} else {
						statusList.add(false);
					}
				} else if (deliveryTimeList.get(i).getContent()
						.equals("01:00~01:30")) {
					if (checkBoxList.get(i).isChecked()) {
						statusList.add(true);
					} else {
						statusList.add(false);
					}
				} else if (deliveryTimeList.get(i).getContent()
						.equals("01:30~02:00")) {
					if (checkBoxList.get(i).isChecked()) {
						statusList.add(true);
					} else {
						statusList.add(false);
					}
				} else if (deliveryTimeList.get(i).getContent()
						.equals("02:00~02:30")) {
					if (checkBoxList.get(i).isChecked()) {
						statusList.add(true);
					} else {
						statusList.add(false);
					}
				} else if (deliveryTimeList.get(i).getContent()
						.equals("02:30~03:00")) {
					if (checkBoxList.get(i).isChecked()) {
						statusList.add(true);
					} else {
						statusList.add(false);
					}
				} else if (deliveryTimeList.get(i).getContent()
						.equals("03:00~03:30")) {
					if (checkBoxList.get(i).isChecked()) {
						statusList.add(true);
					} else {
						statusList.add(false);
					}
				} else if (deliveryTimeList.get(i).getContent()
						.equals("03:30~04:00")) {
					if (checkBoxList.get(i).isChecked()) {
						statusList.add(true);
					} else {
						statusList.add(false);
					}
				} else if (deliveryTimeList.get(i).getContent()
						.equals("04:00~04:30")) {
					if (checkBoxList.get(i).isChecked()) {
						statusList.add(true);
					} else {
						statusList.add(false);
					}
				} else if (deliveryTimeList.get(i).getContent()
						.equals("04:30~05:00")) {
					if (checkBoxList.get(i).isChecked()) {
						statusList.add(true);
					} else {
						statusList.add(false);
					}
				} else if (deliveryTimeList.get(i).getContent()
						.equals("05:00~05:30")) {
					if (checkBoxList.get(i).isChecked()) {
						statusList.add(true);
					} else {
						statusList.add(false);
					}
				} else if (deliveryTimeList.get(i).getContent()
						.equals("05:30~06:00")) {
					if (checkBoxList.get(i).isChecked()) {
						statusList.add(true);
					} else {
						statusList.add(false);
					}
				} else if (deliveryTimeList.get(i).getContent()
						.equals("06:00~06:30")) {
					if (checkBoxList.get(i).isChecked()) {
						statusList.add(true);
					} else {
						statusList.add(false);
					}
				} else if (deliveryTimeList.get(i).getContent()
						.equals("06:30~07:00")) {
					if (checkBoxList.get(i).isChecked()) {
						statusList.add(true);
					} else {
						statusList.add(false);
					}
				} else if (deliveryTimeList.get(i).getContent()
						.equals("07:00~07:30")) {
					if (checkBoxList.get(i).isChecked()) {
						statusList.add(true);
					} else {
						statusList.add(false);
					}
				} else if (deliveryTimeList.get(i).getContent()
						.equals("07:30~08:00")) {
					if (checkBoxList.get(i).isChecked()) {
						statusList.add(true);
					} else {
						statusList.add(false);
					}
				} else if (deliveryTimeList.get(i).getContent()
						.equals("08:00~08:30")) {
					if (checkBoxList.get(i).isChecked()) {
						statusList.add(true);
					} else {
						statusList.add(false);
					}
				} else if (deliveryTimeList.get(i).getContent()
						.equals("08:30~09:00")) {
					if (checkBoxList.get(i).isChecked()) {
						statusList.add(true);
					} else {
						statusList.add(false);
					}
				} else if (deliveryTimeList.get(i).getContent()
						.equals("09:00~09:30")) {
					if (checkBoxList.get(i).isChecked()) {
						statusList.add(true);
					} else {
						statusList.add(false);
					}
				} else if (deliveryTimeList.get(i).getContent()
						.equals("09:30~10:00")) {
					if (checkBoxList.get(i).isChecked()) {
						statusList.add(true);
					} else {
						statusList.add(false);
					}
				} else if (deliveryTimeList.get(i).getContent()
						.equals("10:00~10:30")) {
					if (checkBoxList.get(i).isChecked()) {
						statusList.add(true);
					} else {
						statusList.add(false);
					}
				} else if (deliveryTimeList.get(i).getContent()
						.equals("10:30~11:00")) {
					if (checkBoxList.get(i).isChecked()) {
						statusList.add(true);
					} else {
						statusList.add(false);
					}
				} else if (deliveryTimeList.get(i).getContent()
						.equals("11:00~11:30")) {
					if (checkBoxList.get(i).isChecked()) {
						statusList.add(true);
					} else {
						statusList.add(false);
					}
				} else if (deliveryTimeList.get(i).getContent()
						.equals("11:30~12:00")) {
					if (checkBoxList.get(i).isChecked()) {
						statusList.add(true);
					} else {
						statusList.add(false);
					}
				} else if (deliveryTimeList.get(i).getContent()
						.equals("12:00~12:30")) {
					if (checkBoxList.get(i).isChecked()) {
						statusList.add(true);
					} else {
						statusList.add(false);
					}
				} else if (deliveryTimeList.get(i).getContent()
						.equals("12:30~13:00")) {
					if (checkBoxList.get(i).isChecked()) {
						statusList.add(true);
					} else {
						statusList.add(false);
					}
				} else if (deliveryTimeList.get(i).getContent()
						.equals("13:00~13:30")) {
					if (checkBoxList.get(i).isChecked()) {
						statusList.add(true);
					} else {
						statusList.add(false);
					}
				} else if (deliveryTimeList.get(i).getContent()
						.equals("13:30~14:00")) {
					if (checkBoxList.get(i).isChecked()) {
						statusList.add(true);
					} else {
						statusList.add(false);
					}
				} else if (deliveryTimeList.get(i).getContent()
						.equals("14:00~14:30")) {
					if (checkBoxList.get(i).isChecked()) {
						statusList.add(true);
					} else {
						statusList.add(false);
					}
				} else if (deliveryTimeList.get(i).getContent()
						.equals("14:30~15:00")) {
					if (checkBoxList.get(i).isChecked()) {
						statusList.add(true);
					} else {
						statusList.add(false);
					}
				} else if (deliveryTimeList.get(i).getContent()
						.equals("15:00~15:30")) {
					if (checkBoxList.get(i).isChecked()) {
						statusList.add(true);
					} else {
						statusList.add(false);
					}
				} else if (deliveryTimeList.get(i).getContent()
						.equals("15:30~16:00")) {
					if (checkBoxList.get(i).isChecked()) {
						statusList.add(true);
					} else {
						statusList.add(false);
					}
				} else if (deliveryTimeList.get(i).getContent()
						.equals("16:00~16:30")) {
					if (checkBoxList.get(i).isChecked()) {
						statusList.add(true);
					} else {
						statusList.add(false);
					}
				} else if (deliveryTimeList.get(i).getContent()
						.equals("16:30~17:00")) {
					if (checkBoxList.get(i).isChecked()) {
						statusList.add(true);
					} else {
						statusList.add(false);
					}
				} else if (deliveryTimeList.get(i).getContent()
						.equals("17:00~17:30")) {
					if (checkBoxList.get(i).isChecked()) {
						statusList.add(true);
					} else {
						statusList.add(false);
					}
				} else if (deliveryTimeList.get(i).getContent()
						.equals("17:30~18:00")) {
					if (checkBoxList.get(i).isChecked()) {
						statusList.add(true);
					} else {
						statusList.add(false);
					}
				} else if (deliveryTimeList.get(i).getContent()
						.equals("18:00~18:30")) {
					if (checkBoxList.get(i).isChecked()) {
						statusList.add(true);
					} else {
						statusList.add(false);
					}
				} else if (deliveryTimeList.get(i).getContent()
						.equals("18:30~19:00")) {
					if (checkBoxList.get(i).isChecked()) {
						statusList.add(true);
					} else {
						statusList.add(false);
					}
				} else if (deliveryTimeList.get(i).getContent()
						.equals("19:00~19:30")) {
					if (checkBoxList.get(i).isChecked()) {
						statusList.add(true);
					} else {
						statusList.add(false);
					}
				} else if (deliveryTimeList.get(i).getContent()
						.equals("19:30~20:00")) {
					if (checkBoxList.get(i).isChecked()) {
						statusList.add(true);
					} else {
						statusList.add(false);
					}
				} else if (deliveryTimeList.get(i).getContent()
						.equals("20:00~20:30")) {
					if (checkBoxList.get(i).isChecked()) {
						statusList.add(true);
					} else {
						statusList.add(false);
					}
				} else if (deliveryTimeList.get(i).getContent()
						.equals("20:30~21:00")) {
					if (checkBoxList.get(i).isChecked()) {
						statusList.add(true);
					} else {
						statusList.add(false);
					}
				} else if (deliveryTimeList.get(i).getContent()
						.equals("21:00~21:30")) {
					if (checkBoxList.get(i).isChecked()) {
						statusList.add(true);
					} else {
						statusList.add(false);
					}
				} else if (deliveryTimeList.get(i).getContent()
						.equals("21:30~22:00")) {
					if (checkBoxList.get(i).isChecked()) {
						statusList.add(true);
					} else {
						statusList.add(false);
					}
				} else if (deliveryTimeList.get(i).getContent()
						.equals("22:00~22:30")) {
					if (checkBoxList.get(i).isChecked()) {
						statusList.add(true);
					} else {
						statusList.add(false);
					}
				} else if (deliveryTimeList.get(i).getContent()
						.equals("22:30~23:00")) {
					if (checkBoxList.get(i).isChecked()) {
						statusList.add(true);
					} else {
						statusList.add(false);
					}
				} else if (deliveryTimeList.get(i).getContent()
						.equals("23:00~23:30")) {
					if (checkBoxList.get(i).isChecked()) {
						statusList.add(true);
					} else {
						statusList.add(false);
					}
				} else {
					if (checkBoxList.get(i).isChecked()) {
						statusList.add(true);
					} else {
						statusList.add(false);
					}
				}
			}
		}
	}
	
	private List<Time> timeList = new ArrayList<Time>();
	private WYList<DeliveryTime> saveWyList = new WYList<DeliveryTime>();
	/**
	 * 获取更新所有的Checkbox的状态并重新更新列表
	 */
	private void getUpdateState(){
		timeList.clear();
		saveWyList.clear();
		
		if(deliveryTimeList!=null && deliveryTimeList.size()>0){
			
			for(int i=0;i<deliveryTimeList.size();i++){
				DeliveryTime deliveryTime = deliveryTimeList.get(i);
				
				if(deliveryTimeList.get(i).getContent().equals("00:00~00:30")){
					Time time = new Time();
					time.settId(deliveryTimeList.get(i).gettId());
					if(checkBoxList.get(i).isChecked()){  
						time.setState("1");
						deliveryTime.setFlag("0");
					}else{
						time.setState("2");
						deliveryTime.setFlag("1");
					}
					timeList.add(time);
					saveWyList.add(deliveryTime);
				}
				if(deliveryTimeList.get(i).getContent().equals("00:30~01:00")){
					Time time = new Time();
					time.settId(deliveryTimeList.get(i).gettId());
					if(checkBoxList.get(i).isChecked()){
						time.setState("1");
						deliveryTime.setFlag("0");
					}else{
						time.setState("2");
						deliveryTime.setFlag("1");
					}
					timeList.add(time);
					saveWyList.add(deliveryTime);
				}
				if(deliveryTimeList.get(i).getContent().equals("01:00~01:30")){
					Time time = new Time();
					time.settId(deliveryTimeList.get(i).gettId());
					if(checkBoxList.get(i).isChecked()){
						time.setState("1");
						deliveryTime.setFlag("0");
					}else{
						time.setState("2");
						deliveryTime.setFlag("1");
					}
					timeList.add(time);
					saveWyList.add(deliveryTime);
				}
				if(deliveryTimeList.get(i).getContent().equals("01:30~02:00")){
					Time time = new Time();
					time.settId(deliveryTimeList.get(i).gettId());
					if(checkBoxList.get(i).isChecked()){
						time.setState("1");
						deliveryTime.setFlag("0");
					}else{
						time.setState("2");
						deliveryTime.setFlag("1");
					}
					timeList.add(time);
					saveWyList.add(deliveryTime);
				}
				if(deliveryTimeList.get(i).getContent().equals("02:00~02:30")){
					Time time = new Time();
					time.settId(deliveryTimeList.get(i).gettId());
					if(checkBoxList.get(i).isChecked()){
						time.setState("1");
						deliveryTime.setFlag("0");
					}else{
						time.setState("2");
						deliveryTime.setFlag("1");
					}
					timeList.add(time);
					saveWyList.add(deliveryTime);
				}
				if(deliveryTimeList.get(i).getContent().equals("02:30~03:00")){
					Time time = new Time();
					time.settId(deliveryTimeList.get(i).gettId());
					if(checkBoxList.get(i).isChecked()){
						time.setState("1");
						deliveryTime.setFlag("0");
					}else{
						time.setState("2");
						deliveryTime.setFlag("1");
					}
					timeList.add(time);
					saveWyList.add(deliveryTime);
				}
				if(deliveryTimeList.get(i).getContent().equals("03:00~03:30")){
					Time time = new Time();
					time.settId(deliveryTimeList.get(i).gettId());
					if(checkBoxList.get(i).isChecked()){
						time.setState("1");
						deliveryTime.setFlag("0");
					}else{
						time.setState("2");
						deliveryTime.setFlag("1");
					}
					timeList.add(time);
					saveWyList.add(deliveryTime);
				}
				if(deliveryTimeList.get(i).getContent().equals("03:30~04:00")){
					Time time = new Time();
					time.settId(deliveryTimeList.get(i).gettId());
					if(checkBoxList.get(i).isChecked()){
						time.setState("1");
						deliveryTime.setFlag("0");
					}else{
						time.setState("2");
						deliveryTime.setFlag("1");
					}
					timeList.add(time);
					saveWyList.add(deliveryTime);
				}
				if(deliveryTimeList.get(i).getContent().equals("04:00~04:30")){
					Time time = new Time();
					time.settId(deliveryTimeList.get(i).gettId());
					if(checkBoxList.get(i).isChecked()){
						time.setState("1");
						deliveryTime.setFlag("0");
					}else{
						time.setState("2");
						deliveryTime.setFlag("1");
					}
					timeList.add(time);
					saveWyList.add(deliveryTime);
				}
				if(deliveryTimeList.get(i).getContent().equals("04:30~05:00")){
					Time time = new Time();
					time.settId(deliveryTimeList.get(i).gettId());
					if(checkBoxList.get(i).isChecked()){
						time.setState("1");
						deliveryTime.setFlag("0");
					}else{
						time.setState("2");
						deliveryTime.setFlag("1");
					}
					timeList.add(time);
					saveWyList.add(deliveryTime);
				}
				if(deliveryTimeList.get(i).getContent().equals("05:00~05:30")){
					Time time = new Time();
					time.settId(deliveryTimeList.get(i).gettId());
					if(checkBoxList.get(i).isChecked()){
						time.setState("1");
						deliveryTime.setFlag("0");
					}else{
						time.setState("2");
						deliveryTime.setFlag("1");
					}
					timeList.add(time);
					saveWyList.add(deliveryTime);
				}
				if(deliveryTimeList.get(i).getContent().equals("05:30~06:00")){
					Time time = new Time();
					time.settId(deliveryTimeList.get(i).gettId());
					if(checkBoxList.get(i).isChecked()){
						time.setState("1");
						deliveryTime.setFlag("0");
					}else{
						time.setState("2");
						deliveryTime.setFlag("1");
					}
					timeList.add(time);
					saveWyList.add(deliveryTime);
				}
				if(deliveryTimeList.get(i).getContent().equals("06:00~06:30")){
					Time time = new Time();
					time.settId(deliveryTimeList.get(i).gettId());
					if(checkBoxList.get(i).isChecked()){
						time.setState("1");
						deliveryTime.setFlag("0");
					}else{
						time.setState("2");
						deliveryTime.setFlag("1");
					}
					timeList.add(time);
					saveWyList.add(deliveryTime);
				}
				if(deliveryTimeList.get(i).getContent().equals("06:30~07:00")){
					Time time = new Time();
					time.settId(deliveryTimeList.get(i).gettId());
					if(checkBoxList.get(i).isChecked()){
						time.setState("1");
						deliveryTime.setFlag("0");
					}else{
						time.setState("2");
						deliveryTime.setFlag("1");
					}
					timeList.add(time);
					saveWyList.add(deliveryTime);
				}
				if(deliveryTimeList.get(i).getContent().equals("07:00~07:30")){
					Time time = new Time();
					time.settId(deliveryTimeList.get(i).gettId());
					if(checkBoxList.get(i).isChecked()){
						time.setState("1");
						deliveryTime.setFlag("0");
					}else{
						time.setState("2");
						deliveryTime.setFlag("1");
					}
					timeList.add(time);
					saveWyList.add(deliveryTime);
				}
				if(deliveryTimeList.get(i).getContent().equals("07:30~08:00")){
					Time time = new Time();
					time.settId(deliveryTimeList.get(i).gettId());
					if(checkBoxList.get(i).isChecked()){
						time.setState("1");
						deliveryTime.setFlag("0");
					}else{
						time.setState("2");
						deliveryTime.setFlag("1");
					}
					timeList.add(time);
					saveWyList.add(deliveryTime);
				}
				if(deliveryTimeList.get(i).getContent().equals("08:00~08:30")){
					Time time = new Time();
					time.settId(deliveryTimeList.get(i).gettId());
					if(checkBoxList.get(i).isChecked()){
						time.setState("1");
						deliveryTime.setFlag("0");
					}else{
						time.setState("2");
						deliveryTime.setFlag("1");
					}
					timeList.add(time);
					saveWyList.add(deliveryTime);
				}
				if(deliveryTimeList.get(i).getContent().equals("08:30~09:00")){
					Time time = new Time();
					time.settId(deliveryTimeList.get(i).gettId());
					if(checkBoxList.get(i).isChecked()){
						time.setState("1");
						deliveryTime.setFlag("0");
					}else{
						time.setState("2");
						deliveryTime.setFlag("1");
					}
					timeList.add(time);
					saveWyList.add(deliveryTime);
				}
				if(deliveryTimeList.get(i).getContent().equals("09:00~09:30")){
					Time time = new Time();
					time.settId(deliveryTimeList.get(i).gettId());
					if(checkBoxList.get(i).isChecked()){
						time.setState("1");
						deliveryTime.setFlag("0");
					}else{
						time.setState("2");
						deliveryTime.setFlag("1");
					}
					timeList.add(time);
					saveWyList.add(deliveryTime);
				}
				if(deliveryTimeList.get(i).getContent().equals("09:30~10:00")){
					Time time = new Time();
					time.settId(deliveryTimeList.get(i).gettId());
					if(checkBoxList.get(i).isChecked()){
						time.setState("1");
						deliveryTime.setFlag("0");
					}else{
						time.setState("2");
						deliveryTime.setFlag("1");
					}
					timeList.add(time);
					saveWyList.add(deliveryTime);
				}
				if(deliveryTimeList.get(i).getContent().equals("10:00~10:30")){
					Time time = new Time();
					time.settId(deliveryTimeList.get(i).gettId());
					if(checkBoxList.get(i).isChecked()){
						time.setState("1");
						deliveryTime.setFlag("0");
					}else{
						time.setState("2");
						deliveryTime.setFlag("1");
					}
					timeList.add(time);
					saveWyList.add(deliveryTime);
				}
				if(deliveryTimeList.get(i).getContent().equals("10:30~11:00")){
					Time time = new Time();
					time.settId(deliveryTimeList.get(i).gettId());
					if(checkBoxList.get(i).isChecked()){
						time.setState("1");
						deliveryTime.setFlag("0");
					}else{
						time.setState("2");
						deliveryTime.setFlag("1");
					}
					timeList.add(time);
					saveWyList.add(deliveryTime);
				}
				if(deliveryTimeList.get(i).getContent().equals("11:00~11:30")){
					Time time = new Time();
					time.settId(deliveryTimeList.get(i).gettId());
					if(checkBoxList.get(i).isChecked()){
						time.setState("1");
						deliveryTime.setFlag("0");
					}else{
						time.setState("2");
						deliveryTime.setFlag("1");
					}
					timeList.add(time);
					saveWyList.add(deliveryTime);
				}
				if(deliveryTimeList.get(i).getContent().equals("11:30~12:00")){
					Time time = new Time();
					time.settId(deliveryTimeList.get(i).gettId());
					if(checkBoxList.get(i).isChecked()){
						time.setState("1");
						deliveryTime.setFlag("0");
					}else{
						time.setState("2");
						deliveryTime.setFlag("1");
					}
					timeList.add(time);
					saveWyList.add(deliveryTime);
				}
				if(deliveryTimeList.get(i).getContent().equals("12:00~12:30")){
					Time time = new Time();
					time.settId(deliveryTimeList.get(i).gettId());
					if(checkBoxList.get(i).isChecked()){
						time.setState("1");
						deliveryTime.setFlag("0");
					}else{
						time.setState("2");
						deliveryTime.setFlag("1");
					}
					timeList.add(time);
					saveWyList.add(deliveryTime);
				}
				if(deliveryTimeList.get(i).getContent().equals("12:30~13:00")){
					Time time = new Time();
					time.settId(deliveryTimeList.get(i).gettId());
					if(checkBoxList.get(i).isChecked()){
						time.setState("1");
						deliveryTime.setFlag("0");
					}else{
						time.setState("2");
						deliveryTime.setFlag("1");
					}
					timeList.add(time);
					saveWyList.add(deliveryTime);
				}
				if(deliveryTimeList.get(i).getContent().equals("13:00~13:30")){
					Time time = new Time();
					time.settId(deliveryTimeList.get(i).gettId());
					if(checkBoxList.get(i).isChecked()){
						time.setState("1");
						deliveryTime.setFlag("0");
					}else{
						time.setState("2");
						deliveryTime.setFlag("1");
					}
					timeList.add(time);
					saveWyList.add(deliveryTime);
				}
				if(deliveryTimeList.get(i).getContent().equals("13:30~14:00")){
					Time time = new Time();
					time.settId(deliveryTimeList.get(i).gettId());
					if(checkBoxList.get(i).isChecked()){
						time.setState("1");
						deliveryTime.setFlag("0");
					}else{
						time.setState("2");
						deliveryTime.setFlag("1");
					}
					timeList.add(time);
					saveWyList.add(deliveryTime);
				}
				if(deliveryTimeList.get(i).getContent().equals("14:00~14:30")){
					Time time = new Time();
					time.settId(deliveryTimeList.get(i).gettId());
					if(checkBoxList.get(i).isChecked()){
						time.setState("1");
						deliveryTime.setFlag("0");
					}else{
						time.setState("2");
						deliveryTime.setFlag("1");
					}
					timeList.add(time);
					saveWyList.add(deliveryTime);
				}
				if(deliveryTimeList.get(i).getContent().equals("14:30~15:00")){
					Time time = new Time();
					time.settId(deliveryTimeList.get(i).gettId());
					if(checkBoxList.get(i).isChecked()){
						time.setState("1");
						deliveryTime.setFlag("0");
					}else{
						time.setState("2");
						deliveryTime.setFlag("1");
					}
					timeList.add(time);
					saveWyList.add(deliveryTime);
				}
				if(deliveryTimeList.get(i).getContent().equals("15:00~15:30")){
					Time time = new Time();
					time.settId(deliveryTimeList.get(i).gettId());
					if(checkBoxList.get(i).isChecked()){
						time.setState("1");
						deliveryTime.setFlag("0");
					}else{
						time.setState("2");
						deliveryTime.setFlag("1");
					}
					timeList.add(time);
					saveWyList.add(deliveryTime);
				}
				if(deliveryTimeList.get(i).getContent().equals("15:30~16:00")){
					Time time = new Time();
					time.settId(deliveryTimeList.get(i).gettId());
					if(checkBoxList.get(i).isChecked()){
						time.setState("1");
						deliveryTime.setFlag("0");
					}else{
						time.setState("2");
						deliveryTime.setFlag("1");
					}
					timeList.add(time);
					saveWyList.add(deliveryTime);
				}
				if(deliveryTimeList.get(i).getContent().equals("16:00~16:30")){
					Time time = new Time();
					time.settId(deliveryTimeList.get(i).gettId());
					if(checkBoxList.get(i).isChecked()){
						time.setState("1");
						deliveryTime.setFlag("0");
					}else{
						time.setState("2");
						deliveryTime.setFlag("1");
					}
					timeList.add(time);
					saveWyList.add(deliveryTime);
				}
				if(deliveryTimeList.get(i).getContent().equals("16:30~17:00")){
					Time time = new Time();
					time.settId(deliveryTimeList.get(i).gettId());
					if(checkBoxList.get(i).isChecked()){
						time.setState("1");
						deliveryTime.setFlag("0");
					}else{
						time.setState("2");
						deliveryTime.setFlag("1");
					}
					timeList.add(time);
					saveWyList.add(deliveryTime);
				}
				if(deliveryTimeList.get(i).getContent().equals("17:00~17:30")){
					Time time = new Time();
					time.settId(deliveryTimeList.get(i).gettId());
					if(checkBoxList.get(i).isChecked()){
						time.setState("1");
						deliveryTime.setFlag("0");
					}else{
						time.setState("2");
						deliveryTime.setFlag("1");
					}
					timeList.add(time);
					saveWyList.add(deliveryTime);
				}
				if(deliveryTimeList.get(i).getContent().equals("17:30~18:00")){
					Time time = new Time();
					time.settId(deliveryTimeList.get(i).gettId());
					if(checkBoxList.get(i).isChecked()){
						time.setState("1");
						deliveryTime.setFlag("0");
					}else{
						time.setState("2");
						deliveryTime.setFlag("1");
					}
					timeList.add(time);
					saveWyList.add(deliveryTime);
				}
				if(deliveryTimeList.get(i).getContent().equals("18:00~18:30")){
					Time time = new Time();
					time.settId(deliveryTimeList.get(i).gettId());
					if(checkBoxList.get(i).isChecked()){
						time.setState("1");
						deliveryTime.setFlag("0");
					}else{
						time.setState("2");
						deliveryTime.setFlag("1");
					}
					timeList.add(time);
					saveWyList.add(deliveryTime);
				}
				if(deliveryTimeList.get(i).getContent().equals("18:30~19:00")){
					Time time = new Time();
					time.settId(deliveryTimeList.get(i).gettId());
					if(checkBoxList.get(i).isChecked()){
						time.setState("1");
						deliveryTime.setFlag("0");
					}else{
						time.setState("2");
						deliveryTime.setFlag("1");
					}
					timeList.add(time);
					saveWyList.add(deliveryTime);
				}
				if(deliveryTimeList.get(i).getContent().equals("19:00~19:30")){
					Time time = new Time();
					time.settId(deliveryTimeList.get(i).gettId());
					if(checkBoxList.get(i).isChecked()){
						time.setState("1");
						deliveryTime.setFlag("0");
					}else{
						time.setState("2");
						deliveryTime.setFlag("1");
					}
					timeList.add(time);
					saveWyList.add(deliveryTime);
				}
				if(deliveryTimeList.get(i).getContent().equals("19:30~20:00")){
					Time time = new Time();
					time.settId(deliveryTimeList.get(i).gettId());
					if(checkBoxList.get(i).isChecked()){
						time.setState("1");
						deliveryTime.setFlag("0");
					}else{
						time.setState("2");
						deliveryTime.setFlag("1");
					}
					timeList.add(time);
					saveWyList.add(deliveryTime);
				}
				if(deliveryTimeList.get(i).getContent().equals("20:00~20:30")){
					Time time = new Time();
					time.settId(deliveryTimeList.get(i).gettId());
					if(checkBoxList.get(i).isChecked()){
						time.setState("1");
						deliveryTime.setFlag("0");
					}else{
						time.setState("2");
						deliveryTime.setFlag("1");
					}
					timeList.add(time);
					saveWyList.add(deliveryTime);
				}
				if(deliveryTimeList.get(i).getContent().equals("20:30~21:00")){
					Time time = new Time();
					time.settId(deliveryTimeList.get(i).gettId());
					if(checkBoxList.get(i).isChecked()){
						time.setState("1");
						deliveryTime.setFlag("0");
					}else{
						time.setState("2");
						deliveryTime.setFlag("1");
					}
					timeList.add(time);
					saveWyList.add(deliveryTime);
				}
				if(deliveryTimeList.get(i).getContent().equals("21:00~21:30")){
					Time time = new Time();
					time.settId(deliveryTimeList.get(i).gettId());
					if(checkBoxList.get(i).isChecked()){
						time.setState("1");
						deliveryTime.setFlag("0");
					}else{
						time.setState("2");
						deliveryTime.setFlag("1");
					}
					timeList.add(time);
					saveWyList.add(deliveryTime);
				}
				if(deliveryTimeList.get(i).getContent().equals("21:30~22:00")){
					Time time = new Time();
					time.settId(deliveryTimeList.get(i).gettId());
					if(checkBoxList.get(i).isChecked()){
						time.setState("1");
						deliveryTime.setFlag("0");
					}else{
						time.setState("2");
						deliveryTime.setFlag("1");
					}
					timeList.add(time);
					saveWyList.add(deliveryTime);
				}
				if(deliveryTimeList.get(i).getContent().equals("22:00~22:30")){
					Time time = new Time();
					time.settId(deliveryTimeList.get(i).gettId());
					if(checkBoxList.get(i).isChecked()){
						time.setState("1");
						deliveryTime.setFlag("0");
					}else{
						time.setState("2");
						deliveryTime.setFlag("1");
					}
					timeList.add(time);
					saveWyList.add(deliveryTime);
				}
				if(deliveryTimeList.get(i).getContent().equals("22:30~23:00")){
					Time time = new Time();
					time.settId(deliveryTimeList.get(i).gettId());
					if(checkBoxList.get(i).isChecked()){
						time.setState("1");
						deliveryTime.setFlag("0");
					}else{
						time.setState("2");
						deliveryTime.setFlag("1");
					}
					timeList.add(time);
					saveWyList.add(deliveryTime);
				}
				if(deliveryTimeList.get(i).getContent().equals("23:00~23:30")){
					Time time = new Time();
					time.settId(deliveryTimeList.get(i).gettId());
					if(checkBoxList.get(i).isChecked()){
						time.setState("1");
						deliveryTime.setFlag("0");
					}else{
						time.setState("2");
						deliveryTime.setFlag("1");
					}
					timeList.add(time);
					saveWyList.add(deliveryTime);
				}
				if(deliveryTimeList.get(i).getContent().equals("23:30~24:00")){
					Time time = new Time();
					time.settId(deliveryTimeList.get(i).gettId());
					if(checkBoxList.get(i).isChecked()){
						time.setState("1");
						deliveryTime.setFlag("0");
					}else{
						time.setState("2");
						deliveryTime.setFlag("1");
					}
					timeList.add(time);
					saveWyList.add(deliveryTime);
				}
			}
		}
	}
	
	/**
	 * 更改外送时间
	 */
	private void updateDeliverTimeData(){
		ThreadPoolManager.getInstance().executeTask(new Runnable() {
			@Override
			public void run() {
				try{
					homeActivity.showLoadingHint();
					UpdateDeliveryTimesReq updateDeliveryTimesReq = new UpdateDeliveryTimesReq();
					UserInfo userInfo = LoginUtils.getUserInfo(homeActivity);
					if ( userInfo!=null ){
						updateDeliveryTimesReq.setsId(userInfo.getsId());
						updateDeliveryTimesReq.setPwd(userInfo.getPwd());
					}
					updateDeliveryTimesReq.setTimeList(timeList);
					final Result result = ShopModel.getInstance(homeActivity).updateDeliveryTimes(updateDeliveryTimesReq);
					if ( result!=null && result.code==1 ) {
						homeActivity.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								SuperUI.openToast(homeActivity, result.msg);
								aCache.put("delivertime", saveWyList );	//把外送时间段存入缓存
								fullDeliverTime();
								homeActivity.menuView.init();
							}
						});
					} else if( result != null && result.code==-3 ) {
						homeActivity.homeController.exitLogin();
					} else {
						homeActivity.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								SuperUI.openToast(homeActivity, "保存失败,请重试");
							}
						});
					}
				}catch(RetrofitError e){
					homeActivity.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							SuperUI.openToast(homeActivity, "保存失败,请重试");
						}
					});
				}finally{
					homeActivity.hideLoadingHint();
				}
			}
		});
	}
	
	/**
	 * 初始化CheckBox的状态
	 */
	private void initCheckBox() {
		if (deliveryTimeList != null && deliveryTimeList.size() > 0) {
			for (int i = 0; i < deliveryTimeList.size(); i++) {
				if (deliveryTimeList.get(i).getContent().equals("00:00~00:30")) {
					if (deliveryTimeList.get(i).getFlag().equals("0")) {
						shophours_left0.setChecked(true);
					} else {
						shophours_left0.setChecked(false);
					}
				}
				if (deliveryTimeList.get(i).getContent().equals("00:30~01:00")) {
					if (deliveryTimeList.get(i).getFlag().equals("0")) {
						shophours_right0.setChecked(true);
					} else {
						shophours_right0.setChecked(false);
					}
				}
				if (deliveryTimeList.get(i).getContent().equals("01:00~01:30")) {
					if (deliveryTimeList.get(i).getFlag().equals("0")) {
						shophours_left1.setChecked(true);
					} else {
						shophours_left1.setChecked(false);
					}
				}
				if (deliveryTimeList.get(i).getContent().equals("01:30~02:00")) {
					if (deliveryTimeList.get(i).getFlag().equals("0")) {
						shophours_right1.setChecked(true);
					} else {
						shophours_right1.setChecked(false);
					}
				}
				if (deliveryTimeList.get(i).getContent().equals("02:00~02:30")) {
					if (deliveryTimeList.get(i).getFlag().equals("0")) {
						shophours_left2.setChecked(true);
					} else {
						shophours_left2.setChecked(false);
					}
				}
				if (deliveryTimeList.get(i).getContent().equals("02:30~03:00")) {
					if (deliveryTimeList.get(i).getFlag().equals("0")) {
						shophours_right2.setChecked(true);
					} else {
						shophours_right2.setChecked(false);
					}
				}
				if (deliveryTimeList.get(i).getContent().equals("03:00~03:30")) {
					if (deliveryTimeList.get(i).getFlag().equals("0")) {
						shophours_left3.setChecked(true);
					} else {
						shophours_left3.setChecked(false);
					}
				}
				if (deliveryTimeList.get(i).getContent().equals("03:30~04:00")) {
					if (deliveryTimeList.get(i).getFlag().equals("0")) {
						shophours_right3.setChecked(true);
					} else {
						shophours_right3.setChecked(false);
					}
				}
				if (deliveryTimeList.get(i).getContent().equals("04:00~04:30")) {
					if (deliveryTimeList.get(i).getFlag().equals("0")) {
						shophours_left4.setChecked(true);
					} else {
						shophours_left4.setChecked(false);
					}
				}
				if (deliveryTimeList.get(i).getContent().equals("04:30~05:00")) {
					if (deliveryTimeList.get(i).getFlag().equals("0")) {
						shophours_right4.setChecked(true);
					} else {
						shophours_right4.setChecked(false);
					}
				}
				if (deliveryTimeList.get(i).getContent().equals("05:00~05:30")) {
					if (deliveryTimeList.get(i).getFlag().equals("0")) {
						shophours_left5.setChecked(true);
					} else {
						shophours_left5.setChecked(false);
					}
				}
				if (deliveryTimeList.get(i).getContent().equals("05:30~06:00")) {
					if (deliveryTimeList.get(i).getFlag().equals("0")) {
						shophours_right5.setChecked(true);
					} else {
						shophours_right5.setChecked(false);
					}
				}
				if (deliveryTimeList.get(i).getContent().equals("06:00~06:30")) {
					if (deliveryTimeList.get(i).getFlag().equals("0")) {
						shophours_left6.setChecked(true);
					} else {
						shophours_left6.setChecked(false);
					}
				}
				if (deliveryTimeList.get(i).getContent().equals("06:30~07:00")) {
					if (deliveryTimeList.get(i).getFlag().equals("0")) {
						shophours_right6.setChecked(true);
					} else {
						shophours_right6.setChecked(false);
					}
				}
				if (deliveryTimeList.get(i).getContent().equals("07:00~07:30")) {
					if (deliveryTimeList.get(i).getFlag().equals("0")) {
						shophours_left7.setChecked(true);
					} else {
						shophours_left7.setChecked(false);
					}
				}
				if (deliveryTimeList.get(i).getContent().equals("07:30~08:00")) {
					if (deliveryTimeList.get(i).getFlag().equals("0")) {
						shophours_right7.setChecked(true);
					} else {
						shophours_right7.setChecked(false);
					}
				}
				if (deliveryTimeList.get(i).getContent().equals("08:00~08:30")) {
					if (deliveryTimeList.get(i).getFlag().equals("0")) {
						shophours_left8.setChecked(true);
					} else {
						shophours_left8.setChecked(false);
					}
				}
				if (deliveryTimeList.get(i).getContent().equals("08:30~09:00")) {
					if (deliveryTimeList.get(i).getFlag().equals("0")) {
						shophours_right8.setChecked(true);
					} else {
						shophours_right8.setChecked(false);
					}
				}
				if (deliveryTimeList.get(i).getContent().equals("09:00~09:30")) {
					if (deliveryTimeList.get(i).getFlag().equals("0")) {
						shophours_left9.setChecked(true);
					} else {
						shophours_left9.setChecked(false);
					}
				}
				if (deliveryTimeList.get(i).getContent().equals("09:30~10:00")) {
					if (deliveryTimeList.get(i).getFlag().equals("0")) {
						shophours_right9.setChecked(true);
					} else {
						shophours_right9.setChecked(false);
					}
				}
				if (deliveryTimeList.get(i).getContent().equals("10:00~10:30")) {
					if (deliveryTimeList.get(i).getFlag().equals("0")) {
						shophours_left10.setChecked(true);
					} else {
						shophours_left10.setChecked(false);
					}
				}
				if (deliveryTimeList.get(i).getContent().equals("10:30~11:00")) {
					if (deliveryTimeList.get(i).getFlag().equals("0")) {
						shophours_right10.setChecked(true);
					} else {
						shophours_right10.setChecked(false);
					}
				}
				if (deliveryTimeList.get(i).getContent().equals("11:00~11:30")) {
					if (deliveryTimeList.get(i).getFlag().equals("0")) {
						shophours_left11.setChecked(true);
					} else {
						shophours_left11.setChecked(false);
					}
				}
				if (deliveryTimeList.get(i).getContent().equals("11:30~12:00")) {
					if (deliveryTimeList.get(i).getFlag().equals("0")) {
						shophours_right11.setChecked(true);
					} else {
						shophours_right11.setChecked(false);
					}
				}
				if (deliveryTimeList.get(i).getContent().equals("12:00~12:30")) {
					if (deliveryTimeList.get(i).getFlag().equals("0")) {
						shophours_left12.setChecked(true);
					} else {
						shophours_left12.setChecked(false);
					}
				}
				if (deliveryTimeList.get(i).getContent().equals("12:30~13:00")) {
					if (deliveryTimeList.get(i).getFlag().equals("0")) {
						shophours_right12.setChecked(true);
					} else {
						shophours_right12.setChecked(false);
					}
				}
				if (deliveryTimeList.get(i).getContent().equals("13:00~13:30")) {
					if (deliveryTimeList.get(i).getFlag().equals("0")) {
						shophours_left13.setChecked(true);
					} else {
						shophours_left13.setChecked(false);
					}
				}
				if (deliveryTimeList.get(i).getContent().equals("13:30~14:00")) {
					if (deliveryTimeList.get(i).getFlag().equals("0")) {
						shophours_right13.setChecked(true);
					} else {
						shophours_right13.setChecked(false);
					}
				}
				if (deliveryTimeList.get(i).getContent().equals("14:00~14:30")) {
					if (deliveryTimeList.get(i).getFlag().equals("0")) {
						shophours_left14.setChecked(true);
					} else {
						shophours_left14.setChecked(false);
					}
				}
				if (deliveryTimeList.get(i).getContent().equals("14:30~15:00")) {
					if (deliveryTimeList.get(i).getFlag().equals("0")) {
						shophours_right14.setChecked(true);
					} else {
						shophours_right14.setChecked(false);
					}
				}
				if (deliveryTimeList.get(i).getContent().equals("15:00~15:30")) {
					if (deliveryTimeList.get(i).getFlag().equals("0")) {
						shophours_left15.setChecked(true);
					} else {
						shophours_left15.setChecked(false);
					}
				}
				if (deliveryTimeList.get(i).getContent().equals("15:30~16:00")) {
					if (deliveryTimeList.get(i).getFlag().equals("0")) {
						shophours_right15.setChecked(true);
					} else {
						shophours_right15.setChecked(false);
					}
				}
				if (deliveryTimeList.get(i).getContent().equals("16:00~16:30")) {
					if (deliveryTimeList.get(i).getFlag().equals("0")) {
						shophours_left16.setChecked(true);
					} else {
						shophours_left16.setChecked(false);
					}
				}
				if (deliveryTimeList.get(i).getContent().equals("16:30~17:00")) {
					if (deliveryTimeList.get(i).getFlag().equals("0")) {
						shophours_right16.setChecked(true);
					} else {
						shophours_right16.setChecked(false);
					}
				}
				if (deliveryTimeList.get(i).getContent().equals("17:00~17:30")) {
					if (deliveryTimeList.get(i).getFlag().equals("0")) {
						shophours_left17.setChecked(true);
					} else {
						shophours_left17.setChecked(false);
					}
				}
				if (deliveryTimeList.get(i).getContent().equals("17:30~18:00")) {
					if (deliveryTimeList.get(i).getFlag().equals("0")) {
						shophours_right17.setChecked(true);
					} else {
						shophours_right17.setChecked(false);
					}
				}
				if (deliveryTimeList.get(i).getContent().equals("18:00~18:30")) {
					if (deliveryTimeList.get(i).getFlag().equals("0")) {
						shophours_left18.setChecked(true);
					} else {
						shophours_left18.setChecked(false);
					}
				}
				if (deliveryTimeList.get(i).getContent().equals("18:30~19:00")) {
					if (deliveryTimeList.get(i).getFlag().equals("0")) {
						shophours_right18.setChecked(true);
					} else {
						shophours_right18.setChecked(false);
					}
				}
				if (deliveryTimeList.get(i).getContent().equals("19:00~19:30")) {
					if (deliveryTimeList.get(i).getFlag().equals("0")) {
						shophours_left19.setChecked(true);
					} else {
						shophours_left19.setChecked(false);
					}
				}
				if (deliveryTimeList.get(i).getContent().equals("19:30~20:00")) {
					if (deliveryTimeList.get(i).getFlag().equals("0")) {
						shophours_right19.setChecked(true);
					} else {
						shophours_right19.setChecked(false);
					}
				}
				if (deliveryTimeList.get(i).getContent().equals("20:00~20:30")) {
					if (deliveryTimeList.get(i).getFlag().equals("0")) {
						shophours_left20.setChecked(true);
					} else {
						shophours_left20.setChecked(false);
					}
				}
				if (deliveryTimeList.get(i).getContent().equals("20:30~21:00")) {
					if (deliveryTimeList.get(i).getFlag().equals("0")) {
						shophours_right20.setChecked(true);
					} else {
						shophours_right20.setChecked(false);
					}
				}
				if (deliveryTimeList.get(i).getContent().equals("21:00~21:30")) {
					if (deliveryTimeList.get(i).getFlag().equals("0")) {
						shophours_left21.setChecked(true);
					} else {
						shophours_left21.setChecked(false);
					}
				}
				if (deliveryTimeList.get(i).getContent().equals("21:30~22:00")) {
					if (deliveryTimeList.get(i).getFlag().equals("0")) {
						shophours_right21.setChecked(true);
					} else {
						shophours_right21.setChecked(false);
					}
				}
				if (deliveryTimeList.get(i).getContent().equals("22:00~22:30")) {
					if (deliveryTimeList.get(i).getFlag().equals("0")) {
						shophours_left22.setChecked(true);
					} else {
						shophours_left22.setChecked(false);
					}
				}
				if (deliveryTimeList.get(i).getContent().equals("22:30~23:00")) {
					if (deliveryTimeList.get(i).getFlag().equals("0")) {
						shophours_right22.setChecked(true);
					} else {
						shophours_right22.setChecked(false);
					}
				}
				if (deliveryTimeList.get(i).getContent().equals("23:00~23:30")) {
					if (deliveryTimeList.get(i).getFlag().equals("0")) {
						shophours_left23.setChecked(true);
					} else {
						shophours_left23.setChecked(false);
					}
				}
				if (deliveryTimeList.get(i).getContent().equals("23:30~24:00")) {
					if (deliveryTimeList.get(i).getFlag().equals("0")) {
						shophours_right23.setChecked(true);
					} else {
						shophours_right23.setChecked(false);
					}
				}
			}
		}
		
		// 设置所有Checkbox的监听器
		for (int i = 0; i < checkBoxList.size(); i++) {
			checkBoxList.get(i).setOnCheckedChangeListener(ShopHoursFragment.this);
		}
		// 隐藏操作面板
		layout_shophours_panel.setVisibility(View.GONE);
	}
	
	/**
	 * 是否显示控制面板
	 */
	public boolean isShowPanel(){
		if(checkBoxList.size()!= statusList.size()){
			SuperUI.openToast(homeActivity, "获取服务器的数据有误");
			return false;
		}
		
		for(int i=0;i<checkBoxList.size();i++){
			boolean status=checkBoxList.get(i).isChecked();
			if(!statusList.get(i).equals(status)){
				return true;
			}
		}
		return false;
	}
	
	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.layout_shophours_submit:
			if ( judgeCurrent() ) {
				updateDeliverTimeData();
			} else {
				SuperUI.openToast(homeActivity, "营业时间段不允许为空");
			}
			break;
		case R.id.layout_shophours_cancel:
			fullDeliverTime();
			break;
		}
	}
	
	private boolean judgeCurrent(){
		getUpdateState();
		for (int i = 0; i < timeList.size(); i++) {
			if ( timeList.get(i).getState().equals("1") ) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		if(isShowPanel()){
			layout_shophours_panel.setVisibility(View.VISIBLE);
		}else{
			layout_shophours_panel.setVisibility(View.GONE);
		}
		layout_shophours_panel.setVisibility(View.VISIBLE);
	}
	
}
