package com.woyou.utils;

import java.util.HashMap;

import android.content.Context;
import android.media.MediaPlayer;

import com.jing.biandang.R;

public class SoundManager {
	private Context mContext;
	private static SoundManager soundManager;
	private HashMap<String, Integer> soundHashMap;
	private MediaPlayer player =null;
	public final static int NETERROR = 1;
	public final static int NEWORDER = 2;
	public final static int CANCELORDER = 3;

	private SoundManager(Context context) {
		mContext=context;
		soundHashMap = new HashMap<String, Integer>();
		soundHashMap.put("" + NETERROR,R.raw.sound_click);
		soundHashMap.put("" + NEWORDER,R.raw.sound_new_order);
		soundHashMap.put("" + CANCELORDER,R.raw.sound_cancel_order);
	}

	public static SoundManager getInstance(Context context) {
		if (soundManager == null){
			soundManager = new SoundManager(context);
		}
		return soundManager;
	}

	/**
	 * 播放声音 
	 * 网络错误SoundManager.NETERROR 
	 * 新订单SoundManager.NEWORDER
	 * 取消订单SoundManager.CANCELORDER
	 */
	public void playSound(int soundType) {
		if(player!=null){
			player.release();
		}
		player = MediaPlayer.create(mContext,soundHashMap.get(soundType+""));
		player.setLooping(true);
		player.start();
	}

	/**
	 * 关闭声音
	 */
	public void stopSound() {
		if(player!=null){
			player.release();
		}
		
	}

}
