package com.woyou.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * 商品状态
 * 
 * @author 荣
 * 
 */
public class GoodsState extends SuperModel {
	/**
	 * 商品ID
	 */
	private String gId = "";
	/**
	 * 商品名称
	 */
	private String gName = "";
	/**
	 * 商品分类ID（包括子商品类）
	 */
	private String tId = "";
	/**
	 * 商品分类名称
	 */
	private String tName = "";
	/**
	 * 是否暂缺（1正常，2暂缺）
	 */
	private int state;
	/**
	 * 记录该商品的原始状态
	 */
	private int origin;
	/**
	 * 属性Option列表
	 */
	private List<OptionState> optList = new ArrayList<OptionState>();
	/**
	 * 位置
	 */
	private int position = -1;
	/**
	 * 判断是否显示子类
	 */
	private boolean childIsShow = true;
	
	public String getgId() {
		return gId;
	}

	public void setgId(String gId) {
		this.gId = gId;
	}

	public String getgName() {
		return gName;
	}

	public void setgName(String gName) {
		this.gName = gName;
	}

	public String gettId() {
		return tId;
	}

	public void settId(String tId) {
		this.tId = tId;
	}

	public String gettName() {
		return tName;
	}

	public void settName(String tName) {
		this.tName = tName;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public int getOrigin() {
		return origin;
	}

	public void setOrigin(int origin) {
		this.origin = origin;
	}

	public List<OptionState> getOptList() {
		return optList;
	}

	public void setOptList(List<OptionState> optList) {
		this.optList = optList;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public boolean isChildIsShow() {
		return childIsShow;
	}

	public void setChildIsShow(boolean childIsShow) {
		this.childIsShow = childIsShow;
	}

	@Override
	public String toString() {
		return "GoodsState [gId=" + gId + ", gName=" + gName + ", tId=" + tId
				+ ", tName=" + tName + ", state=" + state + ", origin="
				+ origin + ", optList=" + optList + ", position=" + position
				+ ", childIsShow=" + childIsShow + "]";
	}

}
