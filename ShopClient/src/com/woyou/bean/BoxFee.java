package com.woyou.bean;


/**
 * 餐盒费
 * 
 * @author zhou.ni
 * 
 */
public class BoxFee extends SuperModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int type; // 餐盒费类型（0：不算1：按单算,2:按份算）
	private int num; // 数量
	private float price; // 价格

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

}
