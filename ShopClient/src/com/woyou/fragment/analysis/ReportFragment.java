package com.woyou.fragment.analysis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.jing.biandang.R;
import com.woyou.activity.HomeActivity;
import com.woyou.adapter.ReportDayAdapter;
import com.woyou.adapter.ReportMonthAdapter;
import com.woyou.adapter.SuperViewPagerAdapter;
import com.woyou.bean.FmInfo;
import com.woyou.bean.ReportItem;
import com.woyou.bean.UserInfo;
import com.woyou.bean.rpc.QueryReportListReq;
import com.woyou.bean.rpc.Result;
import com.woyou.component.SuperUI;
import com.woyou.fragment.SuperFragment;
import com.woyou.listener.ItemClickListener;
import com.woyou.listener.ReportPageChangeListener;
import com.woyou.model.rpc.ShopModel;
import com.woyou.utils.FormatTools;
import com.woyou.utils.LoginUtils;
import com.woyou.utils.ThreadPoolManager;
import com.woyou.utils.TypeJudgeTools;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;

/**
 * 营业报表界面
 * 
 * @author zhou.ni
 *
 */
public class ReportFragment extends SuperFragment {
	private HomeActivity homeActivity;
	
	private View layout_report;
	private RelativeLayout layout_report_day_bar;
	private RelativeLayout layout_report_month_bar;
	private int screenWidth;
	private ImageView layout_report_navi_iv;
	private ViewPager layout_report_vp;
	private SuperViewPagerAdapter superViewPagerAdapter;
	private View layout_report_day;
	private View layout_report_month;
	
	// 日报表
	private ArrayList<HashMap<String, Object>> dayData = new ArrayList<HashMap<String, Object>>();
	private HashMap<String, Object> dayMap;
	private ListView layout_report_day_lv;
	private ReportDayAdapter dayAdapter;

	// 月报表
	private ArrayList<HashMap<String, Object>> monthData = new ArrayList<HashMap<String, Object>>();
	private HashMap<String, Object> monthMap;
	private ListView layout_report_month_lv;
	private ReportMonthAdapter monthAdapter;
	
	private QueryReportListReq queryReportListReq = new QueryReportListReq();		//报表详情的请求bean

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		layout_report = inflater.inflate(R.layout.layout_report,container, false);
		homeActivity=(HomeActivity)getActivity();
		return layout_report;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		UserInfo userInfo = LoginUtils.getUserInfo(homeActivity);
		if ( userInfo!=null ){
			queryReportListReq.setsId(userInfo.getsId());
			queryReportListReq.setPwd(userInfo.getPwd());
		}
		// 计算位置
		DisplayMetrics dm = new DisplayMetrics();
		getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
		screenWidth = dm.widthPixels;
		layout_report_navi_iv = (ImageView) layout_report.findViewById(R.id.layout_report_navi_iv);
		LayoutParams layoutParams = layout_report_navi_iv.getLayoutParams();
		layoutParams.width = screenWidth / 2;
		layout_report_navi_iv.setLayoutParams(layoutParams);
		// 导航栏菜单
		layout_report_day_bar = (RelativeLayout) layout_report.findViewById(R.id.layout_report_day_bar);
		layout_report_month_bar = (RelativeLayout) layout_report.findViewById(R.id.layout_report_month_bar);
		// 初始化Viewpager
		layout_report_vp = (ViewPager) layout_report.findViewById(R.id.layout_report_vp);
		List<View> views = new ArrayList<View>();
		layout_report_day = View.inflate(mContext,R.layout.layout_report_day, null);
		layout_report_month = View.inflate(mContext,R.layout.layout_report_month, null);
		views.add(layout_report_day);
		views.add(layout_report_month);
		superViewPagerAdapter = new SuperViewPagerAdapter(views);
		layout_report_vp.setAdapter(superViewPagerAdapter);
		layout_report_vp.setOnPageChangeListener(new ReportPageChangeListener(screenWidth, layout_report_navi_iv));
		// 设置导航监听器
		layout_report_day_bar.setOnClickListener(new ItemClickListener(0,layout_report_vp));
		layout_report_month_bar.setOnClickListener(new ItemClickListener(1,layout_report_vp));
		// 初始化页面内容
		initDay();
		initMonth();
	}

	/**
	 * 载入日报表详情
	 */
	public void loadDayReportDetails(final String date){
		ThreadPoolManager.getInstance().executeTask(new Runnable() {
			@Override
			public void run() {
				try{
					homeActivity.showLoadingHint();
					final Result<ReportItem> result = ShopModel.getInstance(homeActivity).queryReportList(queryReportListReq);
					if ( result!=null && result.getCode()==1 ) {
						homeActivity.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								ReportItem reportItem = result.getData();
								aCache.put("reportSale", reportItem);				//储存日报表详情到aCache中
								jump2ReportDetails(true, date);
							}
						});
					} else if( result != null && result.code==-3 ) {
						homeActivity.homeController.exitLogin();
					} else {
						homeActivity.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								SuperUI.openToast(homeActivity, "该日还没有报表数据呢");
							}
						});
					}
				}catch(Exception e){
					e.printStackTrace();
				}finally{
					homeActivity.hideLoadingHint();
				}
			}
		});
	}
	
	/**
	 * 载入月报表
	 */
	public void loadMonthReportDetails(final String date){
		ThreadPoolManager.getInstance().executeTask(new Runnable() {
			@Override
			public void run() {
				try{
					homeActivity.showLoadingHint();
					final Result<ReportItem> result = ShopModel.getInstance(homeActivity).queryReportList(queryReportListReq);
					if ( result!=null && result.getCode()==1 ) {
						homeActivity.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								ReportItem reportItem = result.getData();
								aCache.put("reportSale", reportItem);			//储存日报表详情到aCache中
								jump2ReportDetails(false, date);
							}
						});
					} else if( result != null && result.code==-3 ) {
						homeActivity.homeController.exitLogin();
					} else {
						homeActivity.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								SuperUI.openToast(homeActivity, "该月还没有报表数据呢");
							}
						});
					}
				}catch(Exception e){
					e.printStackTrace();
				}finally{
					homeActivity.hideLoadingHint();
				}
			}
		});
	}
	
	/**
	 * 跳转到报表详情
	 * @param isDay true是日报表，false是月报表
	 */
	public void jump2ReportDetails(boolean isDay, String date){
		aCache.put("reportdate", date);
		//更改导航条
		if(isDay){
			homeActivity.openSubFM(this, new FmInfo(ReportDetailsFragment.class,"日报表"+ date, false));
		}else{
			homeActivity.openSubFM(this, new FmInfo(ReportDetailsFragment.class,"月报表"+ date, false));
		}
	}
	
	/**
	 * 初始化日报表
	 */
	private void initDay() {
		layout_report_day_lv = (ListView) layout_report_day.findViewById(R.id.layout_report_day_lv);
		dayAdapter = new ReportDayAdapter(getActivity(), dayData);
		layout_report_day_lv.setAdapter(dayAdapter);
		loadDayData();
		layout_report_day_lv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
				String date = dayData.get(position).get("date").toString();
				queryReportListReq.setDate(date);
				loadDayReportDetails(date);
			}
		});
	}

	/**
	 * 载入日报表数据
	 */
	public void loadDayData() {
		String ServerTime = aCache.getAsString("ServerTime");
		if (!TypeJudgeTools.isNull(ServerTime)) {
			long today = Long.valueOf(ServerTime);
			for (int i = 0; i < 15; i++) {
				String date = FormatTools.convertTime(today, "yyyy-MM-dd");
				dayMap = new HashMap<String, Object>();
				dayMap.put("date", "" + date);
				dayData.add(dayMap);
				// 往昨天推一天
				today = today - (24 * 60 * 60 * 1000);
			}
			dayAdapter.notifyDataSetChanged();
		}
	}

	/**
	 * 初始化月报表
	 */
	public void initMonth() {
		layout_report_month_lv = (ListView) layout_report_month.findViewById(R.id.layout_report_month_lv);
		monthAdapter = new ReportMonthAdapter(getActivity(), monthData);
		layout_report_month_lv.setAdapter(monthAdapter);
		loadMonthData();
		layout_report_month_lv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
				String month = monthData.get(position).get("month").toString();
				queryReportListReq.setDate(month);
				loadMonthReportDetails(month);
			}
		});
	}

	/**
	 * 载入月报表数据
	 */
	public void loadMonthData() {
		String ServerTime = aCache.getAsString("ServerTime");
		if (!TypeJudgeTools.isNull(ServerTime)) {
			long today = Long.valueOf(ServerTime);
			int year = Integer.valueOf(FormatTools.convertTime(today, "yyyy"));
			int month = Integer.valueOf(FormatTools.convertTime(today, "MM"));
			String sm = "";
			for (int i = 0; i < 15; i++) {
				if(month==1){
					sm = "0" + month;
					monthMap = new HashMap<String, Object>();
					monthMap.put("month", year + "-" + sm);
					monthData.add(monthMap);
					month=12;
					year=year-1;
				}else if(month<=9){
					sm = "0" + month;
					monthMap = new HashMap<String, Object>();
					monthMap.put("month", year + "-" + sm);
					monthData.add(monthMap);
					// 往前退一个月
					month=month-1;
				}else{
					sm = ""+month;
					monthMap = new HashMap<String, Object>();
					monthMap.put("month", year + "-" + sm);
					monthData.add(monthMap);
					// 往前退一个月
					month=month-1;
				}
			}
			monthAdapter.notifyDataSetChanged();
		}
	}
	
}
