package com.woyou.bean;


/**
 * 优惠券统计
 * @author zhou.ni
 *
 */
public class CouponCount extends SuperModel {

	private static final long serialVersionUID = 1L;
	
	private String cName;		//优惠券名字	
	private int cNum;			//优惠券数量
	private float cPrice;		//优惠券金额
	
	public String getcName() {
		return cName;
	}
	public void setcName(String cName) {
		this.cName = cName;
	}
	public int getcNum() {
		return cNum;
	}
	public void setcNum(int cNum) {
		this.cNum = cNum;
	}
	public float getcPrice() {
		return cPrice;
	}
	public void setcPrice(float cPrice) {
		this.cPrice = cPrice;
	}
	
	@Override
	public String toString() {
		return "CouponCount [cName=" + cName + ", cNum=" + cNum + ", cPrice="
				+ cPrice + "]";
	}
	
	
}
