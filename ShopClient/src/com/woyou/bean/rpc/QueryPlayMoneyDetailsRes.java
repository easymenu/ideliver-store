package com.woyou.bean.rpc;

import java.util.ArrayList;
import java.util.List;

import com.woyou.bean.PlayMoney;
import com.woyou.bean.StayPay;
import com.woyou.bean.SuperModel;

/**
 * 返回打款详情bean
 * 
 * @author xuron
 *
 */
public class QueryPlayMoneyDetailsRes extends SuperModel {
	/**
	 * 是否开通在线支付：
	 */
	private int isOnlinePay;
	/**
	 * 当前绑定的银行卡
	 */
	private String bindCard;
	/**
	 * 待支付列表
	 */
	private List<StayPay> stayPays = new ArrayList<StayPay>();
	/**
	 * 打款历史列表
	 */
	private List<PlayMoney> playMoneys = new ArrayList<PlayMoney>();

	public int getIsOnlinePay() {
		return isOnlinePay;
	}

	public void setIsOnlinePay(int isOnlinePay) {
		this.isOnlinePay = isOnlinePay;
	}

	public String getBindCard() {
		return bindCard;
	}

	public void setBindCard(String bindCard) {
		this.bindCard = bindCard;
	}

	public List<StayPay> getStayPays() {
		return stayPays;
	}

	public void setStayPays(List<StayPay> stayPays) {
		this.stayPays = stayPays;
	}

	public List<PlayMoney> getPlayMoneys() {
		return playMoneys;
	}

	public void setPlayMoneys(List<PlayMoney> playMoneys) {
		this.playMoneys = playMoneys;
	}

}
