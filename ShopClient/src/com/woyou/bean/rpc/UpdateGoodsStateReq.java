package com.woyou.bean.rpc;

import java.util.ArrayList;
import java.util.List;

import com.woyou.bean.GoodsItemInfo;
import com.woyou.bean.SuperModel;

/**
 * 设置商品状态请求
 * 
 * @author tlc
 * 
 */
public class UpdateGoodsStateReq extends SuperModel {

	/**
	 * 店铺ID
	 */
	private String sId = "";
	/**
	 * 密码
	 */
	private String pwd = "";
	/**
	 * 商品列表
	 */
	private List<GoodsItemInfo> goodsList = new ArrayList<GoodsItemInfo>();

	public String getsId() {
		return sId;
	}

	public void setsId(String sId) {
		this.sId = sId;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public List<GoodsItemInfo> getGoodsList() {
		return goodsList;
	}

	public void setGoodsList(List<GoodsItemInfo> goodsList) {
		this.goodsList = goodsList;
	}

}
