package com.woyou.bean.rpc;

import java.util.ArrayList;
import java.util.List;

import com.woyou.bean.Coupon;
import com.woyou.bean.Gift;
import com.woyou.bean.Goods;
import com.woyou.bean.SuperModel;

/**
 * 获取订单详情返回bean
 * 
 * @author 荣
 * 
 */
public class QueryOrderDetailsRes extends SuperModel {
	/**
	 * 订单ID
	 */
	private String oId = "";
	/**
	 * 短编号
	 */
	private String shortNo = "";
	/**
	 * 下单时间
	 */
	private long orderTime;
	/**
	 * 下单人手机号
	 */
	private String userPhone = "";
	/**
	 * 联系人
	 */
	private String contact = "";
	/**
	 * 性别1：女，2：男；
	 */
	private int sex = 0;
	/**
	 * 是否是老用户 1：新用户，2老用户
	 */
	private int isOldUser;
	/**
	 * 该用户在该店下单次数
	 */
	private int times;
	/**
	 * 是否预约单 1：及时单，2：预约单
	 */
	private int isReserve;
	/**
	 * 送餐时间
	 */
	private long reserveTime;
	/**
	 * 送餐手机号
	 */
	private String phone = "";
	/**
	 * 送餐地址
	 */
	private String addr = "";
	/**
	 * 备注
	 */
	private String remark = "";
	/**
	 * 订单总份数
	 */
	private int sumNum;
	/**
	 * 订单总金额
	 */
	private float sumFee;
	/**
	 * 订单是否已支付0：已支付，1：未支付
	 */
	private int isPay;
	/**
	 * 送餐费
	 */
	private float deliverFee;
	/**
	 * 餐盒费类型
	 */
	private int boxType;
	/**
	 * 餐盒数
	 */
	private int boxNum;
	/**
	 * 餐盒费
	 */
	private float boxFee;
	/**
	 * 处理时间
	 */
	private String opTime = "";
	/**
	 * 外送时间
	 */
	private long sendTime;
	/**
	 * 订单状态
	 */
	private String opStatus = "";
	/**
	 * 商品列表
	 */
	private List<Goods> goodsList = new ArrayList<Goods>();
	/**
	 * 赠品列表
	 */
	private List<Gift> giftList = new ArrayList<Gift>();
	/**
	 * 优惠券列表
	 */
	private List<Coupon> couponList = new ArrayList<Coupon>();

	public String getoId() {
		return oId;
	}

	public void setoId(String oId) {
		this.oId = oId;
	}

	public String getShortNo() {
		return shortNo;
	}

	public void setShortNo(String shortNo) {
		this.shortNo = shortNo;
	}

	public long getOrderTime() {
		return orderTime;
	}

	public void setOrderTime(long orderTime) {
		this.orderTime = orderTime;
	}

	public String getUserPhone() {
		return userPhone;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public int getSex() {
		return sex;
	}

	public void setSex(int sex) {
		this.sex = sex;
	}

	public int getIsOldUser() {
		return isOldUser;
	}

	public void setIsOldUser(int isOldUser) {
		this.isOldUser = isOldUser;
	}

	public int getTimes() {
		return times;
	}

	public void setTimes(int times) {
		this.times = times;
	}

	public int getIsReserve() {
		return isReserve;
	}

	public void setIsReserve(int isReserve) {
		this.isReserve = isReserve;
	}

	public long getReserveTime() {
		return reserveTime;
	}

	public void setReserveTime(long reserveTime) {
		this.reserveTime = reserveTime;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public int getSumNum() {
		return sumNum;
	}

	public void setSumNum(int sumNum) {
		this.sumNum = sumNum;
	}

	public int getIsPay() {
		return isPay;
	}

	public void setIsPay(int isPay) {
		this.isPay = isPay;
	}

	public float getSumFee() {
		return sumFee;
	}

	public void setSumFee(float sumFee) {
		this.sumFee = sumFee;
	}

	public float getDeliverFee() {
		return deliverFee;
	}

	public void setDeliverFee(float deliverFee) {
		this.deliverFee = deliverFee;
	}

	public float getBoxFee() {
		return boxFee;
	}

	public void setBoxFee(float boxFee) {
		this.boxFee = boxFee;
	}

	public int getBoxType() {
		return boxType;
	}

	public void setBoxType(int boxType) {
		this.boxType = boxType;
	}

	public int getBoxNum() {
		return boxNum;
	}

	public void setBoxNum(int boxNum) {
		this.boxNum = boxNum;
	}

	public String getOpTime() {
		return opTime;
	}

	public void setOpTime(String opTime) {
		this.opTime = opTime;
	}

	public long getSendTime() {
		return sendTime;
	}

	public void setSendTime(long sendTime) {
		this.sendTime = sendTime;
	}

	public String getOpStatus() {
		return opStatus;
	}

	public void setOpStatus(String opStatus) {
		this.opStatus = opStatus;
	}

	public List<Goods> getGoodsList() {
		return goodsList;
	}

	public void setGoodsList(List<Goods> goodsList) {
		this.goodsList = goodsList;
	}

	public List<Gift> getGiftList() {
		return giftList;
	}

	public void setGiftList(List<Gift> giftList) {
		this.giftList = giftList;
	}

	public List<Coupon> getCouponList() {
		return couponList;
	}

	public void setCouponList(List<Coupon> couponList) {
		this.couponList = couponList;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((oId == null) ? 0 : oId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		QueryOrderDetailsRes other = (QueryOrderDetailsRes) obj;
		if (oId == null) {
			if (other.oId != null)
				return false;
		} else if (!oId.equals(other.oId))
			return false;
		return true;
	}
	
	

}
