package com.woyou.activity;

import com.jing.biandang.R;
import com.woyou.bean.UserInfo;
import com.woyou.controller.LoginController;
import com.woyou.utils.LoginUtils;
import com.woyou.utils.NavigationbarUtils;
import com.woyou.utils.Utils;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

/**
 * 
 * 登陆页面
 * 
 * @author xuron
 * @versionCode 1 <每次修改提交前+1>
 */
public class LoginActivity extends SuperActivity implements OnClickListener {
	/**
	 * 登陆的LoginController
	 */
	public LoginController loginController;
	private UserInfo userInfo;
	// titlebar
	private TextView login_time;
	private TextView login_nettype;
	private ImageView login_neticon;

	// 内容
	private FrameLayout login_bg;
	private EditText login_id_et;
	private EditText login_uname_et;
	private EditText login_code_et;
	private TextView login_code_tv;
	private RelativeLayout login_submit;

	// 载入提示
	private RelativeLayout loading_hint;
	private ImageView loading_iv;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		loginController = new LoginController(this);
		setContentView(R.layout.layout_login);
		init();
		loginController.getNetTypeAndTime(login_time, login_nettype, login_neticon);
	}

	/**
	 * 初始化页面
	 */
	public void init() {
		login_bg = (FrameLayout) findViewById(R.id.layout_login_bg);
		login_time = (TextView) findViewById(R.id.layout_login_time);
		login_nettype = (TextView) findViewById(R.id.layout_login_nettype);
		login_neticon = (ImageView) findViewById(R.id.layout_login_neticon);
		login_id_et = (EditText) findViewById(R.id.layout_login_id_et);
		login_uname_et = (EditText) findViewById(R.id.layout_login_uname_et);
		login_code_et = (EditText) findViewById(R.id.layout_login_code_et);
		login_code_tv = (TextView) findViewById(R.id.layout_login_code_tv);
		login_submit = (RelativeLayout) findViewById(R.id.layout_login_submit);

		login_bg.setOnClickListener(this);
		login_code_tv.setOnClickListener(this);
		login_submit.setOnClickListener(this);

		// 加载提示
		loading_hint = (RelativeLayout) findViewById(R.id.layout_loading_hint);
		loading_iv = (ImageView) findViewById(R.id.layout_loading_iv);
		// 防止下层内容被点击
		loading_hint.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				return true;
			}
		});
		// 监听软键盘的完成按钮
		login_code_et.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				// 监听软键盘的完成按钮
				if (actionId == EditorInfo.IME_ACTION_DONE) {
					loginController.hideSoftInput();
					loginController.login(login_id_et, login_uname_et, login_code_et, loading_iv, loading_hint);
					return true;
				}
				return false;
			}
		});
		userInfo = LoginUtils.getUserInfo(mContext);
		if (userInfo != null && userInfo.getbId().equals("")) {
			// 读取TF卡中的用户信息
			userInfo = Utils.readUserInfo4TF();
		}
		// 读取SharedPreferences中的用户登录信息
		if (userInfo != null && !userInfo.getbId().equals("")) {
			login_id_et.setText(userInfo.getbId());
			login_uname_et.setText(userInfo.getuCode());
		}
		loginController.startAutoLogin(loading_iv, loading_hint);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.layout_login_bg:
			loginController.hideSoftInput();
			break;
		case R.id.layout_login_code_tv:
			loginController.getCode(loading_hint, loading_iv, login_nettype, login_id_et, login_uname_et);
			break;
		case R.id.layout_login_submit:
			loginController.avoidForceClick(v);
			loginController.login(login_id_et, login_uname_et, login_code_et, loading_iv, loading_hint);
			break;
		}
	}

	@Override
	protected void onStart() {
		super.onStart();
		NavigationbarUtils.getInstance(mContext).hide();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		loginController.onDestroy();
	}

}
