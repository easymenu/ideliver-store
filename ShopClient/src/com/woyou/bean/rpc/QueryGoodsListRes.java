package com.woyou.bean.rpc;

import java.util.ArrayList;
import java.util.List;

import com.woyou.bean.GoodsInfo;
import com.woyou.bean.SuperModel;

/**
 * 获取商品列表结果集
 * @author tlc
 *
 */
public class QueryGoodsListRes extends SuperModel {

	/**
	 * 商品列表
	 */
	private List<GoodsInfo> goodsList = new ArrayList<GoodsInfo>();

	public List<GoodsInfo> getGoodsList() {
		return goodsList;
	}

	public void setGoodsList(List<GoodsInfo> goodsList) {
		this.goodsList = goodsList;
	}
}
