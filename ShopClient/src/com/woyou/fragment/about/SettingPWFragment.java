package com.woyou.fragment.about;

import com.jing.biandang.R;
import com.woyou.activity.HomeActivity;
import com.woyou.component.PWSwitchButton;
import com.woyou.component.SuperUI;
import com.woyou.fragment.SuperFragment;
import com.woyou.utils.TypeJudgeTools;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class SettingPWFragment extends SuperFragment implements OnClickListener{
	private HomeActivity homeActivity;
	private View layout_pw;
	private ImageView layout_pw_lock;
	private PWSwitchButton layout_pw_switch;
	private EditText layout_pw_et;

	//数字按钮
	private TextView layout_pw_num0;
	private TextView layout_pw_num1;
	private TextView layout_pw_num2;
	private TextView layout_pw_num3;
	private TextView layout_pw_num4;
	private TextView layout_pw_num5;
	private TextView layout_pw_num6;
	private TextView layout_pw_num7;
	private TextView layout_pw_num8;
	private TextView layout_pw_num9;
	private ImageView layout_pw_clear;
	private ImageView layout_pw_backspace;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		layout_pw = inflater.inflate(R.layout.layout_setting_pw,container, false);
		homeActivity=(HomeActivity)getActivity();
		return layout_pw;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		initView();
	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		if(hidden){
			String settingPW=layout_pw_et.getText().toString();
			aCache.put("settingPW",settingPW);
			if(!TypeJudgeTools.isNull(settingPW) && settingPW.length()==4 && layout_pw_switch.isChecked())
			{
				SuperUI.openToast(homeActivity, "密码设置成功！");
				layout_pw_lock.setImageResource(R.raw.icon_pw_lock);
				aCache.put("settingPW",settingPW);
				aCache.put("isSetPW", "yes");
				homeActivity.isUnlock5Minute = false;
			}
		}
	}

	public void initView(){
		layout_pw_lock=(ImageView)layout_pw.findViewById(R.id.layout_pw_lock);
		layout_pw_switch=(PWSwitchButton)layout_pw.findViewById(R.id.layout_pw_switch);
		layout_pw_et=(EditText)layout_pw.findViewById(R.id.layout_pw_et);
		//数字
		layout_pw_num0=(TextView)layout_pw.findViewById(R.id.layout_pw_num0);
		layout_pw_num1=(TextView)layout_pw.findViewById(R.id.layout_pw_num1);
		layout_pw_num2=(TextView)layout_pw.findViewById(R.id.layout_pw_num2);
		layout_pw_num3=(TextView)layout_pw.findViewById(R.id.layout_pw_num3);
		layout_pw_num4=(TextView)layout_pw.findViewById(R.id.layout_pw_num4);
		layout_pw_num5=(TextView)layout_pw.findViewById(R.id.layout_pw_num5);
		layout_pw_num6=(TextView)layout_pw.findViewById(R.id.layout_pw_num6);
		layout_pw_num7=(TextView)layout_pw.findViewById(R.id.layout_pw_num7);
		layout_pw_num8=(TextView)layout_pw.findViewById(R.id.layout_pw_num8);
		layout_pw_num9=(TextView)layout_pw.findViewById(R.id.layout_pw_num9);
		layout_pw_clear=(ImageView)layout_pw.findViewById(R.id.layout_pw_clear);
		layout_pw_backspace=(ImageView)layout_pw.findViewById(R.id.layout_pw_backspace);
		
		layout_pw_num0.setOnClickListener(this);
		layout_pw_num1.setOnClickListener(this);
		layout_pw_num2.setOnClickListener(this);
		layout_pw_num3.setOnClickListener(this);
		layout_pw_num4.setOnClickListener(this);
		layout_pw_num5.setOnClickListener(this);
		layout_pw_num6.setOnClickListener(this);
		layout_pw_num7.setOnClickListener(this);
		layout_pw_num8.setOnClickListener(this);
		layout_pw_num9.setOnClickListener(this);
		layout_pw_clear.setOnClickListener(this);
		layout_pw_backspace.setOnClickListener(this);
		initData();
		//监听密码锁
		layout_pw_switch.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				String settingPW=layout_pw_et.getText().toString();
				if(TypeJudgeTools.isNull(settingPW)){
					SuperUI.openToast(homeActivity, "密码不能为空！");
					layout_pw_switch.setChecked(false);
					return;
				}
				if(settingPW.length()!=4)
				{
					SuperUI.openToast(homeActivity, "老板锁未开启，必须输入4位密码哦！");
					layout_pw_switch.setChecked(false);
					return;
				}
				if(isChecked){
					SuperUI.openToast(homeActivity, "已锁上");
					layout_pw_lock.setImageResource(R.raw.icon_pw_lock);
					aCache.put("settingPW",settingPW);
					aCache.put("isSetPW", "yes");
					homeActivity.isUnlock5Minute = false;
				}else{
					SuperUI.openToast(homeActivity, "已解锁");
					layout_pw_lock.setImageResource(R.raw.icon_pw_unlock);
					aCache.remove("settingPW");
					aCache.put("isSetPW", "no");
				}
			}
		});
	}
	/**
	 * 初始化页面
	 */
	public void initData(){
		//判断是否设置了密码
		if(TypeJudgeTools.isNull(aCache.getAsString("isSetPW"))){
			//未设置密码
			layout_pw_switch.setChecked(false);
			layout_pw_lock.setImageResource(R.raw.icon_pw_unlock);
		}else{
			if(aCache.getAsString("isSetPW").equals("no")){
				//未设置密码
				layout_pw_switch.setChecked(false);
				layout_pw_lock.setImageResource(R.raw.icon_pw_unlock);
			}else{
				//已设置密码
				layout_pw_switch.setChecked(true);
				layout_pw_lock.setImageResource(R.raw.icon_pw_lock);
				layout_pw_et.setText(aCache.getAsString("settingPW"));
			}
		}
	}

	@Override
	public void onClick(View v) {
		String pw=layout_pw_et.getText().toString();
		switch(v.getId()){
		case R.id.layout_pw_num0:
			layout_pw_et.setText(pw+"0");
			break;
		case R.id.layout_pw_num1:
			layout_pw_et.setText(pw+"1");
			break;
		case R.id.layout_pw_num2:
			layout_pw_et.setText(pw+"2");
			break;
		case R.id.layout_pw_num3:
			layout_pw_et.setText(pw+"3");
			break;
		case R.id.layout_pw_num4:
			layout_pw_et.setText(pw+"4");
			break;
		case R.id.layout_pw_num5:
			layout_pw_et.setText(pw+"5");
			break;
		case R.id.layout_pw_num6:
			layout_pw_et.setText(pw+"6");
			break;
		case R.id.layout_pw_num7:
			layout_pw_et.setText(pw+"7");
			break;
		case R.id.layout_pw_num8:
			layout_pw_et.setText(pw+"8");
			break;
		case R.id.layout_pw_num9:
			layout_pw_et.setText(pw+"9");
			break;
		case R.id.layout_pw_clear:
			layout_pw_switch.setChecked(false);
			layout_pw_et.setText("");
			break;
		case R.id.layout_pw_backspace:
			if(!TypeJudgeTools.isNull(pw)){
				pw=pw.substring(0, pw.length()-1);
				layout_pw_et.setText(pw);
				String newpw=layout_pw_et.getText().toString();
				if(!TypeJudgeTools.isNull(newpw) && newpw.length()<4){
					layout_pw_switch.setChecked(false);
					layout_pw_lock.setImageResource(R.raw.icon_pw_unlock);
					aCache.put("settingPW",newpw);
					aCache.put("isSetPW", "no");
				}
			}
			break;
		}
	}
	
}
