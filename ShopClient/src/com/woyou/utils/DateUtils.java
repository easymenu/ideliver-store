package com.woyou.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import android.content.Context;

/**
 * 日期工具类
 * @author longtao.li
 *
 */
public class DateUtils {
	

	/**
	 * 获取格林威治时间(unix时间戳 即1970年至今的秒数)
	 * 
	 */
	public static long getGMTime() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		sdf.setTimeZone(TimeZone.getTimeZone("Etc/Greenwich"));
//		System.out.println("The current time in Greenwich is: "+sdf.format(new Date());
		String format = sdf.format(new Date());
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date gmDate = null;
		try {
			gmDate = sdf1.parse(format);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return gmDate.getTime()/1000;
	}
	
	/**
	 * 获取格林威治时间(unix时间戳 即1970年至今的秒数)
	 * 
	 */
	public static long _getGMTime(Context context) {
		int round = 0;
		ACache aCache = ACache.get(context);
		String ServerTime = aCache.getAsString("ServerTime");
		if (!TypeJudgeTools.isNull(ServerTime)) {
			long stime = Long.valueOf(ServerTime);
			round = (int) (stime/1000);
		}
		return round;
	}

	/**
	 * 获取时间时间
	 * @return
	 */
	public static String getCurrentTime() {
		String time = null;
		long round = System.currentTimeMillis()/1000;
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String date = sdf.format(round * 1000);
		String[] split = date.split("\\s");
		if ( split.length > 1 ){
			time = split[1];
		}
		return time;
	}
	
	
	/**
	 * 得到昨天的日期
	 * @return
	 */
	public static String getYestoryDate() {
		Calendar calendar = Calendar.getInstance();  
		calendar.add(Calendar.DATE,-1);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String yestoday = sdf.format(calendar.getTime());
		return yestoday;
	}
	
	/**
	 * 得到昨天的日期
	 * @return
	 */
	public static long getYestoryDateTime() {
		Calendar calendar = Calendar.getInstance();  
		calendar.add(Calendar.DATE,-1);
		long timeInMillis = calendar.getTimeInMillis();
		return timeInMillis;
	}
	
	/**
	 * 得到今天的日期
	 * @return
	 */
	public static  String getTodayDate(){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String date = sdf.format(new Date());
		return date;
	}
	
	/**
	 * 时间戳转化为时间格式
	 * @param timeStamp
	 * @return
	 */
	public static String timeStampToStr(long timeStamp) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String date = sdf.format(timeStamp * 1000);
		return date;
	}
	
	/**
	 * 得到日期   yyyy-MM-dd
	 * @param timeStamp  时间戳
	 * @return
	 */
	public static String formatDate(long timeStamp) {   
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String date = sdf.format(timeStamp*1000);
		return date;
	}
	
	/**
	 * 得到时间  HH:mm:ss
	 * @param timeStamp   时间戳
	 * @return
	 */
	public static String getTime(long timeStamp) {  
		String time = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String date = sdf.format(timeStamp * 1000);
		String[] split = date.split("\\s");
		if ( split.length > 1 ){
			time = split[1];
		}
		return time;
	}
	
	/**
	 * 将一个时间戳转换成提示性时间字符串，如刚刚，1秒前
	 * 
	 * @param timeStamp
	 * @return
	 */
	public static String convertTimeToFormat(long timeStamp) {
		long curTime =System.currentTimeMillis() / (long) 1000 ;
		long time = curTime - timeStamp;

		if (time < 60 && time >= 0) {
			return "刚刚";
		} else if (time >= 60 && time < 3600) {
			return time / 60 + "分钟前";
		} else if (time >= 3600 && time < 3600 * 24) {
			return time / 3600 + "小时前";
		} else if (time >= 3600 * 24 && time < 3600 * 24 * 30) {
			return time / 3600 / 24 + "天前";
		} else if (time >= 3600 * 24 * 30 && time < 3600 * 24 * 30 * 12) {
			return time / 3600 / 24 / 30 + "个月前";
		} else if (time >= 3600 * 24 * 30 * 12) {
			return time / 3600 / 24 / 30 / 12 + "年前";
		} else {
			return "刚刚";
		}
	}
	
	/**
	 * 将一个时间戳转换成提示性时间字符串，(多少分钟)
	 * 
	 * @param timeStamp
	 * @return
	 */
	public static String timeStampToFormat(long timeStamp) {
		long curTime =System.currentTimeMillis() / (long) 1000 ;
		long time = curTime - timeStamp;
		return time/60 + "";
	}
	
	/**
	 * 获得当前时间差
	 * @param timeStamp
	 * @return
	 */
	public static int  nowCurrentTime( long timeStamp ) {
		long curTime =System.currentTimeMillis() / (long) 1000 ;
		long time = timeStamp - curTime;
		return (int) time;
	}
	
	/**
	 * 将标准时间格式HH:mm:ss化为当前的时间差值
	 * @param str
	 * @return
	 */
	public static String StandardFormatStr(String str){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
			Date d = sdf.parse(str);
			long timeStamp = d.getTime();
			
			long curTime =System.currentTimeMillis() / (long) 1000 ;
			long time = curTime - timeStamp/1000;
			return time/60 + "";
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

}
