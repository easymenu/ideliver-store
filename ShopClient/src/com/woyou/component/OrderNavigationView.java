/** 
 * @Title:  OrderNavigationView.java 
 * @author:  xuron
 * @data:  2015年11月27日 下午3:24:21 <创建时间>
 * 
 * @history：<以下是历史记录>
 *
 * @modifier: <修改人>
 * @modify date: 2015年11月27日 下午3:24:21 <修改时间>
 * @log: <修改内容>
 *
 * @modifier: <修改人>
 * @modify date: 2015年11月27日 下午3:24:21 <修改时间>
 * @log: <修改内容>
 */
package com.woyou.component;

import com.jing.biandang.R;
import com.woyou.activity.HomeActivity;
import com.woyou.listener.ItemClickListener;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

/** 
 * 自定义订单导航栏
 * @author xuron 
 * @versionCode 1 <每次修改提交前+1>
 */
public class OrderNavigationView extends SuperLayout {
	private RelativeLayout layout_order_unsend_bar;
	private RelativeLayout layout_order_sended_bar;
	private RelativeLayout layout_order_invalid_bar;
	private RelativeLayout layout_order_search_bar;
	// 滚动条
	public ImageView layout_order_navi_iv;
	public int screenWidth;

	public OrderNavigationView(Context context) {
		super(context);
		initView();
	}

	public OrderNavigationView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initView();
	}
	
	public OrderNavigationView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initView();
	}

	@Override
	protected void initView() {
		View.inflate(mContext, R.layout.layout_ordernavigation, this);
	}

	/**
	 * 重新组装
	 * @param homeActivity
	 * @param viewPager
	 */
	public void refreshView(HomeActivity homeActivity,ViewPager viewPager){
		// 计算位置
		DisplayMetrics dm = new DisplayMetrics();
		homeActivity.getWindowManager().getDefaultDisplay().getMetrics(dm);
		screenWidth = dm.widthPixels;

		layout_order_navi_iv = (ImageView)findViewById(R.id.layout_order_navi_iv);
		android.view.ViewGroup.LayoutParams layoutParams = layout_order_navi_iv.getLayoutParams();
		layoutParams.width = screenWidth / 4;
		layout_order_navi_iv.setLayoutParams(layoutParams);
		// 导航菜单
		layout_order_unsend_bar = (RelativeLayout)findViewById(R.id.layout_order_unsend_bar);
		layout_order_sended_bar = (RelativeLayout)findViewById(R.id.layout_order_sended_bar);
		layout_order_invalid_bar = (RelativeLayout)findViewById(R.id.layout_order_invalid_bar);
		layout_order_search_bar = (RelativeLayout)findViewById(R.id.layout_order_search_bar);
		// 设置导航监听器
		layout_order_unsend_bar.setOnClickListener(new ItemClickListener(0,viewPager));
		layout_order_sended_bar.setOnClickListener(new ItemClickListener(1,viewPager));
		layout_order_invalid_bar.setOnClickListener(new ItemClickListener(2,viewPager));
		layout_order_search_bar.setOnClickListener(new ItemClickListener(3,viewPager));
	}
}
