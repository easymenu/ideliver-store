package com.woyou.bean.rpc;

import java.util.ArrayList;
import java.util.List;

import com.woyou.bean.Order;
import com.woyou.bean.SuperModel;

/**
 * 返回订单的bean
 * 
 * @author 荣
 * 
 */
public class QueryOrderRes extends SuperModel {
	/**
	 * 未送订单列表
	 */
	private List<Order> noSent = new ArrayList<Order>();
	/**
	 * 已送订单列表
	 */
	private List<Order> sent = new ArrayList<Order>();
	/**
	 * 无效订单列表
	 */
	private List<Order> invalid = new ArrayList<Order>();

	public List<Order> getNoSent() {
		return noSent;
	}

	public void setNoSent(List<Order> noSent) {
		this.noSent = noSent;
	}

	public List<Order> getSent() {
		return sent;
	}

	public void setSent(List<Order> sent) {
		this.sent = sent;
	}

	public List<Order> getInvalid() {
		return invalid;
	}

	public void setInvalid(List<Order> invalid) {
		this.invalid = invalid;
	}

}
