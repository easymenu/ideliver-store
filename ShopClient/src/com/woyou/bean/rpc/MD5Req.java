package com.woyou.bean.rpc;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.woyou.Constants;
import com.woyou.utils.DateUtils;
import com.woyou.utils.DesUtils;
import com.woyou.utils.MD5Utils;

/**
 * 请求加密实体
 * 
 * @author longtao.li
 * 
 */
public class MD5Req<T> {
	public MD5Req(Context context,T params) {
		this.params = params;
		md5(context);
	}

	public MD5Req(Context context,T params, String isEncrypted) {
		this.params = params;
		this.isEncrypted = isEncrypted;
		md5(context);
	}

	public MD5Req(Context context) {
		md5(context);
	}

	public T params=null; // 请求携带的数据

	public String jsonParams=""; // params的json字符串

	public String isEncrypted = "1"; // 是否加密

	public String timeStamp=""; // 时间戳

	public String randomNum = ""; // 随机数(6位)

	public String sign=""; // md5加密后的字符串

	// 计算方法：
	// MD5(Parameter+IsEncrypted+TimeStamp+RandomNum+MD5(Key))
	private void md5(Context context) { 
		if( TextUtils.isEmpty(timeStamp) ){
			timeStamp = DateUtils._getGMTime(context)+"";
		}
		while(randomNum.length()<6)
			randomNum+=(int)(Math.random()*10);
	
		if(params!=null){
			jsonParams = new Gson().toJson(params);
			jsonParams = jsonParams.replace(" ", "");
			if( isEncrypted.equals("1") ){
				try {
					jsonParams = DesUtils.encode(DesUtils.key, jsonParams);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		
		sign = jsonParams + isEncrypted + timeStamp +randomNum + MD5Utils.md5(Constants.IDELIVER_KEY);
		sign = MD5Utils.md5(sign);
	}

}
