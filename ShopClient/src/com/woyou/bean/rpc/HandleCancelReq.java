package com.woyou.bean.rpc;

import com.woyou.bean.SuperModel;

/**
 * 同意或拒绝取消订单请求bean
 * 
 * @author 荣
 * 
 */
public class HandleCancelReq extends SuperModel {
	/**
	 * 商铺Id
	 */
	private String sId = "";
	/**
	 * 密码
	 */
	private String pwd = "";
	/**
	 * 订单ID
	 */
	private String oId = "";
	/**
	 * 订单结果（1：同意取消订单2：拒绝取消订单）
	 */
	private int handle;

	public String getsId() {
		return sId;
	}

	public void setsId(String sId) {
		this.sId = sId;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getoId() {
		return oId;
	}

	public void setoId(String oId) {
		this.oId = oId;
	}

	public int getHandle() {
		return handle;
	}

	public void setHandle(int handle) {
		this.handle = handle;
	}

}
