package com.woyou.adapter;

import java.util.List;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jing.biandang.R;
import com.woyou.bean.BoxFee;
import com.woyou.bean.Coupon;
import com.woyou.bean.DeliverFee;
import com.woyou.bean.Gift;
import com.woyou.bean.Goods;
import com.woyou.bean.Remark;
import com.woyou.utils.FormatTools;

public class OrderDetailsAdapter extends BaseAdapter{
	private LayoutInflater layoutInflater;
	private List<Object> data;
	private Context context;

	public OrderDetailsAdapter(Context c, List<Object> data) {
		layoutInflater = LayoutInflater.from(c);
		context = c;
		this.data = data;
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder=null;
		
		if(convertView==null){
			holder=new ViewHolder();
			convertView = layoutInflater.inflate(R.layout.item_orderdetails_lv,null);
			holder.view=(LinearLayout)convertView.findViewById(R.id.item_orderdetails_view);
			holder.name=(TextView)convertView.findViewById(R.id.item_orderdetails_name);
			holder.num=(TextView)convertView.findViewById(R.id.item_orderdetails_num);
			holder.sum=(TextView)convertView.findViewById(R.id.item_orderdetails_sum);
			holder.price=(TextView)convertView.findViewById(R.id.item_orderdetails_price);
			holder.nature=(TextView)convertView.findViewById(R.id.item_orderdetails_nature);
			
			holder.layout_remark=(RelativeLayout)convertView.findViewById(R.id.layout_details_remark_r);
			holder.remark=(TextView)convertView.findViewById(R.id.layout_details_remark);
			convertView.setTag(holder);
		}else{
			holder = (ViewHolder) convertView.getTag();
		}
		holder.view.setVisibility(View.VISIBLE);
		holder.layout_remark.setVisibility(View.GONE);
		//判断商品属性是否能显示
		holder.nature.setVisibility(View.GONE);
		Object obj = data.get(position);
		if(obj instanceof Goods){
			Goods goods=(Goods)obj;
			holder.name.setText(""+goods.getgName());
			holder.num.setText("×"+FormatTools.String2Money(goods.getSaleNum()+""));
			holder.sum.setText(Html.fromHtml("<small><small>¥</small></small>"+FormatTools.String2Money(goods.getSaleFee()+"")));
			//计算单价和属性附加价
			holder.price.setText(Html.fromHtml("<small><small>¥</small></small>"+FormatTools.String2Money(goods.getPrice()+"")+"/<small>"+goods.getUnit()+"<small>"));
			holder.nature.setVisibility(View.VISIBLE);
			holder.nature.setText(""+goods.getProps());
		}else if(obj instanceof DeliverFee){
			DeliverFee deliverFee=(DeliverFee)obj;
			holder.name.setText("外送费");
			holder.num.setText("");
			holder.sum.setText(Html.fromHtml("<small><small>¥</small></small>"+FormatTools.String2Money(deliverFee.getPrice()+"")));
			holder.price.setText("");
		}else if(obj instanceof BoxFee){
			BoxFee boxFee=(BoxFee)obj;
			holder.name.setText("餐盒费");
			//(0：不算1：按单算,2:按份算）
			if(boxFee.getType()==0){
				holder.num.setText("");
				holder.sum.setText("");
				holder.price.setText("");
			}else if(boxFee.getType()==1){
				holder.num.setText("");
				holder.sum.setText(Html.fromHtml("<small><small>¥</small></small>"+FormatTools.String2Money(boxFee.getPrice()+"")));
				holder.price.setText("");
			}else if(boxFee.getType()==2){
				holder.num.setText("");
				holder.sum.setText(Html.fromHtml("<small><small>¥</small></small>"+FormatTools.String2Money(boxFee.getPrice()+"")));
				holder.price.setText("");
			}
		}else if(obj instanceof Gift){
			Gift gift=(Gift)obj;
			holder.name.setText(""+gift.getgName());
			holder.num.setText("×"+FormatTools.String2Money(gift.getSaleNum()+""));
			float money=gift.getPrice();
			if(money<0){
				holder.sum.setText(Html.fromHtml("-<small><small>¥</small></small>"+FormatTools.Float2Money(Math.abs(money))));
			}else if(money==0){
				holder.sum.setText(Html.fromHtml("赠送"));
			}else{
				holder.sum.setText(Html.fromHtml("<small><small>¥</small></small>"+FormatTools.Float2Money(Math.abs(money))));
			}
			//计算单价和属性附加价
			holder.price.setText("");
		}else if(obj instanceof Coupon){
			Coupon coupon=(Coupon)obj;
			holder.name.setText(""+coupon.getcName());
			holder.num.setText("");
			holder.sum.setText(Html.fromHtml("-<small><small>¥</small></small>"+FormatTools.String2Money(coupon.getValue())));
			//计算单价和属性附加价
			holder.price.setText("");
		}else if(obj instanceof Remark){
			//备注
			holder.view.setVisibility(View.GONE);
			holder.layout_remark.setVisibility(View.VISIBLE);
			Remark remark=(Remark)obj;
			holder.remark.setText(""+remark.getRemark());
		}

		return convertView;
	}
	/**
	 * 数据缓存
	 * @author lenovo
	 *
	 */
	public static class ViewHolder {
		LinearLayout view;
		TextView name;
		TextView num;
		TextView sum;
		TextView price;
		TextView nature;
		
		RelativeLayout layout_remark;
		TextView remark;
	}
}
