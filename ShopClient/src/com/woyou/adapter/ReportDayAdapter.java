package com.woyou.adapter;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.jing.biandang.R;

public class ReportDayAdapter extends BaseAdapter{
	private LayoutInflater layoutInflater;
	private ArrayList<HashMap<String, Object>> data;

	public ReportDayAdapter(Context c, ArrayList<HashMap<String, Object>> data) {
		layoutInflater = LayoutInflater.from(c);
		this.data = data;
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;
		if (convertView == null) {
			viewHolder=new ViewHolder();
			convertView = layoutInflater.inflate(R.layout.item_report_day_lv,null);
			viewHolder.item_report_day_tv=(TextView)convertView.findViewById(R.id.item_report_day_tv);
			convertView.setTag(viewHolder);
		}else{
			viewHolder=(ViewHolder) convertView.getTag();
		}
		viewHolder.item_report_day_tv.setText(data.get(position).get("date").toString());
		return convertView;
	}
	
	static class ViewHolder {
		TextView item_report_day_tv;
	}

}
