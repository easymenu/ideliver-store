package com.woyou.bean.rpc;

import com.woyou.bean.SuperModel;

/**
 * queryOrderDetails
 * 
 * @author 荣
 * 
 */
public class QueryOrderDetailsReq extends SuperModel {
	/**
	 * 商铺Id
	 */
	private String sId = "";
	/**
	 * 密码
	 */
	private String pwd = "";
	/**
	 * 订单Id
	 */
	private String oId = "";

	public String getsId() {
		return sId;
	}

	public void setsId(String sId) {
		this.sId = sId;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getoId() {
		return oId;
	}

	public void setoId(String oId) {
		this.oId = oId;
	}

}
