package com.woyou.bean;


/**
 * 商品统计
 * @author zhou.ni
 *
 */
public class GoodsCount extends SuperModel {

	private static final long serialVersionUID = 1L;
	
	private String gName;			//商品名字
	private int gNum;				//下单量
	private float gPrice;			//消费总金额
	
	public String getgName() {
		return gName;
	}
	public void setgName(String gName) {
		this.gName = gName;
	}
	public int getgNum() {
		return gNum;
	}
	public void setgNum(int gNum) {
		this.gNum = gNum;
	}
	public float getgPrice() {
		return gPrice;
	}
	public void setgPrice(float gPrice) {
		this.gPrice = gPrice;
	}
	
	@Override
	public String toString() {
		return "GoodsCount [gName=" + gName + ", gNum=" + gNum + ", gPrice="
				+ gPrice + "]";
	}
	
}
