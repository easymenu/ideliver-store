package com.woyou.bean.rpc;

import com.woyou.bean.SuperModel;
/**
 * 获取服务器时间
 * @author 荣
 *
 */
public class ServerTimeRes extends SuperModel {
	/**
	 * 时间戳Unix时间戳
	 */
	private long sTime;

	public long getsTime() {
		return sTime;
	}

	public void setsTime(long sTime) {
		this.sTime = sTime;
	}

}
