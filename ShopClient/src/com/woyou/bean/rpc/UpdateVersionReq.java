package com.woyou.bean.rpc;

import com.woyou.bean.SuperModel;

/**
 * 应用检查更新请求
 * @author tlc
 *
 */
public class UpdateVersionReq extends SuperModel {
	
	/**
	 * 软件版本号
	 */
	private String version = "";
	/**
	 * 应用名称（BD_VERSION2）
	 */
	private String appName = "";
	/**
	 * 主板类型
	 */
	private String boardType = "";
	/**
	 * 店铺Id
	 */
	private String sId = "";
	/**
	 * 密码（随机自动生成6位数字）
	 */
	private String pwd = "";
	
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getAppName() {
		return appName;
	}
	public void setAppName(String appName) {
		this.appName = appName;
	}
	public String getBoardType() {
		return boardType;
	}
	public void setBoardType(String boardType) {
		this.boardType = boardType;
	}
	public String getsId() {
		return sId;
	}
	public void setsId(String sId) {
		this.sId = sId;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
}
