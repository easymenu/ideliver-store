package com.woyou.adapter;

import java.util.List;

import android.content.Context;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.jing.biandang.R;
import com.woyou.bean.UserReportItem;
import com.woyou.utils.FormatTools;

/**
 * 顾客分析的适配器
 * @author zhou.ni
 *
 */
public class CustomerAdapter extends BaseAdapter {
	private LayoutInflater layoutInflater;
	private List<UserReportItem> data;

	public CustomerAdapter(Context c, List<UserReportItem> data) {
		layoutInflater = LayoutInflater.from(c);
		this.data = data;
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder holder=null;
		
		if(convertView==null){
			holder=new ViewHolder();
			convertView = layoutInflater.inflate(R.layout.item_customer_lv,null);
			holder.name=(TextView)convertView.findViewById(R.id.layout_customer_name);
			holder.phone=(TextView)convertView.findViewById(R.id.layout_customer_phone);
			holder.ordernum=(TextView)convertView.findViewById(R.id.layout_customer_ordernum);
			holder.orderprice=(TextView)convertView.findViewById(R.id.layout_customer_ordervalue);
			convertView.setTag(holder);
		}else{
			holder = (ViewHolder) convertView.getTag();
		}
		
		UserReportItem item = data.get(position);
		if ( !TextUtils.isEmpty(item.getuName()) ){
			holder.name.setText(item.getuName());
		} else {
			holder.name.setText("");
		}
		
		if ( !TextUtils.isEmpty(item.getPhone()) ) {
			holder.phone.setText(item.getPhone());
		} else {
			holder.phone.setText("");
		}
		
		holder.ordernum.setText(Html.fromHtml(item.getSumNum() + "<small>单</small>"));
		
		holder.orderprice.setText(Html.fromHtml("<small>¥</small> "+FormatTools.String2Money(item.getSumFee()+"")+"<small>元</small>"));
		
		return convertView;
	}
	/**
	 * 数据缓存
	 * @author lenovo
	 *
	 */
	public static class ViewHolder {
		TextView name;
		TextView phone;
		TextView ordernum;
		TextView orderprice;
	}

}
