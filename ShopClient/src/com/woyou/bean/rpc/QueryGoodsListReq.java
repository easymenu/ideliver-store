package com.woyou.bean.rpc;

import com.woyou.bean.SuperModel;

/**
 * 获取商品列表请求
 * @author tlc
 *
 */
public class QueryGoodsListReq  extends SuperModel{

	/**
	 * 商铺的Id
	 */
	private String sId = "";
	/**
	 * 商品类别Id(-1:获取全部暂缺商品)
	 */
	private String tId = "";
	/**
	 * 密码
	 */
	private String pwd = "";
	
	public String getsId() {
		return sId;
	}
	public void setsId(String sId) {
		this.sId = sId;
	}
	public String gettId() {
		return tId;
	}
	public void settId(String tId) {
		this.tId = tId;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
}
