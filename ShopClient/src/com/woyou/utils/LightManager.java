package com.woyou.utils;

import android.util.Log;


public class LightManager {
	//打开jni文件
	static {
        mPosCtrlOp = new NewPosCtrl();
        turnOnOffLight();
    }
	private static NewPosCtrl mPosCtrlOp;
	
	//灯泡控制全局变量
	private static boolean blueLightFlag= false;		//蓝灯控制变量
	private static boolean redLightFlag= false;       //红灯控制变量
	private static boolean redAndBlueFlag = false;
	
	public static void turnOnRedLight(boolean flag){
		blueLightFlag = flag;
	}
	
	public static void turnOnBlueLight(boolean flag){
		redLightFlag = flag;
	}
	
	public static void turnOnRedBlueLight(boolean flag){
		redAndBlueFlag = flag;
	}
	
	//控制灯泡闪动
	private static void turnOnOffLight(){
		ThreadPoolManager.getInstance().executeTask (new Runnable(){
				@Override
				public void run() {
					try {
						while(true) {
							
							if(redAndBlueFlag){
								
								mPosCtrlOp.trunOnoffRedlight(true);
								Thread.sleep(150);
								mPosCtrlOp.trunOnoffYellowlight(false);
								Thread.sleep(150);
								mPosCtrlOp.trunOnoffRedlight(false);
								Thread.sleep(150);
								mPosCtrlOp.trunOnoffYellowlight(true);
								Thread.sleep(150);
								
							}else{
							
								//控制蓝色灯闪动
								if(blueLightFlag) {
									if (mPosCtrlOp.isRedlightOn()) {
			            				mPosCtrlOp.trunOnoffRedlight(false);
			            			} else {
			            				mPosCtrlOp.trunOnoffRedlight(true);
			            			}
								} else {
									if (mPosCtrlOp.isRedlightOn()) {
			            				mPosCtrlOp.trunOnoffRedlight(false);
									}
								}
								
								//控制红色灯闪动
								if(redLightFlag) {
									if (mPosCtrlOp.isYellowlightOn()) {
			            				mPosCtrlOp.trunOnoffYellowlight(false);
			            			} else {
			            				mPosCtrlOp.trunOnoffYellowlight(true);
			            			}
								}
								else {
									if (mPosCtrlOp.isYellowlightOn()) {
			            				mPosCtrlOp.trunOnoffYellowlight(false);
									}
								}
								//停顿0.5秒
								Thread.sleep(500);
							
							}
						}
					}
					catch(Exception e) {
						Log.e("LightUtil", e.getMessage(),e);
					}
				}
		});
	}

}