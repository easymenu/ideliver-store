package com.woyou.adapter;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.jing.biandang.R;
import com.woyou.activity.HomeActivity;
import com.woyou.bean.Order;
import com.woyou.bean.WYList;
import com.woyou.bean.rpc.Result;
import com.woyou.bean.rpc.SendOrderReq;
import com.woyou.bean.rpc.SendOrderRes;
import com.woyou.event.EventSendOrder;
import com.woyou.fragment.order.OrderFragment;
import com.woyou.model.rpc.OrderModel;
import com.woyou.utils.ACache;
import com.woyou.utils.FormatTools;
import com.woyou.utils.LoginUtils;
import com.woyou.utils.ThreadPoolManager;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import de.greenrobot.event.EventBus;

public class UnsendOrderAdapter extends BaseAdapter {
	private HomeActivity homeActivity;
	private OrderFragment orderFragment;
	private LayoutInflater layoutInflater;
	private List<Order> data;
	public ACache aCache;
	private OrderModel orderModel;

	public UnsendOrderAdapter(final HomeActivity homeActivity, final OrderFragment orderFragment,List<Order> data) {
		orderModel = OrderModel.getInstance(homeActivity);
		aCache=ACache.get(homeActivity);
		this.homeActivity = homeActivity;
		this.orderFragment = orderFragment;
		layoutInflater = LayoutInflater.from(homeActivity);
		this.data = data;
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		Order order = data.get(position);
		ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = layoutInflater.inflate(R.layout.item_order_unsend_lv,null);
			holder.bg=(RelativeLayout)convertView.findViewById(R.id.item_order_unsend_bg);
			holder.num = (TextView) convertView.findViewById(R.id.item_order_unsend_num);
			holder.name = (TextView) convertView.findViewById(R.id.item_order_unsend_name);
			holder.phone = (TextView) convertView.findViewById(R.id.item_order_unsend_phone);
			holder.price = (TextView) convertView.findViewById(R.id.item_order_unsend_price);
			holder.content = (TextView) convertView.findViewById(R.id.item_order_unsend_content);
			holder.time = (TextView) convertView.findViewById(R.id.item_order_unsend_time);
			holder.send = (Button) convertView.findViewById(R.id.item_order_unsend_send);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.num.setText(order.getShortNo());
		holder.name.setText(order.getContact());
		holder.phone.setText(order.getPhone());
		holder.price.setText(Html.fromHtml("<small>￥</small>" + FormatTools.String2Money(order.getPrice()+"")));
		holder.content.setText(order.getGoodsSummary()+" -> "+order.getAddrName());
		
		// 判断是预约单还是及时单
		int type = order.getIsReserve();
		if (type==1) {
			String ftime = FormatTools.convertTimeToHtml(homeActivity,order.getOrderTime()*1000);
			holder.time.setText(Html.fromHtml(ftime));
			holder.time.setTextColor(0xffcf0000);
		}else{
			// 比较预约日期与当前日期的大小
			Date nowDate = new Date();
			if(order.getReserveTime()*1000>nowDate.getTime()){
				// 预约单待送出未超时
				holder.time.setText(Html.fromHtml(FormatTools.convertTimeToHint(homeActivity,order.getReserveTime()*1000)));
				holder.time.setTextColor(0xff63ac7c);
			}else{
				// 预约单待送出超时
				String ftime = FormatTools.convertTimeToHtml(homeActivity,order.getReserveTime()*1000);
				holder.time.setText(Html.fromHtml(ftime));
				holder.time.setTextColor(0xffcf0000);
			}
		
		}

		holder.bg.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// 将未送出订单信息存储到ACache中
				WYList<HashMap<String, String>> orderList = new WYList<HashMap<String, String>>();
				HashMap<String, String> map;
				for (int i = 0; i < data.size(); i++) {
					map = new HashMap<String, String>();
					map.put("No", data.get(i).getoId());
					orderList.add(map);
				}
				// 存储未送订单信息和点击item的位置信息
				aCache.put("OrderList", orderList);
				aCache.put("Position", position + "");
				// 先获取订单详情再跳转到订单详情页面
				orderFragment.orderController.jumpToOrderDetails(position, orderList);
			}
		});
		holder.send.setTag(order);
		holder.send.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				final Order order=(Order)v.getTag();
				ThreadPoolManager.getInstance().executeTask(new Runnable() {
					@Override
					public void run() {
						try{
							homeActivity.showLoadingHint();
							SendOrderReq sendOrderReq=new SendOrderReq();
							sendOrderReq.setsId(LoginUtils.getUserInfo(homeActivity).getsId());
							sendOrderReq.setPwd(LoginUtils.getUserInfo(homeActivity).getPwd());
							sendOrderReq.setoId(order.getoId());
							Result<SendOrderRes> result=orderModel.sendOrder(sendOrderReq);
							if(result!=null&&result.getCode()==1){
								EventBus.getDefault().post(new EventSendOrder(result.getData()));
							}
						}catch(Exception e){
							e.printStackTrace();
						}finally{
							homeActivity.hideLoadingHint();
						}
					}
				});
			}
		});
		return convertView;
	}

	/**
	 * 暴露未送订单接口
	 * 
	 * @author lenovo
	 * 
	 */
	public interface SendOrderImpl {
		public void HandleSendOrder();
	}

	/**
	 * ViewHolder
	 * 
	 * @author lenovo
	 * 
	 */
	static class ViewHolder {
		RelativeLayout bg;
		TextView num;
		TextView name;
		TextView phone;
		TextView price;
		TextView content;
		TextView time;
		Button send;
	}
}
