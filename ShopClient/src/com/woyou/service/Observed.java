package com.woyou.service;
/**
 * 自定义一个被观察者
 * @author lenovo
 *
 */
public interface Observed {
	/**
	 * 注册观察者
	 * @param o
	 */
	public void registerObserver(Observer o);
	/**
	 * 移除观察者
	 * @param o
	 */
	public void removeObserver(Observer o);
	/**
	 * 通知观察者
	 */
	public void notifyObserver(int type,String s);
}
