package com.woyou.model.rpc;

import java.util.ArrayList;
import java.util.List;

import com.woyou.bean.Order;
import com.woyou.bean.PlayMoney;
import com.woyou.bean.StayPay;
import com.woyou.bean.rpc.HandleCancelReq;
import com.woyou.bean.rpc.HandleCancelRes;
import com.woyou.bean.rpc.HandleOrderReq;
import com.woyou.bean.rpc.MD5Req;
import com.woyou.bean.rpc.QueryNewOidListReq;
import com.woyou.bean.rpc.QueryNewOidListRes;
import com.woyou.bean.rpc.QueryOrderDetailsReq;
import com.woyou.bean.rpc.QueryOrderDetailsRes;
import com.woyou.bean.rpc.QueryOrderReq;
import com.woyou.bean.rpc.QueryOrderRes;
import com.woyou.bean.rpc.QueryPlayMoneyDetailsReq;
import com.woyou.bean.rpc.QueryPlayMoneyDetailsRes;
import com.woyou.bean.rpc.QueryTodayReq;
import com.woyou.bean.rpc.QueryTodayRes;
import com.woyou.bean.rpc.QueryYesterdayInfoReq;
import com.woyou.bean.rpc.QueryYesterdayInfoRes;
import com.woyou.bean.rpc.Result;
import com.woyou.bean.rpc.SearchOrderListReq;
import com.woyou.bean.rpc.SearchOrderListRes;
import com.woyou.bean.rpc.SendOrderReq;
import com.woyou.bean.rpc.SendOrderRes;
import com.woyou.bean.rpc.UpdateOrdersStatusReq;
import com.woyou.service.OrderService;
import com.woyou.service.RetrofitWrapper;

import android.content.Context;
import retrofit.RetrofitError;

/**
 * 订单业务逻辑控制类
 * 
 * @author 荣
 * 
 */
public class OrderModel extends SuperModel {
	private static Context mContext;
	private static OrderService orderService;
	private static OrderModel orderModel = new OrderModel();

	private OrderModel() {
	}

	/**
	 * 单例模式
	 * 
	 * @return
	 */
	public static OrderModel getInstance(Context context) {
		mContext = context;
		if (orderModel == null) {
			orderModel = new OrderModel();
		}
		if (mNeWrapper == null) {
			mNeWrapper = new RetrofitWrapper();
		}
		if (orderService == null) {
			orderService = mNeWrapper.getNetService(OrderService.class);
		}
		return orderModel;
	}

	/**
	 * 查询订单列表
	 * 
	 * @param queryOrderReq
	 * @return
	 * @throws RetrofitError
	 */
	public Result<QueryOrderRes> queryOrderList(QueryOrderReq queryOrderReq) throws RetrofitError {
		MD5Req<QueryOrderReq> md5Req = new MD5Req<QueryOrderReq>(mContext, queryOrderReq);
		Result<QueryOrderRes> result = orderService.queryOrderList(md5Req.jsonParams, md5Req.isEncrypted,
				md5Req.timeStamp, md5Req.randomNum, md5Req.sign);
		return result;
	}

	/**
	 * 获取昨日信息简介
	 * 
	 * @param queryYesterdayInfoReq
	 * @return
	 * @throws RetrofitError
	 */
	public Result<QueryYesterdayInfoRes> queryYesterdayInfo(QueryYesterdayInfoReq queryYesterdayInfoReq)
			throws RetrofitError {
		MD5Req<QueryYesterdayInfoReq> md5Req = new MD5Req<QueryYesterdayInfoReq>(mContext, queryYesterdayInfoReq);
		Result<QueryYesterdayInfoRes> result = orderService.queryYesterdayInfo(md5Req.jsonParams, md5Req.isEncrypted,
				md5Req.timeStamp, md5Req.randomNum, md5Req.sign);
		return result;
	}

	/**
	 * 获取今日信息简介
	 * 
	 * @param queryTodayReq
	 * @return
	 * @throws RetrofitError
	 */
	public Result<QueryTodayRes> queryTodayInfo(QueryTodayReq queryTodayReq) throws RetrofitError {
		MD5Req<QueryTodayReq> md5Req = new MD5Req<QueryTodayReq>(mContext, queryTodayReq);
		Result<QueryTodayRes> result = orderService.queryTodayInfo(md5Req.jsonParams, md5Req.isEncrypted,
				md5Req.timeStamp, md5Req.randomNum, md5Req.sign);
		return result;
	}

	/**
	 * 送出订单
	 * 
	 * @param sendOrderReq
	 * @return
	 * @throws RetrofitError
	 */
	public Result<SendOrderRes> sendOrder(SendOrderReq sendOrderReq) throws RetrofitError {
		MD5Req<SendOrderReq> md5Req = new MD5Req<SendOrderReq>(mContext, sendOrderReq);
		Result<SendOrderRes> result = orderService.sendOrder(md5Req.jsonParams, md5Req.isEncrypted, md5Req.timeStamp,
				md5Req.randomNum, md5Req.sign);
		return result;
	}
	 /**
	 * 获取订单详情
	 * @param queryOrderDetailsReq
	 * @return
	 * @throws RetrofitError
	 */
//	 public Result<QueryOrderDetailsRes> queryOrderDetails(QueryOrderDetailsReq
//	 queryOrderDetailsReq) throws RetrofitError {
//	 MD5Req<QueryOrderDetailsReq> md5Req = new
//	 MD5Req<QueryOrderDetailsReq>(mContext,queryOrderDetailsReq);
//	 Result<QueryOrderDetailsRes> result =orderService.queryOrderDetails(md5Req.jsonParams, md5Req.isEncrypted,
//	 md5Req.timeStamp,md5Req.randomNum, md5Req.sign);
//	 return result;
//	 }

	/**
	 * 获取新订单ID列表
	 * 
	 * @param queryNewOidListReq
	 * @return
	 * @throws RetrofitError
	 */
	public Result<List<QueryNewOidListRes>> queryNewOidList(QueryNewOidListReq queryNewOidListReq)
			throws RetrofitError {
		MD5Req<QueryNewOidListReq> md5Req = new MD5Req<QueryNewOidListReq>(mContext, queryNewOidListReq);
		Result<List<QueryNewOidListRes>> result = orderService.queryNewOidList(md5Req.jsonParams, md5Req.isEncrypted,
				md5Req.timeStamp, md5Req.randomNum, md5Req.sign);
		return result;
	}

	/**
	 * 更新订单状态（置S）
	 * 
	 * @param updateOrdersStatusReq
	 * @return
	 * @throws RetrofitError
	 */
	public Result updateOrdersStatus(UpdateOrdersStatusReq updateOrdersStatusReq) throws RetrofitError {
		MD5Req<UpdateOrdersStatusReq> md5Req = new MD5Req<UpdateOrdersStatusReq>(mContext, updateOrdersStatusReq);
		Result result = orderService.updateOrdersStatus(md5Req.jsonParams, md5Req.isEncrypted, md5Req.timeStamp,
				md5Req.randomNum, md5Req.sign);
		return result;
	}

	/**
	 * 接受或拒绝订单
	 * 
	 * @param handleOrderReq
	 * @return
	 * @throws RetrofitError
	 */
	public Result<Order> handleOrder(HandleOrderReq handleOrderReq) throws RetrofitError {
		MD5Req<HandleOrderReq> md5Req = new MD5Req<HandleOrderReq>(mContext, handleOrderReq);
		Result<Order> result = orderService.handleOrder(md5Req.jsonParams, md5Req.isEncrypted, md5Req.timeStamp,
				md5Req.randomNum, md5Req.sign);
		return result;
	}

	/**
	 * 同意或拒绝取消订单
	 * 
	 * @param handleOrderReq
	 * @return
	 * @throws RetrofitError
	 */
	public Result<HandleCancelRes> handleCancel(HandleCancelReq handleCancelReq) throws RetrofitError {
		MD5Req<HandleCancelReq> md5Req = new MD5Req<HandleCancelReq>(mContext, handleCancelReq);
		Result<HandleCancelRes> result = orderService.handleCancel(md5Req.jsonParams, md5Req.isEncrypted,
				md5Req.timeStamp, md5Req.randomNum, md5Req.sign);
		return result;
	}

	/**
	 * 搜索订单列表
	 * 
	 * @param searchOrderListReq
	 * @return
	 * @throws RetrofitError
	 */
	public Result<SearchOrderListRes> searchOrderList(SearchOrderListReq searchOrderListReq) throws RetrofitError {
		MD5Req<SearchOrderListReq> md5Req = new MD5Req<SearchOrderListReq>(mContext, searchOrderListReq);
		Result<SearchOrderListRes> result = orderService.searchOrderList(md5Req.jsonParams, md5Req.isEncrypted,
				md5Req.timeStamp, md5Req.randomNum, md5Req.sign);
		return result;
	}

	/**
	 * 获取订单详情v2_3queryOrderDetails
	 * 
	 * @param queryOrderDetailsReq
	 * @return
	 * @throws RetrofitError
	 */
	public Result<QueryOrderDetailsRes> v2_3queryOrderDetails(QueryOrderDetailsReq queryOrderDetailsReq)
			throws RetrofitError {
		MD5Req<QueryOrderDetailsReq> md5Req = new MD5Req<QueryOrderDetailsReq>(mContext, queryOrderDetailsReq);
		Result<QueryOrderDetailsRes> result = orderService.v2_3queryOrderDetails(md5Req.jsonParams, md5Req.isEncrypted,
				md5Req.timeStamp, md5Req.randomNum, md5Req.sign);
		return result;
	}

	/**
	 * 获取打款详情v2_3queryPlayMoneyDetails
	 * 
	 * @param QueryPlayMoneyDetailsReq
	 * @return
	 * @throws RetrofitError
	 */
	public Result<QueryPlayMoneyDetailsRes> v2_3queryPlayMoneyDetails(QueryPlayMoneyDetailsReq queryPlayMoneyDetailsReq)
			throws RetrofitError {
		MD5Req<QueryPlayMoneyDetailsReq> md5Req = new MD5Req<QueryPlayMoneyDetailsReq>(mContext,
				queryPlayMoneyDetailsReq);
		 Result<QueryPlayMoneyDetailsRes> result =
		 orderService.v2_3queryPlayMoneyDetails(md5Req.jsonParams,
		 md5Req.isEncrypted, md5Req.timeStamp,md5Req.randomNum, md5Req.sign);
//		Result<QueryPlayMoneyDetailsRes> result = mockV2_3queryPlayMoneyDetails();
		return result;
	}

	/**
	 * mock打款详情数据
	 * 
	 * @return
	 */
	public Result<QueryPlayMoneyDetailsRes> mockV2_3queryPlayMoneyDetails() {
		Result<QueryPlayMoneyDetailsRes> result = new Result<QueryPlayMoneyDetailsRes>();
		result.setCode(1);
		result.setMsg("成功");
		QueryPlayMoneyDetailsRes moneyDetailsRes = new QueryPlayMoneyDetailsRes();
		moneyDetailsRes.setIsOnlinePay(1);
		moneyDetailsRes.setBindCard("兴业银行/尾号6817");
		List<StayPay> stayPays = new ArrayList<StayPay>();
		StayPay stayPay1 = new StayPay();
		stayPay1.setsTime("自2015年6月1日起00:00起，待支付给您：");
		stayPay1.setsType("货款");
		stayPay1.setsMoney("4580");
		stayPays.add(stayPay1);
		StayPay stayPay2 = new StayPay();
		stayPay2.setsTime("自2015年6月1日起00:00起，待支付给您：");
		stayPay2.setsType("优惠券");
		stayPay2.setsMoney("68");
		stayPays.add(stayPay2);
		moneyDetailsRes.setStayPays(stayPays);

		List<PlayMoney> playMoneys = new ArrayList<PlayMoney>();

		for (int i = 0; i < 5; i++) {
			PlayMoney playMoney = new PlayMoney();
			if (i % 2 == 0) {
				playMoney.setpTime(System.currentTimeMillis());
				playMoney.setpMoney(383);
				playMoney.setpPayee("工商银行/尾号6817");
				playMoney.setpMatters("2015-05-01 00:00:00到2015-05-31 23:59:59的货款");
			} else {
				playMoney.setpTime(System.currentTimeMillis());
				playMoney.setpMoney(77);
				playMoney.setpPayee("兴业银行/尾号6817");
				playMoney.setpMatters("2015-05-01 00:00:00到2015-05-31 23:59:59的优惠券");
			}
			playMoneys.add(playMoney);
		}

		moneyDetailsRes.setPlayMoneys(playMoneys);
		result.setData(moneyDetailsRes);
		return result;
	}
}
