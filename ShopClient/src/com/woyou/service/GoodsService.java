package com.woyou.service;

import java.util.List;

import retrofit.RetrofitError;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

import com.woyou.bean.GoodsState;
import com.woyou.bean.GoodsTypeInfo;
import com.woyou.bean.rpc.Result;

/**
 * 获取商品相关信息的Retrofit Service
 * @author 荣
 *
 */
public interface GoodsService {
	/**
	 *2.28	获取商品状态列表（http://xxxx/queryGoodsStateList）
	 * @param params
	 * @param isEncrypted
	 * @param timeStamp
	 * @param randomNum
	 * @param sign
	 * @return
	 * @throws RetrofitError
	 */
	@FormUrlEncoded
	@POST("/queryGoodsStateList")
	public Result<List<GoodsState>> queryGoodsStateList(
			@Field("params") String params, 
			@Field("isEncrypted")String isEncrypted, 
			@Field("timeStamp") String timeStamp, 
			@Field("randomNum")String randomNum,
			@Field("sign")String sign)throws RetrofitError;
	/**
	 * 2.9	获取商品分类列表（http://xxxx/queryTypeList）
	 * @param params
	 * @param isEncrypted
	 * @param timeStamp
	 * @param randomNum
	 * @param sign
	 * @return
	 * @throws RetrofitError
	 */
	@FormUrlEncoded
	@POST("/queryTypeList")
	public Result<List<GoodsTypeInfo>> queryTypeList(
			@Field("params") String params, 
			@Field("isEncrypted")String isEncrypted, 
			@Field("timeStamp") String timeStamp, 
			@Field("randomNum")String randomNum,
			@Field("sign")String sign)throws RetrofitError;
	/**
	 * 2.12	修改商品状态（http://xxxx/updateGoodsState）
	 * @param params
	 * @param isEncrypted
	 * @param timeStamp
	 * @param randomNum
	 * @param sign
	 * @return
	 * @throws RetrofitError
	 */
	@FormUrlEncoded
	@POST("/updateGoodsState")
	public Result updateGoodsState(
			@Field("params") String params, 
			@Field("isEncrypted")String isEncrypted, 
			@Field("timeStamp") String timeStamp, 
			@Field("randomNum")String randomNum,
			@Field("sign")String sign)throws RetrofitError;
}
