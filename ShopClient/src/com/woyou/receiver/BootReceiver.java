package com.woyou.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.woyou.Constants;
import com.woyou.activity.WelcomeActivity;
import com.woyou.bean.UserInfo;
import com.woyou.bean.rpc.Result;
import com.woyou.bean.rpc.SetShopStatusReq;
import com.woyou.model.rpc.ShopModel;
import com.woyou.utils.LoginUtils;
import com.woyou.utils.ThreadPoolManager;

/**
 * 监听开机广播事件
 * 
 * @author lenovo
 * 
 */
public class BootReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(final Context context, Intent intent) {
		String action = intent.getAction();
		if (Intent.ACTION_BOOT_COMPLETED.equals(action)) {
			Intent i = new Intent(context, WelcomeActivity.class);
			i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(i);
		} else if (Intent.ACTION_SHUTDOWN.equals(action)) {
			Log.i("BootReceiver", "关机设置店铺状态休息");
			final SetShopStatusReq setShopStatusReq = new SetShopStatusReq();
			UserInfo userInfo = LoginUtils.getUserInfo(context);
			if ( userInfo!=null ){
				setShopStatusReq.setsId(userInfo.getsId());
				setShopStatusReq.setPwd(userInfo.getPwd());
			}
			setShopStatusReq.setStatus("C");
			ThreadPoolManager.getInstance().executeTask(new Runnable() {
				@Override
				public void run() {
					try {
						for(int i=0;i<3;i++){
							Result result = ShopModel.getInstance(context).setShopStatus(setShopStatusReq);
							if (result != null && result.code==1 ) {
								// 设置营业状态
								Constants.shopStatus = "C";
								break;
							} 
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
		}
	}

}
