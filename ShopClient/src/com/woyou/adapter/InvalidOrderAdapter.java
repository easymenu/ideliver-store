package com.woyou.adapter;

import java.util.List;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.jing.biandang.R;
import com.woyou.bean.Order;
import com.woyou.utils.FormatTools;

public class InvalidOrderAdapter extends BaseAdapter {
	private LayoutInflater layoutInflater;
	private List<Order> data;
	private Context context;
	public InvalidOrderAdapter(Context c,List<Order> data) {
		context=c;
		layoutInflater = LayoutInflater.from(c);
		this.data = data;
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Order order=data.get(position);
		ViewHolder holder;
		if(convertView==null){
			holder=new ViewHolder();
			convertView = layoutInflater.inflate(R.layout.item_order_invalid_lv, null);
			holder.num=(TextView)convertView.findViewById(R.id.item_order_invalid_num);
			holder.name=(TextView)convertView.findViewById(R.id.item_order_invalid_name);
			holder.phone=(TextView)convertView.findViewById(R.id.item_order_invalid_phone);
			holder.price=(TextView)convertView.findViewById(R.id.item_order_invalid_price);
			holder.content=(TextView)convertView.findViewById(R.id.item_order_invalid_content);
			holder.date=(TextView)convertView.findViewById(R.id.item_order_invalid_date);
			holder.time=(TextView)convertView.findViewById(R.id.item_order_invalid_time);
			holder.status=(TextView)convertView.findViewById(R.id.item_order_invalid_status);
			convertView.setTag(holder);
		}else{
			holder=(ViewHolder) convertView.getTag();
		}
		holder.num.setText(order.getShortNo());
		holder.name.setText(order.getContact());
		holder.phone.setText(order.getPhone());
		holder.price.setText(Html.fromHtml("<small>￥</small>" + FormatTools.String2Money(order.getPrice()+"")));
		holder.content.setText(order.getGoodsSummary()+" -> "+order.getAddrName());
		String ftime = FormatTools.convertTimeToCustom(context,order.getOrderTime());
		holder.date.setText(ftime);
		holder.time.setText("");
		holder.status.setText(""+order.getStatus());
		return convertView;
	}
	
	static class ViewHolder{
		TextView num;
		TextView name;
		TextView phone;
		TextView price;
		TextView content;
		TextView date;
		TextView time;
		TextView status;
	}

}
