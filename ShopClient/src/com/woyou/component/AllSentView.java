/** 
 * @Title:  AllSentView.java 
 * @author:  xuron
 * @data:  2015年11月27日 上午10:34:39 <创建时间>
 * 
 * @history：<以下是历史记录>
 *
 * @modifier: <修改人>
 * @modify date: 2015年11月27日 上午10:34:39 <修改时间>
 * @log: <修改内容>
 *
 * @modifier: <修改人>
 * @modify date: 2015年11月27日 上午10:34:39 <修改时间>
 * @log: <修改内容>
 */
package com.woyou.component;

import com.jing.biandang.R;
import com.woyou.activity.HomeActivity;
import com.woyou.bean.rpc.QueryTodayReq;
import com.woyou.bean.rpc.QueryTodayRes;
import com.woyou.bean.rpc.Result;
import com.woyou.fragment.order.OrderFragment;
import com.woyou.model.OrderManageModel;
import com.woyou.model.rpc.OrderModel;
import com.woyou.utils.LoginUtils;
import com.woyou.utils.ThreadPoolManager;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import retrofit.RetrofitError;

/**
 * 将全部送出做成自定义View
 * 
 * @author xuron
 * @versionCode 1 <每次修改提交前+1>
 */
public class AllSentView extends SuperLayout implements OnClickListener {
	private HomeActivity homeActivity;
	private OrderFragment orderFragment;
	private OrderModel orderModel;
	private OrderManageModel omModel;
	private QueryTodayRes queryTodayRes;

	private RelativeLayout layout_order_allsent;
	private TextView layout_allsent_good;
	private TextView layout_allsend_noread_tv;
	private TextView layout_allsend_soldout_tv;
	private TextView layout_allsend_todayorder_tv;
	private TextView layout_allsend_soldnum_tv;
	private TextView layout_allsend_income_tv;

	private RelativeLayout layout_allsend_noread;
	private RelativeLayout layout_allsend_soldout;
	private RelativeLayout layout_allsend_todayorder;
	private RelativeLayout layout_allsend_income;
	private RelativeLayout layout_allsend_print;

	public AllSentView(Context context) {
		super(context);
		initView();
	}

	public AllSentView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initView();
	}

	public AllSentView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initView();
	}

	@Override
	protected void initView() {
		View.inflate(mContext, R.layout.layout_allsent, this);
		layout_order_allsent = (RelativeLayout) findViewById(R.id.layout_order_allsent);
		layout_allsent_good = (TextView) findViewById(R.id.layout_allsent_good);
		layout_allsend_noread = (RelativeLayout) findViewById(R.id.layout_allsend_noread);
		layout_allsend_noread_tv = (TextView) findViewById(R.id.layout_allsend_noread_tv);
		layout_allsend_soldout_tv = (TextView) findViewById(R.id.layout_allsend_soldout_tv);
		layout_allsend_todayorder_tv = (TextView) findViewById(R.id.layout_allsend_todayorder_tv);
		layout_allsend_soldnum_tv = (TextView) findViewById(R.id.layout_allsend_soldnum_tv);
		layout_allsend_income_tv = (TextView) findViewById(R.id.layout_allsend_income_tv);
		layout_allsend_soldout = (RelativeLayout) findViewById(R.id.layout_allsend_soldout);
		layout_allsend_todayorder = (RelativeLayout) findViewById(R.id.layout_allsend_todayorder);
		layout_allsend_income = (RelativeLayout) findViewById(R.id.layout_allsend_income);
		layout_allsend_print = (RelativeLayout) findViewById(R.id.layout_allsend_print);

		layout_allsend_noread.setOnClickListener(this);
		layout_allsend_soldout.setOnClickListener(this);
		layout_allsend_todayorder.setOnClickListener(this);
		layout_allsend_income.setOnClickListener(this);
	}

	/**
	 * 设置参数
	 */
	public void setParams(HomeActivity homeActivity,OrderFragment orderFragment,OrderModel orderModel, OrderManageModel omModel) {
		this.homeActivity = homeActivity;
		this.orderFragment=orderFragment;
		this.orderModel = orderModel;
		this.omModel = omModel;
	}

	/**
	 * 显示所有订单都送出页面，并加载数据
	 */
	public void showAllSendOrderView() {

		ThreadPoolManager.getInstance().executeTask(new Runnable() {
			@Override
			public void run() {
				try {
					homeActivity.showLoadingHint();
					QueryTodayReq queryTodayReq = new QueryTodayReq();
					queryTodayReq.setsId(LoginUtils.getUserInfo(homeActivity).getsId());
					queryTodayReq.setPwd(LoginUtils.getUserInfo(homeActivity).getPwd());
					Result<QueryTodayRes> result = orderModel.queryTodayInfo(queryTodayReq);
					if (result != null && result.getCode() == 1) {
						queryTodayRes = result.getData();
						homeActivity.runOnUiThread(new Runnable() {
							@Override
							public void run() {

								if (omModel.getSentList().size() > 0) {
									layout_allsent_good.setText("超赞！" + omModel.getSentList().size() + "个订单全部送出了");
								} else {
									layout_allsent_good.setText("今日暂无待送订单");
								}

								layout_allsend_noread_tv.setText("" + queryTodayRes.getNewCommentNum());
								layout_allsend_soldout_tv.setText("" + queryTodayRes.getNAnum());
								layout_allsend_todayorder_tv.setText("" + queryTodayRes.getOrderNum());
								layout_allsend_soldnum_tv.setText("" + queryTodayRes.getSoldNum());
								layout_allsend_income_tv.setText("" + queryTodayRes.getIncome());
							}
						});
					} else if (result != null && result.getCode() == -3) {
						homeActivity.homeController.exitLogin();
					}
				} catch (RetrofitError error) {

				} finally {
					homeActivity.hideLoadingHint();
				}
			}
		});
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.layout_allsend_soldout:
			orderFragment.orderController.jump2SoldoutFragment();
			break;
		case R.id.layout_allsend_noread:
			orderFragment.orderController.jump2MessageFragment();
			break;
		}
	}

}
