package com.woyou.bean;


/**
 * 优惠券
 * 
 * @author 荣
 * 
 */
public class Coupon extends SuperModel {
	/**
	 * 优惠券ID
	 */
	private String cId = "";
	/**
	 * 优惠券名称
	 */
	private String cName = "";
	/**
	 * 优惠券类型
	 */
	private String cType = "";
	/**
	 * 抵用金额
	 */
	private String value = "";

	public String getcId() {
		return cId;
	}

	public void setcId(String cId) {
		this.cId = cId;
	}

	public String getcName() {
		return cName;
	}

	public void setcName(String cName) {
		this.cName = cName;
	}

	public String getcType() {
		return cType;
	}

	public void setcType(String cType) {
		this.cType = cType;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
