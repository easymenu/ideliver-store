package com.woyou.utils;

import com.woyou.Constants;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
/**
 * 采用SharedPreferences记录订单打印张数
 * @author lenovo
 *
 */
public class PrintNumUtils {
	
	public static void savePrintNum(Context context,int printPages){
		SharedPreferences preferences = context.getSharedPreferences(Constants.SETTING_CONFIG_FILE, Context.MODE_PRIVATE);
		Editor editor = preferences.edit();
		editor.putInt(Constants.SETTING_PRINT_PAGES, printPages);
		editor.commit();
	}
	
	public static int getPrintNum(Context context){
		SharedPreferences sharedPrefrences = context.getSharedPreferences( Constants.SETTING_CONFIG_FILE,  Context.MODE_PRIVATE);
		return sharedPrefrences.getInt(Constants.SETTING_PRINT_PAGES, Constants.PRINT_PAGES_DEFAULT_VALUE);
	}	
	
	public static void clearPrintNum(Context context){
		SharedPreferences preferences = context.getSharedPreferences(Constants.SETTING_CONFIG_FILE, Context.MODE_PRIVATE);
		Editor editor = preferences.edit();
		editor.remove(Constants.SETTING_PRINT_PAGES);
		editor.commit();
	}
	
}
