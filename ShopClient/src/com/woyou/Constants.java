package com.woyou;

public class Constants {
	public static final String MAIN_BOARD_SMDKV210 = "smdkv210";
	public static final String MAIN_BOARD_RK30 = "rk30sdk";
	public static final String MAIN_BOARD_C500 = "c500";
	public static final String MAIN_BOARD_UNKNOW = "unknow";

	/**
	 * 店铺状态
	 */
	public static String shopStatus = "S";
	/**
	 * 店铺id
	 */
	public static String shopCode = "";
	/**
	 * 店铺名
	 */
	public static String shopName = "";
	/**
	 * 函数加密的密钥
	 */
	// public static final String IDELIVER_KEY = "woyouwaimaiapp";//捷码服务器密钥
	public static final String IDELIVER_KEY = "WoYouPhone";// 运营key
	public static String versionName = "1.0";

	// public static String SERVER_ADDRESS2="115.29.249.152";//新HTTP测试服务器地址
	public static String SERVER_ADDRESS2 = "bdj.ideliver1.cn";// 运营服务器
	public static String BDJ_PORT2 = "201";
	public static String SERVER2 = "http://" + SERVER_ADDRESS2 + ":" + BDJ_PORT2;

	// 115.29.249.152
	public static String HEART_BEAT_SERVER = "bdj.ideliver1.cn";// 心跳IP
	public static int HEART_BEAT_SERVER_PORT = 9898;
	public static final String WEB_ADDRESS = "http://www.woyouwaimai.com";

	// for SharedPreferences
	public static final String SETTING_CONFIG_FILE = "AppSetting";
	public static final String SETTING_PRINT_PAGES = "Print_pages";
	public static final String SETTING_UPDATE_DATE = "Update_date";

	// 默认打印机的张数
	public static final int PRINT_PAGES_DEFAULT_VALUE = 2;

	public static final String UPDATE_APP_NAME = "BD_VERSION2";

	public static final int Reset3GMaxSeconds = 60 * 5;

	public static String curMainBoard = "";
	/**
	 * 记录当前订单拒绝的次数
	 */
	public static int refuseOrderNum = 0;

}
