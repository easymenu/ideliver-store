package com.woyou.bean;


/**
 * 订单id列表
 * 
 * @author 荣
 * 
 */
public class OrderId extends SuperModel {
	/**
	 * 新订单Id
	 */
	private String oId = "";

	public String getoId() {
		return oId;
	}

	public void setoId(String oId) {
		this.oId = oId;
	}

}
