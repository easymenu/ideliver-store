package com.woyou.service;

import java.util.List;

import retrofit.RetrofitError;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

import com.woyou.bean.CommentItem;
import com.woyou.bean.ShopInfoItem;
import com.woyou.bean.rpc.Result;
import com.woyou.bean.rpc.ServerTimeRes;
import com.woyou.bean.rpc.UpdateVersionRes;
import com.woyou.bean.rpc.UserLoginReq;
import com.woyou.bean.rpc.UserLoginRes;

/**
 * 用户相关信息的Retrofit Service
 * @author tlc
 *
 */
public interface UserService {
	
	/**
	 * 2.2	获取验证码（http://xxxx/queryVerificationCode）
	 * @param params      请求bean的json字符串
	 * @param isEncrypted 是否加密
	 * @param timeStamp   时间戳
	 * @param randomNum   6位随机数 
	 * @param sign        加密后的字符串
	 * 算法:sign = MD5(Parameter+IsEncrypted+TimeStamp+RandomNum+MD5(Key))
	 * @return
	 * @throws RetrofitError
	 */
	@FormUrlEncoded
	@POST("/queryVerificationCode")
	public Result queryVerificationCode(
			@Field("params") String params, 
			@Field("isEncrypted")String isEncrypted, 
			@Field("timeStamp") String timeStamp, 
			@Field("randomNum")String randomNum,
			@Field("sign")String sign)throws RetrofitError;
	
//	/**
//	 * 2.3用户登录（http://xxxx/userLogin）
//	 * @param params      请求bean的json字符串
//	 * @param isEncrypted 是否加密
//	 * @param timeStamp   时间戳
//	 * @param randomNum   6位随机数 
//	 * @param sign        加密后的字符串
//	 * 算法:sign = MD5(Parameter+IsEncrypted+TimeStamp+RandomNum+MD5(Key))
//	 * @return
//	 * @throws RetrofitError
//	 */
//	@FormUrlEncoded
//	@POST("/userLogin")
//	public Result<UserLoginRes> userLogin(
//			@Field("params") String params, 
//			@Field("isEncrypted")String isEncrypted, 
//			@Field("timeStamp") String timeStamp, 
//			@Field("randomNum")String randomNum,
//			@Field("sign")String sign)throws RetrofitError;

	/**
	 * 2.3	便当机用户登录（http://xxxx/v2_3userLogin）
	 * @param params      请求bean的json字符串
	 * @param isEncrypted 是否加密
	 * @param timeStamp   时间戳
	 * @param randomNum   6位随机数 
	 * @param sign        加密后的字符串
	 * 算法:sign = MD5(Parameter+IsEncrypted+TimeStamp+RandomNum+MD5(Key))
	 * @return
	 * @throws RetrofitError
	 */
	@FormUrlEncoded
	@POST("/v2_3userLogin")
	public Result<UserLoginRes> v2_3userLogin(
			@Field("params") String params, 
			@Field("isEncrypted")String isEncrypted, 
			@Field("timeStamp") String timeStamp, 
			@Field("randomNum")String randomNum,
			@Field("sign")String sign)throws RetrofitError;
	/**
	 * 2.4应用更新（http://xxxx/updateVersion）
	 * @param params      请求bean的json字符串
	 * @param isEncrypted 是否加密
	 * @param timeStamp   时间戳
	 * @param randomNum   6位随机数 
	 * @param sign        加密后的字符串
	 * 算法:sign = MD5(Parameter+IsEncrypted+TimeStamp+RandomNum+MD5(Key))
	 * @return
	 * @throws RetrofitError
	 */
	@FormUrlEncoded
	@POST("/updateVersion")
	public Result<UpdateVersionRes> updateVersion(
			@Field("params") String params, 
			@Field("isEncrypted")String isEncrypted, 
			@Field("timeStamp") String timeStamp, 
			@Field("randomNum")String randomNum,
			@Field("sign")String sign)throws RetrofitError;
}
