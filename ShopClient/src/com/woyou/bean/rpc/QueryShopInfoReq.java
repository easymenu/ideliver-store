package com.woyou.bean.rpc;


/**
 * 获取商铺信息的请求bean
 * 
 * @author zhou.ni
 * 
 */
public class QueryShopInfoReq {

	private String sId; 	// 商铺ID
	private String pwd; 	// 密码

	public String getsId() {
		return sId;
	}

	public void setsId(String sId) {
		this.sId = sId;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

}
