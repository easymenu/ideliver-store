package com.woyou.bean;


public class GoodsTypeInfo extends SuperModel{
	
	/**
	 * 商品分类ID（包括子商品类）
	 */
	private String tId = "";
	/**
	 * 商品分类名称
	 */
	private String tName = "";
	/**
	 * 商品数量（包含暂缺商品）
	 */
	private int gNum;
	
	public String gettId() {
		return tId;
	}
	public void settId(String tId) {
		this.tId = tId;
	}
	public String gettName() {
		return tName;
	}
	public void settName(String tName) {
		this.tName = tName;
	}
	public int getgNum() {
		return gNum;
	}
	public void setgNum(int gNum) {
		this.gNum = gNum;
	}
}
