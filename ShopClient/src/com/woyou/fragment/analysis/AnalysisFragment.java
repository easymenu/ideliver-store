package com.woyou.fragment.analysis;

import com.jing.biandang.R;
import com.woyou.activity.HomeActivity;
import com.woyou.bean.FmInfo;
import com.woyou.fragment.SuperFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

public class AnalysisFragment extends SuperFragment implements OnClickListener {
	private HomeActivity homeActivity;
	private View layout_analysis;
	private ImageView layout_analysis_report_iv;
	private ImageView layout_analysis_customer_iv;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		layout_analysis = inflater.inflate(R.layout.layout_analysis, container,false);
		homeActivity=(HomeActivity)getActivity();
		return layout_analysis;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		layout_analysis_report_iv = (ImageView) layout_analysis.findViewById(R.id.layout_analysis_report_iv);
		layout_analysis_customer_iv=(ImageView)layout_analysis.findViewById(R.id.layout_analysis_customer_iv);
		
		layout_analysis_customer_iv.setOnClickListener(this);
		layout_analysis_report_iv.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.layout_analysis_report_iv:
			homeActivity.openSubFM(this, new FmInfo(ReportFragment.class, "营业报表", false));
			break;
		case R.id.layout_analysis_customer_iv:
			homeActivity.openSubFM(this, new FmInfo(CustomerFragment.class, "客户分析", false));
			break;
		}
	}

}
