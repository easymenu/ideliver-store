package com.woyou.bean;


/**
 * 商品属性状态
 * 
 * @author 荣
 * 
 */
public class OptionState extends SuperModel {
	/**
	 * 属性ID
	 */
	private String optId = "";
	/**
	 * 属性名称
	 */
	private String optName = "";
	/**
	 * 缺货标志（1正常，2暂缺）
	 */
	private int state;
	/**
	 * 记录该商品的原始状态
	 */
	private int origin;

	public String getOptId() {
		return optId;
	}

	public void setOptId(String optId) {
		this.optId = optId;
	}

	public String getOptName() {
		return optName;
	}

	public void setOptName(String optName) {
		this.optName = optName;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public int getOrigin() {
		return origin;
	}

	public void setOrigin(int origin) {
		this.origin = origin;
	}

	@Override
	public String toString() {
		return "OptionState [optId=" + optId + ", optName=" + optName
				+ ", state=" + state + ", origin=" + origin + "]";
	}

}
