package com.woyou.bean.rpc;

import com.woyou.bean.SuperModel;

/**
 * 搜索订单列表请求bean
 * 
 * @author 荣
 * 
 */
public class SearchOrderListReq extends SuperModel {
	/**
	 * 店铺Id
	 */
	private String sId = "";
	/**
	 * 密码
	 */
	private String pwd = "";
	/**
	 * 关键字
	 */
	private String key = "";

	public String getsId() {
		return sId;
	}

	public void setsId(String sId) {
		this.sId = sId;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

}
