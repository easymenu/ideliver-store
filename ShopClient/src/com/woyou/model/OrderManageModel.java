package com.woyou.model;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import com.woyou.bean.Order;
import com.woyou.bean.rpc.QueryOrderRes;

/**
 * 订单管理类
 * 
 * @author 荣
 * 
 */
public class OrderManageModel {
	/**
	 * 未送订单
	 */
	private List<Order> noSentList = new ArrayList<Order>();
	/**
	 * 已送订单
	 */
	private List<Order> sentList = new ArrayList<Order>();
	/**
	 * 无效订单
	 */
	private List<Order> invalidList = new ArrayList<Order>();
	/**
	 * 所有订单
	 */
	private List<Order> allOrderList = new ArrayList<Order>();

	private static OrderManageModel omModel = new OrderManageModel();

	private OrderManageModel() {
	}

	/**
	 * 单例模式
	 * 
	 * @return
	 */
	public static OrderManageModel getInstance() {
		if (omModel == null) {
			omModel = new OrderManageModel();
		}
		return omModel;
	}

	/**
	 * 载入所有订单数据
	 */
	public void loadAllOrders(QueryOrderRes queryOrderRes) {

		if (queryOrderRes != null && queryOrderRes.getNoSent() != null) {
			noSentList = queryOrderRes.getNoSent();
		}
		if (queryOrderRes != null && queryOrderRes.getSent() != null) {
			sentList = queryOrderRes.getSent();
		}
		if (queryOrderRes != null && queryOrderRes.getInvalid() != null) {
			invalidList.clear();
			invalidList = queryOrderRes.getInvalid();
		}
	}

	/**
	 * 将新订单添加到未送订单
	 */
	public void addNewOrder2NoSend(Order order) {
		if (noSentList.size() > 0) {
			for (int i = 0; i < noSentList.size(); i++) {
				Order current = noSentList.get(i);
				if (current.getReserveTime() > order.getReserveTime()) {
					noSentList.add(i, order);
					return;
				}
			}
			noSentList.add(order);
		} else {
			noSentList.add(order);
		}
	}

	/**
	 * 将新订单添加到无效订单
	 */
	public void addNewOrder2Invalid(Order order) {
		invalidList.add(0, order);
	}

	/**
	 * 将未送订单移到已送订单列表
	 * 
	 * @param oId
	 * @param sendTime
	 * @param status
	 */
	public void moveOrder2SentList(String oId, long sendTime, String status) {
		Order order = getOrderById(oId, noSentList);
		if (order != null) {
			order.setOrderTime(sendTime);
			order.setStatus(status);
			sentList.add(0, order);
			noSentList.remove(order);
		}
	}

	/**
	 * 将未送订单或者已送订单移到无效订单列表
	 * 
	 * @param oId
	 * @param status
	 */
	public void moveOrder2Invalid(String oId, String status) {
		Order order = getOrderById(oId, noSentList);
		if (order != null) {
			order.setStatus(status);
			if (order != null) {
				invalidList.add(0, order);
				noSentList.remove(order);
			}
		} else {
			order = getOrderById(oId, sentList);
			if (order != null) {
				order.setStatus(status);
				if (order != null) {
					invalidList.add(0, order);
					sentList.remove(order);
				}
			}
		}
	}

	/**
	 * 搜索本店内的所有订单
	 * 
	 * @param keyword
	 */
	public List<Order> searchOrder(String keyword) {

		List<Order> allOrder = new ArrayList<Order>();
		allOrder.clear();
		String regEx = "/*" + keyword + "/*";
		allOrderList = getAllOrderList();
		if (allOrderList != null && allOrderList.size() > 0) {
			// 匹配订单号
			for (int i = 0; i < allOrderList.size(); i++) {
				Order order = allOrderList.get(i);
				boolean result = Pattern.compile(regEx).matcher(order.getoId()).find();
				if (result) {
					allOrder.add(order);
				}
			}
			// 匹配姓名
			for (int i = 0; i < allOrderList.size(); i++) {
				Order order = allOrderList.get(i);
				boolean result = Pattern.compile(regEx).matcher(order.getContact()).find();
				if (result) {
					if (!allOrder.contains(order)) {
						allOrder.add(order);
					}
				}
			}
			// 匹配手机号
			for (int i = 0; i < allOrderList.size(); i++) {
				Order order = allOrderList.get(i);
				boolean result = Pattern.compile(regEx).matcher(order.getPhone()).find();
				if (result) {
					if (!allOrder.contains(order)) {
						allOrder.add(order);
					}
				}
			}
			// 匹配地址
			for (int i = 0; i < allOrderList.size(); i++) {
				Order order = allOrderList.get(i);
				boolean result = Pattern.compile(regEx).matcher(order.getAddrName()).find();
				if (result) {
					if (!allOrder.contains(order)) {
						allOrder.add(order);
					}
				}
			}
		}
		return allOrder;
	}

	public List<Order> getNoSentList() {
		return noSentList;
	}

	public List<Order> getSentList() {
		return sentList;
	}

	public List<Order> getInvalidList() {
		return invalidList;
	}

	/**
	 * 返回所有订单
	 * 
	 * @return
	 */
	public List<Order> getAllOrderList() {
		allOrderList.clear();
		allOrderList.addAll(getNoSentList());
		allOrderList.addAll(getSentList());
		allOrderList.addAll(getInvalidList());
		return allOrderList;
	}

	/**
	 * 根据订单id从列表中获取该订单对象
	 * 
	 * @return
	 */
	private Order getOrderById(String oId, List<Order> list) {
		for (Order order : list) {
			if (order.getoId().equals(oId)) {
				return order;
			}
		}
		return null;
	}

	/**
	 * 清除所有数据
	 */
	public void clearAllData() {
		noSentList.clear();
		sentList.clear();
		invalidList.clear();
	}
}
