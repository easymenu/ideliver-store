package com.woyou.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.jing.biandang.R;

public class SpinnerPopAdapter extends BaseAdapter {

	private ArrayList<String> arrayList;
	private LayoutInflater layoutInflater;

	public SpinnerPopAdapter(Context context, ArrayList<String> arrayList) {
		this.arrayList = arrayList;
		layoutInflater=LayoutInflater.from(context);
	}
	@Override
	public int getCount() {
		return arrayList.size();
	}
	@Override
	public Object getItem(int position) {
		return position;
	}
	@Override
	public long getItemId(int position) {
		return position;
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = layoutInflater.inflate(R.layout.item_spinner_listview,null);
			TextView item_taskmanager_spinner_tv= (TextView) convertView.findViewById(R.id.item_taskmanager_spinner_tv);
			item_taskmanager_spinner_tv.setText(arrayList.get(position));
		}
		return convertView;
	}
}
