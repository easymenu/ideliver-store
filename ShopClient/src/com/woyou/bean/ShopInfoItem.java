package com.woyou.bean;

import java.util.List;

/**
 * 商铺信息
 * @author zhou.ni
 *
 */
public class ShopInfoItem extends SuperModel{
	
	private static final long serialVersionUID = 1L;
	
	private String oNum;			//订单数
	private String cNum;			//顾客数
	private float score;			//评分
	private int scoreNum;			//评分人数
	private String grade;			//等级
	private String rate;			//好评率
	private String sName;			// 商铺名称
	private String shopLogo;		//商铺logo的Url地址
	private int salesNum;			//月销售数
	private String startPrice;		// 起送价
	private String state;			// 营业状态
	private String register;		// 注册人
	private long rdate;				// 注册时间
	private String sType;			// 商铺类型
	private String rPhone; 			// 注册手机号
	private String sPhone; 			// 店铺电话
	private String notice;			// 店铺公告
	private String brief;			// 店铺简介
	private String deliveryTime;	// 外卖时段价列表
	private List<MarkUp> markUpList;				//阶梯价列表
	private List<DeliveryTime> deliveryTimeList;   	//外卖时段价列表
	private float boxFee;			//餐盒费
	private int boxType;			//餐盒费类型（0：不算1：按单算,2:按份算）
	
	public int getScoreNum() {
		return scoreNum;
	}
	public void setScoreNum(int scoreNum) {
		this.scoreNum = scoreNum;
	}
	public String getoNum() {
		return oNum;
	}
	public void setoNum(String oNum) {
		this.oNum = oNum;
	}
	public String getcNum() {
		return cNum;
	}
	public void setcNum(String cNum) {
		this.cNum = cNum;
	}
	public float getScore() {
		return score;
	}
	public void setScore(float score) {
		this.score = score;
	}
	public String getGrade() {
		return grade;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}
	public String getRate() {
		return rate;
	}
	public void setRate(String rate) {
		this.rate = rate;
	}
	public String getsName() {
		return sName;
	}
	public void setsName(String sName) {
		this.sName = sName;
	}
	public String getShopLogo() {
		return shopLogo;
	}
	public void setShopLogo(String shopLogo) {
		this.shopLogo = shopLogo;
	}
	public int getSalesNum() {
		return salesNum;
	}
	public void setSalesNum(int salesNum) {
		this.salesNum = salesNum;
	}
	public String getStartPrice() {
		return startPrice;
	}
	public void setStartPrice(String startPrice) {
		this.startPrice = startPrice;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getRegister() {
		return register;
	}
	public void setRegister(String register) {
		this.register = register;
	}
	public long getRdate() {
		return rdate;
	}
	public void setRdate(long rdate) {
		this.rdate = rdate;
	}
	public String getsType() {
		return sType;
	}
	public void setsType(String sType) {
		this.sType = sType;
	}
	public String getrPhone() {
		return rPhone;
	}
	public void setrPhone(String rPhone) {
		this.rPhone = rPhone;
	}
	public String getsPhone() {
		return sPhone;
	}
	public void setsPhone(String sPhone) {
		this.sPhone = sPhone;
	}
	public String getNotice() {
		return notice;
	}
	public void setNotice(String notice) {
		this.notice = notice;
	}
	public String getBrief() {
		return brief;
	}
	public void setBrief(String brief) {
		this.brief = brief;
	}
	public String getDeliveryTime() {
		return deliveryTime;
	}
	public void setDeliveryTime(String deliveryTime) {
		this.deliveryTime = deliveryTime;
	}
	public List<MarkUp> getMarkUpList() {
		return markUpList;
	}
	public void setMarkUpList(List<MarkUp> markUpList) {
		this.markUpList = markUpList;
	}
	public List<DeliveryTime> getDeliveryTimeList() {
		return deliveryTimeList;
	}
	public void setDeliveryTimeList(List<DeliveryTime> deliveryTimeList) {
		this.deliveryTimeList = deliveryTimeList;
	}
	public float getBoxFee() {
		return boxFee;
	}
	public void setBoxFee(float boxFee) {
		this.boxFee = boxFee;
	}
	public int getBoxType() {
		return boxType;
	}
	public void setBoxType(int boxType) {
		this.boxType = boxType;
	}
	
	@Override
	public String toString() {
		return "ShopInfoItem [oNum=" + oNum + ", cNum=" + cNum + ", score="
				+ score + ", scoreNum=" + scoreNum + ", grade=" + grade
				+ ", rate=" + rate + ", sName=" + sName + ", shopLogo="
				+ shopLogo + ", salesNum=" + salesNum + ", startPrice="
				+ startPrice + ", state=" + state + ", fans=" 
				+ ", register=" + register + ", rdate=" + rdate + ", sType="
				+ sType + ", rPhone=" + rPhone + ", sPhone=" + sPhone
				+ ", notice=" + notice + ", brief=" + brief + ", deliveryTime="
				+ deliveryTime + ", markUpList=" + markUpList
				+ ", deliveryTimeList=" + deliveryTimeList + ", boxFee="
				+ boxFee + ", boxType=" + boxType + "]";
	}
	
	
	
}