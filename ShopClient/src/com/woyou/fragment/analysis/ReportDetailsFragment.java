package com.woyou.fragment.analysis;

import java.util.ArrayList;
import java.util.List;

import com.jing.biandang.R;
import com.woyou.activity.HomeActivity;
import com.woyou.adapter.ReportDayDetailsAdapter;
import com.woyou.bean.BoxCount;
import com.woyou.bean.CouponCount;
import com.woyou.bean.GiftCount;
import com.woyou.bean.GoodsCount;
import com.woyou.bean.ReportData;
import com.woyou.bean.ReportItem;
import com.woyou.component.SuperUI;
import com.woyou.fragment.SuperFragment;
import com.woyou.utils.FormatTools;
import com.woyou.utils.PrintUtil;
import com.woyou.utils.ThreadPoolManager;

import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * 报表详情界面
 * 
 * @author zhou.ni
 *
 */
public class ReportDetailsFragment extends SuperFragment implements OnClickListener{
	private HomeActivity homeActivity;
	private View layout_reportdetails;
	private ListView layout_reportdetails_lv;
	private RelativeLayout layout_reportdetails_print;		//打印
	private TextView layout_reportdetails_sum;				//总金额
	private TextView layout_reportdetails_coupon;			//优惠券
	private ReportDayDetailsAdapter detailsAdapter;
	private ReportItem reportItem;							

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		layout_reportdetails = inflater.inflate(R.layout.layout_reportdetails,container, false);
		homeActivity=(HomeActivity)getActivity();
		return layout_reportdetails;
	}
	
	@Override
	public void onHiddenChanged(boolean hidden) {
		super.onHiddenChanged(hidden);
		if(!hidden){
			loadData();
		}
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		init();
		loadData();
	}

	/**
	 * 初始化页面
	 */
	public void init(){
		layout_reportdetails_lv=(ListView)layout_reportdetails.findViewById(R.id.layout_reportdetails_lv);
		layout_reportdetails_print=(RelativeLayout)layout_reportdetails.findViewById(R.id.layout_reportdetails_print);
		layout_reportdetails_sum=(TextView)layout_reportdetails.findViewById(R.id.layout_reportdetails_sum);
		layout_reportdetails_coupon=(TextView)layout_reportdetails.findViewById(R.id.layout_reportdetails_coupon);
		layout_reportdetails_print.setOnClickListener(this);
	}

	/**
	 * 设置数据
	 */
	public void loadData(){
		if(layout_reportdetails_lv==null){
			return;
		}
		reportItem = (ReportItem)aCache.getAsObject("reportSale");
		
		if( reportItem!=null ){
			List<ReportData> reportList = new ArrayList<ReportData>();
			List<GoodsCount> goodsList = reportItem.getGoodsCount();
			if ( goodsList!=null && goodsList.size()>0 ) {
				for (GoodsCount goodsCount : goodsList) {
					ReportData item  = new ReportData();
					item.setName(goodsCount.getgName());
					item.setNum(goodsCount.getgNum()+"");
					item.setPrice(goodsCount.getgPrice() + "");
					item.setType(0);
					reportList.add(item);
				}
			}
			List<GiftCount> giftList = reportItem.getGiftCount();
			if ( giftList!=null && giftList.size()>0 ) {
				for (GiftCount giftCount : giftList) {
					ReportData item  = new ReportData();
					item.setName(giftCount.getGiftName());
					item.setNum(giftCount.getGiftNum()+"");
					item.setPrice(giftCount.getGiftPrice()+"");
					item.setType(1);
					reportList.add(item);
				}
			}
			List<CouponCount> couponList = reportItem.getCouponCount();
			if ( couponList!=null && couponList.size()>0 ) {
				for (CouponCount couponCount : couponList) {
					ReportData item  = new ReportData();
					item.setName(couponCount.getcName());
					item.setNum(couponCount.getcNum()+"");
					item.setPrice(couponCount.getcPrice()+"");
					item.setType(2);
					reportList.add(item);
				}
			}
			
			List<BoxCount> countList = reportItem.getBoxCount();
			if ( countList!=null && countList.size()>0 ) {
				for (BoxCount boxCount : countList) {
					ReportData item  = new ReportData();
					item.setName(boxCount.getbName());
					item.setNum(boxCount.getbNum()+"");
					item.setPrice(boxCount.getbPrice()+"");
					item.setType(3);
					reportList.add(item);
				}
			}
			
			detailsAdapter = new ReportDayDetailsAdapter(homeActivity, reportList);
			layout_reportdetails_lv.setAdapter(detailsAdapter);
			detailsAdapter.notifyDataSetChanged();
			
			layout_reportdetails_sum.setText(Html.fromHtml("<small>¥</small>"+FormatTools.String2Money(reportItem.getSumFee()+"")));
			List<CouponCount> countsList = reportItem.getCouponCount();
			int sum = 0;
			if ( countsList!=null && countsList.size()>0 ) {
				for (int i = 0; i <countsList.size(); i++) {
					CouponCount coupon = countsList.get(i);
//					int price = (int) coupon.getcPrice() * coupon.getcNum();
					int price = (int) coupon.getcPrice();
					sum += price;
				}
			}
			layout_reportdetails_coupon.setText(Html.fromHtml("<small>¥</small>"+ FormatTools.String2Money(reportItem.getCouponFee()+"")));
			
		}else{
			SuperUI.openToast(homeActivity, "无法获取到数据，请检查您的网络");
		}
	}
	
	@Override
	public void onClick(View v) {
		if(v.getId()==R.id.layout_reportdetails_print){
			ThreadPoolManager.getInstance().executeTask(new Runnable() {
				@Override
				public void run() {
					try{
						if( reportItem!=null ){
							String date = aCache.getAsString("reportdate");
							if ( !TextUtils.isEmpty(date) ) {
								PrintUtil.printReport("报表", reportItem, date);
							}
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			});
		}
	}
	
}
