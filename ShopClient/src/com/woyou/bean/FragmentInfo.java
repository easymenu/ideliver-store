package com.woyou.bean;

import android.support.v4.app.Fragment;

/**
 * fragment info
 * 
 * @author xuron
 *
 */
public class FragmentInfo {
	private String backName;
	private Fragment backFragment;

	public String getBackName() {
		return backName;
	}

	public void setBackName(String backName) {
		this.backName = backName;
	}

	public Fragment getBackFragment() {
		return backFragment;
	}

	public void setBackFragment(Fragment backFragment) {
		this.backFragment = backFragment;
	}

}
