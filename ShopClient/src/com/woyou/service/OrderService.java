package com.woyou.service;

import java.util.List;

import com.woyou.bean.Order;
import com.woyou.bean.rpc.HandleCancelRes;
import com.woyou.bean.rpc.QueryNewOidListRes;
import com.woyou.bean.rpc.QueryOrderDetailsRes;
import com.woyou.bean.rpc.QueryOrderRes;
import com.woyou.bean.rpc.QueryPlayMoneyDetailsRes;
import com.woyou.bean.rpc.QueryTodayRes;
import com.woyou.bean.rpc.QueryYesterdayInfoRes;
import com.woyou.bean.rpc.Result;
import com.woyou.bean.rpc.SearchOrderListRes;
import com.woyou.bean.rpc.SendOrderRes;

import retrofit.RetrofitError;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

/**
 * 获取订单相关信息的Retrofit Service
 * @author 荣
 *
 */
public interface OrderService {
	/**
	 * 2.5	获取各状态订单列表（http://xxxx/queryOrderList）
	 * @param params      请求bean的json字符串
	 * @param isEncrypted 是否加密
	 * @param timeStamp   时间戳
	 * @param randomNum   6位随机数 
	 * @param sign        加密后的字符串
	 * 算法:sign = MD5(Parameter+IsEncrypted+TimeStamp+RandomNum+MD5(Key))
	 * @return
	 * @throws RetrofitError
	 */
	@FormUrlEncoded
	@POST("/queryOrderList")
	public Result<QueryOrderRes> queryOrderList(
			@Field("params") String params, 
			@Field("isEncrypted")String isEncrypted, 
			@Field("timeStamp") String timeStamp, 
			@Field("randomNum")String randomNum,
			@Field("sign")String sign)throws RetrofitError;
	/**
	 * 2.6	获取昨日信息简介（http://xxxx/queryYesterdayInfo）
	 * @param params      请求bean的json字符串
	 * @param isEncrypted 是否加密
	 * @param timeStamp   时间戳
	 * @param randomNum   6位随机数 
	 * @param sign        加密后的字符串
	 * 算法:sign = MD5(Parameter+IsEncrypted+TimeStamp+RandomNum+MD5(Key))
	 * @return
	 * @throws RetrofitError
	 */
	@FormUrlEncoded
	@POST("/queryYesterdayInfo")
	public Result<QueryYesterdayInfoRes> queryYesterdayInfo(
			@Field("params") String params, 
			@Field("isEncrypted")String isEncrypted, 
			@Field("timeStamp") String timeStamp, 
			@Field("randomNum")String randomNum,
			@Field("sign")String sign)throws RetrofitError;
	/**
	 * 2.7	获取今日信息简介（http://xxxx/queryTodayInfo）
	 * @param params      请求bean的json字符串
	 * @param isEncrypted 是否加密
	 * @param timeStamp   时间戳
	 * @param randomNum   6位随机数 
	 * @param sign        加密后的字符串
	 * 算法:sign = MD5(Parameter+IsEncrypted+TimeStamp+RandomNum+MD5(Key))
	 * @return
	 * @throws RetrofitError
	 */
	@FormUrlEncoded
	@POST("/queryTodayInfo")
	public Result<QueryTodayRes> queryTodayInfo(
			@Field("params") String params, 
			@Field("isEncrypted")String isEncrypted, 
			@Field("timeStamp") String timeStamp, 
			@Field("randomNum")String randomNum,
			@Field("sign")String sign)throws RetrofitError;
	/**
	 * 2.8	送出订单（http://xxxx/sendOrder）
	 * @param params      请求bean的json字符串
	 * @param isEncrypted 是否加密
	 * @param timeStamp   时间戳
	 * @param randomNum   6位随机数 
	 * @param sign        加密后的字符串
	 * 算法:sign = MD5(Parameter+IsEncrypted+TimeStamp+RandomNum+MD5(Key))
	 * @return
	 * @throws RetrofitError
	 */
	@FormUrlEncoded
	@POST("/sendOrder")
	public Result<SendOrderRes> sendOrder(
			@Field("params") String params, 
			@Field("isEncrypted")String isEncrypted, 
			@Field("timeStamp") String timeStamp, 
			@Field("randomNum")String randomNum,
			@Field("sign")String sign)throws RetrofitError;
//	/**
//	 * 2.13	获取订单详情（http://xxxx/queryOrderDetails）
//	 * @param params      请求bean的json字符串
//	 * @param isEncrypted 是否加密
//	 * @param timeStamp   时间戳
//	 * @param randomNum   6位随机数 
//	 * @param sign        加密后的字符串
//	 * 算法:sign = MD5(Parameter+IsEncrypted+TimeStamp+RandomNum+MD5(Key))
//	 * @return
//	 * @throws RetrofitError
//	 */
//	@FormUrlEncoded
//	@POST("/queryOrderDetails")
//	public Result<QueryOrderDetailsRes> queryOrderDetails(
//			@Field("params") String params, 
//			@Field("isEncrypted")String isEncrypted, 
//			@Field("timeStamp") String timeStamp, 
//			@Field("randomNum")String randomNum,
//			@Field("sign")String sign)throws RetrofitError;

	/**
	 * 2.14	获取新订单ID列表（http://xxxx/queryNewOidList）
	 * @param params      请求bean的json字符串
	 * @param isEncrypted 是否加密
	 * @param timeStamp   时间戳
	 * @param randomNum   6位随机数 
	 * @param sign        加密后的字符串
	 * 算法:sign = MD5(Parameter+IsEncrypted+TimeStamp+RandomNum+MD5(Key))
	 * @return
	 * @throws RetrofitError
	 */
	@FormUrlEncoded
	@POST("/queryNewOidList")
	public Result<List<QueryNewOidListRes>> queryNewOidList(
			@Field("params") String params, 
			@Field("isEncrypted")String isEncrypted, 
			@Field("timeStamp") String timeStamp, 
			@Field("randomNum")String randomNum,
			@Field("sign")String sign)throws RetrofitError;
	/**
	 * 2.15	更新订单状态（置S）（http://xxxx/updateOrdersStatus）
	 * @param params      请求bean的json字符串
	 * @param isEncrypted 是否加密
	 * @param timeStamp   时间戳
	 * @param randomNum   6位随机数 
	 * @param sign        加密后的字符串
	 * 算法:sign = MD5(Parameter+IsEncrypted+TimeStamp+RandomNum+MD5(Key))
	 * @return
	 * @throws RetrofitError
	 */
	@FormUrlEncoded
	@POST("/updateOrdersStatus")
	public Result updateOrdersStatus(
			@Field("params") String params, 
			@Field("isEncrypted")String isEncrypted, 
			@Field("timeStamp") String timeStamp, 
			@Field("randomNum")String randomNum,
			@Field("sign")String sign)throws RetrofitError;
	/**
	 * 2.16	接受或拒绝订单（http://xxxx/handleOrder）
	 * @param params      请求bean的json字符串
	 * @param isEncrypted 是否加密
	 * @param timeStamp   时间戳
	 * @param randomNum   6位随机数 
	 * @param sign        加密后的字符串
	 * 算法:sign = MD5(Parameter+IsEncrypted+TimeStamp+RandomNum+MD5(Key))
	 * @return
	 * @throws RetrofitError
	 */
	@FormUrlEncoded
	@POST("/handleOrder")
	public Result<Order> handleOrder(
			@Field("params") String params, 
			@Field("isEncrypted")String isEncrypted, 
			@Field("timeStamp") String timeStamp, 
			@Field("randomNum")String randomNum,
			@Field("sign")String sign)throws RetrofitError;
	/**
	 * 2.17	同意或拒绝取消订单（http://xxxx/handleCancel）
	 * @param params      请求bean的json字符串
	 * @param isEncrypted 是否加密
	 * @param timeStamp   时间戳
	 * @param randomNum   6位随机数 
	 * @param sign        加密后的字符串
	 * 算法:sign = MD5(Parameter+IsEncrypted+TimeStamp+RandomNum+MD5(Key))
	 * @return
	 * @throws RetrofitError
	 */
	@FormUrlEncoded
	@POST("/handleCancel")
	public Result<HandleCancelRes> handleCancel(
			@Field("params") String params, 
			@Field("isEncrypted")String isEncrypted, 
			@Field("timeStamp") String timeStamp, 
			@Field("randomNum")String randomNum,
			@Field("sign")String sign)throws RetrofitError;
	/**
	 * 2.17	同意或拒绝取消订单（http://xxxx/handleCancel）
	 * @param params      请求bean的json字符串
	 * @param isEncrypted 是否加密
	 * @param timeStamp   时间戳
	 * @param randomNum   6位随机数 
	 * @param sign        加密后的字符串
	 * 算法:sign = MD5(Parameter+IsEncrypted+TimeStamp+RandomNum+MD5(Key))
	 * @return
	 * @throws RetrofitError
	 */
	@FormUrlEncoded
	@POST("/searchOrderList")
	public Result<SearchOrderListRes> searchOrderList(
			@Field("params") String params, 
			@Field("isEncrypted")String isEncrypted, 
			@Field("timeStamp") String timeStamp, 
			@Field("randomNum")String randomNum,
			@Field("sign")String sign)throws RetrofitError;
	/**
	 * 2.2	获取订单详情（http://xxxx/v2_3queryOrderDetails）
	 * @param params      请求bean的json字符串
	 * @param isEncrypted 是否加密
	 * @param timeStamp   时间戳
	 * @param randomNum   6位随机数 
	 * @param sign        加密后的字符串
	 * 算法:sign = MD5(Parameter+IsEncrypted+TimeStamp+RandomNum+MD5(Key))
	 * @return
	 * @throws RetrofitError
	 */
	@FormUrlEncoded
	@POST("/v2_3queryOrderDetails")
	public Result<QueryOrderDetailsRes> v2_3queryOrderDetails(
			@Field("params") String params, 
			@Field("isEncrypted")String isEncrypted, 
			@Field("timeStamp") String timeStamp, 
			@Field("randomNum")String randomNum,
			@Field("sign")String sign)throws RetrofitError;

	/**
	 * 2.1	获取打款详情（http://xxxx/ v2_3queryPlayMoneyDetails）
	 * @param params      请求bean的json字符串
	 * @param isEncrypted 是否加密
	 * @param timeStamp   时间戳
	 * @param randomNum   6位随机数 
	 * @param sign        加密后的字符串
	 * 算法:sign = MD5(Parameter+IsEncrypted+TimeStamp+RandomNum+MD5(Key))
	 * @return
	 * @throws RetrofitError
	 */
	@FormUrlEncoded
	@POST("/v2_3queryPlayMoneyDetails")
	public Result<QueryPlayMoneyDetailsRes> v2_3queryPlayMoneyDetails(
			@Field("params") String params, 
			@Field("isEncrypted")String isEncrypted, 
			@Field("timeStamp") String timeStamp, 
			@Field("randomNum")String randomNum,
			@Field("sign")String sign)throws RetrofitError;
}
