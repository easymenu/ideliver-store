package com.woyou.bean;

import java.util.List;

/**
 * 每一条日报表或者月报表
 * 
 * @author zhou.ni
 * 
 */
public class ReportItem extends SuperModel {

	private static final long serialVersionUID = 1L;

	private int sumNum; // 订单总计
	private float sumFee; // 收入总金额
	private float couponFee; // 平台优惠券补贴
	private float businessFee; // 总营业额(“收款总计”和“平台优惠券补贴”之和。)
	private List<GoodsCount> goodsCount; // 商品列表
	private List<GiftCount> giftCount; // 赠品列表
	private List<CouponCount> couponCount; // 优惠券列表
	private List<BoxCount> boxCount; // 餐盒费统计列表

	public int getSumNum() {
		return sumNum;
	}

	public void setSumNum(int sumNum) {
		this.sumNum = sumNum;
	}

	public float getCouponFee() {
		return couponFee;
	}

	public void setCouponFee(float couponFee) {
		this.couponFee = couponFee;
	}

	public float getBusinessFee() {
		return businessFee;
	}

	public void setBusinessFee(float businessFee) {
		this.businessFee = businessFee;
	}

	public float getSumFee() {
		return sumFee;
	}

	public void setSumFee(float sumFee) {
		this.sumFee = sumFee;
	}

	public List<GoodsCount> getGoodsCount() {
		return goodsCount;
	}

	public void setGoodsCount(List<GoodsCount> goodsCount) {
		this.goodsCount = goodsCount;
	}

	public List<GiftCount> getGiftCount() {
		return giftCount;
	}

	public void setGiftCount(List<GiftCount> giftCount) {
		this.giftCount = giftCount;
	}

	public List<CouponCount> getCouponCount() {
		return couponCount;
	}

	public void setCouponCount(List<CouponCount> couponCount) {
		this.couponCount = couponCount;
	}

	@Override
	public String toString() {
		return "ReportItem [sumNum=" + sumNum + ", sumFee=" + sumFee
				+ ", couponFee=" + couponFee + ", businessFee=" + businessFee
				+ ", goodsCount=" + goodsCount + ", giftCount=" + giftCount
				+ ", couponCount=" + couponCount + ", boxCount=" + boxCount
				+ "]";
	}

	public List<BoxCount> getBoxCount() {
		return boxCount;
	}

	public void setBoxCount(List<BoxCount> boxCount) {
		this.boxCount = boxCount;
	}

}
