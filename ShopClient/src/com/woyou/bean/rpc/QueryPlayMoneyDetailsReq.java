package com.woyou.bean.rpc;

import com.woyou.bean.SuperModel;

/**
 * 查询订单详情请求bean
 * 
 * @author xuron
 *
 */
public class QueryPlayMoneyDetailsReq extends SuperModel {
	/**
	 * 店铺ID
	 */
	private String sId;
	/**
	 * 密码
	 */
	private String pwd;

	public String getsId() {
		return sId;
	}

	public void setsId(String sId) {
		this.sId = sId;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	@Override
	public String toString() {
		return "QueryPlayMoneyDetailsReq [sId=" + sId + ", pwd=" + pwd + "]";
	}

}
