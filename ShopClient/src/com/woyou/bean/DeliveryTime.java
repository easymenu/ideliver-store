package com.woyou.bean;


/**
 * 每一条外卖时段
 * @author zhou.ni
 *
 */
public class DeliveryTime extends SuperModel {

	private static final long serialVersionUID = 1L;

	private String tId;				//外卖时段ID
	private String flag;			//繁忙标志0-可送餐1-繁忙时段不可送餐
	private String content;			//时段内容

	public String gettId() {
		return tId;
	}

	public void settId(String tId) {
		this.tId = tId;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public String toString() {
		return "DeliveryTime [tId=" + tId + ", flag=" + flag + ", content="
				+ content + "]";
	}

}
