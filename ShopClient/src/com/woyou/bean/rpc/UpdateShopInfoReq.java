package com.woyou.bean.rpc;

/**
 * 更改店铺信息的请求bean
 * @author zhou.ni
 *
 */
public class UpdateShopInfoReq {
	
	private String sId;			//店铺id
	private String pwd;			//密码
	private String phone;		//店铺电话
	private String notice;		//店铺公告
	private String brief;		//店铺简介
	
	public String getsId() {
		return sId;
	}
	public void setsId(String sId) {
		this.sId = sId;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getNotice() {
		return notice;
	}
	public void setNotice(String notice) {
		this.notice = notice;
	}
	public String getBrief() {
		return brief;
	}
	public void setBrief(String brief) {
		this.brief = brief;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	
	
}
