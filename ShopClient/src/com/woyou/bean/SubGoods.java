package com.woyou.bean;


/**
 * 子商品
 * 
 * @author 荣
 * 
 */
public class SubGoods extends SuperModel {
	/**
	 * 子商品ID
	 */
	private String gId = "";
	/**
	 * 子商品名称
	 */
	private String gName = "";
	/**
	 * 子商品单价
	 */
	private float price;
	/**
	 * 子商品单位
	 */
	private String unit = "";
	/**
	 * 子商品数量
	 */
	private int saleNum;

	public String getgId() {
		return gId;
	}

	public void setgId(String gId) {
		this.gId = gId;
	}

	public String getgName() {
		return gName;
	}

	public void setgName(String gName) {
		this.gName = gName;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public int getSaleNum() {
		return saleNum;
	}

	public void setSaleNum(int saleNum) {
		this.saleNum = saleNum;
	}

}
