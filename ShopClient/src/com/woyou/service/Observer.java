package com.woyou.service;
/**
 * 自定义一个观察者
 * @author lenovo
 *
 */
public interface Observer {
	/**
	 * 处理推送消息
	 * @param type 1:表示新订单	2:表示退单
	 * @param orderNum 订单编号
	 */
	public void update(int type,String orderNum);
}
