package com.woyou.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jing.biandang.R;
import com.woyou.bean.GoodsTypeInfo;

public class SoldoutLeftAdapter extends BaseAdapter {
	private LayoutInflater layoutInflater;
	private List<GoodsTypeInfo> data;
	private int selectItem = 0;
	private int currentItem=0;

	/**
	 * 设置那个item被选中了
	 * @param selectItem
	 */
	public void setSelectItem(int selectItem) {
		this.selectItem = selectItem;
	}

	public SoldoutLeftAdapter(Context c,List<GoodsTypeInfo> data) {
		layoutInflater = LayoutInflater.from(c);
		this.data = data;
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;
		if(convertView==null){
			viewHolder=new ViewHolder();
			convertView = layoutInflater.inflate(R.layout.item_soldout_allleft_lv, null);
			viewHolder.allleft_layout= (RelativeLayout) convertView.findViewById(R.id.item_soldout_allleft_layout);
			viewHolder.allleft_name=(TextView)convertView.findViewById(R.id.item_soldout_allleft_name);
			convertView.setTag(viewHolder);
		}else{
			viewHolder=(ViewHolder)convertView.getTag();
		}
		if (currentItem == 0) {
			viewHolder.allleft_layout.setBackgroundResource(R.raw.soldout_allleft_bg_s);
		}
		/**
		 * 设置被选中的Item的背景色改变
		 */
		if(position==selectItem){
			viewHolder.allleft_layout.setBackgroundResource(R.raw.soldout_allleft_bg_s);
			currentItem=position;
		}else{
			viewHolder.allleft_layout.setBackgroundResource(R.raw.soldout_allleft_bg);
		}
		viewHolder.allleft_name.setText(""+data.get(position).gettName()+" ( "+data.get(position).getgNum()+" )");
		return convertView;
	}

	static class ViewHolder {
		RelativeLayout allleft_layout;
		TextView allleft_name;
	}

}
