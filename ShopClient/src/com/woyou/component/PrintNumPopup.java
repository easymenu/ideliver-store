package com.woyou.component;

import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jing.biandang.R;
import com.woyou.activity.HomeActivity;
import com.woyou.utils.PrintNumUtils;

public class PrintNumPopup implements OnClickListener {
	private HomeActivity homeActivity;
	private TextView positionView;
	private PopupWindow popupWindow;
	private RelativeLayout pop_setting_zero;
	private RelativeLayout pop_setting_one;
	private RelativeLayout pop_setting_two;
	private RelativeLayout pop_setting_three;

	public PrintNumPopup(HomeActivity homeActivity, TextView positionView) {
		this.homeActivity = homeActivity;
		this.positionView = positionView;
	}

	// 弹出泡泡窗体
	public void showPopup() {
		// 实例化泡泡窗台的布局文件
		View contentView = LayoutInflater.from(homeActivity).inflate(R.layout.pop_setting_choice, null);
		pop_setting_zero = (RelativeLayout) contentView.findViewById(R.id.pop_setting_zero);
		pop_setting_one = (RelativeLayout) contentView.findViewById(R.id.pop_setting_one);
		pop_setting_two = (RelativeLayout) contentView.findViewById(R.id.pop_setting_two);
		pop_setting_three = (RelativeLayout) contentView.findViewById(R.id.pop_setting_three);

		pop_setting_zero.setOnClickListener(this);
		pop_setting_one.setOnClickListener(this);
		pop_setting_two.setOnClickListener(this);
		pop_setting_three.setOnClickListener(this);

		// 实例化泡泡窗体
		if (popupWindow == null) {
			popupWindow = new PopupWindow(contentView, 200, ViewGroup.LayoutParams.WRAP_CONTENT);
		}
		popupWindow.setFocusable(true);
		popupWindow.setBackgroundDrawable(new BitmapDrawable());

		// 显示泡泡窗体
		popupWindow.showAsDropDown(positionView, 0, -10);
	}

	/**
	 * 隐藏pop窗体
	 */
	public void hidePopup() {
		if (popupWindow != null && popupWindow.isShowing()) {
			popupWindow.dismiss();
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.pop_setting_zero:
			positionView.setText("不打印");
			PrintNumUtils.savePrintNum(homeActivity, 0);
			break;
		case R.id.pop_setting_one:
			positionView.setText("1张");
			PrintNumUtils.savePrintNum(homeActivity, 1);
			break;
		case R.id.pop_setting_two:
			positionView.setText("2张");
			PrintNumUtils.savePrintNum(homeActivity, 2);
			break;
		case R.id.pop_setting_three:
			positionView.setText("3张");
			PrintNumUtils.savePrintNum(homeActivity, 3);
			break;
		}
		popupWindow.dismiss();
	}
}
