package com.woyou.bean;

import java.io.Serializable;
import java.util.ArrayList;


/**
 * 为存储自定义的List 实现序列号接口是为了写入ACache
 * 
 * @author lenovo
 * 
 * @param <E>
 */
public class WYList<E> extends ArrayList<E> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
