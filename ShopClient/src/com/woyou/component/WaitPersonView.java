/** 
 * @Title:  WaitPersonView.java 
 * @author:  xuron
 * @data:  2015年11月27日 上午11:16:20 <创建时间>
 * 
 * @history：<以下是历史记录>
 *
 * @modifier: <修改人>
 * @modify date: 2015年11月27日 上午11:16:20 <修改时间>
 * @log: <修改内容>
 *
 * @modifier: <修改人>
 * @modify date: 2015年11月27日 上午11:16:20 <修改时间>
 * @log: <修改内容>
 */
package com.woyou.component;

import com.jing.biandang.R;
import com.woyou.model.OrderManageModel;

import android.content.Context;
import android.text.Html;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * 自定义等待人数的View
 * 
 * @author xuron
 * @versionCode 1 <每次修改提交前+1>
 */
public class WaitPersonView extends SuperLayout {
	private OrderManageModel omModel;
	public int wait4Num = 0;
	private ImageView layout_order_p1;
	private ImageView layout_order_p2;
	private ImageView layout_order_p3;
	private ImageView layout_order_p4;
	private ImageView layout_order_p5;
	private ImageView layout_order_p6;
	private ImageView layout_order_p7;
	private TextView layout_order_pnum;

	public WaitPersonView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initView();
	}

	public WaitPersonView(Context context) {
		super(context);
		initView();
	}

	public WaitPersonView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initView();
	}

	@Override
	protected void initView() {
		View.inflate(mContext, R.layout.layout_waitpersonview, this);
		layout_order_p1 = (ImageView)findViewById(R.id.layout_order_p1);
		layout_order_p2 = (ImageView)findViewById(R.id.layout_order_p2);
		layout_order_p3 = (ImageView)findViewById(R.id.layout_order_p3);
		layout_order_p4 = (ImageView)findViewById(R.id.layout_order_p4);
		layout_order_p5 = (ImageView)findViewById(R.id.layout_order_p5);
		layout_order_p6 = (ImageView)findViewById(R.id.layout_order_p6);
		layout_order_p7 = (ImageView)findViewById(R.id.layout_order_p7);
		layout_order_pnum = (TextView)findViewById(R.id.layout_order_pnum);
	}
	/**
	 * 设置参数
	 */
	public void setParams(OrderManageModel omModel) {
		this.omModel = omModel;
	}

	/**
	 * 显示多少个等待的人
	 */
	public void showPersonNum() {
		// 未送订单
		wait4Num = omModel.getNoSentList().size();
		layout_order_p1.setVisibility(View.VISIBLE);
		layout_order_p2.setVisibility(View.VISIBLE);
		layout_order_p3.setVisibility(View.VISIBLE);
		layout_order_p4.setVisibility(View.VISIBLE);
		layout_order_p5.setVisibility(View.VISIBLE);
		layout_order_p6.setVisibility(View.VISIBLE);
		layout_order_p7.setVisibility(View.VISIBLE);
		layout_order_pnum.setVisibility(View.VISIBLE);
		layout_order_pnum.setText(Html.fromHtml("<big><big>" + wait4Num + "</big></big>人<br/><small>等待中</small>"));
		if (wait4Num > 7) {
			return;
		}
		layout_order_p1.setVisibility(View.INVISIBLE);
		layout_order_p2.setVisibility(View.INVISIBLE);
		layout_order_p3.setVisibility(View.INVISIBLE);
		layout_order_p4.setVisibility(View.INVISIBLE);
		layout_order_p5.setVisibility(View.INVISIBLE);
		layout_order_p6.setVisibility(View.INVISIBLE);
		layout_order_p7.setVisibility(View.INVISIBLE);
		switch (wait4Num) {
		case 1:
			layout_order_p1.setVisibility(View.VISIBLE);
			break;
		case 2:
			layout_order_p1.setVisibility(View.VISIBLE);
			layout_order_p2.setVisibility(View.VISIBLE);
			break;
		case 3:
			layout_order_p1.setVisibility(View.VISIBLE);
			layout_order_p2.setVisibility(View.VISIBLE);
			layout_order_p3.setVisibility(View.VISIBLE);
			break;
		case 4:
			layout_order_p1.setVisibility(View.VISIBLE);
			layout_order_p2.setVisibility(View.VISIBLE);
			layout_order_p3.setVisibility(View.VISIBLE);
			layout_order_p4.setVisibility(View.VISIBLE);
			break;
		case 5:
			layout_order_p1.setVisibility(View.VISIBLE);
			layout_order_p2.setVisibility(View.VISIBLE);
			layout_order_p3.setVisibility(View.VISIBLE);
			layout_order_p4.setVisibility(View.VISIBLE);
			layout_order_p5.setVisibility(View.VISIBLE);
			break;
		case 6:
			layout_order_p1.setVisibility(View.VISIBLE);
			layout_order_p2.setVisibility(View.VISIBLE);
			layout_order_p3.setVisibility(View.VISIBLE);
			layout_order_p4.setVisibility(View.VISIBLE);
			layout_order_p5.setVisibility(View.VISIBLE);
			layout_order_p6.setVisibility(View.VISIBLE);
			break;
		case 7:
			layout_order_p1.setVisibility(View.VISIBLE);
			layout_order_p2.setVisibility(View.VISIBLE);
			layout_order_p3.setVisibility(View.VISIBLE);
			layout_order_p4.setVisibility(View.VISIBLE);
			layout_order_p5.setVisibility(View.VISIBLE);
			layout_order_p6.setVisibility(View.VISIBLE);
			layout_order_p7.setVisibility(View.VISIBLE);
			break;
		}

	}
}
