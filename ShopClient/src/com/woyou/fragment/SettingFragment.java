package com.woyou.fragment;

import com.jing.biandang.R;
import com.woyou.activity.HomeActivity;
import com.woyou.bean.FmInfo;
import com.woyou.component.PrintNumPopup;
import com.woyou.component.SuperUI;
import com.woyou.fragment.shop.ShopFragment;
import com.woyou.utils.NavigationbarUtils;
import com.woyou.utils.PrintNumUtils;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * 设置界面
 * @author zhou.ni
 *
 */
public class SettingFragment extends SuperFragment implements OnClickListener {
	private HomeActivity homeActivity;
	private View layout_setting;
	private TextView layout_setting_printnum;
	private RelativeLayout layout_setting_network;
	private TextView layout_setting_network_type;
	private RelativeLayout layout_setting_shop;
	private RelativeLayout layout_setting_sound;
	private RelativeLayout layout_setting_sms;
	private RelativeLayout layout_setting_exit;
	private PrintNumPopup printNumPopup;
	private Dialog openExitDialog;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		layout_setting = inflater.inflate(R.layout.layout_setting, container, false);
		homeActivity=(HomeActivity)getActivity();
		return layout_setting;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		layout_setting_network = (RelativeLayout) layout_setting.findViewById(R.id.layout_setting_network);
		layout_setting_network_type = (TextView) layout_setting.findViewById(R.id.layout_setting_network_type);
		layout_setting_printnum = (TextView) layout_setting.findViewById(R.id.layout_setting_printnum);
		layout_setting_shop = (RelativeLayout) layout_setting.findViewById(R.id.layout_setting_shop);
		layout_setting_sms = (RelativeLayout) layout_setting.findViewById(R.id.layout_setting_sms);
		layout_setting_sound = (RelativeLayout) layout_setting.findViewById(R.id.layout_setting_sound);
		layout_setting_exit = (RelativeLayout) layout_setting.findViewById(R.id.layout_setting_exit);

		layout_setting_network.setOnClickListener(this);
		layout_setting_printnum.setOnClickListener(this);
		layout_setting_shop.setOnClickListener(this);
		layout_setting_sms.setOnClickListener(this);
		layout_setting_sound.setOnClickListener(this);
		layout_setting_exit.setOnClickListener(this);

		// 读取SharedPreferences显示设置打印张数
		if (PrintNumUtils.getPrintNum(homeActivity) == 0) {
			layout_setting_printnum.setText("不打印");
		} else {
			layout_setting_printnum.setText(PrintNumUtils.getPrintNum(homeActivity) + "张");
		}

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.layout_setting_network:
			Intent intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
			startActivity(intent);
			NavigationbarUtils.getInstance(mContext).show();
			break;
		case R.id.layout_setting_printnum:
			printNumPopup =new PrintNumPopup(homeActivity, layout_setting_printnum);
			printNumPopup.showPopup();
			break;
		case R.id.layout_setting_shop:
			homeActivity.openSubFM(this, new FmInfo(ShopFragment.class,"店铺设置", true));
			break;
		case R.id.layout_setting_exit:
			openExitDialog=SuperUI.openExitDialog(homeActivity, "是否确认退出当前店铺账号？", false);
			break;
		}
	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		super.onHiddenChanged(hidden);
		if (!hidden) {
			NavigationbarUtils.getInstance(mContext).hide();
		} else {
			if(openExitDialog!=null&&openExitDialog.isShowing()){
				openExitDialog.cancel();
			}
			if (printNumPopup != null) {
				printNumPopup.hidePopup();
			}
		}
	}
}
