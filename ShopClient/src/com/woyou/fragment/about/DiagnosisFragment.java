package com.woyou.fragment.about;

import com.jing.biandang.R;
import com.woyou.Constants;
import com.woyou.activity.HomeActivity;
import com.woyou.bean.rpc.Result;
import com.woyou.bean.rpc.ServerTimeRes;
import com.woyou.fragment.SuperFragment;
import com.woyou.model.rpc.ShopModel;
import com.woyou.service.MessageConnection;
import com.woyou.utils.StateJudgeTools;
import com.woyou.utils.ThreadPoolManager;
import com.woyou.utils.Utils;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import retrofit.RetrofitError;

/**
 * 网络诊断界面
 * 
 * @author zhou.ni
 * 
 */
public class DiagnosisFragment extends SuperFragment implements OnClickListener {
	private HomeActivity homeActivity;
	private View layout_diagnosis;
	private RelativeLayout layout_diagnosis_bg;
	private ImageView layout_diagnosis_yun;
	private TextView layout_diagnosis_status;
	private ImageView layout_diagnosis_gateway_iv;
	private ImageView layout_diagnosis_server_iv;
	private ImageView layout_diagnosis_web_iv;
	private ImageView layout_diagnosis_heartbeat_iv;
	private ImageView layout_diagnosis_interface_iv;
	private ImageView layout_diagnosis_replay;
	private TextView layout_diagnosis_replay_tv;
	private ImageView layout_diagnosis_helf;
	private TextView layout_diagnosis_nettype;
	private ImageView layout_diagnosis_icon;
	private TextView layout_diagnosis_speed;

	private TextView layout_diagnosis_gateway_tv;
	private TextView layout_diagnosis_server_tv;
	private TextView layout_diagnosis_web_tv;
	private TextView layout_diagnosis_heartbeat_tv;
	private TextView layout_diagnosis_interface_tv;
	private TextView layout_diagnosis_gateway_num;
	private TextView layout_diagnosis_server_num;
	private TextView layout_diagnosis_web_num;
	private TextView layout_diagnosis_heartbeat_num;
	private TextView layout_diagnosis_interface_num;

	private RotateAnimation animGateway;
	private RotateAnimation animServer;
	private RotateAnimation animWeb;
	private RotateAnimation animHeartbeat;
	private RotateAnimation animInterface;
	private boolean gateway = false;
	private boolean server = false;
	private boolean web = false;
	private boolean heartbeat = false;
	private boolean interfac = false;

	private TelephonyManager tel;
	private String STRNetworkOperator[] = { "46000", "46002", "46001", "46003" }; // 中国移动、中国联通、中国电信
	private int mark = -1;

	/**
	 * 统计检测了几项测试数据
	 */
	private int times = 0;
	private Handler handler = new Handler();

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		layout_diagnosis = inflater.inflate(R.layout.layout_setting_diagnosis,
				container, false);
		homeActivity=(HomeActivity)getActivity();
		return layout_diagnosis;
	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		super.onHiddenChanged(hidden);
		if(!hidden){
			checkout();
		}
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		initView();
		tel = (TelephonyManager) homeActivity
				.getSystemService(Context.TELEPHONY_SERVICE);
		getmark();
		checkout();
	}

	/**
	 * 初始化
	 */
	public void initView() {
		layout_diagnosis_bg = (RelativeLayout) layout_diagnosis
				.findViewById(R.id.layout_diagnosis_bg);
		layout_diagnosis_yun = (ImageView) layout_diagnosis
				.findViewById(R.id.layout_diagnosis_yun);
		layout_diagnosis_status = (TextView) layout_diagnosis
				.findViewById(R.id.layout_diagnosis_status);

		layout_diagnosis_gateway_iv = (ImageView) layout_diagnosis
				.findViewById(R.id.layout_diagnosis_gateway_iv);
		layout_diagnosis_server_iv = (ImageView) layout_diagnosis
				.findViewById(R.id.layout_diagnosis_server_iv);
		layout_diagnosis_web_iv = (ImageView) layout_diagnosis
				.findViewById(R.id.layout_diagnosis_web_iv);
		layout_diagnosis_heartbeat_iv = (ImageView) layout_diagnosis
				.findViewById(R.id.layout_diagnosis_heartbeat_iv);
		layout_diagnosis_interface_iv = (ImageView) layout_diagnosis
				.findViewById(R.id.layout_diagnosis_interface_iv);
		layout_diagnosis_replay = (ImageView) layout_diagnosis
				.findViewById(R.id.layout_diagnosis_replay);
		layout_diagnosis_replay_tv = (TextView) layout_diagnosis
				.findViewById(R.id.layout_diagnosis_replay_tv);
		layout_diagnosis_helf = (ImageView) layout_diagnosis
				.findViewById(R.id.layout_diagnosis_helf);
		layout_diagnosis_nettype = (TextView) layout_diagnosis
				.findViewById(R.id.layout_diagnosis_nettype);
		layout_diagnosis_icon = (ImageView) layout_diagnosis
				.findViewById(R.id.layout_diagnosis_icon);
		layout_diagnosis_speed = (TextView) layout_diagnosis
				.findViewById(R.id.layout_diagnosis_speed);
		layout_diagnosis_replay.setOnClickListener(this);

		layout_diagnosis_gateway_tv = (TextView) layout_diagnosis
				.findViewById(R.id.layout_diagnosis_gateway_tv);
		layout_diagnosis_server_tv = (TextView) layout_diagnosis
				.findViewById(R.id.layout_diagnosis_server_tv);
		layout_diagnosis_web_tv = (TextView) layout_diagnosis
				.findViewById(R.id.layout_diagnosis_web_tv);
		layout_diagnosis_heartbeat_tv = (TextView) layout_diagnosis
				.findViewById(R.id.layout_diagnosis_heartbeat_tv);
		layout_diagnosis_interface_tv = (TextView) layout_diagnosis
				.findViewById(R.id.layout_diagnosis_interface_tv);
		layout_diagnosis_gateway_num = (TextView) layout_diagnosis
				.findViewById(R.id.layout_diagnosis_gateway_num);
		layout_diagnosis_server_num = (TextView) layout_diagnosis
				.findViewById(R.id.layout_diagnosis_server_num);
		layout_diagnosis_web_num = (TextView) layout_diagnosis
				.findViewById(R.id.layout_diagnosis_web_num);
		layout_diagnosis_heartbeat_num = (TextView) layout_diagnosis
				.findViewById(R.id.layout_diagnosis_heartbeat_num);
		layout_diagnosis_interface_num = (TextView) layout_diagnosis
				.findViewById(R.id.layout_diagnosis_interface_num);
	}

	/**
	 * 开始测试
	 */
	public void checkout() {
		if (layout_diagnosis_gateway_iv != null) {
			resetView();
			startAnimGateway(layout_diagnosis_gateway_iv, animGateway);
			startAnimServer(layout_diagnosis_server_iv, animServer);
			startAnimWeb(layout_diagnosis_web_iv, animWeb);
			startAnimHeartbeat(layout_diagnosis_heartbeat_iv, animHeartbeat);
			startAnimInterface(layout_diagnosis_interface_iv, animInterface);
			runDiagnosisThread();
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.layout_diagnosis_replay:
			resetView();
			checkout();
			break;
		}
	}

	/**
	 * 启动测试
	 */
	public void runDiagnosisThread() {
		// ping网关
		ThreadPoolManager.getInstance().executeTask(new Runnable() {
			@Override
			public void run() {
				try {
					times = times + 1;
					// ping 联通网关
					boolean b = Utils.ping("221.5.88.88");
					if (b) {
						gateway = true;
						stopAnim(animGateway, true);
					} else {
						gateway = false;
						stopAnim(animGateway, false);
					}
				} catch (Exception e) {
					e.printStackTrace();
					gateway = false;
					stopAnim(animGateway, false);
				}
			}
		});

		// ping服务器
		ThreadPoolManager.getInstance().executeTask(new Runnable() {
			@Override
			public void run() {
				try {
					times = times + 1;
					boolean b = Utils.ping(Constants.HEART_BEAT_SERVER);
					if (b) {
						server = true;
						stopAnim(animServer, true);
					} else {
						server = false;
						stopAnim(animServer, false);
					}
				} catch (Exception e) {
					e.printStackTrace();
					server = false;
					stopAnim(animServer, false);
				}
			}
		});

		// 测试网页
		ThreadPoolManager.getInstance().executeTask(new Runnable() {

			@Override
			public void run() {
				try {
					times = times + 1;
					boolean b = Utils.accessWeb(Constants.WEB_ADDRESS);
					if (b) {
						web = true;
						stopAnim(animWeb, true);
					} else {
						web = false;
						stopAnim(animWeb, false);
					}
				} catch (Exception e) {
					e.printStackTrace();
					web = false;
					stopAnim(animWeb, false);
				}
			}
		});

		// 测试接口
		ThreadPoolManager.getInstance().executeTask(new Runnable() {
			@Override
			public void run() {
				try {
					times = times + 1;
					Result<ServerTimeRes> result = ShopModel.getInstance(homeActivity).queryServerTime();
					if (result != null && result.getCode() == 1) {
						interfac = true;
						stopAnim(animInterface, true);
					} else {
						interfac = false;
						stopAnim(animInterface, false);
					}
				} catch (RetrofitError error) {
					interfac = false;
					stopAnim(animInterface, false);
				}
			}
		});

		// 查看心跳状态
		ThreadPoolManager.getInstance().executeTask(new Runnable() {

			@Override
			public void run() {
				try {
					times = times + 1;
					boolean isHeartbeatOk = MessageConnection.isHeartbeatOk;
					if (isHeartbeatOk) {
						heartbeat = true;
						stopAnim(animHeartbeat, true);
					} else {
						heartbeat = false;
						stopAnim(animHeartbeat, false);
					}
				} catch (Exception e) {
					e.printStackTrace();
					heartbeat = false;
					stopAnim(animHeartbeat, false);
				}
			}
		});

		// 查看当前网络状态
		getNowNetWorkState();
	}

	/**
	 * 查看当前网络状态 获取当前的网络状态 -1：没有网络 ，1：WIFI网络， 2：2G网络， 3：3G网络，4：4G网络，5：以太网
	 */
	public void getNowNetWorkState() {
		int netState = StateJudgeTools.getNetworkState(homeActivity);
		switch (netState) {
		case -1:
			tel.listen(null, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS
					| PhoneStateListener.LISTEN_SERVICE_STATE);
			layout_diagnosis_nettype.setText("未连接");
			layout_diagnosis_nettype.setTextColor(0xffd40000);
			layout_diagnosis_speed.setText("网络未连接");
			layout_diagnosis_icon.setImageResource(R.raw.icon_net_no_28);
			break;
		case 1:
			tel.listen(null, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS
					| PhoneStateListener.LISTEN_SERVICE_STATE);
			layout_diagnosis_nettype.setText("WIFI");
			layout_diagnosis_speed.setText(""
					+ StateJudgeTools.getWifiRssi(homeActivity) + "db");
			layout_diagnosis_icon
					.setImageResource(R.raw.icon_diagnosis_wifi4_46);
			break;
		case 2:
		case 3:
		case 4:
			tel.listen(new PhoneStateMonitor(),
					PhoneStateListener.LISTEN_SIGNAL_STRENGTHS
							| PhoneStateListener.LISTEN_SERVICE_STATE);
			layout_diagnosis_nettype.setText("3G");
			layout_diagnosis_icon.setImageResource(R.raw.icon_diagnosis_g5_46);
			break;
		case 5:
			tel.listen(new PhoneStateMonitor(),
					PhoneStateListener.LISTEN_SIGNAL_STRENGTHS
							| PhoneStateListener.LISTEN_SERVICE_STATE);
			layout_diagnosis_nettype.setText("有线网络");
			layout_diagnosis_icon
					.setImageResource(R.raw.icon_diagnosis_line_46);
			break;
		}
	}

	/**
	 * ping网关
	 * 
	 * @param iv
	 * @param anim
	 */
	private void startAnimGateway(ImageView iv, RotateAnimation anim) {
		if (anim == null) {
			animGateway = new RotateAnimation(0, 360,
					Animation.RELATIVE_TO_SELF, 0.5f,
					Animation.RELATIVE_TO_SELF, 0.5f);
		}
		startAnim(iv, animGateway);
	}

	/**
	 * ping服务器
	 * 
	 * @param iv
	 * @param anim
	 */
	private void startAnimServer(ImageView iv, RotateAnimation anim) {
		if (anim == null) {
			animServer = new RotateAnimation(0, 360,
					Animation.RELATIVE_TO_SELF, 0.5f,
					Animation.RELATIVE_TO_SELF, 0.5f);
		}
		startAnim(iv, animServer);
	}

	/**
	 * 测试网页
	 * 
	 * @param iv
	 * @param anim
	 */
	private void startAnimWeb(ImageView iv, RotateAnimation anim) {
		if (anim == null) {
			animWeb = new RotateAnimation(0, 360, Animation.RELATIVE_TO_SELF,
					0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
		}
		startAnim(iv, animWeb);
	}

	/**
	 * 测试心跳
	 * 
	 * @param iv
	 * @param anim
	 */
	private void startAnimHeartbeat(ImageView iv, RotateAnimation anim) {
		if (anim == null) {
			animHeartbeat = new RotateAnimation(0, 360,
					Animation.RELATIVE_TO_SELF, 0.5f,
					Animation.RELATIVE_TO_SELF, 0.5f);
		}
		startAnim(iv, animHeartbeat);
	}

	/**
	 * 测试接口
	 * 
	 * @param iv
	 * @param anim
	 */
	private void startAnimInterface(ImageView iv, RotateAnimation anim) {
		if (anim == null) {
			animInterface = new RotateAnimation(0, 360,
					Animation.RELATIVE_TO_SELF, 0.5f,
					Animation.RELATIVE_TO_SELF, 0.5f);
		}
		startAnim(iv, animInterface);
	}

	/**
	 * 启动图片旋转动画
	 */
	private void startAnim(ImageView iv, RotateAnimation anim) {
		anim.setDuration(2000);
		anim.setInterpolator(new LinearInterpolator());
		anim.setRepeatCount(-1);
		iv.startAnimation(anim);
	}

	/**
	 * 关闭图片旋转动画
	 * 
	 * @param anim
	 * @param type
	 *            true成功，false失败
	 */
	private void stopAnim(final RotateAnimation anim, final boolean type) {
		homeActivity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (anim != null) {
					anim.getFillBefore();
					anim.cancel();
				}
				// 延迟200毫米刷新页面保证页面不变脸
				handler.postDelayed(new Runnable() {
					@Override
					public void run() {
						if (type) {
							if (anim == animGateway) {
								layout_diagnosis_gateway_iv
										.setImageResource(R.raw.icon_diagnosis_green);
								layout_diagnosis_gateway_tv.setVisibility(View.GONE);
								layout_diagnosis_gateway_num
										.setVisibility(View.VISIBLE);
							} else if (anim == animServer) {
								layout_diagnosis_server_iv
										.setImageResource(R.raw.icon_diagnosis_green);
								layout_diagnosis_server_tv.setVisibility(View.GONE);
								layout_diagnosis_server_num.setVisibility(View.VISIBLE);
							} else if (anim == animWeb) {
								layout_diagnosis_web_iv
										.setImageResource(R.raw.icon_diagnosis_green);
								layout_diagnosis_web_tv.setVisibility(View.GONE);
								layout_diagnosis_web_num.setVisibility(View.VISIBLE);
							} else if (anim == animHeartbeat) {
								layout_diagnosis_heartbeat_iv
										.setImageResource(R.raw.icon_diagnosis_green);
								layout_diagnosis_heartbeat_tv.setVisibility(View.GONE);
								layout_diagnosis_heartbeat_num
										.setVisibility(View.VISIBLE);
							} else if (anim == animInterface) {
								layout_diagnosis_interface_iv
										.setImageResource(R.raw.icon_diagnosis_green);
								layout_diagnosis_interface_tv.setVisibility(View.GONE);
								layout_diagnosis_interface_num
										.setVisibility(View.VISIBLE);
							}
						} else {
							if (anim == animGateway) {
								layout_diagnosis_gateway_iv
										.setImageResource(R.raw.icon_diagnosis_red);
								layout_diagnosis_gateway_tv.setVisibility(View.GONE);
								layout_diagnosis_gateway_num
										.setVisibility(View.VISIBLE);
							} else if (anim == animServer) {
								layout_diagnosis_server_iv
										.setImageResource(R.raw.icon_diagnosis_red);
								layout_diagnosis_server_tv.setVisibility(View.GONE);
								layout_diagnosis_server_num.setVisibility(View.VISIBLE);
							} else if (anim == animWeb) {
								layout_diagnosis_web_iv
										.setImageResource(R.raw.icon_diagnosis_red);
								layout_diagnosis_web_tv.setVisibility(View.GONE);
								layout_diagnosis_web_num.setVisibility(View.VISIBLE);
							} else if (anim == animHeartbeat) {
								layout_diagnosis_heartbeat_iv
										.setImageResource(R.raw.icon_diagnosis_red);
								layout_diagnosis_heartbeat_tv.setVisibility(View.GONE);
								layout_diagnosis_heartbeat_num
										.setVisibility(View.VISIBLE);
							} else if (anim == animInterface) {
								layout_diagnosis_interface_iv
										.setImageResource(R.raw.icon_diagnosis_red);
								layout_diagnosis_interface_tv.setVisibility(View.GONE);
								layout_diagnosis_interface_num
										.setVisibility(View.VISIBLE);
							}
						}
						/**
						 * 当每一项都检测完成后
						 */
						if (times == 5) {
							layout_diagnosis_replay.setVisibility(View.VISIBLE);
							layout_diagnosis_replay_tv.setVisibility(View.VISIBLE);
							if (heartbeat && interfac) {
								layout_diagnosis_status.setText("服务器可用");
								layout_diagnosis_status.setTextColor(0xff00cd00);
								layout_diagnosis_bg
										.setBackgroundResource(R.drawable.gradient_diagnosis_yes_bg);
								layout_diagnosis_yun
										.setImageResource(R.raw.icon_diagnosis_yun_yes);
								layout_diagnosis_helf
										.setImageResource(R.raw.icon_diagnosis_helf);
							} else {
								layout_diagnosis_status.setText("服务器不可用");
								layout_diagnosis_status.setTextColor(0xffd40000);
								layout_diagnosis_bg
										.setBackgroundResource(R.drawable.gradient_diagnosis_no_bg);
								layout_diagnosis_yun
										.setImageResource(R.raw.icon_diagnosis_yun_no);
								layout_diagnosis_helf
										.setImageResource(R.raw.icon_diagnosis_helf_red);
							}
						}
					}
				}, 200);
			}
		});
	}

	/**
	 * 重置页面
	 */
	private void resetView() {
		times = 0;
		layout_diagnosis_bg
				.setBackgroundResource(R.drawable.gradient_diagnosis_bg);
		layout_diagnosis_yun.setImageResource(R.raw.icon_diagnosis_yun);
		layout_diagnosis_status.setText("正在检测...");
		layout_diagnosis_status.setTextColor(0xff009cff);
		layout_diagnosis_replay.setVisibility(View.GONE);
		layout_diagnosis_replay_tv.setVisibility(View.GONE);
		layout_diagnosis_nettype.setText("");
		layout_diagnosis_nettype.setTextColor(0xff00cd00);
		// layout_diagnosis_speed.setText("");
		layout_diagnosis_helf.setImageResource(R.raw.icon_diagnosis_helf);
		layout_diagnosis_gateway_iv.setImageResource(R.raw.icon_diagnosis_blue);
		layout_diagnosis_server_iv.setImageResource(R.raw.icon_diagnosis_blue);
		layout_diagnosis_web_iv.setImageResource(R.raw.icon_diagnosis_blue);
		layout_diagnosis_heartbeat_iv
				.setImageResource(R.raw.icon_diagnosis_blue);
		layout_diagnosis_interface_iv
				.setImageResource(R.raw.icon_diagnosis_blue);
		layout_diagnosis_gateway_tv.setVisibility(View.VISIBLE);
		layout_diagnosis_server_tv.setVisibility(View.VISIBLE);
		layout_diagnosis_web_tv.setVisibility(View.VISIBLE);
		layout_diagnosis_heartbeat_tv.setVisibility(View.VISIBLE);
		layout_diagnosis_interface_tv.setVisibility(View.VISIBLE);
		layout_diagnosis_gateway_num.setVisibility(View.GONE);
		layout_diagnosis_server_num.setVisibility(View.GONE);
		layout_diagnosis_web_num.setVisibility(View.GONE);
		layout_diagnosis_heartbeat_num.setVisibility(View.GONE);
		layout_diagnosis_interface_num.setVisibility(View.GONE);
	}

	/**
	 * 得到当前电话卡的归属运营商
	 */
	private void getmark() {
		String strNetworkOperator = tel.getNetworkOperator();
		if (strNetworkOperator != null) {
			if (strNetworkOperator.equals(STRNetworkOperator[0])|| strNetworkOperator.equals(STRNetworkOperator[1])) {
				mark = 0;
				Log.v("3G", "mark==" + 0);
			} else if (strNetworkOperator.equals(STRNetworkOperator[2])) {
				mark = 1;
				Log.v("3G", "mark==" + 1);
			} else if (strNetworkOperator.equals(STRNetworkOperator[3])) {
				mark = 2;
				Log.v("3G", "mark==" + 2);
			} else {
				mark = -1;
				Log.v("3G", "mark==" + -1);
			}
		} else {
			mark = -1;
			Log.v("3G", "mark==" + -1);
		}
	}

	class PhoneStateMonitor extends PhoneStateListener {

		@Override
		public void onSignalStrengthsChanged(SignalStrength signalStrength) {
			super.onSignalStrengthsChanged(signalStrength);
			/*
			 * signalStrength.isGsm() 是否GSM信号 2G or 3G
			 * signalStrength.getCdmaDbm(); 联通3G 信号强度
			 * signalStrength.getCdmaEcio(); 联通3G 载干比
			 * signalStrength.getEvdoDbm(); 电信3G 信号强度
			 * signalStrength.getEvdoEcio(); 电信3G 载干比
			 * signalStrength.getEvdoSnr(); 电信3G 信噪比
			 * signalStrength.getGsmSignalStrength(); 2G 信号强度
			 * signalStrength.getGsmBitErrorRate(); 2G 误码率 载干比
			 * ，它是指空中模拟电波中的信号与噪声的比值
			 */
			Log.v("3G", "change signal");
			if (mark == -1) {
				layout_diagnosis_speed.setText(""
						+ StateJudgeTools.getWifiRssi(homeActivity) + "db");
			}
			if (mark == 0) {
				layout_diagnosis_speed.setText(signalStrength
						.getGsmSignalStrength() + "db");
			} else if (mark == 1) {
				layout_diagnosis_speed.setText(signalStrength.getCdmaDbm()
						+ "db");
			} else if (mark == 2) {
				layout_diagnosis_speed.setText(signalStrength.getEvdoDbm()
						+ "db");
			}

		}

		/**
		 * 3g状态的改变
		 */
		@Override
		public void onServiceStateChanged(ServiceState serviceState) {
			super.onServiceStateChanged(serviceState);
			/*
			 * ServiceState.STATE_EMERGENCY_ONLY 仅限紧急呼叫
			 * ServiceState.STATE_IN_SERVICE 信号正常
			 * ServiceState.STATE_OUT_OF_SERVICE 不在服务区
			 * ServiceState.STATE_POWER_OFF 断电
			 */
			int pos = serviceState.getState();
			Log.v("3G", "state change===" + pos);
			switch (pos) {
			case ServiceState.STATE_EMERGENCY_ONLY:
				break;
			case ServiceState.STATE_IN_SERVICE:
				break;
			case ServiceState.STATE_OUT_OF_SERVICE:
				break;
			case ServiceState.STATE_POWER_OFF:
				break;
			default:
				break;
			}
		}

	}

}
