package com.woyou.fragment;

import java.lang.reflect.Field;

import com.woyou.bean.FmInfo;
import com.woyou.utils.ACache;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
/**
 * Fragment的超类
 * @author xuron 
 * @versionCode 1 <每次修改提交前+1>
 */
public class SuperFragment extends Fragment {
	public Context mContext;
	public ACache aCache;
	/**
	 * Fragment之间传递参数
	 */
	public FmInfo fmInfo;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext=getActivity();
		aCache=getACache();
	}

	/**
	 * 获取Fm传递的值
	 * @return
	 */
	public FmInfo getFmInfo() {
		return fmInfo;
	}

	/**
	 * 发送Fm传递的值
	 * @param fmInfo
	 */
	public void setFmInfo(FmInfo fmInfo) {
		this.fmInfo = fmInfo;
	}

	/**
	 * 缓存对象Acache
	 */
	public ACache getACache() {
		if (aCache == null) {
			aCache = ACache.get(mContext);
		}
		return aCache;
	}

	@Override
    public void onDetach() {
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        super.onDetach();
    }
	
	/**
	 * 隐藏输入法
	 * 
	 * @param context
	 * @param achor
	 */
	public static void hideSoftInput(Context context, View achor) {
		InputMethodManager imm = (InputMethodManager) context
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(achor.getWindowToken(),
				InputMethodManager.HIDE_NOT_ALWAYS);
	}
	
	
}
