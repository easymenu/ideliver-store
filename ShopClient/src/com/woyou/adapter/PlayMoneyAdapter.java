/**
* @Title: PlayMoneyAdapter.java
* @Package com.woyou.adapter
* @Description: TODO(用一句话描述该文件做什么)
* @author blanks
* @date 2015年9月8日
* @version V1.0
*/
package com.woyou.adapter;

import java.util.List;

import com.jing.biandang.R;
import com.woyou.bean.PlayMoney;
import com.woyou.bean.StayPay;
import com.woyou.component.NumberTextView;
import com.woyou.utils.FormatTools;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * @author blanks
 * @创建时间 2015年9月8日 下午7:58:23
 * @version v1.0
 */
public class PlayMoneyAdapter extends BaseAdapter {

	private Context mContext;
	private List<Object> mData;

	public PlayMoneyAdapter(Context context, List<Object> data) {
		// TODO Auto-generated constructor stub
		this.mContext = context;
		this.mData = data;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getCount()
	 */
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mData.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getItem(int)
	 */
	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mData.get(position);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getItemId(int)
	 */
	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.Adapter#getView(int, android.view.View,
	 * android.view.ViewGroup)
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder = null;
		Object object = mData.get(position);
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.item_playmoney_mainview, null);
			holder = new ViewHolder();
			holder.textType = (TextView) convertView.findViewById(R.id.textType);
			holder.textMoney = (NumberTextView) convertView.findViewById(R.id.textMoney);
			holder.textTip = (TextView) convertView.findViewById(R.id.textTip);
			holder.textNumber = (NumberTextView) convertView.findViewById(R.id.textNum);
			holder.textTime = (TextView) convertView.findViewById(R.id.textTime);
			holder.textPayee = (TextView) convertView.findViewById(R.id.textPayee);
			holder.textMatter = (TextView) convertView.findViewById(R.id.textMatter);
			holder.rootPedingLayout = (RelativeLayout) convertView.findViewById(R.id.rootPedingLayout);
			holder.rootRecordLayout = (LinearLayout) convertView.findViewById(R.id.rootRecordLayout);
			holder.rootTipLayout = convertView.findViewById(R.id.rootTipLayout);
			holder.rootTip2Layout=(TextView)convertView.findViewById(R.id.rootTip2Layout);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.rootPedingLayout.setVisibility(View.GONE);
		holder.rootRecordLayout.setVisibility(View.GONE);
		holder.rootTipLayout.setVisibility(View.GONE);
		holder.	rootTip2Layout.setVisibility(View.GONE);
		if (object instanceof StayPay) {
			StayPay stayPay = (StayPay) object;
			holder.rootPedingLayout.setVisibility(View.VISIBLE);
			holder.textType.setText(stayPay.getsType());
			holder.textMoney.setText(stayPay.getsMoney());
			holder.textTip.setText(stayPay.getsTime());
		} else if (object instanceof PlayMoney) {
			PlayMoney playMoney = (PlayMoney) object;
			holder.rootRecordLayout.setVisibility(View.VISIBLE);
			holder.textNumber.setText(playMoney.getpMoney() + "");
			holder.textMatter.setText("打款事项："+playMoney.getpMatters());
			holder.textTime.setText(FormatTools.convertTime(playMoney.getpTime()*1000));
			holder.textPayee.setText("收款方："+playMoney.getpPayee());
		} else {
			String info=(String)object;
			if(info!=null&&info.equals("打款记录")){
				holder.rootTipLayout.setVisibility(View.VISIBLE);
			}else if(info!=null&&info.equals("最近3个月内尚无打款记录")){
				holder.	rootTip2Layout.setVisibility(View.VISIBLE);
			}
		}
		return convertView;
	}

	static class ViewHolder {
		TextView textTip;
		NumberTextView textMoney;
		TextView textType;
		TextView textTime;
		NumberTextView textNumber;
		TextView textPayee;
		TextView textMatter;
		LinearLayout rootRecordLayout;
		RelativeLayout rootPedingLayout;
		View rootTipLayout;
		TextView rootTip2Layout;
	}

}
