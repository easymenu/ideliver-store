/** 
 * @Title:  ActionBarController.java 
 * @author:  xuron
 * @data:  2015年11月24日 上午10:54:50 <创建时间>
 * 
 * @history：<以下是历史记录>
 *
 * @modifier: <修改人>
 * @modify date: 2015年11月24日 上午10:54:50 <修改时间>
 * @log: <修改内容>
 *
 * @modifier: <修改人>
 * @modify date: 2015年11月24日 上午10:54:50 <修改时间>
 * @log: <修改内容>
 */
package com.woyou.controller;

import com.jing.biandang.R;
import com.woyou.Constants;
import com.woyou.activity.HomeActivity;
import com.woyou.bean.DeliveryTime;
import com.woyou.bean.WYList;
import com.woyou.bean.rpc.Result;
import com.woyou.bean.rpc.SetShopStatusReq;
import com.woyou.component.ActionbarView;
import com.woyou.event.EventModifyShopStatus;
import com.woyou.model.rpc.ShopModel;
import com.woyou.utils.FormatTools;
import com.woyou.utils.LoginUtils;
import com.woyou.utils.StateJudgeTools;
import com.woyou.utils.TypeJudgeTools;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import de.greenrobot.event.EventBus;
import net.simonvt.menudrawer.MenuDrawer;

/**
 * 抽取Actionbar的逻辑
 * 
 * @author xuron
 * @versionCode 1 <每次修改提交前+1>
 */
public class ActionBarController extends SuperController {
	/**
	 * activity对象
	 */
	public HomeActivity homeActivity;
	/**
	 * 自定义ActionbarView
	 */
	public ActionbarView actionbarView;
	/**
	 * 同步时间
	 */
	private boolean isSyncTimeRunning = true;
	/**
	 * 是否正在设置店铺状态
	 */
	public boolean isSettingShopStatus = true;
	private int shopStatusNum = 0;
	/**
	 * 设置繁忙状态0:繁忙中，1：一小时，2:两小时
	 */
	public int busyStatus = 0;
	/**
	 * 营业状态
	 */
	public String shopStatus;

	/**
	 * 设置店铺营业状态
	 */
	private SetShopStatusReq setShopStatusReq = new SetShopStatusReq();
	private ShopModel mShopModel;

	/**
	 * @param mContext
	 */
	public ActionBarController(Context mContext,ActionbarView actionbarView) {
		super(mContext);
		this.mContext = mContext;
		homeActivity = (HomeActivity) mContext;
		mShopModel = ShopModel.getInstance(mContext);
		this.actionbarView = actionbarView;
	}

	/**
	 * 设置店铺营业状态通过营业时间段
	 */
	public void setShopStatusByTimerList(String atTime) {
		DeliveryTime deliveryTime = null;
		// 获取外送时间段
		WYList<DeliveryTime> deliveryWyList = (WYList<DeliveryTime>) aCache.getAsObject("delivertime");
		for (int i = 0; i < deliveryWyList.size(); i++) {
			if (!TextUtils.isEmpty(deliveryWyList.get(i).getContent())) {
				String time = deliveryWyList.get(i).getContent().substring(0, 5);
				if (time.equals(atTime)) {
					deliveryTime = deliveryWyList.get(i);
					break;
				}
			}
		}
		// 根据该营业时间段来设置营业状态
		// 繁忙标志0-可送餐1-繁忙时段不可送餐
		if (deliveryTime != null) {
			if (deliveryTime.getFlag().equals("0")) {
				Constants.shopStatus = "S";
				linkingShopStatus();
			} else {
				Constants.shopStatus = "B";
				busyStatus = 0;
				linkingShopStatus();
			}
		}
	}

	/**
	 * 显示mask屏蔽掉actionbar上的点击事件
	 */
	public void showMask() {
		runOnUI(new Runnable() {
			@Override
			public void run() {
				homeActivity.menuDrawer.setTouchMode(MenuDrawer.TOUCH_MODE_NONE);
				homeActivity.actionbarView.layout_actionbar_mask.setVisibility(View.VISIBLE);
			}
		});
	}

	/**
	 * 设置店铺状态
	 */
	public void linkingShopStatus() {
		// 设置默认状态
		isSettingShopStatus = true;
		// 连接中
		executeTask(new Runnable() {
			@Override
			public void run() {
				while (isSettingShopStatus) {
					runOnUI(new Runnable() {
						public void run() {
							actionbarView.layout_actionbar_choice.setBackgroundResource(R.raw.actionbar_state_link);
							switch (shopStatusNum) {
							case 0:
								actionbarView.layout_actionbar_choice_tv.setText("连接中");
								shopStatusNum = 1;
								break;
							case 1:
								actionbarView.layout_actionbar_choice_tv.setText("连接中.");
								shopStatusNum = 2;
								break;
							case 2:
								actionbarView.layout_actionbar_choice_tv.setText("连接中..");
								shopStatusNum = 3;
								break;
							case 3:
								actionbarView.layout_actionbar_choice_tv.setText("连接中...");
								shopStatusNum = 0;
								break;
							default:
								shopStatusNum = 0;
								break;
							}
						}
					});
					// 间隔300毫秒
					try {
						Thread.sleep(300);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		});

		// 设置营业状态
		executeTask(new Runnable() {
			@Override
			public void run() {
				while (isSettingShopStatus) {
					try {
						setShopStatusReq.setsId(LoginUtils.getUserInfo(mContext).getsId());
						setShopStatusReq.setPwd(LoginUtils.getUserInfo(mContext).getPwd());
						setShopStatusReq.setStatus(Constants.shopStatus);
						Result result = mShopModel.setShopStatus(setShopStatusReq);
						if (result != null && result.code == 1) {
							// 取消连接中
							isSettingShopStatus = false;
							runOnUI(new Runnable() {
								public void run() {
									if (Constants.shopStatus.equals("S")) {
										actionbarView.layout_actionbar_choice
												.setBackgroundResource(R.raw.actionbar_state_green);
										actionbarView.layout_actionbar_choice_tv.setText("营业中");
										EventBus.getDefault().post(new EventModifyShopStatus("S"));
									} else if (Constants.shopStatus.equals("C")) {
										actionbarView.layout_actionbar_choice
												.setBackgroundResource(R.raw.actionbar_state_red);
										actionbarView.layout_actionbar_choice_tv.setText("休息");
										EventBus.getDefault().post(new EventModifyShopStatus("C"));
									} else if (Constants.shopStatus.equals("B")) {
										if (busyStatus == 1) {
											actionbarView.layout_actionbar_choice
													.setBackgroundResource(R.raw.actionbar_state_yellow);
											actionbarView.layout_actionbar_choice_tv.setText("繁忙1小时");
										} else if (busyStatus == 2) {
											actionbarView.layout_actionbar_choice
													.setBackgroundResource(R.raw.actionbar_state_orange);
											actionbarView.layout_actionbar_choice_tv.setText("繁忙2小时");
										} else {
											actionbarView.layout_actionbar_choice
													.setBackgroundResource(R.raw.actionbar_state_yellow);
											actionbarView.layout_actionbar_choice_tv.setText("繁忙中");
										}
										EventBus.getDefault().post(new EventModifyShopStatus("B"));
									}
								}
							});
						} else if (result != null && result.code == -3) {
							homeActivity.homeController.exitLogin();
						}
						Thread.sleep(300); // 延时300毫秒
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		});
	}

	/**
	 * 设置店铺状态
	 */
	public void linkingShopStatus(final RelativeLayout choice, final TextView choice_tv) {
		// 设置默认状态
		isSettingShopStatus = true;
		// 连接中
		executeTask(new Runnable() {
			@Override
			public void run() {
				while (isSettingShopStatus) {
					runOnUI(new Runnable() {
						public void run() {
							choice.setBackgroundResource(R.raw.actionbar_state_link);
							switch (shopStatusNum) {
							case 0:
								choice_tv.setText("连接中");
								shopStatusNum = 1;
								break;
							case 1:
								choice_tv.setText("连接中.");
								shopStatusNum = 2;
								break;
							case 2:
								choice_tv.setText("连接中..");
								shopStatusNum = 3;
								break;
							case 3:
								choice_tv.setText("连接中...");
								shopStatusNum = 0;
								break;
							default:
								shopStatusNum = 0;
								break;
							}
						}
					});
					// 间隔300毫秒
					try {
						Thread.sleep(300);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		});

		// 设置营业状态
		executeTask(new Runnable() {
			@Override
			public void run() {
				while (isSettingShopStatus) {
					try {
						setShopStatusReq.setsId(LoginUtils.getUserInfo(mContext).getsId());
						setShopStatusReq.setPwd(LoginUtils.getUserInfo(mContext).getPwd());
						setShopStatusReq.setStatus(Constants.shopStatus);
						Result result = mShopModel.setShopStatus(setShopStatusReq);
						if (result != null && result.code == 1) {
							// 取消连接中
							isSettingShopStatus = false;
							runOnUI(new Runnable() {
								public void run() {
									if (Constants.shopStatus.equals("S")) {
										choice.setBackgroundResource(R.raw.actionbar_state_green);
										choice_tv.setText("营业中");
										EventBus.getDefault().post(new EventModifyShopStatus("S"));
									} else if (Constants.shopStatus.equals("C")) {
										choice.setBackgroundResource(R.raw.actionbar_state_red);
										choice_tv.setText("休息");
										EventBus.getDefault().post(new EventModifyShopStatus("C"));
									} else if (Constants.shopStatus.equals("B")) {
										if (busyStatus == 1) {
											choice.setBackgroundResource(R.raw.actionbar_state_yellow);
											choice_tv.setText("繁忙1小时");
										} else if (busyStatus == 2) {
											choice.setBackgroundResource(R.raw.actionbar_state_orange);
											choice_tv.setText("繁忙2小时");
										} else {
											choice.setBackgroundResource(R.raw.actionbar_state_yellow);
											choice_tv.setText("繁忙中");
										}
										EventBus.getDefault().post(new EventModifyShopStatus("B"));
									}
								}
							});
						} else if (result != null && result.code == -3) {
							homeActivity.homeController.exitLogin();
						}
						Thread.sleep(300); // 延时300毫秒
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		});
	}

	/**
	 * 获取网络类型和服务器时间 -1：没有网络 1：WIFI网络 2：wap网络 3：net网络
	 */
	public void getNetTypeAndTime(final TextView time, final TextView nettype, final ImageView neticon) {
		// 更新时间
		executeTask(new Runnable() {
			@Override
			public void run() {
				try {
					while (isSyncTimeRunning) {
						runOnUI(new Runnable() {
							@Override
							public void run() {
								String ServerTime = aCache.getAsString("ServerTime");
								if (!TypeJudgeTools.isNull(ServerTime)) {
									String times = FormatTools.convertTime(Long.valueOf(ServerTime), "HH:mm");
									time.setText(times);
								}
								// 更改网络
								int netType = StateJudgeTools.getNetworkState(mContext);
								switch (netType) {
								case 1:
									nettype.setText("Wifi");
									neticon.setImageResource(R.raw.icon_net_wifi_28);
									break;
								case 3:
									nettype.setText("3G");
									neticon.setImageResource(R.raw.icon_net_3g_28);
									break;
								case 5:
									nettype.setText("有线网络");
									neticon.setImageResource(R.raw.icon_net_line_28);
									break;
								default:
									nettype.setText("无网络");
									neticon.setImageResource(R.raw.icon_net_no_28);
									break;
								}
							}
						});
						// 10s刷新一次
						Thread.sleep(10 * 1000);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * 获取当前店铺状态
	 * 
	 * @return
	 */
	public boolean isSettingShopStatus() {
		return isSettingShopStatus;
	}

	/**
	 * 设置当前店铺状态
	 * 
	 * @param isSettingShopStatus
	 */
	public void setSettingShopStatus(boolean isSettingShopStatus) {
		this.isSettingShopStatus = isSettingShopStatus;
	}

	/**
	 * 获取店铺状态
	 * 
	 * @return
	 */
	public int getBusyStatus() {
		return busyStatus;
	}

	/**
	 * 设置店铺状态
	 * 
	 * @param busyStatus
	 */
	public void setBusyStatus(int busyStatus) {
		this.busyStatus = busyStatus;
	}

}
