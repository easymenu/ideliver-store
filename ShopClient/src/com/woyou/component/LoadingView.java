/** 
 * @Title:  LoadingView.java 
 * @author:  xuron
 * @data:  2015年11月25日 上午10:52:24 <创建时间>
 * 
 * @history：<以下是历史记录>
 *
 * @modifier: <修改人>
 * @modify date: 2015年11月25日 上午10:52:24 <修改时间>
 * @log: <修改内容>
 *
 * @modifier: <修改人>
 * @modify date: 2015年11月25日 上午10:52:24 <修改时间>
 * @log: <修改内容>
 */
package com.woyou.component;

import com.jing.biandang.R;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;

/**
 * 加载提示框
 * 
 * @author xuron
 * @versionCode 1 <每次修改提交前+1>
 */
public class LoadingView extends SuperLayout {
	// 载入提示
	private RelativeLayout layout_loading_hint;
	private ImageView layout_loading_iv;
	private RotateAnimation rotateAnimation;

	/**
	 * @param context
	 */
	public LoadingView(Context context) {
		super(context);
		initView();
	}

	public LoadingView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initView();
	}

	public LoadingView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initView();
	}

	@Override
	protected void initView() {
		View.inflate(mContext, R.layout.layout_loading, this);
		// 加载提示
		layout_loading_hint = (RelativeLayout) findViewById(R.id.layout_loading_hint);
		layout_loading_iv = (ImageView) findViewById(R.id.layout_loading_iv);
		// 防止下层内容被点击
		layout_loading_hint.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				return true;
			}
		});
	}

	/**
	 * 显示隐藏载入View
	 */
	public void showLoadingHint() {
		post(new Runnable() {
			@Override
			public void run() {
				startAnim(layout_loading_iv);
				layout_loading_hint.setVisibility(View.VISIBLE);
			}
		});
	}

	/**
	 * 隐藏数据载入View
	 */
	public void hideLoadingHint() {
		post(new Runnable() {
			@Override
			public void run() {
				stopAnim();
				layout_loading_hint.setVisibility(View.GONE);
			}
		});

	}

	/**
	 * 启动图片旋转动画
	 */
	private void startAnim(ImageView layout_loadding_iv) {

		if (rotateAnimation == null) {
			rotateAnimation = new RotateAnimation(0, 360, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
					0.5f);
		}
		rotateAnimation.setDuration(1000);
		rotateAnimation.setInterpolator(new LinearInterpolator());
		rotateAnimation.setRepeatCount(-1);
		layout_loadding_iv.startAnimation(rotateAnimation);
	}

	/**
	 * 关闭图片旋转动画
	 */
	private void stopAnim() {
		if (rotateAnimation != null) {
			rotateAnimation.cancel();
		}
	}
}
