package com.woyou.adapter;

import java.util.List;

import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jing.biandang.R;
import com.woyou.activity.HomeActivity;
import com.woyou.bean.GoodsState;
import com.woyou.bean.OptionState;
import com.woyou.component.SwitchButton;
import com.woyou.event.EventRefreshAllView;
import com.woyou.fragment.SoldoutFragment;

import de.greenrobot.event.EventBus;

public class SoldoutAdapter extends BaseAdapter {
	private SoldoutFragment fragment;
	private LayoutInflater layoutInflater;
	private List<GoodsState> data;
	
	public SoldoutAdapter(HomeActivity homeActivity,SoldoutFragment fragment,List<GoodsState> data) {
		this.fragment=fragment;
		layoutInflater = LayoutInflater.from(homeActivity);
		this.data = data;
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder = null;
		if (convertView == null) {
			convertView = layoutInflater.inflate(R.layout.item_soldout_group,null);
			viewHolder = new ViewHolder();
			viewHolder.group_bg = (LinearLayout) convertView.findViewById(R.id.item_soldout_group_bg);
			viewHolder.group_child = (LinearLayout) convertView.findViewById(R.id.item_soldout_group_child);
			viewHolder.group_im = (ImageView) convertView.findViewById(R.id.item_soldout_group_im);
			viewHolder.group_tv = (TextView) convertView.findViewById(R.id.item_soldout_group_tv);
			viewHolder.group_sb = (SwitchButton) convertView.findViewById(R.id.item_soldout_group_sb);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		GoodsState goodsState = data.get(position);
		goodsState.setPosition(position);

		if (goodsState.getState() == 1) {
			viewHolder.group_sb.setChecked(true, false);
		} else {
			viewHolder.group_sb.setChecked(false, false);
		}
		viewHolder.group_tv.setText(goodsState.getgName());
		// 点击item
		viewHolder.group_bg.setTag(R.id.goods, goodsState);
		viewHolder.group_bg.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				GoodsState goodsState = (GoodsState) v.getTag(R.id.goods);
				LinearLayout child = (LinearLayout) v.findViewById(R.id.item_soldout_group_child);
				ImageView group_im = (ImageView) v.findViewById(R.id.item_soldout_group_im);
				boolean isShow = goodsState.isChildIsShow();
				if (!isShow) {
					goodsState.setChildIsShow(true);
					addProp(goodsState, child, group_im);
				} else {
					goodsState.setChildIsShow(false);
					child.setVisibility(View.GONE);
					group_im.setImageResource(R.raw.icon_soldout_close);
				}
			}
		});
		// 点击Group
		viewHolder.group_sb.setOnCheckedChangeListener(new GroupListener(goodsState));
		// 动态添加属性
		if (goodsState.getOptList()!=null&&goodsState.getOptList().size() > 0) {
			if(goodsState.getState()==1){
				viewHolder.group_sb.setVisibility(View.GONE);
			}else{
				viewHolder.group_sb.setVisibility(View.VISIBLE);
			}
			if (goodsState.isChildIsShow()) {
				addProp(goodsState, viewHolder.group_child, viewHolder.group_im);
			} else {
				viewHolder.group_im.setImageResource(R.raw.icon_soldout_close);
				viewHolder.group_child.removeAllViews();
			}
			viewHolder.group_im.setVisibility(View.VISIBLE);
		} else {
			viewHolder.group_sb.setVisibility(View.VISIBLE);
			viewHolder.group_im.setImageResource(R.raw.icon_soldout_close);
			viewHolder.group_child.removeAllViews();
			viewHolder.group_im.setVisibility(View.GONE);
		}
		return convertView;
	}

	/**
	 * 更改switchButton的状态并刷新数据
	 */
	public void changeGroupSBState(GoodsState goodsState, boolean isChecked) {

		if (isChecked) {
			goodsState.setState(1);
		} else {
			goodsState.setState(2);
		}
	}

	/**
	 * 更改暂缺数据
	 */
	public void refreshSoldoutData() {

		if (fragment.soldoutData.size() >= 1) {
			fragment.layout_soldout_num.setText("暂缺商品（"+ fragment.soldoutData.size() + "）");
		} else {
			fragment.layout_soldout_num.setText("暂缺商品");
		}
	}

	private void addProp(GoodsState goodsState, LinearLayout child,ImageView group_im) {
		child.setVisibility(View.VISIBLE);
		child.setOnClickListener(null);
		group_im.setImageResource(R.raw.icon_soldout_open);
		child.removeAllViews();
		List<OptionState>  optionStates=goodsState.getOptList();
		if(optionStates!=null&&optionStates.size()>0){
			for (int i = 0; i <optionStates.size(); i++) {
				View childView = layoutInflater.inflate(R.layout.item_soldout_child, null);
				
				TextView child_tv = (TextView) childView.findViewById(R.id.item_soldout_child_tv);
				SwitchButton child_sb = (SwitchButton) childView.findViewById(R.id.item_soldout_child_sb);
				child_tv.setText("" + optionStates.get(i).getOptName());
				if (optionStates.get(i).getState() == 1) {
					child_sb.setChecked(true);
				} else {
					child_sb.setChecked(false);
				}
				// 属性SB的监听器
				child_sb.setOnCheckedChangeListener(new ChildListener(i, goodsState));

				//添加暂缺商品
				if(optionStates.get(i).getState()==2){
					child.addView(childView);
				}
			}
		}
	}

	static class ViewHolder {
		LinearLayout group_bg;
		LinearLayout group_child;
		ImageView group_im;
		TextView group_tv;
		SwitchButton group_sb;
	}

	/**
	 * Group属性SB监听器
	 * 
	 * @author lenovo
	 * 
	 */
	// private boolean groupClick = false;
	public class GroupListener implements OnCheckedChangeListener {
		private GoodsState goodsState;

		public GroupListener(GoodsState goodsState) {
			this.goodsState = goodsState;
		}

		@Override
		public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {

			changeGroupSBState(goodsState, isChecked);
			List<OptionState> optionStates=goodsState.getOptList();
			if(optionStates!=null){
				if (isChecked) {
					for (int i = 0; i < optionStates.size(); i++) {
						optionStates.get(i).setState(1);
					}
				} else {
					for (int i = 0; i < optionStates.size(); i++) {
						optionStates.get(i).setState(2);
					}
				}
			}
			//刷新整个列表
			EventBus.getDefault().post(new EventRefreshAllView());
		}

	}

	/**
	 * Child 属性SB监听器
	 * 
	 * @author lenovo
	 * 
	 */
	// private boolean propClick = false;
	public class ChildListener implements OnCheckedChangeListener {
		int index;
		GoodsState goodsState;

		public ChildListener(int index, GoodsState goodsState) {
			this.index = index;
			this.goodsState=goodsState;
		}

		@Override
		public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
			List<OptionState> optionStates=goodsState.getOptList();

			if (isChecked) {
				optionStates.get(index).setState(1);// 有货
			} else {
				optionStates.get(index).setState(2);// 缺货
			}
			// 分别记录被选中的数量
			int size = optionStates.size();
			int status1 = 0;
			int status2 = 0;
			for (int i = 0; i < optionStates.size(); i++) {
				if (optionStates.get(i).getState() == 1) {
					status1 = status1 + 1;
				} else if (optionStates.get(i).getState() == 2) {
					status2 = status2 + 1;
				}
			}
			// 两种状态分别
			if (status1 == size) {
				changeGroupSBState(goodsState, true);// 商品设置有货
			} else if (status2 == size) {
				changeGroupSBState(goodsState, false);// 商品设置缺货
			} else {
				for (int i = 0; i < optionStates.size(); i++) {
					if (optionStates.get(i).getState() == 1) {
						changeGroupSBState(goodsState, true);
					}
				}
			}
			//刷新整个列表
			EventBus.getDefault().post(new EventRefreshAllView());
		}
	}
}
