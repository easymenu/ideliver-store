package com.woyou.fragment.about;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jing.biandang.R;
import com.woyou.fragment.SuperFragment;

public class ShareFragment extends SuperFragment {
	private View layout_share;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		layout_share = inflater.inflate(R.layout.layout_share, container, false);
		return layout_share;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}

}
