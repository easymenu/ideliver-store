package com.woyou.bean;

/**
 * 打款记录
 * 
 * @author xuron
 *
 */
public class PlayMoney extends SuperModel {
	/**
	 * 打款日期（时间戳）
	 */
	private long pTime;
	/**
	 * 待支付金额
	 */
	private float pMoney;
	/**
	 * 收款人
	 */
	private String pPayee;
	/**
	 * 打款事项
	 */
	private String pMatters;

	public long getpTime() {
		return pTime;
	}

	public void setpTime(long pTime) {
		this.pTime = pTime;
	}

	public float getpMoney() {
		return pMoney;
	}

	public void setpMoney(float pMoney) {
		this.pMoney = pMoney;
	}

	public String getpPayee() {
		return pPayee;
	}

	public void setpPayee(String pPayee) {
		this.pPayee = pPayee;
	}

	public String getpMatters() {
		return pMatters;
	}

	public void setpMatters(String pMatters) {
		this.pMatters = pMatters;
	}

	@Override
	public String toString() {
		return "PlayMoney [pTime=" + pTime + ", pMoney=" + pMoney + ", pPayee=" + pPayee + ", pMatters=" + pMatters
				+ "]";
	}

}
