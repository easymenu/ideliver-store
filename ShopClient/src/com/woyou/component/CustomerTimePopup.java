package com.woyou.component;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jing.biandang.R;

public class CustomerTimePopup implements OnClickListener {
	private Context context;
	private RelativeLayout positionView;
	private TextView valueView;
	private PopupWindow popupWindow;
	private RelativeLayout pop_customer_month;
	private RelativeLayout pop_customer_quarter;
	private RelativeLayout pop_customer_halfayear;
	private RelativeLayout pop_customer_year;
	private CustomerTimeListener mListener;
	
	public void setmListener(CustomerTimeListener mListener) {
		this.mListener = mListener;
	}

	public CustomerTimePopup(Context context, RelativeLayout positionView,
			TextView valueView) {
		this.context = context;
		this.positionView = positionView;
		this.valueView = valueView;
	}

	// 弹出泡泡窗体
	public void showPopup() {
		// 实例化泡泡窗台的布局文件
		View contentView = LayoutInflater.from(context).inflate(R.layout.pop_customer_time, null);
		pop_customer_month = (RelativeLayout) contentView.findViewById(R.id.pop_customer_month);
		pop_customer_quarter = (RelativeLayout) contentView.findViewById(R.id.pop_customer_quarter);
		pop_customer_halfayear = (RelativeLayout) contentView.findViewById(R.id.pop_customer_halfayear);
		pop_customer_year = (RelativeLayout) contentView.findViewById(R.id.pop_customer_year);

		pop_customer_month.setOnClickListener(this);
		pop_customer_quarter.setOnClickListener(this);
		pop_customer_halfayear.setOnClickListener(this);
		pop_customer_year.setOnClickListener(this);

		// 实例化泡泡窗体
		if (popupWindow == null) {
			popupWindow = new PopupWindow(contentView, positionView.getWidth(), ViewGroup.LayoutParams.WRAP_CONTENT);
		}
		popupWindow.setFocusable(true);
		popupWindow.setBackgroundDrawable(new BitmapDrawable());
		// 显示泡泡窗体
		popupWindow.showAsDropDown(positionView, 0, 0);
	}

	/**
	 * 隐藏pop窗体
	 */
	public void hidePopup() {
		if (popupWindow != null && popupWindow.isShowing()) {
			popupWindow.dismiss();
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.pop_customer_month:
			valueView.setText("一个月内消费");
			mListener.refreshCustomerTime("30");
			break;
		case R.id.pop_customer_quarter:
			valueView.setText("一个季度内消费");
			mListener.refreshCustomerTime("90");
			break;
		case R.id.pop_customer_halfayear:
			valueView.setText("半年内消费");
			mListener.refreshCustomerTime("180");
			break;
		case R.id.pop_customer_year:
			valueView.setText("一年内消费");
			mListener.refreshCustomerTime("360");
			break;
		}
		popupWindow.dismiss();
	}
	
	/**
	 * 刷新顾客分析数据
	 * @author zhou.ni
	 *
	 */
	public interface CustomerTimeListener{
		void refreshCustomerTime(String day);
	}

}
