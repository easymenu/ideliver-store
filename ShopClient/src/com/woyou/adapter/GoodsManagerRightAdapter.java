package com.woyou.adapter;

import java.util.List;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jing.biandang.R;
import com.squareup.picasso.Picasso;
import com.woyou.activity.HomeActivity;
import com.woyou.bean.GoodsInfo;
import com.woyou.utils.FormatTools;
import com.woyou.utils.TypeJudgeTools;

public class GoodsManagerRightAdapter extends BaseAdapter {

	private HomeActivity homeActivity;
	private LayoutInflater layoutInflater;
	private List<GoodsInfo> data;

	public GoodsManagerRightAdapter(HomeActivity homeActivity, List<GoodsInfo> data) {
		this.homeActivity = homeActivity;
		layoutInflater = LayoutInflater.from(homeActivity);
		this.data = data;
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;
		if (convertView == null) {
			convertView = layoutInflater.inflate(R.layout.item_goodsmanager_gv, null);
			viewHolder = new ViewHolder();
			viewHolder.group_bg = (LinearLayout) convertView.findViewById(R.id.item_goodsmanager_bg);
			viewHolder.group_iv = (ImageView) convertView.findViewById(R.id.item_goodsmanager_iv);
			viewHolder.group_name_tv = (TextView) convertView.findViewById(R.id.item_goodsmanager_name_tv);
			viewHolder.group_value_tv = (TextView) convertView.findViewById(R.id.item_goodsmanager_value_tv);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		// 设置默认图片
		viewHolder.group_iv.setImageResource(R.raw.icon_default);

		GoodsInfo goodsInfo = data.get(position);
		viewHolder.group_name_tv.setText(goodsInfo.getgName());
		viewHolder.group_value_tv.setText(Html.fromHtml("<small><small>￥</small></small>"
				+ FormatTools.String2Money(String.valueOf(goodsInfo.getPrice())) + "<small>/" + goodsInfo.getUnit() + "</small>"));
		if(!TypeJudgeTools.isNull(goodsInfo.getPrcUrl())){
			Picasso.with(homeActivity).load(goodsInfo.getPrcUrl()).into(viewHolder.group_iv);
		}
		return convertView;
	}

	static class ViewHolder {
		LinearLayout group_bg;
		ImageView group_iv;
		TextView group_name_tv;
		TextView group_value_tv;
	}

}
