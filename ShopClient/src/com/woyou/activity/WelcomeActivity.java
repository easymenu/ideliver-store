package com.woyou.activity;

import java.util.ArrayList;
import java.util.List;

import com.jing.biandang.R;
import com.woyou.adapter.SuperViewPagerAdapter;
import com.woyou.bean.UserInfo;
import com.woyou.controller.WelcomeController;

import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * 
 * 欢迎引导页面
 * 
 * @author xuron
 * @versionCode 1 <每次修改提交前+1>
 */
public class WelcomeActivity extends SuperActivity {
	/**
	 * welcomeController
	 */
	public WelcomeController welcomeController;
	// 欢迎页面
	private RelativeLayout welcome_panel;
	private TextView welcome_load_tv;
	private ImageView welcome_loading_iv;
	private AnimationDrawable animationDrawable;
	// 介绍页面
	private RelativeLayout introduct_time;
	private TextView introduct_time_tv;
	private RelativeLayout introduct_guide;
	private ImageView introduct_hand;

	private ViewPager introduct_vp;
	private SuperViewPagerAdapter viewPagerAdapter;
	private List<View> views = new ArrayList<View>();
	private ImageView introduct_iv1;
	private ImageView introduct_iv2;
	private ImageView introduct_iv3;
	/**
	 * 用户登陆信息
	 */
	UserInfo userInfo = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_welcome);
		welcomeController = new WelcomeController(mContext);
		userInfo = welcomeController.updateUserInfo();
		init();
	}

	/**
	 * 初始化页面
	 */
	public void init() {
		// 欢迎页面
		welcome_panel = (RelativeLayout) findViewById(R.id.layout_welcome_panel);
		welcome_load_tv = (TextView) findViewById(R.id.layout_welcome_load_tv);
		welcome_loading_iv = (ImageView) findViewById(R.id.layout_welcome_loading_iv);
		// 启动动画
		welcome_loading_iv.post(new Runnable() {

			@Override
			public void run() {
				// 获取imageview动画列表
				animationDrawable = (AnimationDrawable) welcome_loading_iv.getDrawable();
				animationDrawable.start();
			}
		});
		// 介绍页面
		introduct_time = (RelativeLayout) findViewById(R.id.layout_introduct_time);
		introduct_time_tv = (TextView) findViewById(R.id.layout_introduct_time_tv);
		introduct_guide = (RelativeLayout) findViewById(R.id.layout_introduct_guide);
		introduct_hand = (ImageView) findViewById(R.id.layout_introduct_hand);
		// 防止下层被点击
		introduct_guide.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				return true;
			}
		});

		introduct_iv1 = (ImageView) findViewById(R.id.layout_introduct_iv1);
		introduct_iv2 = (ImageView) findViewById(R.id.layout_introduct_iv2);
		introduct_iv3 = (ImageView) findViewById(R.id.layout_introduct_iv3);
		introduct_vp = (ViewPager) findViewById(R.id.layout_introduct_vp);
		LayoutInflater layoutInflater = LayoutInflater.from(mContext);
		View view1 = layoutInflater.inflate(R.layout.item_introduct_v1, null);
		View view2 = layoutInflater.inflate(R.layout.item_introduct_v2, null);
		View view3 = layoutInflater.inflate(R.layout.item_introduct_v3, null);
		// 监听登录立即体验按钮
		RelativeLayout item_introduct_finish = (RelativeLayout) view3.findViewById(R.id.item_introduct_finish);
		item_introduct_finish.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				welcome_panel.setVisibility(View.VISIBLE);
				welcomeController.autoLogin(WelcomeActivity.this, userInfo, welcome_load_tv);
			}
		});

		views.add(view1);
		views.add(view2);
		views.add(view3);
		viewPagerAdapter = new SuperViewPagerAdapter(views);
		introduct_vp.setAdapter(viewPagerAdapter);
		introduct_vp.setOnPageChangeListener(new OnPageChangeListener() {
			@Override
			public void onPageSelected(int arg0) {
				introduct_time.setVisibility(View.GONE);
				// 关闭引导动画
				welcomeController.closeGuideTimer();
				welcomeController.setItemFocus(arg0, introduct_iv1, introduct_iv2, introduct_iv3);
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {

			}

			@Override
			public void onPageScrollStateChanged(int arg0) {

			}
		});
		welcomeController.judge2Page(userInfo, welcome_load_tv, welcome_panel, introduct_guide, introduct_hand,
				introduct_time, introduct_time_tv);
	}

	@Override
	protected void onStart() {
		super.onStart();
		welcomeController.resetMacStatus();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		welcomeController.onDestroy();
	}

}
