package com.woyou.bean.rpc;


/**
 * 设置店铺营业状态的请求bean
 * @author zhou.ni
 * 
 */
public class SetShopStatusReq {

	private String sId;			//店铺ID
	private String pwd;			//密码
	private String status;		//营业状态（S：营业B：繁忙C：休息）
	
	public String getsId() {
		return sId;
	}
	public void setsId(String sId) {
		this.sId = sId;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "SetShopStatusReq [sId=" + sId + ", pwd=" + pwd + ", status="
				+ status + "]";
	}
	
}
