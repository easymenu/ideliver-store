package com.woyou.bean;

import java.io.Serializable;

public class UserInfo implements Serializable {
	/**
	 * 店铺编号
	 */
	private String bId = "";
	/**
	 * 店铺ID
	 */
	private String sId = "";
	/**
	 * 店铺名称
	 */
	private String sName = "";
	/**
	 * 用户编号
	 */
	private String uCode = "";
	/**
	 * 验证码
	 */
	private String code = "";
	/**
	 * 密码（随机自动生成6位数字）
	 */
	private String pwd = "";
	/**
	 * 1:初次登录；2：再次登录
	 */
	private int loginType;
	/**
	 * 设备编号
	 */
	private String deviceId = "";
	/**
	 * 外卖机版本
	 */
	private String version = "";
	/**
	 * 心跳服务端程序所在服务器IP地址
	 */
	private String serverIP = "";
	/**
	 * 心跳服务端程序监听端口号
	 */
	private String serverPort = "";

	public String getServerIP() {
		return serverIP;
	}

	public void setServerIP(String serverIP) {
		this.serverIP = serverIP;
	}

	public String getServerPort() {
		return serverPort;
	}

	public void setServerPort(String serverPort) {
		this.serverPort = serverPort;
	}

	public String getbId() {
		return bId;
	}

	public void setbId(String bId) {
		this.bId = bId;
	}

	public String getsId() {
		return sId;
	}

	public void setsId(String sId) {
		this.sId = sId;
	}

	public String getsName() {
		return sName;
	}

	public void setsName(String sName) {
		this.sName = sName;
	}

	public String getuCode() {
		return uCode;
	}

	public void setuCode(String uCode) {
		this.uCode = uCode;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public int getLoginType() {
		return loginType;
	}

	public void setLoginType(int loginType) {
		this.loginType = loginType;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

}
