package com.woyou.bean;


/**
 * 没一条外卖时段
 * @author zhou.ni
 *
 */
public class Time extends SuperModel {

	private static final long serialVersionUID = 1L;

	private String tId;				//时段的id
	private String state;			//更新状态（1：正常 2：休息）
	
	public String gettId() {
		return tId;
	}
	public void settId(String tId) {
		this.tId = tId;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
	@Override
	public String toString() {
		return "Time [tId=" + tId + ", state=" + state + "]";
	}
	
}	
