package com.woyou.bean;

/**
 * 待支付bean
 * 
 * @author xuron
 *
 */
public class StayPay extends SuperModel {
	/**
	 * 待支付类型
	 */
	private String sType;
	/**
	 * 打款起始日期
	 */
	private String sTime;
	/**
	 * 待支付金额
	 */
	private String sMoney;

	public String getsType() {
		return sType;
	}

	public void setsType(String sType) {
		this.sType = sType;
	}

	public String getsTime() {
		return sTime;
	}

	public void setsTime(String sTime) {
		this.sTime = sTime;
	}

	public String getsMoney() {
		return sMoney;
	}

	public void setsMoney(String sMoney) {
		this.sMoney = sMoney;
	}

	@Override
	public String toString() {
		return "StayPay [sType=" + sType + ", sTime=" + sTime + ", sMoney=" + sMoney + "]";
	}

}
