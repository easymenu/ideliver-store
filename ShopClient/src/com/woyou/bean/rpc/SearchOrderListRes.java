package com.woyou.bean.rpc;

import java.util.ArrayList;
import java.util.List;

import com.woyou.bean.Order;
import com.woyou.bean.SuperModel;
/**
 * 搜索订单列表返回bean
 * @author 荣
 *
 */
public class SearchOrderListRes extends SuperModel {
	/**
	 * 订单列表
	 */
	private List<Order> orderList=new ArrayList<Order>();

	public List<Order> getOrderList() {
		return orderList;
	}

	public void setOrderList(List<Order> orderList) {
		this.orderList = orderList;
	}
	
}
