/** 
 * @Title:  Info.java 
 * @author:  xuron
 * @data:  2015年11月24日 下午3:58:24 <创建时间>
 * 
 * @history：<以下是历史记录>
 *
 * @modifier: <修改人>
 * @modify date: 2015年11月24日 下午3:58:24 <修改时间>
 * @log: <修改内容>
 *
 * @modifier: <修改人>
 * @modify date: 2015年11月24日 下午3:58:24 <修改时间>
 * @log: <修改内容>
 */
package com.woyou.bean;

/**
 * Fragment传递数据的载体
 * 
 * @author xuron
 * @versionCode 1 <每次修改提交前+1>
 */
public class InfoBean extends SuperModel {
	/**
	 * 打开Fragment
	 */
	private Class classFragment;
	/**
	 * Fragment的名字
	 */
	private String className;

	public InfoBean(Class classFragment, String className) {
		this.classFragment = classFragment;
		this.className = className;
	}

	public Class getClassFragment() {
		return classFragment;
	}

	public void setClassFragment(Class classFragment) {
		this.classFragment = classFragment;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	@Override
	public String toString() {
		return "Info [classFragment=" + classFragment + ", className=" + className + "]";
	}

}
