package com.woyou.bean.rpc;

import java.util.List;

import com.woyou.bean.DeliveryTime;
import com.woyou.bean.SuperModel;

/**
 * 便当机用户登录结果集
 * 
 * @author tlc
 * 
 */
public class UserLoginRes extends SuperModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 店铺Id
	 */
	private String sId = "";
	/**
	 * 店铺名称
	 */
	private String sName = "";
	/**
	 * 是否是连锁店
	 */
	private int isChain;
	/**
	 * 是否支持在线支付
	 */
	private int isOnlinePay;
	/**
	 * 评分
	 */
	private float score;
	/**
	 * 评分人数
	 */
	private int scoreNum = 0;
	/**
	 * 店铺公告
	 */
	private String notice = "";
	/**
	 * 等级
	 */
	private String grade = "";
	/**
	 * 商铺logo的Url地址
	 */
	private String shopLogo = "";
	/**
	 * 月销售数
	 */
	private int salesNum;
	/**
	 * 起送价（最低起送价）
	 */
	private float startPrice;
	/**
	 * 心跳服务端程序所在服务器IP地址
	 */
	private String serverIP = "";
	/**
	 * 心跳服务端程序监听端口号
	 */
	private String serverPort = "";
	/**
	 * 外卖时段价列表
	 */
	private List<DeliveryTime> deliveryTimeList;

	public String getsId() {
		return sId;
	}

	public void setsId(String sId) {
		this.sId = sId;
	}

	public String getsName() {
		return sName;
	}

	public void setsName(String sName) {
		this.sName = sName;
	}

	public int getIsChain() {
		return isChain;
	}

	public void setIsChain(int isChain) {
		this.isChain = isChain;
	}

	public int getIsOnlinePay() {
		return isOnlinePay;
	}

	public void setIsOnlinePay(int isOnlinePay) {
		this.isOnlinePay = isOnlinePay;
	}

	public float getScore() {
		return score;
	}

	public void setScore(float score) {
		this.score = score;
	}

	public int getScoreNum() {
		return scoreNum;
	}

	public void setScoreNum(int scoreNum) {
		this.scoreNum = scoreNum;
	}

	public String getNotice() {
		return notice;
	}

	public void setNotice(String notice) {
		this.notice = notice;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public String getShopLogo() {
		return shopLogo;
	}

	public void setShopLogo(String shopLogo) {
		this.shopLogo = shopLogo;
	}

	public int getSalesNum() {
		return salesNum;
	}

	public void setSalesNum(int salesNum) {
		this.salesNum = salesNum;
	}

	public float getStartPrice() {
		return startPrice;
	}

	public void setStartPrice(float startPrice) {
		this.startPrice = startPrice;
	}

	public String getServerIP() {
		return serverIP;
	}

	public void setServerIP(String serverIP) {
		this.serverIP = serverIP;
	}

	public String getServerPort() {
		return serverPort;
	}

	public void setServerPort(String serverPort) {
		this.serverPort = serverPort;
	}

	public List<DeliveryTime> getDeliveryTimeList() {
		return deliveryTimeList;
	}

	public void setDeliveryTimeList(List<DeliveryTime> deliveryTimeList) {
		this.deliveryTimeList = deliveryTimeList;
	}

	@Override
	public String toString() {
		return "UserLoginRes [sId=" + sId + ", sName=" + sName + ", score=" + score + ", scoreNum=" + scoreNum
				+ ", notice=" + notice + ", grade=" + grade + ", shopLogo=" + shopLogo + ", salesNum=" + salesNum
				+ ", startPrice=" + startPrice + ", serverIP=" + serverIP + ", serverPort=" + serverPort + "]";
	}

}
