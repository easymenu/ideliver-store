package com.woyou.bean;


/**
 * 用户排名报表的每一条item
 * @author zhou.ni
 *
 */
public class UserReportItem extends SuperModel {

	private static final long serialVersionUID = 1L;
	
	private String uId;			//用户id
	private String uName;		//用户名
	private String phone;		//用户手机号
	private int sumNum;			//下单量
	private float sumFee;		//消费总金额
	private String picUrl;		//用户头像的url
	
	public String getuId() {
		return uId;
	}
	public void setuId(String uId) {
		this.uId = uId;
	}
	public String getuName() {
		return uName;
	}
	public void setuName(String uName) {
		this.uName = uName;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public int getSumNum() {
		return sumNum;
	}
	public void setSumNum(int sumNum) {
		this.sumNum = sumNum;
	}
	public float getSumFee() {
		return sumFee;
	}
	public void setSumFee(float sumFee) {
		this.sumFee = sumFee;
	}
	public String getPicUrl() {
		return picUrl;
	}
	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}
	
	@Override
	public String toString() {
		return "UserReportItem [uId=" + uId + ", uName=" + uName + ", phone="
				+ phone + ", sumNum=" + sumNum + ", sumFee=" + sumFee
				+ ", picUrl=" + picUrl + "]";
	}
	
	
	
}
