package com.woyou.utils;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 类型判断工具
 * 
 * @author 荣
 * 
 */
public class TypeJudgeTools {
	
	/**
	 * 判断一个ArrayList里的数据是否有重复
	 * @param list
	 * @return
	 * true 没有
	 * false 有
	 */
	public static boolean isValueRepeat(ArrayList<String> list) {
		if (list != null) {
			for (int i = 0; i < list.size(); i++) {
				for (int j = 1; j < list.size(); j++) {
					if (i != j && list.get(i).equals(list.get(j))) {
						return false;
					}
				}
			}
		}
		return true;
	}
	
	/**
	 * 判断一个字符串是否为正数
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isPositiveNumber(String str) {
		Pattern pattern = Pattern.compile("^[0-9]\\d*$");
		Matcher isNum = pattern.matcher(str);
		if (!isNum.matches()) {
			return false;
		}
		return true;
	}

	/**
	 * 判断一个字符串是否为正浮点数
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isFloatingPointNumber(String str) {
		Pattern pattern = Pattern.compile("^[0-9]\\d*\\.\\d*|0\\.\\d*[0-9]\\d*$");
		Matcher isNum = pattern.matcher(str);
		if (!isNum.matches()) {
			return false;
		}
		return true;
	}

	/**
	 * 判断传入的字符串是否是一个邮箱地址
	 * 
	 * @param strEmail
	 * @return
	 */
	public static boolean isEmail(String strEmail) {
		Pattern p = Pattern.compile("\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*");
		Matcher m = p.matcher(strEmail);
		return m.find();
	}

	/**
	 * 判断传入的字符串是否是一个固定电话
	 * 
	 * @param strPhone
	 * @return
	 */
	public static boolean isTelephoneNumber(String strPhone) {
		Pattern p = Pattern.compile("^\\(?\\d{3,4}[-\\)]?\\d{7,8}$");
		Matcher m = p.matcher(strPhone);
		return m.find();
	}
	
	/**
	 * 手机号码验证
	 * @param phonenum
	 * @return
	 */
	public static boolean isPhoneNum(String phonenum) {
		Pattern pattern = Pattern.compile("^(1[0-9])\\d{9}$"); 
		Matcher m = pattern.matcher(phonenum);
		if (m.matches()) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 判断传入的字符串是否是一个手机号码
	 * 
	 * @param strPhone
	 * @return
	 */
	public static boolean isPhoneNumber(String strPhone) {
		Pattern p = Pattern.compile("^(13[0-9]|15[0-9]|18[0-9])\\d{8}$");
		Matcher m = p.matcher(strPhone);
		return m.find();
	}

	/**
	 * 判断传入的字符是否是一个网址
	 * 
	 * @param strWebSite
	 * @return
	 */
	public static boolean isWebSite(String strPhone) {
		Pattern p = Pattern.compile("(([a-zA-z0-9]|-){1,}\\.){1,}[a-zA-z0-9]{1,}-*");
		Matcher m = p.matcher(strPhone);
		return m.find();
	}

	/**
	 * 判断传入的字符是否是一个空的
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isNull(String str) {
		if (str == null || str.equals(""))
			return true;
		else
			return false;
	}
	
	/**
	 * 判断给定字符串是否空白串。 空白串是指由空格、制表符、回车符、换行符组成的字符串 若输入字符串为null或空字符串，返回true 
	 * @param input
	 * @return boolean
	 */
	public static boolean isEmpty(String input) {
		if (input == null || "".equals(input))
			return true;

		for (int i = 0; i < input.length(); i++) {
			char c = input.charAt(i);
			if (c != ' ' && c != '\t' && c != '\r' && c != '\n') {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * 去除换行符之类的
	 * @param str
	 * @return
	 */
	public static String replaceBlank(String str) {  
        String dest = "";  
        if (str!=null) {  
	        Pattern p = Pattern.compile("\\s*|\t|\r|\n");  
	        Matcher m = p.matcher(str);  
	        dest = m.replaceAll("");  
        }  
        return dest;  
	}  
	
	
}
