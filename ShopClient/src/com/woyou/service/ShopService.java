package com.woyou.service;

import java.util.List;

import retrofit.RetrofitError;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

import com.woyou.bean.CommentItem;
import com.woyou.bean.GoodsInfo;
import com.woyou.bean.GoodsTypeInfo;
import com.woyou.bean.MarkUp;
import com.woyou.bean.ReportItem;
import com.woyou.bean.ShopInfoItem;
import com.woyou.bean.UserReportItem;
import com.woyou.bean.rpc.QueryGoodsListRes;
import com.woyou.bean.rpc.QueryTypeListRes;
import com.woyou.bean.rpc.Result;
import com.woyou.bean.rpc.ServerTimeRes;
import com.woyou.bean.rpc.UpdateGoodsInfoReq;

/**
 * 获取店铺相关信息的Retrofit Service
 * @author 荣
 *
 */
public interface ShopService {
	
	/**
	 * 2.1	获取服务器时间（http://xxxx/queryServerTime）
	 * @param params      请求bean的json字符串
	 * @param isEncrypted 是否加密
	 * @param timeStamp   时间戳
	 * @param randomNum   6位随机数 
	 * @param sign        加密后的字符串
	 * 算法:sign = MD5(Parameter+IsEncrypted+TimeStamp+RandomNum+MD5(Key))
	 * @return
	 * @throws RetrofitError
	 */
	@FormUrlEncoded
	@POST("/queryServerTime")
	public Result<ServerTimeRes> queryServerTime(
			@Field("params") String params, 
			@Field("isEncrypted")String isEncrypted, 
			@Field("timeStamp") String timeStamp, 
			@Field("randomNum")String randomNum,
			@Field("sign")String sign)throws RetrofitError;
	
	/**
	 * 2.18	设置店铺营业状态（http://xxxx/SetShopStatus）
	 * @param params
	 * @param isEncrypted
	 * @param timeStamp
	 * @param randomNum
	 * @param sign
	 * @return
	 * @throws RetrofitError
	 */
	@FormUrlEncoded
	@POST("/setShopStatus")
	public Result setShopStatus(
			@Field("params") String params, 
			@Field("isEncrypted")String isEncrypted, 
			@Field("timeStamp") String timeStamp, 
			@Field("randomNum")String randomNum,
			@Field("sign")String sign)throws RetrofitError;
	
	/**
	 * 2.19	获取用户评论（http://xxxx/queryCommentList）
	 * @param params
	 * @param isEncrypted
	 * @param timeStamp
	 * @param randomNum
	 * @param sign
	 * @return
	 * @throws RetrofitError
	 */
	@FormUrlEncoded
	@POST("/queryCommentList")
	public Result<List<CommentItem>> queryCommentList(
			@Field("params") String params, 
			@Field("isEncrypted")String isEncrypted, 
			@Field("timeStamp") String timeStamp, 
			@Field("randomNum")String randomNum,
			@Field("sign")String sign)throws RetrofitError;
	
	/**
	 * 2.20	回复评论（http://xxxx/replyComment）
	 * @param params
	 * @param isEncrypted
	 * @param timeStamp
	 * @param randomNum
	 * @param sign
	 * @return
	 * @throws RetrofitError
	 */
	@FormUrlEncoded
	@POST("/replyComment")
	public Result replyComment(
			@Field("params") String params, 
			@Field("isEncrypted")String isEncrypted, 
			@Field("timeStamp") String timeStamp, 
			@Field("randomNum")String randomNum,
			@Field("sign")String sign)throws RetrofitError;
	
	/**
	 * 2.21	获取商铺信息（http://xxxx/queryShopInfo）
	 * @param params
	 * @param isEncrypted
	 * @param timeStamp
	 * @param randomNum
	 * @param sign
	 * @return
	 * @throws RetrofitError
	 */
	@FormUrlEncoded
	@POST("/queryShopInfo")
	public Result<ShopInfoItem> queryShopInfo(
			@Field("params") String params, 
			@Field("isEncrypted")String isEncrypted, 
			@Field("timeStamp") String timeStamp, 
			@Field("randomNum")String randomNum,
			@Field("sign")String sign)throws RetrofitError;
	
	/**
	 * 2.22	更改店铺信息（http://xxxx/updateShopInfo）
	 * @param params
	 * @param isEncrypted
	 * @param timeStamp
	 * @param randomNum
	 * @param sign
	 * @return
	 * @throws RetrofitError
	 */
	@FormUrlEncoded
	@POST("/updateShopInfo")
	public Result updateShopInfo(
			@Field("params") String params, 
			@Field("isEncrypted")String isEncrypted, 
			@Field("timeStamp") String timeStamp, 
			@Field("randomNum")String randomNum,
			@Field("sign")String sign)throws RetrofitError;
	
	/**
	 * 2.23	更新外卖时段（http://xxxx/updateDeliveryTimes）
	 * @param params
	 * @param isEncrypted
	 * @param timeStamp
	 * @param randomNum
	 * @param sign
	 * @return
	 * @throws RetrofitError
	 */
	@FormUrlEncoded
	@POST("/updateDeliveryTimes")
	public Result updateDeliveryTimes(
			@Field("params") String params, 
			@Field("isEncrypted")String isEncrypted, 
			@Field("timeStamp") String timeStamp, 
			@Field("randomNum")String randomNum,
			@Field("sign")String sign)throws RetrofitError;
	
	/**
	 * 2.24	更新商铺阶梯外送费（http://xxxx/updateLadderFee）
	 * @param params
	 * @param isEncrypted
	 * @param timeStamp
	 * @param randomNum
	 * @param sign
	 * @return
	 * @throws RetrofitError
	 */
	@FormUrlEncoded
	@POST("/updateLadderFee")
	public Result<List<MarkUp>> updateLadderFee(
			@Field("params") String params, 
			@Field("isEncrypted")String isEncrypted, 
			@Field("timeStamp") String timeStamp, 
			@Field("randomNum")String randomNum,
			@Field("sign")String sign)throws RetrofitError;
	
	/**
	 * 2.25	获取日或月营业报表（http://xxxx/queryReportList）
	 * @param params
	 * @param isEncrypted
	 * @param timeStamp
	 * @param randomNum
	 * @param sign
	 * @return
	 * @throws RetrofitError
	 */
	@FormUrlEncoded
	@POST("/queryReportList")
	public Result<ReportItem> queryReportList(
			@Field("params") String params, 
			@Field("isEncrypted")String isEncrypted, 
			@Field("timeStamp") String timeStamp, 
			@Field("randomNum")String randomNum,
			@Field("sign")String sign)throws RetrofitError;
	
	/**
	 * 2.26	用户排名报表（http://xxxx/queryUserReportList）
	 * @param params
	 * @param isEncrypted
	 * @param timeStamp
	 * @param randomNum
	 * @param sign
	 * @return
	 * @throws RetrofitError
	 */
	@FormUrlEncoded
	@POST("/queryUserReportList")
	public Result<List<UserReportItem>> queryUserReportList(
			@Field("params") String params, 
			@Field("isEncrypted")String isEncrypted, 
			@Field("timeStamp") String timeStamp, 
			@Field("randomNum")String randomNum,
			@Field("sign")String sign)throws RetrofitError;
	
	/**
	 * 2.9	获取商品分类列表（http://xxxx/queryTypeList）
	 * @param params
	 * @param isEncrypted
	 * @param timeStamp
	 * @param randomNum
	 * @param sign
	 * @return
	 * @throws RetrofitError
	 */
	@FormUrlEncoded
	@POST("/queryTypeList")
	public Result<List<GoodsTypeInfo>> queryTypeList(
			@Field("params") String params, 
			@Field("isEncrypted")String isEncrypted, 
			@Field("timeStamp") String timeStamp, 
			@Field("randomNum")String randomNum,
			@Field("sign")String sign)throws RetrofitError;
	
	/**
	 * 2.10	获取商品列表（http://xxxx/queryGoodsList）
	 * @param params
	 * @param isEncrypted
	 * @param timeStamp
	 * @param randomNum
	 * @param sign
	 * @return
	 * @throws RetrofitError
	 */
	@FormUrlEncoded
	@POST("/queryGoodsList")
	public Result<List<GoodsInfo>> queryGoodsList(
			@Field("params") String params, 
			@Field("isEncrypted")String isEncrypted, 
			@Field("timeStamp") String timeStamp, 
			@Field("randomNum")String randomNum,
			@Field("sign")String sign)throws RetrofitError;
	
	/**
	 * 2.11	修改商品信息（http://xxxx/updateGoodsInfo）
	 * @param params
	 * @param isEncrypted
	 * @param timeStamp
	 * @param randomNum
	 * @param sign
	 * @return
	 * @throws RetrofitError
	 */
	@FormUrlEncoded
	@POST("/updateGoodsInfo")
	public Result updateGoodsInfo(
			@Field("params") String params, 
			@Field("isEncrypted")String isEncrypted, 
			@Field("timeStamp") String timeStamp, 
			@Field("randomNum")String randomNum,
			@Field("sign")String sign)throws RetrofitError;
	
	/**
	 * 2.28	更新餐盒费（http://xxxx/updateBoxFee）
	 * @param params
	 * @param isEncrypted
	 * @param timeStamp
	 * @param randomNum
	 * @param sign
	 * @return
	 * @throws RetrofitError
	 */
	@FormUrlEncoded
	@POST("/updateBoxFee")
	public Result updateBoxFee(
			@Field("params") String params, 
			@Field("isEncrypted")String isEncrypted, 
			@Field("timeStamp") String timeStamp, 
			@Field("randomNum")String randomNum,
			@Field("sign")String sign)throws RetrofitError;
	
}
