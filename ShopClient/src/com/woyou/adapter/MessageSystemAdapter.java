package com.woyou.adapter;

import java.util.ArrayList;
import java.util.HashMap;

import com.jing.biandang.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public class MessageSystemAdapter extends BaseAdapter {

	private LayoutInflater layoutInflater;
	private ArrayList<HashMap<String, Object>> data;

	public MessageSystemAdapter(Context c, ArrayList<HashMap<String, Object>> data) {
		layoutInflater = LayoutInflater.from(c);
		this.data = data;
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		if (convertView == null) {
			convertView = layoutInflater.inflate(R.layout.item_message_system_lv,null);
		}
		return convertView;
	}

	static class ViewHolder {

	}

}
