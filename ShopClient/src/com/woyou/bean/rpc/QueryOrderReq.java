package com.woyou.bean.rpc;

import com.woyou.bean.SuperModel;
/**
 * 查询订单的请求bean
 * @author 荣
 *
 */
public class QueryOrderReq extends SuperModel {
	/**
	 * 店铺ID
	 */
	private String sId="";
	/**
	 * 密码
	 */
	private String pwd="";
	/**
	 * 状态编号（1全部订单，2未送订单，3已送订单，4无效订单）
	 */
	private int type;

	public String getsId() {
		return sId;
	}
	public void setsId(String sId) {
		this.sId = sId;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	
}
