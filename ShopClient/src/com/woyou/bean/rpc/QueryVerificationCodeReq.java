package com.woyou.bean.rpc;

import com.woyou.bean.SuperModel;

/**
 * 获取验证码请求
 * @author tlc
 *
 */
public class QueryVerificationCodeReq extends SuperModel {

	/**
	 * 外卖机编号
	 */
	private String bId = "";
	/**
	 * 用户编号
	 */
	private String uCode = "";
	
	public String getbId() {
		return bId;
	}
	public void setbId(String bId) {
		this.bId = bId;
	}
	public String getuCode() {
		return uCode;
	}
	public void setuCode(String uCode) {
		this.uCode = uCode;
	}
	
}
