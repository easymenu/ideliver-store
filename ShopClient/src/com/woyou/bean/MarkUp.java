package com.woyou.bean;


/**
 * 每一条阶梯价
 * @author zhou.ni
 *
 */
public class MarkUp extends SuperModel {

	private static final long serialVersionUID = 1L;
	
	private String fId;				//外送阶梯价的Id
	private float range;			//外送范围单位米
	private float deliverFee;		//外送费
	private float sendUpFee;		//起送价
	private int type;				//0:不变，1新增，2删除，3修改
	
	public String getfId() {
		return fId;
	}
	public void setfId(String fId) {
		this.fId = fId;
	}
	public float getRange() {
		return range;
	}
	public void setRange(float range) {
		this.range = range;
	}
	public float getDeliverFee() {
		return deliverFee;
	}
	public void setDeliverFee(float deliverFee) {
		this.deliverFee = deliverFee;
	}
	public float getSendUpFee() {
		return sendUpFee;
	}
	public void setSendUpFee(float sendUpFee) {
		this.sendUpFee = sendUpFee;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	
	@Override
	public String toString() {
		return "MarkUp [fId=" + fId + ", range=" + range + ", deliverFee="
				+ deliverFee + ", sendUpFee=" + sendUpFee + ", type=" + type
				+ "]";
	}
	
}
