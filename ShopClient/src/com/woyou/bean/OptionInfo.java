package com.woyou.bean;


/**
 * 商品属性Model
 * @author tlc
 *
 */
public class OptionInfo extends SuperModel{

	/**
	 * 属性ID
	 */
	private String optId = "";
	/**
	 * 属性名称
	 */
	private String optName = "";
	/**
	 * 增加费用
	 */
	private float price;
	/**
	 * 缺货标志（1正常，2暂缺）
	 */
	private int state;
	
	public String getOptId() {
		return optId;
	}
	public void setOptId(String optId) {
		this.optId = optId;
	}
	public String getOptName() {
		return optName;
	}
	public void setOptName(String optName) {
		this.optName = optName;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}
	@Override
	public String toString() {
		return "OptionInfo [optId=" + optId + ", optName=" + optName
				+ ", price=" + price + ", state=" + state + "]";
	}
	
}
