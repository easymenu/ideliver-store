package com.woyou.fragment;

import java.util.Timer;
import java.util.TimerTask;

import com.jing.biandang.R;
import com.woyou.activity.HomeActivity;
import com.woyou.bean.FmInfo;
import com.woyou.component.SuperUI;
import com.woyou.fragment.about.SettingPWFragment;
import com.woyou.fragment.analysis.AnalysisFragment;
import com.woyou.fragment.analysis.ReportFragment;
import com.woyou.fragment.goods.GoodsManagerFragment;
import com.woyou.fragment.shop.BoxFeeFragment;
import com.woyou.fragment.shop.DeliveryFeeFragment;
import com.woyou.fragment.shop.ShopFragment;
import com.woyou.fragment.shop.ShopHoursFragment;
import com.woyou.utils.FormatTools;
import com.woyou.utils.TypeJudgeTools;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class PWPanelFragment extends SuperFragment implements OnClickListener, TextWatcher {

	private HomeActivity homeActivity;
	private View layout_pwpanel;
	private TextView layout_pwpanel_title;
	private EditText layout_pwpanel_et;
	private CheckBox layout_pwpanel_cb;
	private RelativeLayout layout_pwpanel_pan;

	// 数字按钮
	private TextView layout_pwpanel_num0;
	private TextView layout_pwpanel_num1;
	private TextView layout_pwpanel_num2;
	private TextView layout_pwpanel_num3;
	private TextView layout_pwpanel_num4;
	private TextView layout_pwpanel_num5;
	private TextView layout_pwpanel_num6;
	private TextView layout_pwpanel_num7;
	private TextView layout_pwpanel_num8;
	private TextView layout_pwpanel_num9;
	private ImageView layout_pwpanel_clear;
	private ImageView layout_pwpanel_backspace;
	// 保持解锁5分钟的计时器
	private Timer timer;
	private TimerTask timerTask;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		layout_pwpanel = inflater.inflate(R.layout.layout_pwpanel, container, false);
		homeActivity=(HomeActivity)getActivity();
		return layout_pwpanel;
	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		if (!hidden) {
			layout_pwpanel_et.setText("");
			initTitle();
		}
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		initView();
		initTitle();
	}

	/**
	 * 初始化页面
	 */
	public void initView() {
		layout_pwpanel_title = (TextView) layout_pwpanel.findViewById(R.id.layout_pwpanel_title);
		layout_pwpanel_et = (EditText) layout_pwpanel.findViewById(R.id.layout_pwpanel_et);
		layout_pwpanel_cb = (CheckBox) layout_pwpanel.findViewById(R.id.layout_pwpanel_cb);
		layout_pwpanel_pan = (RelativeLayout) layout_pwpanel.findViewById(R.id.layout_pwpanel_pan);
		// 数字
		layout_pwpanel_num0 = (TextView) layout_pwpanel.findViewById(R.id.layout_pwpanel_num0);
		layout_pwpanel_num1 = (TextView) layout_pwpanel.findViewById(R.id.layout_pwpanel_num1);
		layout_pwpanel_num2 = (TextView) layout_pwpanel.findViewById(R.id.layout_pwpanel_num2);
		layout_pwpanel_num3 = (TextView) layout_pwpanel.findViewById(R.id.layout_pwpanel_num3);
		layout_pwpanel_num4 = (TextView) layout_pwpanel.findViewById(R.id.layout_pwpanel_num4);
		layout_pwpanel_num5 = (TextView) layout_pwpanel.findViewById(R.id.layout_pwpanel_num5);
		layout_pwpanel_num6 = (TextView) layout_pwpanel.findViewById(R.id.layout_pwpanel_num6);
		layout_pwpanel_num7 = (TextView) layout_pwpanel.findViewById(R.id.layout_pwpanel_num7);
		layout_pwpanel_num8 = (TextView) layout_pwpanel.findViewById(R.id.layout_pwpanel_num8);
		layout_pwpanel_num9 = (TextView) layout_pwpanel.findViewById(R.id.layout_pwpanel_num9);
		layout_pwpanel_clear = (ImageView) layout_pwpanel.findViewById(R.id.layout_pwpanel_clear);
		layout_pwpanel_backspace = (ImageView) layout_pwpanel.findViewById(R.id.layout_pwpanel_backspace);

		layout_pwpanel_num0.setOnClickListener(this);
		layout_pwpanel_num1.setOnClickListener(this);
		layout_pwpanel_num2.setOnClickListener(this);
		layout_pwpanel_num3.setOnClickListener(this);
		layout_pwpanel_num4.setOnClickListener(this);
		layout_pwpanel_num5.setOnClickListener(this);
		layout_pwpanel_num6.setOnClickListener(this);
		layout_pwpanel_num7.setOnClickListener(this);
		layout_pwpanel_num8.setOnClickListener(this);
		layout_pwpanel_num9.setOnClickListener(this);
		layout_pwpanel_clear.setOnClickListener(this);
		layout_pwpanel_backspace.setOnClickListener(this);
		// 监听密码框
		layout_pwpanel_et.addTextChangedListener(this);
		layout_pwpanel_cb.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked) {
					layout_pwpanel_cb.setBackgroundResource(R.raw.icon_pwpanel_check);
				} else {
					layout_pwpanel_cb.setBackgroundResource(R.raw.icon_pwpanel_uncheck);
				}
			}
		});
		//
		layout_pwpanel_pan.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (layout_pwpanel_cb.isChecked()) {
					layout_pwpanel_cb.setChecked(false);
					layout_pwpanel_cb.setBackgroundResource(R.raw.icon_pwpanel_uncheck);
				} else {
					layout_pwpanel_cb.setChecked(true);
					layout_pwpanel_cb.setBackgroundResource(R.raw.icon_pwpanel_check);
				}
			}
		});
	}

	@Override
	public void onClick(View v) {
		String pw = layout_pwpanel_et.getText().toString();
		switch (v.getId()) {
		case R.id.layout_pwpanel_num0:
			layout_pwpanel_et.setText(pw + "0");
			break;
		case R.id.layout_pwpanel_num1:
			layout_pwpanel_et.setText(pw + "1");
			break;
		case R.id.layout_pwpanel_num2:
			layout_pwpanel_et.setText(pw + "2");
			break;
		case R.id.layout_pwpanel_num3:
			layout_pwpanel_et.setText(pw + "3");
			break;
		case R.id.layout_pwpanel_num4:
			layout_pwpanel_et.setText(pw + "4");
			break;
		case R.id.layout_pwpanel_num5:
			layout_pwpanel_et.setText(pw + "5");
			break;
		case R.id.layout_pwpanel_num6:
			layout_pwpanel_et.setText(pw + "6");
			break;
		case R.id.layout_pwpanel_num7:
			layout_pwpanel_et.setText(pw + "7");
			break;
		case R.id.layout_pwpanel_num8:
			layout_pwpanel_et.setText(pw + "8");
			break;
		case R.id.layout_pwpanel_num9:
			layout_pwpanel_et.setText(pw + "9");
			break;
		case R.id.layout_pwpanel_clear:
			layout_pwpanel_et.setText("");
			break;
		case R.id.layout_pwpanel_backspace:
			if (!TypeJudgeTools.isNull(pw)) {
				pw = pw.substring(0, pw.length() - 1);
				layout_pwpanel_et.setText(pw);
			}
			break;
		}
	}

	/**
	 * 根据当前jumpDirection标志位初始化title
	 */
	public void initTitle() {
		if (layout_pwpanel_et != null) {
			layout_pwpanel_et.setText("");
		}
		if (layout_pwpanel_title != null) {
			int jumpDirection = Integer.valueOf(aCache.getAsString("jumpDirection"));
			switch (jumpDirection) {
			case 1:
				layout_pwpanel_title.setText("解锁使用“营销分析”");
				break;
			case 2:
				layout_pwpanel_title.setText("解锁使用“设置老板密码”");
				break;
			case 3:
				layout_pwpanel_title.setText("解锁使用“营业报表”");
				break;
			case 4:
				layout_pwpanel_title.setText("解锁使用“商品管理”");
				break;
			case 5:
				layout_pwpanel_title.setText("解锁修改“店铺信息”");
				break;
			case 6:
				layout_pwpanel_title.setText("解锁使用“打款管理”");
				break;
			}
		}
	}

	/**
	 * 跳转到相应页面上去
	 * 
	 * @param type
	 *            type 1:跳转到AnalysisFragment type 2:跳转到settingFragment
	 *            3:跳转到ReportFragment,4:跳转到goodsManagerFragment
	 *            5:跳转到ShopFragment，6:跳转到PMManagerFragment
	 *            7:跳转到DeliveryFeeFragment，8:跳转到BoxFeeFragment
	 *            9:跳转到ShopHoursFragment
	 */
	public void jump2Fragment(int type) {
		switch (type) {
		case 1:
			homeActivity.openFM(new FmInfo(AnalysisFragment.class, "营销分析", true));
			break;
		case 2:
			homeActivity.openFM(new FmInfo(SettingPWFragment.class, "设置老板密码", true));
			break;
		case 3:
			homeActivity.openFM(new FmInfo(ReportFragment.class, "营业报表", true));
			break;
		case 4:
			homeActivity.openFM(new FmInfo(GoodsManagerFragment.class, "商品管理", true));
			break;
		case 5:
			homeActivity.openFM(new FmInfo(ShopFragment.class, "店铺信息", true));
			homeActivity.isCanShopSetting = true;
			break;
		case 6:
			homeActivity.openFM(new FmInfo(PMManagerFragment.class, "打款管理", true));
			homeActivity.isCanShopSetting = true;
			break;
		case 7:
			homeActivity.openSubFM(homeActivity.getShopFm(),new FmInfo(DeliveryFeeFragment.class,"起送价&外送费", true));
			break;
		case 8:
			homeActivity.openSubFM(homeActivity.getShopFm(), new FmInfo(BoxFeeFragment.class,"餐盒费", true));
			break;
		case 9:	
			homeActivity.openSubFM(homeActivity.getShopFm(), new FmInfo(ShopHoursFragment.class,"外卖时段", true));
			break;
		}
	}

	/**
	 * 保持解锁5分钟
	 * 
	 * @param isChecked
	 *            true：表示保存解锁5分钟 false : 反之则反
	 */
	public void keepUnlock5Minute(boolean isChecked) {
		if (timer != null) {
			timer.cancel();
		}
		if (timerTask != null) {
			timerTask.cancel();
		}

		if (isChecked) {
			homeActivity.isUnlock5Minute = true;
			timer = new Timer();
			timerTask = new TimerTask() {
				@Override
				public void run() {
					homeActivity.isUnlock5Minute = false;
				}
			};
			timer.schedule(timerTask, 5 * 60 * 1000);
		} else {
			homeActivity.isUnlock5Minute = false;
		}
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		// 判断输入密码是否和保存密码一致，如果一致跳转到相应页面
		String pw = layout_pwpanel_et.getText().toString();
		if (!pw.isEmpty() && pw.length() == 4) {
			if (aCache.getAsString("settingPW").equals(pw)) {
				int jumpDirection = Integer.valueOf(aCache.getAsString("jumpDirection"));
				jump2Fragment(jumpDirection);
				// 保持解锁5分钟
				keepUnlock5Minute(layout_pwpanel_cb.isChecked());
			} else {
				// 判断是否是默认密码，
				String ServerTime = aCache.getAsString("ServerTime");
				long datetime = Long.parseLong(ServerTime);
				String defaultPW = FormatTools.convertTime(datetime, "MMdd");
				defaultPW = FormatTools.reverseString(defaultPW);
				if (pw.equals(defaultPW)) {
					SuperUI.openToast(homeActivity, "哎呀妈呀，这都被你发现了！");
					int jumpDirection = Integer.valueOf(aCache.getAsString("jumpDirection"));
					jump2Fragment(jumpDirection);
				} else {
					layout_pwpanel_et.setText("");
					SuperUI.openToast(homeActivity, "密码不正确，请修改再尝试！");
				}
			}
		}
	}

	@Override
	public void afterTextChanged(Editable s) {

	}
}
