package com.woyou.bean;


/**
 * 赠品统计
 * @author zhou.ni
 *
 */
public class GiftCount extends SuperModel{

	private static final long serialVersionUID = 1L;
	
	private String giftName;		//赠品名字
	private int giftNum;			//赠品数量
	private float giftPrice;		//赠品金额
	
	public String getGiftName() {
		return giftName;
	}
	public void setGiftName(String giftName) {
		this.giftName = giftName;
	}
	public int getGiftNum() {
		return giftNum;
	}
	public void setGiftNum(int giftNum) {
		this.giftNum = giftNum;
	}
	public float getGiftPrice() {
		return giftPrice;
	}
	public void setGiftPrice(float giftPrice) {
		this.giftPrice = giftPrice;
	}
	
	@Override
	public String toString() {
		return "GiftCount [giftName=" + giftName + ", giftNum=" + giftNum
				+ ", giftPrice=" + giftPrice + "]";
	}
	
	
	
}
