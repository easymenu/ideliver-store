package com.woyou.adapter;

import java.util.List;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.jing.biandang.R;
import com.squareup.picasso.Picasso;
import com.woyou.activity.HomeActivity;
import com.woyou.bean.CommentItem;
import com.woyou.fragment.MessageFragment;
import com.woyou.utils.FormatTools;

/**
 * 消息评论的适配器
 * 
 * @author zhou.ni
 *
 */
public class MessageCommentAdapter extends BaseAdapter {
	private HomeActivity homeActivity;
	private MessageFragment fragment;
	private LayoutInflater layoutInflater;
	private List<CommentItem> data;
	
	public MessageCommentAdapter(HomeActivity homeActivity,MessageFragment fragment, List<CommentItem> data) {
		this.homeActivity=homeActivity;
		this.fragment=fragment;
		layoutInflater = LayoutInflater.from(homeActivity);
		this.data = data;
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;
		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = layoutInflater.inflate(R.layout.item_message_comment_lv, null);
			viewHolder.item_comment_icon = (ImageView) convertView.findViewById(R.id.item_comment_icon);
			viewHolder.item_comment_name = (TextView) convertView.findViewById(R.id.item_comment_name);
			viewHolder.item_comment_times = (TextView) convertView.findViewById(R.id.item_comment_times);
			viewHolder.item_comment_date = (TextView) convertView.findViewById(R.id.item_comment_date);
			viewHolder.item_comment_info = (TextView) convertView.findViewById(R.id.item_comment_info);
			viewHolder.item_comment_reply = (TextView) convertView.findViewById(R.id.item_comment_reply);
			viewHolder.item_comment_replyinfo = (TextView) convertView.findViewById(R.id.item_comment_replyinfo);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
			
		CommentItem item = data.get(position);
		
		// 加载用户头像
		if ( !TextUtils.isEmpty(item.getuPic()) ){
			Picasso.with(homeActivity).load(item.getuPic()).placeholder(R.raw.icon_analysis_customer).error(R.raw.icon_analysis_customer).into(viewHolder.item_comment_icon);
		} else {
			viewHolder.item_comment_icon.setImageResource(R.raw.icon_analysis_customer);
		}
		
		if ( !TextUtils.isEmpty(item.getuName()) ) {
			viewHolder.item_comment_name.setText(item.getuName());
		} else {
			viewHolder.item_comment_name.setText("");
		}
		
		// 设置评论时间
		long cTime = item.getcTime();
		if ( cTime!=0 ) {
			String commentTime = FormatTools.convertTimeToFormat(homeActivity, cTime);
			viewHolder.item_comment_date.setText(commentTime);
		} else {
			viewHolder.item_comment_date.setText("");
		}
		
		if ( !TextUtils.isEmpty(item.getComments()) ){
			viewHolder.item_comment_info.setText(item.getComments());
		} else {
			viewHolder.item_comment_info.setText("");
		}
		
		// 回复评论内容
		if ( TextUtils.isEmpty(item.getReply()) ) {
			viewHolder.item_comment_reply.setVisibility(View.VISIBLE);
			viewHolder.item_comment_replyinfo.setVisibility(View.GONE);
		} else {
			viewHolder.item_comment_reply.setVisibility(View.GONE);
			viewHolder.item_comment_replyinfo.setVisibility(View.VISIBLE);
			viewHolder.item_comment_replyinfo.setText(item.getReply());
		}
		
		//点击回复按钮
		viewHolder.item_comment_reply.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				fragment.showCommentPanel(data.get(position));
			}
		});
		
		return convertView;
	}

	static class ViewHolder {
		ImageView item_comment_icon;
		TextView item_comment_name;
		TextView item_comment_times;
		TextView item_comment_date;
		TextView item_comment_info;
		// 回复操作
		TextView item_comment_replyinfo;
		TextView item_comment_reply;
	}
}
