package com.woyou.fragment.about;

import com.jing.biandang.R;
import com.woyou.WoYouApplication;
import com.woyou.activity.HomeActivity;
import com.woyou.bean.FmInfo;
import com.woyou.bean.rpc.Result;
import com.woyou.bean.rpc.UpdateVersionReq;
import com.woyou.bean.rpc.UpdateVersionRes;
import com.woyou.component.SuperUI;
import com.woyou.fragment.SuperFragment;
import com.woyou.model.rpc.UserModel;
import com.woyou.utils.LoginUtils;
import com.woyou.utils.MainBoardUtil;
import com.woyou.utils.ThreadPoolManager;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import retrofit.RetrofitError;

/**
 * 关于界面
 * 
 * @author zhou.ni
 *
 */
public class AboutFragment extends SuperFragment implements OnClickListener{
	private HomeActivity homeActivity;
	private View layout_about;
	private RelativeLayout layout_about_share;
	private RelativeLayout layout_about_checkupdate;
	private TextView layout_about_version;
	private RelativeLayout layout_about_introduct;
	private RelativeLayout layout_about_diagnosis;
	
	private Result<UpdateVersionRes> updateVersionResult;
	
	private	long lastClickTime = 0;

	Dialog openUpdateDialog;
	
	@Override
	public void onHiddenChanged(boolean hidden) {
		super.onHiddenChanged(hidden);
		if(hidden){
			if(openUpdateDialog!=null&&openUpdateDialog.isShowing()){
				openUpdateDialog.cancel();
			}
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		layout_about = inflater.inflate(R.layout.layout_about,container, false);
		homeActivity=(HomeActivity)getActivity();
		return layout_about;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		layout_about_share=(RelativeLayout)layout_about.findViewById(R.id.layout_about_share);
		layout_about_checkupdate=(RelativeLayout)layout_about.findViewById(R.id.layout_about_checkupdate);
		layout_about_diagnosis=(RelativeLayout)layout_about.findViewById(R.id.layout_about_diagnosis);
		layout_about_introduct=(RelativeLayout)layout_about.findViewById(R.id.layout_about_introduct);
		layout_about_version=(TextView)layout_about.findViewById(R.id.layout_about_version);
		
		layout_about_share.setOnClickListener(this);
		layout_about_checkupdate.setOnClickListener(this);
		layout_about_introduct.setOnClickListener(this);
		layout_about_diagnosis.setOnClickListener(this);
		layout_about_version.setText("V"+WoYouApplication.VersionCode);
	}
	
	/**
	 * 防止重复点击
	 * @return
	 */
	private boolean isFastDoubleClick() {
        long time = System.currentTimeMillis();
        long timeD = time - lastClickTime;
        if ( 0 < timeD && timeD < 1000) {   
            return true;   
        }   
        lastClickTime = time;   
        return false;   
    }
	

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.layout_about_share:
			if ( isFastDoubleClick() ) {
				return;
			}			
			homeActivity.openSubFM(this,new FmInfo(ShareFragment.class, "分享给朋友",false));
			break;
		case R.id.layout_about_checkupdate:
			if ( isFastDoubleClick() ) {
				return;
			}
			homeActivity.homeController.avoidForceClick(v);
			checkUpdate();
			break;
			
		case R.id.layout_about_introduct:
			if ( isFastDoubleClick() ) {
				return;
			}			
			homeActivity.openSubFM(this,new FmInfo(IntroductFragment.class, "欢迎页面",false));
			break;
		case R.id.layout_about_diagnosis:
			if ( isFastDoubleClick() ) {
				return;
			}
			homeActivity.openSubFM(this,new FmInfo(DiagnosisFragment.class, "网络诊断",false));
			break;
		}
		
	}
	
	/**
	 * 检测软件更新
	 */
	public void checkUpdate(){
		
		ThreadPoolManager.getInstance().executeTask(new Runnable() {
			@Override
			public void run() {
				try{
					homeActivity.showLoadingHint();
					UpdateVersionReq updateVersionReq = new UpdateVersionReq();
					updateVersionReq.setAppName("BD_VERSION2");
					updateVersionReq.setBoardType(MainBoardUtil.getModel());
					updateVersionReq.setPwd(LoginUtils.getUserInfo(homeActivity).getPwd());
					updateVersionReq.setsId(LoginUtils.getUserInfo(homeActivity).getsId());
					updateVersionReq.setVersion(LoginUtils.getUserInfo(homeActivity).getVersion());
					updateVersionResult = UserModel.getInstance(getActivity()).updateVersion(updateVersionReq);
					homeActivity.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							if(updateVersionResult != null && updateVersionResult.code == 1&&updateVersionResult.getData()!=null){
								UpdateVersionRes updateVersionRes = updateVersionResult.getData();
								if(updateVersionRes.getIsUpdate()==0){
									SuperUI.openToast(homeActivity, "当前是最新版本！");
								}else if(updateVersionRes.getIsUpdate()==-1){
									SuperUI.openToast(homeActivity, "异常");
								}else if(updateVersionRes.getIsUpdate()==1){
									if(updateVersionRes.getType()==1){
										//强制更新
										openUpdateDialog=SuperUI.openUpdateDialog(true,homeActivity,updateVersionRes);
									}else{
										//选择更新
										openUpdateDialog=SuperUI.openUpdateDialog(false,homeActivity,updateVersionRes);
									}
								}
							}else if(updateVersionResult != null && updateVersionResult.code == 1&&updateVersionResult.getData()==null){
								SuperUI.openToast(homeActivity, ""+updateVersionResult.msg);
							}
							else if (updateVersionResult!=null && updateVersionResult.code==-3) {
								homeActivity.homeController.exitLogin();
							}
						}
					});
				}catch (RetrofitError error) {
					switch (error.getKind()) {
					case CONVERSION:
						homeActivity.runOnUiThread(new Runnable() {
							public void run() {
								SuperUI.openToast(homeActivity, "服务器又任性了！");
							}
						});
						break;
					case HTTP:
						homeActivity.runOnUiThread(new Runnable() {
							public void run() {
								SuperUI.openToast(homeActivity, "服务器又任性了！");
							}
						});
						break;
					case NETWORK:
						homeActivity.runOnUiThread(new Runnable() {
							public void run() {
								SuperUI.openToast(homeActivity, "请检查网络连接！");
							}
						});
						break;
					case UNEXPECTED:
						homeActivity.runOnUiThread(new Runnable() {
							public void run() {
								SuperUI.openToast(homeActivity, "请检查网络连接！");
							}
						});
						break;
					default:
						break;
					}
				}finally{
					homeActivity.hideLoadingHint();
				}
			}
		});
	}
}
