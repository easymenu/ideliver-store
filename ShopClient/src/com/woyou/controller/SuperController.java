/** 
 * @Title:  SuperController.java 
 * @author:  xuron
 * @data:  2015年11月23日 上午10:52:33 <创建时间>
 * 
 * @history：<以下是历史记录>
 *
 * @modifier: <修改人>
 * @modify date: 2015年11月23日 上午10:52:33 <修改时间>
 * @log: <修改内容>
 *
 * @modifier: <修改人>
 * @modify date: 2015年11月23日 上午10:52:33 <修改时间>
 * @log: <修改内容>
 */
package com.woyou.controller;

import com.woyou.utils.ACache;
import com.woyou.utils.ThreadPoolManager;

import android.app.Activity;
import android.content.Context;

/**
 * controller超类
 * 
 * @author xuron
 * @versionCode 1 <每次修改提交前+1>
 */
public class SuperController {
	/**
	 * 上下文
	 */
	public Context mContext;
	/**
	 * 缓存类
	 */
	public static ACache aCache;
	/**
	 * activity
	 */
	public Activity mActivity;
	/**
	 * 线程池
	 */
	public ThreadPoolManager mThreadPoolManager;

	public SuperController(Context mContext) {
		this.mContext = mContext;
		mActivity = (Activity) mContext;
		aCache = getACache();
		mThreadPoolManager = ThreadPoolManager.getInstance();
	}

	/**
	 * 缓存对象Acache
	 */
	public ACache getACache() {
		if (aCache == null) {
			aCache = ACache.get(mContext);
		}
		return aCache;
	}

	/**
	 * UI线程执行一个任务
	 * 
	 * @param run
	 */
	public void runOnUI(Runnable run) {
		mActivity.runOnUiThread(run);
	}

	/**
	 * 子线程运行
	 * 
	 * @param run
	 */
	public void executeTask(Runnable run) {
		mThreadPoolManager.executeTask(run);
	}
}
