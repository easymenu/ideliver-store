/** 
 * @Title:  NetHintView.java 
 * @author:  xuron
 * @data:  2015年11月25日 上午11:18:15 <创建时间>
 * 
 * @history：<以下是历史记录>
 *
 * @modifier: <修改人>
 * @modify date: 2015年11月25日 上午11:18:15 <修改时间>
 * @log: <修改内容>
 *
 * @modifier: <修改人>
 * @modify date: 2015年11月25日 上午11:18:15 <修改时间>
 * @log: <修改内容>
 */
package com.woyou.component;

import com.jing.biandang.R;
import com.woyou.activity.HomeActivity;
import com.woyou.bean.FmInfo;
import com.woyou.fragment.about.DiagnosisFragment;
import com.woyou.service.MessageConnection;
import com.woyou.utils.LightManager;
import com.woyou.utils.ThreadPoolManager;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;

/**
 * 蓝色的网络提示框
 * 
 * @author xuron
 * @versionCode 1 <每次修改提交前+1>
 */
public class NetHintView extends SuperLayout {
	private HomeActivity homeActivity;
	private Button layout_net_b;

	public NetHintView(Context context) {
		super(context);
		initView();
	}

	public NetHintView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initView();
	}

	public NetHintView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initView();
	}

	@Override
	protected void initView() {
		View.inflate(getContext(), R.layout.layout_hint, this);
		layout_net_b = (Button) findViewById(R.id.layout_net_b);
		layout_net_b.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				homeActivity.openFM(new FmInfo(DiagnosisFragment.class, "网络诊断",false));
			}
		});
	}

	/**
	 * 设置参数
	 */
	public void setParams(HomeActivity homeActivity) {
		this.homeActivity = homeActivity;
	}

	/**
	 * 提示与服务器断开
	 */
	public void startNetHint() {
		if (getVisibility() == View.VISIBLE) {
			return;
		}

		/**
		 * 延迟48秒弹蓝色窗口，如果网络依旧连不上这弹出蓝框，如果连上了就不弹了
		 */
		ThreadPoolManager.getInstance().executeTask(new Runnable() {
			@Override
			public void run() {
				try {
					Thread.sleep(4 * 12 * 1000);
					homeActivity.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							if (!MessageConnection.getInstance().isHeartbeatOk) {
								setVisibility(View.VISIBLE);
							}
						}
					});
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		/**
		 * 延迟24秒闪灯，如果网络依旧连不上则闪灯，如果连上了就不闪灯
		 */
		ThreadPoolManager.getInstance().executeTask(new Runnable() {
			@Override
			public void run() {
				try {
					Thread.sleep(2 * 12 * 1000);
					if (!MessageConnection.getInstance().isHeartbeatOk) {
						LightManager.turnOnRedBlueLight(true);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * 停止提示
	 */
	public void stopNetHint() {
		post(new Runnable() {
			@Override
			public void run() {
				if (getVisibility() == View.VISIBLE) {
					setVisibility(View.GONE);
				}
				LightManager.turnOnRedBlueLight(false);
			}
		});
	}
}
