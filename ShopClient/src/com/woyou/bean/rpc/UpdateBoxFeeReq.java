package com.woyou.bean.rpc;

/**
 * 更新餐盒费的请求bean
 * 
 * @author zhou.ni
 * 
 */
public class UpdateBoxFeeReq {
	
	private String sId;			//店铺Id
	private String pwd;			//密码
	private int type;			//餐盒费类型（0：不算1：按单算,2:按份算）
	private float price;		//价格
	
	public String getsId() {
		return sId;
	}
	public void setsId(String sId) {
		this.sId = sId;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	
}
