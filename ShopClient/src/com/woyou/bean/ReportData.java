package com.woyou.bean;


/**
 * 每一条报表详情的数据
 * 
 * @author zhou.ni
 * 
 */
public class ReportData extends SuperModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String name; // 商品名字
	private String num; // 下单量
	private String price; // 消费总金额
	private int type; // 0 表示商品 1 表示赠品 2 表示优惠券 3表示餐盒费

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNum() {
		return num;
	}

	public void setNum(String num) {
		this.num = num;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "ReportData [name=" + name + ", num=" + num + ", price=" + price
				+ ", type=" + type + "]";
	}

}
