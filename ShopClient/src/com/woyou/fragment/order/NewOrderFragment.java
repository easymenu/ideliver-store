package com.woyou.fragment.order;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import com.jing.biandang.R;
import com.woyou.Constants;
import com.woyou.activity.HomeActivity;
import com.woyou.adapter.NewOrderAdapter;
import com.woyou.bean.BoxFee;
import com.woyou.bean.DeliverFee;
import com.woyou.bean.FmInfo;
import com.woyou.bean.Order;
import com.woyou.bean.OrderId;
import com.woyou.bean.rpc.HandleOrderReq;
import com.woyou.bean.rpc.QueryNewOidListReq;
import com.woyou.bean.rpc.QueryNewOidListRes;
import com.woyou.bean.rpc.QueryOrderDetailsReq;
import com.woyou.bean.rpc.QueryOrderDetailsRes;
import com.woyou.bean.rpc.Result;
import com.woyou.bean.rpc.UpdateOrdersStatusReq;
import com.woyou.component.NewOrderCancelPop;
import com.woyou.component.SuperUI;
import com.woyou.fragment.SuperFragment;
import com.woyou.model.OrderManageModel;
import com.woyou.model.rpc.OrderModel;
import com.woyou.utils.CloneUtils;
import com.woyou.utils.FormatTools;
import com.woyou.utils.LightManager;
import com.woyou.utils.LoginUtils;
import com.woyou.utils.PrintNumUtils;
import com.woyou.utils.PrintUtil;
import com.woyou.utils.SoundManager;
import com.woyou.utils.ThreadPoolManager;
import com.woyou.utils.TypeJudgeTools;

import android.app.Dialog;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import retrofit.RetrofitError;

public class NewOrderFragment extends SuperFragment implements OnClickListener {
	private HomeActivity homeActivity;
	private View newOrder;
	private LinearLayout layout_neworder_bg;
	private ImageView layout_neworder_anim;
	private AnimationDrawable animationDrawable;
	private TextView layout_neworder_hint;
	private TextView layout_neworder_num;
	private TextView layout_neworder_time;
	private TextView layout_neworder_address;
	private TextView layout_neworder_name;
	private TextView layout_neworder_sex;
	private TextView layout_neworder_sum;
	private TextView layout_neworder_remarks;
	private ProgressBar layout_neworder_pb;
	private RelativeLayout layout_neworder_skip;
	private RelativeLayout layout_neworder_cancel;
	private RelativeLayout layout_neworder_ok;
	private RelativeLayout layout_neworder_reorder;
	private ListView layout_neworder_lv;
	private List<Object> goodsList = new ArrayList<Object>();
	private NewOrderAdapter newOrderAdapter;
	// 新订单的列表
	private List<String> newOrderList;
	// 订单
	Result<QueryOrderDetailsRes> result;
	// 订单详情
	QueryOrderDetailsRes queryOrderDetailsRes;

	/**
	 * 当前新订单列表订单的位置
	 */
	public int currentOrder = 0;
	/**
	 * 刷新新订单的timer
	 */
	private Timer timer;
	private TimerTask timerTask;
	/**
	 * 往哪边跳转 true:表示跳转到订单页面 false:表示跳转到原有页面
	 */
	private Boolean jumpType = false;
	/**
	 * 进度条
	 */
	private int progress = 120;
	private Timer progressTimer;
	private TimerTask progressTimerTask;
	/**
	 * pop
	 */
	private NewOrderCancelPop newOrderCancelPop;
	private OrderModel orderModel;
	private OrderManageModel omModel = OrderManageModel
			.getInstance();

	@Override
	public void onHiddenChanged(boolean hidden) {
		if (hidden) {
			if (newOrderCancelPop != null) {
				newOrderCancelPop.hidePopup();
			}
			// 判断是否5分钟可以切换
			homeActivity.isCanSwitchView = true;
		}else{
			initData();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		newOrder = inflater.inflate(R.layout.layout_neworder, container, false);
		homeActivity=(HomeActivity)getActivity();
		orderModel = OrderModel.getInstance(homeActivity);
		return newOrder;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		initView();
		initData();
	}

	/**
	 * 显示新订单详情
	 */
	public void showNewOrderDetails() {
		homeActivity.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				layout_neworder_ok.setVisibility(View.VISIBLE);
				layout_neworder_reorder.setVisibility(View.GONE);

				// 刷新当前新订单的数量并且判断订单数量是否大于等于2则显示跳过按钮
				newOrderList = homeActivity.homeController.newOrderList;
				if (newOrderList.size() >= 2) {
					layout_neworder_hint.setText(newOrderList.size()
							+ "个新订单\n快应答");
					layout_neworder_bg
							.setBackgroundResource(R.raw.neworder_bg_more);
					layout_neworder_skip.setVisibility(View.VISIBLE);
				} else {
					layout_neworder_hint.setText("新订单来了\n快应答");
					layout_neworder_bg.setBackgroundResource(R.raw.neworder_bg);
					layout_neworder_skip.setVisibility(View.GONE);
				}

				// 截取订单号获取后三位
				layout_neworder_num.setText(queryOrderDetailsRes.getShortNo());
				// 判断是预约单还是及时单
				int isReserve = queryOrderDetailsRes.getIsReserve();
				if (isReserve == 1) {
					String time = FormatTools.convertTime(
							queryOrderDetailsRes.getReserveTime() * 1000,
							"yyyy-MM-dd");
					layout_neworder_time.setText(time + "  尽快送达");
					layout_neworder_time.setTextColor(0xff000000);
					layout_neworder_time.setBackgroundColor(0x00000000);
				} else {
					String time = FormatTools.convertTime(
							queryOrderDetailsRes.getReserveTime() * 1000,
							"yyyy-MM-dd HH:mm");
					layout_neworder_time.setText("预约:	" + time);
					layout_neworder_time.setTextColor(0xff009600);
					layout_neworder_time.setBackgroundColor(0xff9eff9e);
				}

				layout_neworder_address.setText(queryOrderDetailsRes.getAddr());
				layout_neworder_name.setText(queryOrderDetailsRes.getContact());
				// 判断用户性别
				if (queryOrderDetailsRes.getSex() == 1) {
					layout_neworder_sex.setText("小姐");
				} else if (queryOrderDetailsRes.getSex() == 2) {
					layout_neworder_sex.setText("先生");
				} else {
					layout_neworder_sex.setText("");
				}
				if(queryOrderDetailsRes.getIsPay()==0){
					layout_neworder_sum.setText(Html.fromHtml("<small>¥</small>" + FormatTools.String2Money(queryOrderDetailsRes.getSumFee()+"")+"<small> ( 已支付 ) </small>"));
				}else{
					layout_neworder_sum.setText(Html.fromHtml("<small>¥</small>" + FormatTools.String2Money(queryOrderDetailsRes.getSumFee()+"")));
				}

				if (TypeJudgeTools.isNull(queryOrderDetailsRes.getRemark())) {
					layout_neworder_remarks.setText("");
				} else {
					layout_neworder_remarks.setText("备注："
							+ queryOrderDetailsRes.getRemark());
				}

				// 重置列表
				goodsList.clear();
				// 添加商品
				if (queryOrderDetailsRes.getGoodsList() != null
						&& queryOrderDetailsRes.getGoodsList().size() > 0) {
					goodsList.addAll(queryOrderDetailsRes.getGoodsList());
				}
				// 外送费
				if (queryOrderDetailsRes.getDeliverFee() > 0) {
					DeliverFee deliverFee = new DeliverFee();
					deliverFee.setPrice(queryOrderDetailsRes.getDeliverFee());
					goodsList.add(deliverFee);
				}
				// 餐盒费
				if (queryOrderDetailsRes.getBoxFee() > 0) {
					BoxFee boxFee = new BoxFee();
					boxFee.setType(queryOrderDetailsRes.getBoxType());
					boxFee.setNum(queryOrderDetailsRes.getBoxNum());
					boxFee.setPrice(queryOrderDetailsRes.getBoxFee());
					goodsList.add(boxFee);
				}
				// 添加赠品
				if (queryOrderDetailsRes.getGiftList() != null
						&& queryOrderDetailsRes.getGiftList().size() > 0) {
					goodsList.addAll(queryOrderDetailsRes.getGiftList());
				}
				// 添加优惠券
				if (queryOrderDetailsRes.getCouponList() != null
						&& queryOrderDetailsRes.getCouponList().size() > 0) {
					goodsList.addAll(queryOrderDetailsRes.getCouponList());
				}
				if (newOrderAdapter == null) {
					newOrderAdapter = new NewOrderAdapter(
							NewOrderFragment.this, getActivity(), goodsList);
					layout_neworder_lv.setAdapter(newOrderAdapter);
				} else {
					newOrderAdapter.notifyDataSetChanged();
				}
			}
		});
	}

	/**
	 * 初始化页面
	 */
	public void initView() {
		layout_neworder_bg = (LinearLayout) newOrder
				.findViewById(R.id.layout_neworder_bg);
		layout_neworder_anim = (ImageView) newOrder
				.findViewById(R.id.layout_neworder_anim);
		// 动画
		layout_neworder_anim.setImageResource(R.drawable.anim_neworder);
		animationDrawable = (AnimationDrawable) layout_neworder_anim
				.getDrawable();
		// 启动闪烁动画
		startFlashAnim();
		layout_neworder_hint = (TextView) newOrder
				.findViewById(R.id.layout_neworder_hint);
		layout_neworder_num = (TextView) newOrder
				.findViewById(R.id.layout_neworder_num);
		layout_neworder_time = (TextView) newOrder
				.findViewById(R.id.layout_neworder_time);
		layout_neworder_address = (TextView) newOrder
				.findViewById(R.id.layout_neworder_address);
		layout_neworder_name = (TextView) newOrder
				.findViewById(R.id.layout_neworder_name);
		layout_neworder_sex = (TextView) newOrder
				.findViewById(R.id.layout_neworder_sex);
		layout_neworder_sum = (TextView) newOrder
				.findViewById(R.id.layout_neworder_sum);
		layout_neworder_remarks = (TextView) newOrder
				.findViewById(R.id.layout_neworder_remarks);
		layout_neworder_lv = (ListView) newOrder
				.findViewById(R.id.layout_neworder_lv);

		layout_neworder_pb = (ProgressBar) newOrder
				.findViewById(R.id.layout_neworder_pb);
		layout_neworder_skip = (RelativeLayout) newOrder
				.findViewById(R.id.layout_neworder_skip);
		layout_neworder_cancel = (RelativeLayout) newOrder
				.findViewById(R.id.layout_neworder_cancel);
		layout_neworder_ok = (RelativeLayout) newOrder
				.findViewById(R.id.layout_neworder_ok);
		layout_neworder_reorder = (RelativeLayout) newOrder
				.findViewById(R.id.layout_neworder_reorder);

		layout_neworder_skip.setOnClickListener(this);
		layout_neworder_cancel.setOnClickListener(this);
		layout_neworder_ok.setOnClickListener(this);
		layout_neworder_reorder.setOnClickListener(this);
	}

	/**
	 * 初始化数据
	 */
	public void initData() {
		// 判断是否5分钟可以切换
		homeActivity.isCanSwitchView = false;
		// 重置
		currentOrder = 0;
		// 显示蒙版，屏蔽actionbar上的点击事件
		homeActivity.actionbarView.actionBarController.showMask();
		// 设置标志位，使取消订单页面不能显示
		homeActivity.homeController.isShowCancelOrder = false;
		// 启动闪烁动画
		startFlashAnim();
		ThreadPoolManager.getInstance().executeTask(new Runnable() {
			@Override
			public void run() {
				try {
					homeActivity.showLoadingHint();
					newOrderList = homeActivity.homeController.newOrderList;
					if (newOrderList != null && newOrderList.size() > 0) {
						UpdateOrdersStatusReq updateOrdersStatusReq = new UpdateOrdersStatusReq();
						updateOrdersStatusReq.setsId(LoginUtils.getUserInfo(
								homeActivity).getsId());
						updateOrdersStatusReq.setPwd(LoginUtils.getUserInfo(
								homeActivity).getPwd());

						List<OrderId> orders = new ArrayList<OrderId>();
						for (int i = 0; i < newOrderList.size(); i++) {
							OrderId orderId = new OrderId();
							orderId.setoId(newOrderList.get(i));
							orders.add(orderId);
						}
						updateOrdersStatusReq.setOrderList(orders);
						// 订单置S
						Result updateOrdersStatusRes = orderModel
								.updateOrdersStatus(updateOrdersStatusReq);
						if (updateOrdersStatusRes != null
								&& updateOrdersStatusRes.getCode() == 1) {
							QueryOrderDetailsReq queryOrderDetailsReq = new QueryOrderDetailsReq();
							queryOrderDetailsReq.setsId(LoginUtils.getUserInfo(
									homeActivity).getsId());
							queryOrderDetailsReq.setPwd(LoginUtils.getUserInfo(
									homeActivity).getPwd());
							queryOrderDetailsReq.setoId(newOrderList.get(0));
							// 获取该订单详情
							result = orderModel.v2_3queryOrderDetails(queryOrderDetailsReq);
							if (result != null && result.getCode() == 1) {
								SoundManager.getInstance(homeActivity)
										.playSound(SoundManager.NEWORDER);
								LightManager.turnOnBlueLight(true);
								queryOrderDetailsRes = result.getData();
								showNewOrderDetails();
							} else if (result != null && result.getCode() == -3) {
								homeActivity.homeController.exitLogin();
							}
						} else if (result != null && result.getCode() == -3) {
							homeActivity.homeController.exitLogin();
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					homeActivity.hideLoadingHint();
					// 取消timer
					if (timer != null) {
						timer.cancel();
						timer = null;
					}
					if (timerTask != null) {
						timerTask.cancel();
						timerTask = null;
					}
					//
					timer = new Timer();
					timerTask = new TimerTask() {
						@Override
						public void run() {
							hasNextOrder();
						}
					};
					// 120s之后刷新新订单列表
					timer.schedule(timerTask, 1000 * 120);
				}
			}
		});
		initProgressBar();
	}

	/**
	 * 初始化progressbar
	 */
	public void initProgressBar() {
		progress = 120;
		if (progressTimer != null) {
			progressTimer.cancel();
			progressTimer = null;
		}
		if (progressTimerTask != null) {
			progressTimerTask.cancel();
			progressTimerTask = null;
		}
		progressTimer = new Timer();
		progressTimerTask = new TimerTask() {
			@Override
			public void run() {
				homeActivity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						layout_neworder_pb.setProgress(progress);
						progress = progress - 1;
					}
				});
			}
		};
		progressTimer.schedule(progressTimerTask, 1000, 1000);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.layout_neworder_skip:
			skipNewOrder();
			break;
		case R.id.layout_neworder_ok:
			acceptNewOrder();
			break;
		case R.id.layout_neworder_cancel:
			if(newOrderCancelPop==null){
				newOrderCancelPop = new NewOrderCancelPop(homeActivity,layout_neworder_cancel);
			}
			newOrderCancelPop.showPopup(currentOrder);
			break;
		case R.id.layout_neworder_reorder:
			break;
		}
	}

	/**
	 * 清除所有新订单返回OrderFragment
	 */
	public void clearAllNewOrder() {
		try {
			// 刷新无效订单
			homeActivity.getOrderFm().orderController.loadInvalidOrderList();

			// 查看当前是否有取消订单，如果有就进入取消订单页面处理取消订单
			if (homeActivity.homeController.cancelOrderList != null&& homeActivity.homeController.cancelOrderList.size() > 0) {
				// 重置actionbar
				homeActivity.resetActionBar(true);
				// 重置，记录要跳往的地方
				jumpType = false;
				// 把定时刷新计时器给关掉
				if (timer != null) {
					timer.cancel();
					timer = null;
				}
				// 设置标志位，使取消订单页面能显示
				homeActivity.homeController.isShowCancelOrder = true;
				// 关闭闪烁动画
				stopFlashAnim();
				// 关灯关声音
				LightManager.turnOnBlueLight(false);
				SoundManager.getInstance(homeActivity).stopSound();
				// 移除new订单列表
				homeActivity.homeController.newOrderList.clear();
				// 跳转到取消订单页面
				homeActivity.homeController.jump2CancelOrder();
				return;
			}

			// 刷新新订单的数据
			if (jumpType) {
				homeActivity.openFM(new FmInfo(OrderFragment.class, null,false));
				homeActivity.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						homeActivity.getOrderFm().layout_order_vp.setCurrentItem(0, false);
					}
				});
			} else {
				// 显示相应的Fragment
				Class clazz=homeActivity.getCurrentFragment().getClass();
				homeActivity.openFM(new FmInfo(clazz, null,false));
			}
			// 重置，记录要跳往的地方
			jumpType = false;
			// 隐藏蒙版，使actionbar上的点击事件可用
			homeActivity.menuView.menuViewController.hideMask();
			// 把定时刷新计时器给关掉
			if (timer != null) {
				timer.cancel();
				timer = null;
			}
			// 设置标志位，使取消订单页面能显示
			homeActivity.homeController.isShowCancelOrder = true;
			// 关闭闪烁动画
			stopFlashAnim();
			// 关灯关声音
			LightManager.turnOnBlueLight(false);
			SoundManager.getInstance(homeActivity).stopSound();
			// 移除new订单列表
			aCache.remove("newOrderList");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			currentOrder = 0;
		}
	}

	/**
	 * 跳过当前订单，到下一个新订单currentOrder
	 */
	public void skipNewOrder() {

		ThreadPoolManager.getInstance().executeTask(new Runnable() {

			@Override
			public void run() {
				try {
					newOrderList = homeActivity.homeController.newOrderList;
					// 新订单列表位置往后挪一位
					currentOrder = currentOrder + 1;
					// 如果当前位置大于列表的大小者当前的位置设为0
					if (currentOrder >= newOrderList.size()) {
						currentOrder = 0;
					}
					if (newOrderList != null && newOrderList.size() > 0) {
						UpdateOrdersStatusReq updateOrdersStatusReq = new UpdateOrdersStatusReq();
						updateOrdersStatusReq.setsId(LoginUtils.getUserInfo(
								homeActivity).getsId());
						updateOrdersStatusReq.setPwd(LoginUtils.getUserInfo(
								homeActivity).getPwd());

						List<OrderId> orders = new ArrayList<OrderId>();
						for (int i = 0; i < newOrderList.size(); i++) {
							OrderId orderId = new OrderId();
							orderId.setoId(newOrderList.get(i));
							orders.add(orderId);
						}
						updateOrdersStatusReq.setOrderList(orders);
						// 订单置S
						Result updateOrdersStatusRes = orderModel
								.updateOrdersStatus(updateOrdersStatusReq);
						if (updateOrdersStatusRes != null
								&& updateOrdersStatusRes.getCode() == 1) {
							QueryOrderDetailsReq queryOrderDetailsReq = new QueryOrderDetailsReq();
							queryOrderDetailsReq.setsId(LoginUtils.getUserInfo(
									homeActivity).getsId());
							queryOrderDetailsReq.setPwd(LoginUtils.getUserInfo(
									homeActivity).getPwd());
							queryOrderDetailsReq.setoId(newOrderList
									.get(currentOrder));
							result = orderModel.v2_3queryOrderDetails(queryOrderDetailsReq);
							if (result != null && result.getCode() == 1) {
								SoundManager.getInstance(homeActivity).playSound(
										SoundManager.NEWORDER);
								LightManager.turnOnBlueLight(true);
								queryOrderDetailsRes = result.getData();
								showNewOrderDetails();
							} else if (result != null && result.getCode() == -3) {
								homeActivity.homeController.exitLogin();
							}
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					homeActivity.hideLoadingHint();
				}
			}
		});
	}

	/**
	 * 接新订单
	 */
	public void acceptNewOrder() {
		ThreadPoolManager.getInstance().executeTask(new Runnable() {

			@Override
			public void run() {
				try {
					homeActivity.showLoadingHint();
					// 将统计拒绝次数设为0
					Constants.refuseOrderNum = 0;

					newOrderList = homeActivity.homeController.newOrderList;
					HandleOrderReq handleOrderReq = new HandleOrderReq();
					handleOrderReq.setoId(newOrderList.get(currentOrder));
					handleOrderReq.setsId(LoginUtils.getUserInfo(homeActivity)
							.getsId());
					handleOrderReq.setPwd(LoginUtils.getUserInfo(homeActivity)
							.getPwd());
					handleOrderReq.setHandle(1);
					final Result<Order> result = orderModel
							.handleOrder(handleOrderReq);

					// 移除新订单列表的第一个数据并将新的newOrderList存储到aCache中
					newOrderList.remove(currentOrder);
					if (result != null && result.getCode() == 1
							&& result.getData() != null) {
						homeActivity.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								// 记录要跳往的地方
								jumpType = true;
								omModel.addNewOrder2NoSend(result.getData());
								homeActivity.getOrderFm().refreshUnSendOrder();

								// 判断要打印几张订单
								final int printNum = PrintNumUtils
										.getPrintNum(homeActivity);
								if (printNum == 0) {
									Dialog notPrintHint = SuperUI
											.openNotPrintHint(homeActivity);
									notPrintHint.show();
								} else {
									Dialog printingDialog = SuperUI
											.openPrintingHint(homeActivity);
									printingDialog.show();
								}

								/**
								 * 开启线程，去打印订单
								 */
								printOrder(printNum);
							}
						});
					} else if (result != null && result.getCode() == -1) {
						homeActivity.runOnUiThread(new Runnable() {

							@Override
							public void run() {
								SuperUI.openToast(homeActivity,
										"" + result.getMsg());
								// 判断订单列表newOrderList.size()是否为小于0，如果小于0返回订单页面，如果大于0继续接单
								hasNextOrder();
							}
						});
					} else if (result != null && result.getCode() == -3) {
						homeActivity.homeController.exitLogin();
					} else {
						// 判断订单列表newOrderList.size()是否为小于0，如果小于0返回订单页面，如果大于0继续接单
						hasNextOrder();
					}
				} catch (Exception e) {
					e.printStackTrace();
					acceptNewOrder();
				}
			}
		});

	}

	/**
	 * 打印订单
	 */
	public void printOrder(final int printNum){
		ThreadPoolManager.getInstance().executeTask(
				new Runnable() {
					@Override
					public void run() {
						try {
							QueryOrderDetailsRes orderDetails=(QueryOrderDetailsRes)CloneUtils.copy(queryOrderDetailsRes);
							orderDetails.setOpStatus("已确认");
							for (int i = 0; i < printNum; i++) {
								final boolean isOk = PrintUtil.printOrder(homeActivity,orderDetails);
								homeActivity.runOnUiThread(new Runnable() {
											@Override
											public void run() {
												if (!isOk) {
													SuperUI.openToast(homeActivity,"请检查您的打印机");
												}
											}
										});
							}
							// 判断订单列表newOrderList.size()是否为小于0，如果小于0返回订单页面，如果大于0继续接单
							hasNextOrder();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
	}
	/**
	 * 判断订单列表newOrderList.size()是否为小于0，如果小于0返回订单页面，如果大于0继续接单
	 */
	public void hasNextOrder() {

		ThreadPoolManager.getInstance().executeTask(new Runnable() {
			@Override
			public void run() {
				try {
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					homeActivity.showLoadingHint();
					// 获取新订单列表
					QueryNewOidListReq queryNewOidListReq = new QueryNewOidListReq();
					queryNewOidListReq.setsId(LoginUtils.getUserInfo(homeActivity).getsId());
					queryNewOidListReq.setPwd(LoginUtils.getUserInfo(homeActivity).getPwd());
					Result<List<QueryNewOidListRes>> newOrder = orderModel.queryNewOidList(queryNewOidListReq);
					if (newOrder != null && newOrder.getCode() == 1&& newOrder.getData() != null) {
						List<QueryNewOidListRes> list = newOrder.getData();
						newOrderList.clear();
						for (int i = 0; i < list.size(); i++) {
							newOrderList.add(list.get(i).getoId());
						}
						int hasData = newOrderList.size();
						if (hasData <= 0) {
							clearAllNewOrder();
						} else {
							initData();
						}
					} else if (result != null && result.getCode() == -3) {
						homeActivity.homeController.exitLogin();
					} else {
						clearAllNewOrder();
					}
				} catch (RetrofitError error) {
					clearAllNewOrder();
				} finally {
					homeActivity.hideLoadingHint();
				}
			}
		});

	}

	/**
	 * 启动闪烁动画
	 */
	private void startFlashAnim() {
		if (animationDrawable != null) {
			if (!animationDrawable.isRunning()) {
				animationDrawable.start();
			}
		}
	}

	/**
	 * 关闭闪烁动画
	 */
	private void stopFlashAnim() {
		if (animationDrawable != null) {
			animationDrawable.stop();
		}
	}

	/**
	 * 更新订单数据接口
	 * 
	 * @author lenovo
	 * 
	 */
	public interface RefreshOrderImpl {
		/**
		 * 更新未送订单
		 */
		public void refreshUnSendOrder();

		/**
		 * 更新无效订单
		 */
		public void refreshInvalidOrder();
	}

}
