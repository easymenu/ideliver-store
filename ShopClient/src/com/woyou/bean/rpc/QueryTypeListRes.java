package com.woyou.bean.rpc;

import java.util.ArrayList;
import java.util.List;

import com.woyou.bean.GoodsTypeInfo;
import com.woyou.bean.SuperModel;

/**
 * 获取商品分类列表结果集
 * @author tlc
 *
 */
public class QueryTypeListRes extends SuperModel {
	
	private List<GoodsTypeInfo> list = new ArrayList<GoodsTypeInfo>();

	public List<GoodsTypeInfo> getList() {
		return list;
	}

	public void setList(List<GoodsTypeInfo> list) {
		this.list = list;
	}
	
}
