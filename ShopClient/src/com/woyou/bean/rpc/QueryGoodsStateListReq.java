package com.woyou.bean.rpc;

import com.woyou.bean.SuperModel;

/**
 * 2.28 获取商品状态列表
 * 
 * @author 荣
 * 
 */
public class QueryGoodsStateListReq extends SuperModel {
	private String sId = "";
	private String tId = "";
	private String pwd = "";

	public String getsId() {
		return sId;
	}

	public void setsId(String sId) {
		this.sId = sId;
	}

	public String gettId() {
		return tId;
	}

	public void settId(String tId) {
		this.tId = tId;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

}
