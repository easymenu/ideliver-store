package com.woyou.bean.rpc;

import java.util.List;

import com.woyou.bean.MarkUp;

/**
 * 更新商铺阶梯外送费的请求bean
 * @author zhou.ni
 *
 */
public class UpdateLadderFeeReq {
	
	private String sId;					//店铺id
	private String pwd;
	private List<MarkUp> markUpList;	//外送阶梯价列表
	
	public String getsId() {
		return sId;
	}
	public void setsId(String sId) {
		this.sId = sId;
	}
	public List<MarkUp> getMarkUpList() {
		return markUpList;
	}
	public void setMarkUpList(List<MarkUp> markUpList) {
		this.markUpList = markUpList;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	
	
	
}
