package com.woyou.listener;

import com.woyou.activity.HomeActivity;

import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

/**
 * 监听Viewpage切换事件
 * 
 * @author lenovo
 *
 */
public class MessagePageChangeListener implements OnPageChangeListener {
    //当前焦点视图
	private HomeActivity homeActivity;
    private int currIndex=0;
    private int position_one;
    private int position_two;
	//滚动条
	private ImageView layout_track_iv;
	
	public MessagePageChangeListener(HomeActivity homeActivity,int screenWidth,ImageView iv){
		this.homeActivity=homeActivity;
        position_one=screenWidth/2;
        position_two=position_one*2;
        layout_track_iv=iv;
	}
	@Override
	public void onPageSelected(int arg0) {
		Animation animation = null;
		switch (arg0) {
		case 0:
			homeActivity.homeController.hideInputMethod();
			// 设置滚动条
			switch (currIndex) {
			case 1:
				animation = new TranslateAnimation(position_one, 0, 0, 0);
				break;
			case 2:
				animation = new TranslateAnimation(position_two, 0, 0, 0);
				break;
			}
			break;
		case 1:
			// 设置滚动条
			switch (currIndex) {
			case 0:
				animation = new TranslateAnimation(0, position_one, 0, 0);
				break;
			case 2:
				animation = new TranslateAnimation(position_two, position_one,
						0, 0);
				break;
			}
			break;
		}
		currIndex = arg0;
		animation.setFillAfter(true);
		animation.setDuration(300);
		layout_track_iv.startAnimation(animation);
	}

	@Override
	public void onPageScrollStateChanged(int arg0) {

	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {

	}

}