package com.woyou.adapter;

import java.util.List;

import android.content.Context;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.jing.biandang.R;
import com.woyou.bean.ReportData;
import com.woyou.utils.FormatTools;

/**
 * 报表详情适配器
 * @author zhou.ni
 *
 */
public class ReportDayDetailsAdapter extends BaseAdapter {

	private LayoutInflater layoutInflater;
	private List<ReportData> mList;

	public ReportDayDetailsAdapter(Context c, List<ReportData> mList) {
		layoutInflater = LayoutInflater.from(c);
		this.mList = mList;
	}

	@Override
	public int getCount() {
		return mList.size();
	}

	@Override
	public Object getItem(int position) {
		return mList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;
		if (convertView == null) {
			viewHolder=new ViewHolder();
			convertView = layoutInflater.inflate(R.layout.item_reportdaydetail,null);
			viewHolder.item_reportdaydetail_name=(TextView)convertView.findViewById(R.id.item_reportdaydetail_name);
			viewHolder.item_reportdaydetail_num=(TextView)convertView.findViewById(R.id.item_reportdaydetail_num);
			viewHolder.item_reportdaydetail_sum=(TextView)convertView.findViewById(R.id.item_reportdaydetail_sum);
			convertView.setTag(viewHolder);
		}else{
			viewHolder=(ViewHolder) convertView.getTag();
		}
		
		ReportData item = mList.get(position);
		
		viewHolder.item_reportdaydetail_name.setText(""+item.getName());
		viewHolder.item_reportdaydetail_sum.setText(Html.fromHtml(FormatTools.String2Money(item.getNum()+"") + "<small>份</small>"));
		if ( item.getType()==0 ) {
			viewHolder.item_reportdaydetail_num.setText(Html.fromHtml("<small>¥</small>"+FormatTools.String2Money(item.getPrice()+"")));
		} else if ( item.getType()==1 ) {
			String money = FormatTools.String2Money(item.getPrice()+"");
			if ( TextUtils.isEmpty(money) || TextUtils.equals(money, "0") || TextUtils.equals(money, "-0") ){
				viewHolder.item_reportdaydetail_num.setText("免费");
			} else {
				if ( money.contains("-") ) {
					money = money.replace("-", "");
					viewHolder.item_reportdaydetail_num.setText(Html.fromHtml("<small>-¥</small>"+money));
				} else {
					viewHolder.item_reportdaydetail_num.setText(Html.fromHtml("<small>¥</small>"+money));
				}
			}
		} else if ( item.getType()==3 ) {
			viewHolder.item_reportdaydetail_num.setText(Html.fromHtml("<small>¥</small>"+FormatTools.String2Money(item.getPrice()+"")));
		} else {
			viewHolder.item_reportdaydetail_num.setText(Html.fromHtml("<small>-¥</small>"+FormatTools.String2Money(item.getPrice()+"")));
		}
		return convertView;
	}
	
	static class ViewHolder {
		TextView item_reportdaydetail_name;
		TextView item_reportdaydetail_num;
		TextView item_reportdaydetail_sum;
	}
	
}
