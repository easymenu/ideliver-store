package com.woyou.activity;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.woyou.utils.ACache;

public class SuperFragmentActivity extends FragmentActivity {
	public static ACache aCache;

	/**
	 * 缓存对象Acache
	 */
	public ACache getACache() {
		if (aCache == null) {
			aCache = ACache.get(this);
		}
		return aCache;
	}

	/**
	 * 返回对象本身
	 * 
	 * @return
	 */
	public Context This() {
		return this;
	}

	/**
	 * 打印info级log
	 * 
	 * @param info
	 */
	public void LogInfo(String info) {
		Log.i("info--->", info);
	}

}
