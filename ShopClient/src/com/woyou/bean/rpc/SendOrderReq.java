package com.woyou.bean.rpc;

import com.woyou.bean.SuperModel;

/**
 * 发送订单的请求Bean
 * 
 * @author 荣
 * 
 */
public class SendOrderReq extends SuperModel {
	/**
	 * 订单ID
	 */
	private String oId = "";
	/**
	 * 店铺编号
	 */
	private String sId = "";
	/**
	 * 密码
	 */
	private String pwd = "";

	public String getoId() {
		return oId;
	}

	public void setoId(String oId) {
		this.oId = oId;
	}

	public String getsId() {
		return sId;
	}

	public void setsId(String sId) {
		this.sId = sId;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

}
