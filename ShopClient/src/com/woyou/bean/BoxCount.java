package com.woyou.bean;

/**
 * 餐盒费统计列表
 * 
 * @author zhou.ni
 *
 */
public class BoxCount extends SuperModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String bName;		//餐盒费或外送费的名字
	private int bNum;			//餐盒费或外送费的数量
	private float bPrice;		//餐盒费或外送费的总金额	
	public String getbName() {
		return bName;
	}
	public void setbName(String bName) {
		this.bName = bName;
	}
	public int getbNum() {
		return bNum;
	}
	public void setbNum(int bNum) {
		this.bNum = bNum;
	}
	public float getbPrice() {
		return bPrice;
	}
	public void setbPrice(float bPrice) {
		this.bPrice = bPrice;
	}
	
	@Override
	public String toString() {
		return "BoxCount [bName=" + bName + ", bNum=" + bNum + ", bPrice="
				+ bPrice + "]";
	}

	
	
}
