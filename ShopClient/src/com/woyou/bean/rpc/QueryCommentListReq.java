package com.woyou.bean.rpc;

/**
 * 获取用户评论的请求bean
 * @author zhou.ni
 *
 */
public class QueryCommentListReq {
	
	private String sId;			//店铺编号
	private String pwd;			//密码
	private int page;			//页码
	
	public String getsId() {
		return sId;
	}
	public void setsId(String sId) {
		this.sId = sId;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	
}
