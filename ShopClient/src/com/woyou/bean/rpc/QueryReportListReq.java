package com.woyou.bean.rpc;

/**
 * 获取日或月营业报表的请求bean
 * @author zhou.ni
 *
 */
public class QueryReportListReq {
	
	private String sId;			//用户Id
	private String pwd;			//密码
	private String date;		//日期（2015-04-01）或月份（2015-04）的字符串
	
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public String getsId() {
		return sId;
	}
	public void setsId(String sId) {
		this.sId = sId;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
	
}
