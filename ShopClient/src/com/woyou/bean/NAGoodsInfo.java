package com.woyou.bean;


/**
 * 获取昨日信息简介Model
 * @author tlc
 *
 */
public class NAGoodsInfo extends SuperModel{
	
	private String goods = "";

	public String getGoods() {
		return goods;
	}

	public void setGoods(String goods) {
		this.goods = goods;
	}
}
