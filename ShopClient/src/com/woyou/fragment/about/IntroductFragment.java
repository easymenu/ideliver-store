package com.woyou.fragment.about;

import java.util.ArrayList;
import java.util.List;

import com.jing.biandang.R;
import com.woyou.activity.HomeActivity;
import com.woyou.adapter.SuperViewPagerAdapter;
import com.woyou.fragment.SuperFragment;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class IntroductFragment extends SuperFragment {
	private HomeActivity homeActivity;
	
	private View layout_introduct;
	private ViewPager layout_introduct_vp;
	private SuperViewPagerAdapter viewPagerAdapter;
	private List<View> views=new ArrayList<View>();
	private ImageView layout_introduct_iv1;
	private ImageView layout_introduct_iv2;
	private ImageView layout_introduct_iv3;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		layout_introduct = inflater.inflate(R.layout.layout_introduct, container, false);
		homeActivity=(HomeActivity)getActivity();
		return layout_introduct;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		init();
	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		if(!hidden){
			layout_introduct_vp.setCurrentItem(0, false);
		}
	}
	/**
	 * 初始化页面
	 */
	public void init(){
		layout_introduct_iv1=(ImageView)layout_introduct.findViewById(R.id.layout_introduct_iv1);
		layout_introduct_iv2=(ImageView)layout_introduct.findViewById(R.id.layout_introduct_iv2);
		layout_introduct_iv3=(ImageView)layout_introduct.findViewById(R.id.layout_introduct_iv3);
		layout_introduct_vp=(ViewPager)layout_introduct.findViewById(R.id.layout_introduct_vp);
		LayoutInflater layoutInflater=LayoutInflater.from(homeActivity);
		View view1=layoutInflater.inflate(R.layout.item_introduct_v1, null);
		View view2=layoutInflater.inflate(R.layout.item_introduct_v2, null);
		View view3=layoutInflater.inflate(R.layout.item_introduct_v3, null);
		
		RelativeLayout item_introduct_finish=(RelativeLayout)view3.findViewById(R.id.item_introduct_finish);
		item_introduct_finish.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				homeActivity.backBeforeFM();
			}
		});
		views.add(view1);
		views.add(view2);
		views.add(view3);
		viewPagerAdapter=new SuperViewPagerAdapter(views);
		layout_introduct_vp.setAdapter(viewPagerAdapter);

		layout_introduct_vp.setOnPageChangeListener(new OnPageChangeListener() {
			@Override
			public void onPageSelected(int arg0) {
				setItemFocus(arg0);
			}			
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {

			}
			@Override
			public void onPageScrollStateChanged(int arg0) {
				
			}
		});
	}
	/**
	 * 根据传入参数设置焦点
	 * @param index
	 */
	public void setItemFocus(int index){
		layout_introduct_iv1.setImageResource(R.raw.icon_introduct_unfocus);
		layout_introduct_iv2.setImageResource(R.raw.icon_introduct_unfocus);
		layout_introduct_iv3.setImageResource(R.raw.icon_introduct_unfocus);
		switch(index){
			case 0:
				layout_introduct_iv1.setImageResource(R.raw.icon_introduct_focus);
				break;
			case 1:
				layout_introduct_iv2.setImageResource(R.raw.icon_introduct_focus);
				break;
			case 2:
				layout_introduct_iv3.setImageResource(R.raw.icon_introduct_focus);
				break;
		}
	}

}
