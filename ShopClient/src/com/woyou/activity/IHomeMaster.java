/** 
 * @Title:  IHomeController.java 
 * @author:  xuron
 * @data:  2015年11月30日 上午11:30:37 <创建时间>
 * 
 * @history：<以下是历史记录>
 *
 * @modifier: <修改人>
 * @modify date: 2015年11月30日 上午11:30:37 <修改时间>
 * @log: <修改内容>
 *
 * @modifier: <修改人>
 * @modify date: 2015年11月30日 上午11:30:37 <修改时间>
 * @log: <修改内容>
 */
package com.woyou.activity;

import com.woyou.bean.FmInfo;
import com.woyou.fragment.order.NewOrderFragment;
import com.woyou.fragment.order.OrderFragment;
import com.woyou.fragment.shop.ShopFragment;

import android.support.v4.app.Fragment;

/** 
 * 抽离HomeActivity的通用方法
 * @author xuron 
 * @versionCode 1 <每次修改提交前+1>
 */
public interface IHomeMaster {
	/**
	 * 获取ShopFragment
	 * @return
	 */
	public ShopFragment getShopFm();
	/**
	 * 获取OrderFragment
	 * @return
	 */
	public OrderFragment getOrderFm();
	/**
	 * 获取NewOrderFragment
	 */
	public NewOrderFragment getNewOrderFm();

	/**
	 * 显示隐藏载入View
	 */
	public void showLoadingHint();

	/**
	 * 隐藏数据载入View
	 */
	public void hideLoadingHint();
	/**
	 * 获取当前记录的Fragment
	 * @return
	 */
	public Fragment getCurrentFragment();
	/**
	 * 打开某个FM
	 * @param <T>
	 */
	public void openFM(FmInfo  fmInfo);
	/**
	 * 打开二级FM
	 * @param fmInfo
	 */
	public void openSubFM(Fragment beforeFm,FmInfo  fmInfo);
	/**
	 * 返回上一个FM
	 */
	public void backBeforeFM();
	/**
	 * 动态修改ActionBar
	 * @param backFragment
	 * @param name 记录从哪个Fragment跳转过来的 和返回按钮的名字
	 */
	public void modifyActionBar(Fragment fragment,String name);
	/**
	 * 重置Actionbar
	 * 
	 * @param isClear
	 *            是否要清除backFragments列表
	 */
	public void resetActionBar(boolean isClear);
	/**
	 * 验证老板密码 跳转到哪个fragment
	 */
	public void checkBossPW();
	/**
	 * 隐藏输入法
	 */
	public void hideInputMethod();
	
}
