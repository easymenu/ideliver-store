package com.woyou.bean.rpc;

import java.util.ArrayList;
import java.util.List;

import com.woyou.bean.OrderId;
import com.woyou.bean.SuperModel;

/**
 * 更新订单状态（置S）
 * 
 * @author 荣
 * 
 */
public class UpdateOrdersStatusReq extends SuperModel {
	/**
	 * 商铺Id
	 */
	private String sId = "";
	/**
	 * 密码
	 */
	private String pwd = "";
	/**
	 * 订单列表
	 */
	private List<OrderId> orderList = new ArrayList<OrderId>();

	public String getsId() {
		return sId;
	}

	public void setsId(String sId) {
		this.sId = sId;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public List<OrderId> getOrderList() {
		return orderList;
	}

	public void setOrderList(List<OrderId> orderList) {
		this.orderList = orderList;
	}

}
