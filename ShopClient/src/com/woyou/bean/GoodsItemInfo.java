package com.woyou.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * 修改商品状态请求Model
 * 
 * @author tlc
 * 
 */
public class GoodsItemInfo extends SuperModel {

	/**
	 * 商品ID
	 */
	private String gId = "";
	/**
	 * 商品状态（1正常，2暂缺）
	 */
	private int state;
	/**
	 * 1：商品状态有修改，2没有修改
	 */
	private int isChange;
	/**
	 * 商品属性列表
	 */
	private List<OptionInfo> propList = new ArrayList<OptionInfo>();

	public String getgId() {
		return gId;
	}

	public void setgId(String gId) {
		this.gId = gId;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public int getIsChange() {
		return isChange;
	}

	public void setIsChange(int isChange) {
		this.isChange = isChange;
	}

	public List<OptionInfo> getPropList() {
		return propList;
	}

	public void setPropList(List<OptionInfo> propList) {
		this.propList = propList;
	}

	@Override
	public String toString() {
		return "GoodsItemInfo [gId=" + gId + ", state=" + state + ", isChange="
				+ isChange + ", propList=" + propList + "]";
	}

}
