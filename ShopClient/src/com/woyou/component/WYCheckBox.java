package com.woyou.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.CheckBox;

import com.jing.biandang.R;

public class WYCheckBox extends CheckBox {
	/**
	 * checkbox判断是左边还是右边 type 0:左边，1:右边
	 */
	private int type = 0;
	private String text = "";
	private Paint paint;
	
	public WYCheckBox(Context context) {
		super(context);
	}

	public WYCheckBox(Context context, AttributeSet attrs) {
		super(context, attrs);
		TypedArray typedArray = context.obtainStyledAttributes(attrs,R.styleable.WYCheckBox);
		type = typedArray.getInteger(R.styleable.WYCheckBox_type, 0);
		text = typedArray.getString(R.styleable.WYCheckBox_text);
		typedArray.recycle();
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		paint=new Paint();
		paint.setTextSize(24f);
		paint.setAntiAlias(true);
		if (type == 0) {
			if (isChecked()) {
				paint.setColor(0xffffffff);
				canvas.drawText(text, 40, 35, paint);
				this.setBackgroundResource(R.raw.shophours_shop_left);
			} else {
				paint.setColor(0xffdddddd);
				canvas.drawText(text, 40, 35, paint);
				this.setBackgroundResource(R.raw.shophours_rest_left);
			}
		} else {
			if (isChecked()) {
				paint.setColor(0xffffffff);
				canvas.drawText(text, 20, 35, paint);
				this.setBackgroundResource(R.raw.shophours_shop_right);
			} else {
				paint.setColor(0xffdddddd);
				canvas.drawText(text, 20, 35, paint);
				this.setBackgroundResource(R.raw.shophours_rest_right);
			}
		}
		invalidate();
	}

}
