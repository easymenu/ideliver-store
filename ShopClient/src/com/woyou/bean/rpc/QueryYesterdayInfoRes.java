package com.woyou.bean.rpc;

import java.util.ArrayList;
import java.util.List;

import com.woyou.bean.NAGoodsInfo;
import com.woyou.bean.SuperModel;

/**
 * 获取昨日信息简介结果集
 * @author tlc
 *
 */
public class QueryYesterdayInfoRes extends SuperModel {

	/**
	 * 暂缺商品数量
	 */
	private int NAnum;
	/**
	 * 未读评论数量
	 */
	private int newCommentNum;
	/**
	 * 未读评论摘要（返回最新一条评论）
	 */
	private String newComment = "";
	/**
	 * 昨日订单数量（成功接单的数量）
	 */
	private int orderNum;
	/**
	 * 昨日售出商品的数量
	 */
	private int soldNum;
	/**
	 * 昨日收入金额
	 */
	private float income;
	/**
	 * 暂缺商品列表（最多返回4个商品）
	 */
	private List<NAGoodsInfo> NAGoods = new ArrayList<NAGoodsInfo>();
	
	public int getNAnum() {
		return NAnum;
	}
	public void setNAnum(int nAnum) {
		NAnum = nAnum;
	}
	public int getNewCommentNum() {
		return newCommentNum;
	}
	public void setNewCommentNum(int newCommentNum) {
		this.newCommentNum = newCommentNum;
	}
	public String getNewComment() {
		return newComment;
	}
	public void setNewComment(String newComment) {
		this.newComment = newComment;
	}
	public int getOrderNum() {
		return orderNum;
	}
	public void setOrderNum(int orderNum) {
		this.orderNum = orderNum;
	}
	public int getSoldNum() {
		return soldNum;
	}
	public void setSoldNum(int soldNum) {
		this.soldNum = soldNum;
	}
	public float getIncome() {
		return income;
	}
	public void setIncome(float income) {
		this.income = income;
	}
	public List<NAGoodsInfo> getNAGoods() {
		return NAGoods;
	}
	public void setNAGoods(List<NAGoodsInfo> nAGoods) {
		NAGoods = nAGoods;
	}
}
