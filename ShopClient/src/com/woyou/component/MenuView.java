/** 
 * @Title:  MenuView.java 
 * @author:  xuron
 * @data:  2015年11月24日 上午9:58:43 <创建时间>
 * 
 * @history：<以下是历史记录>
 *
 * @modifier: <修改人>
 * @modify date: 2015年11月24日 上午9:58:43 <修改时间>
 * @log: <修改内容>
 *
 * @modifier: <修改人>
 * @modify date: 2015年11月24日 上午9:58:43 <修改时间>
 * @log: <修改内容>
 */
package com.woyou.component;

import com.jing.biandang.R;
import com.squareup.picasso.Picasso;
import com.woyou.activity.HomeActivity;
import com.woyou.bean.FmInfo;
import com.woyou.bean.rpc.UserLoginRes;
import com.woyou.controller.MenuViewController;
import com.woyou.fragment.MessageFragment;
import com.woyou.fragment.PMManagerFragment;
import com.woyou.fragment.SettingFragment;
import com.woyou.fragment.SoldoutFragment;
import com.woyou.fragment.about.AboutFragment;
import com.woyou.fragment.about.SettingPWFragment;
import com.woyou.fragment.analysis.AnalysisFragment;
import com.woyou.fragment.goods.GoodsManagerFragment;
import com.woyou.fragment.order.OrderFragment;
import com.woyou.fragment.shop.ShopFragment;
import com.woyou.utils.FormatTools;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import net.simonvt.menudrawer.MenuDrawer;

/**
 * 抽出抽屉View作为自定义控件
 * 
 * @author xuron
 * @versionCode 1 <每次修改提交前+1>
 */
public class MenuView extends SuperLayout implements OnClickListener {
	public MenuViewController menuViewController;
	/**
	 * HomeActivity
	 */
	private HomeActivity homeActivity;
	/**
	 * 抽屉类
	 */
	private MenuDrawer menuDrawer;
	/**
	 * 自定义的Actionbar
	 */
	private ActionbarView actionbarView;
	private FragmentManager fm;
	private RelativeLayout menuShopinfo;
	public ImageView menuImage; // 店铺logo
	public TextView menuName; // 店铺名称
	public TextView menuInfo; // 店铺公告
	public ImageView menuStart; // 星级
	public TextView menuScorenum; // 评分人数
	public ImageView menuStatus; // 店铺状态
	public ImageView menuPay;// 在线支付
	public TextView menuSendup; // 起送价
	public TextView menuSalesnum; // 月售
	public TextView menuDistance; // 距离

	private RelativeLayout menu_businessorder;
	private RelativeLayout menu_soldout;
	private RelativeLayout menu_goodsmanager;
	private RelativeLayout menu_analysis;
	public RelativeLayout menu_pmmanager;
	private RelativeLayout menu_message;
	private RelativeLayout menu_boss;
	private RelativeLayout menu_bosspw;
	private RelativeLayout menu_setting;
	private RelativeLayout menu_about;
	public ImageView menu_order_mark;
	public ImageView menu_soldout_mark;
	public ImageView menu_message_mark;
	public ImageView menu_boss_mark;
	public ImageView menu_goodsmanager_mark;
	public ImageView menu_analysis_mark;
	public ImageView menu_pmmanager_mark;
	public ImageView menu_bosspw_mark;
	public ImageView menu_setting_mark;
	public ImageView menu_about_mark;
	public ImageView menu_boss_iv;
	public LinearLayout menu_folder;
	private boolean folderIsOpen = false;

	/**
	 * 是否按时间表来设置营业时间段
	 */
	public boolean isRefreshAtTime = true;

	/**
	 * @param context
	 */
	public MenuView(Context context, FragmentManager fm, MenuDrawer menuDrawer, ActionbarView actionbarView) {
		super(context);
		homeActivity = (HomeActivity) context;
		this.fm = fm;
		this.menuDrawer = menuDrawer;
		this.actionbarView = actionbarView;
		initView();
	}

	@Override
	protected void initView() {
		menuViewController=new MenuViewController(mContext, fm, menuDrawer);
		LayoutInflater.from(getContext()).inflate(R.layout.layout_home_menu, this);
		menuShopinfo = (RelativeLayout) findViewById(R.id.layout_menu_shopinfo);
		menuImage = (ImageView) findViewById(R.id.layout_menu_image);
		menuName = (TextView) findViewById(R.id.layout_menu_name);
		menuInfo = (TextView) findViewById(R.id.layout_menu_info);
		menuStart = (ImageView) findViewById(R.id.layout_menu_start);
		menuScorenum = (TextView) findViewById(R.id.layout_menu_scorenum);
		menuStatus = (ImageView) findViewById(R.id.layout_menu_status);
		menuPay = (ImageView) findViewById(R.id.layout_menu_pay);
		menuSalesnum = (TextView) findViewById(R.id.layout_menu_salesnum);
		menuSendup = (TextView) findViewById(R.id.layout_menu_sendup);
		menuDistance = (TextView) findViewById(R.id.layout_menu_distance);

		// 左菜单
		menu_businessorder = (RelativeLayout) findViewById(R.id.layout_menu_businessorder);
		menu_soldout = (RelativeLayout) findViewById(R.id.layout_menu_soldout);
		menu_goodsmanager = (RelativeLayout) findViewById(R.id.layout_menu_goodsmanager);
		menu_analysis = (RelativeLayout) findViewById(R.id.layout_menu_analysis);
		menu_pmmanager = (RelativeLayout) findViewById(R.id.layout_menu_pmmanager);
		menu_message = (RelativeLayout) findViewById(R.id.layout_menu_message);
		menu_boss = (RelativeLayout) findViewById(R.id.layout_menu_boss);
		menu_boss_iv = (ImageView) findViewById(R.id.layout_menu_boss_iv);
		menu_bosspw = (RelativeLayout) findViewById(R.id.layout_menu_bosspw);
		menu_folder = (LinearLayout) findViewById(R.id.layout_menu_folder);
		menu_setting = (RelativeLayout) findViewById(R.id.layout_menu_setting);
		menu_about = (RelativeLayout) findViewById(R.id.layout_menu_about);

		// mark
		menu_order_mark = (ImageView) findViewById(R.id.menu_order_mark);
		menu_soldout_mark = (ImageView) findViewById(R.id.menu_soldout_mark);
		menu_message_mark = (ImageView) findViewById(R.id.menu_message_mark);
		menu_boss_mark = (ImageView) findViewById(R.id.menu_boss_mark);
		menu_goodsmanager_mark = (ImageView) findViewById(R.id.menu_goodsmanager_mark);
		menu_analysis_mark = (ImageView) findViewById(R.id.menu_analysis_mark);
		menu_pmmanager_mark = (ImageView) findViewById(R.id.menu_pmmanager_mark);
		menu_bosspw_mark = (ImageView) findViewById(R.id.menu_bosspw_mark);
		menu_setting_mark = (ImageView) findViewById(R.id.menu_setting_mark);
		menu_about_mark = (ImageView) findViewById(R.id.menu_about_mark);

		// 设置监听器
		menuShopinfo.setOnClickListener(this);
		menu_businessorder.setOnClickListener(this);
		menu_soldout.setOnClickListener(this);
		menu_goodsmanager.setOnClickListener(this);
		menu_analysis.setOnClickListener(this);
		menu_pmmanager.setOnClickListener(this);
		menu_bosspw.setOnClickListener(this);
		menu_message.setOnClickListener(this);
		menu_setting.setOnClickListener(this);
		menu_about.setOnClickListener(this);
		menu_boss.setOnClickListener(this);
		// 判断是否显示打款管理选项
		menuViewController.isShowPMManager(this);
		
		init();
	}

	/**
	 * 初始化数据操作
	 */
	public void init() {
		post(new Runnable() {

			@Override
			public void run() {
				isRefreshAtTime = true;
				// 获取根据时间表获取当前店铺营业状态
				menuViewController.getShopStatusByTimerList();
				// 连接中
				actionbarView.actionBarController.linkingShopStatus();
				// 获取店铺信息
				UserLoginRes loginRes = (UserLoginRes) aCache.getAsObject("loginUserInfo");
				if (loginRes != null) {
					if (!TextUtils.isEmpty(loginRes.getShopLogo())) {
						Picasso.with(mContext).load(loginRes.getShopLogo()).placeholder(R.raw.icon_default).error(R.raw.icon_default).into(menuImage);
					} else {
						menuImage.setImageResource(R.raw.icon_default);
					}

					String[] sNameArray = loginRes.getsName().split("\\(");
					menuName.setText(sNameArray[0]);
					if (!TextUtils.isEmpty(loginRes.getNotice())) {
						menuInfo.setText(loginRes.getNotice());
					} else if (sNameArray.length > 1) {
						menuInfo.setText("(" + sNameArray[1]);
					} else {
						menuInfo.setText("");
					}

					if (loginRes.getScoreNum() != 0) {
						menuScorenum.setText("(" + loginRes.getScoreNum() + ")");
					} else {
						menuScorenum.setText("");
					}

					// 星级
					menuViewController.setPraiseRate(menuStart, loginRes.getScore());
					// 判断是否支持在线支付
					if (loginRes.getIsOnlinePay() == 1) {
						menuPay.setVisibility(View.VISIBLE);
					} else {
						menuPay.setVisibility(View.GONE);
					}
					// 20元起送 / 月售200份
					String prompt = FormatTools.String2Money(loginRes.getStartPrice() + "") + "元起送 / ";
					menuSendup.setText(prompt);
					menuSalesnum.setText("月售" + loginRes.getSalesNum() + "份");
				}
			}
		});
	}

	/**
	 * 点击左边菜单切换到相应的Fragment
	 */
	@Override
	public void onClick(View v) {
		// 使actionbar上的点击事件可用
		menuViewController.hideMask();
		/**
		 * 判断老板功能是否展开
		 */
		if (v.getId() == R.id.layout_menu_boss) {
			menuViewController.showMark(this,3);
			if (folderIsOpen) {
				menu_folder.setVisibility(View.GONE);
				menu_boss_iv.setImageResource(R.raw.icon_menu_folder_collapse_48);
				folderIsOpen = false;
			} else {
				menu_folder.setVisibility(View.VISIBLE);
				menu_boss_iv.setImageResource(R.raw.icon_menu_folder_expand_48);
				folderIsOpen = true;
			}
			return;
		}

		switch (v.getId()) {
		case R.id.layout_menu_shopinfo:
			homeActivity.openFM(new FmInfo(ShopFragment.class,"店铺信息",false));
			break;
		case R.id.layout_menu_businessorder:
			menuViewController.showMark(this,0);
			if (aCache.getAsObject("loginUserInfo") != null) {
				UserLoginRes userLoginRes = (UserLoginRes) aCache.getAsObject("loginUserInfo");
				homeActivity.openFM(new FmInfo(OrderFragment.class,userLoginRes.getsName(),false));
			}
			break;
		case R.id.layout_menu_soldout:
			menuViewController.showMark(this,1);
			homeActivity.openFM(new FmInfo(SoldoutFragment.class,"暂缺商品",false));
			break;
		case R.id.layout_menu_message:
			menuViewController.showMark(this,2);
			homeActivity.openFM(new FmInfo(MessageFragment.class,"消息中心",false));
			break;
		case R.id.layout_menu_goodsmanager:
			menuViewController.showMark(this,4);
			actionbarView.layout_actionbar_shopname.setText("商品管理");
			// 判断是否在5分钟内的解锁期内
			if (!homeActivity.isUnlock5Minute) {
				// 判断是否设置了老板锁
				if (aCache.getAsString("isSetPW") != null) {
					if (aCache.getAsString("isSetPW").equals("yes")) {
						// 跳转方向1表示analysisFragment
						aCache.put("jumpDirection", "4");
						homeActivity.checkBossPW();
						return;
					}
				}
			}
			homeActivity.openFM(new FmInfo(GoodsManagerFragment.class,"商品管理",false));
			break;
		case R.id.layout_menu_analysis:
			menuViewController.showMark(this,5);
			actionbarView.layout_actionbar_shopname.setText("营销分析");
			// 判断是否在5分钟内的解锁期内
			if (!homeActivity.isUnlock5Minute) {
				// 判断是否设置了老板锁
				if (aCache.getAsString("isSetPW") != null) {
					if (aCache.getAsString("isSetPW").equals("yes")) {
						// 跳转方向1表示analysisFragment
						aCache.put("jumpDirection", "1");
						homeActivity.checkBossPW();
						return;
					}
				}
			}
			homeActivity.openFM(new FmInfo(AnalysisFragment.class,"营销分析",false));
			break;
		case R.id.layout_menu_pmmanager:
			menuViewController.showMark(this,9);
			actionbarView.layout_actionbar_shopname.setText("打款管理");
			// 判断是否在5分钟内的解锁期内
			if (!homeActivity.isUnlock5Minute) {
				// 判断是否设置了老板锁
				if (aCache.getAsString("isSetPW") != null) {
					if (aCache.getAsString("isSetPW").equals("yes")) {
						// 跳转方向1表示analysisFragment
						aCache.put("jumpDirection", "6");
						homeActivity.checkBossPW();
						return;
					}
				}
			}
			homeActivity.openFM(new FmInfo(PMManagerFragment.class,"打款管理",false));
			break;
		case R.id.layout_menu_bosspw:
			menuViewController.showMark(this,6);
			actionbarView.layout_actionbar_shopname.setText("设置老板密码");
			// 判断是否在5分钟内的解锁期内
			if (!homeActivity.isUnlock5Minute) {
				// 判断是否设置了老板锁
				if (aCache.getAsString("isSetPW") != null) {
					if (aCache.getAsString("isSetPW").equals("yes")) {
						// 跳转方向1表示analysisFragment
						aCache.put("jumpDirection", "2");
						homeActivity.checkBossPW();
						return;
					}
				}
			}
			homeActivity.openFM(new FmInfo(SettingPWFragment.class,"设置老板密码",false));
			break;
		case R.id.layout_menu_setting:
			menuViewController.showMark(this,7);
			homeActivity.openFM(new FmInfo(SettingFragment.class,"设置",false));
			break;
		case R.id.layout_menu_about:
			menuViewController.showMark(this,8);
			homeActivity.openFM(new FmInfo(AboutFragment.class,"关于",false));
			break;
		}
	}
}
