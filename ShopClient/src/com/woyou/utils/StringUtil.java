package com.woyou.utils;

public class StringUtil {

	public static boolean isEmpty(String str){
		if(str == null || "".equals(str.trim())){
			return true;
		}
		return false;
	}
	
	public static String wipeFullstop(String str){
		String newStr = null;
		if( str.contains(".")  ){
			newStr = str.substring(0, str.lastIndexOf("."));
		}else{
			newStr = str;
		}
		return newStr;
	}
}
