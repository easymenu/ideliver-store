package com.woyou.bean.rpc;

import com.woyou.bean.SuperModel;

/**
 * 应用检查更新结果集
 * @author tlc
 *
 */
public class UpdateVersionRes extends SuperModel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * 0不更新，1更新
	 */
	private int isUpdate;
	/**
	 * 应用下载地址
	 */
	private String updateUrl = "";
	/**
	 * 升级说明,可以为空
	 */
	private String info = "";
	/**
	 * 升级模式，1=允许选择升级，2=要求强制升级
	 */
	private int type;
	
	public int getIsUpdate() {
		return isUpdate;
	}
	public void setIsUpdate(int isUpdate) {
		this.isUpdate = isUpdate;
	}
	public String getUpdateUrl() {
		return updateUrl;
	}
	public void setUpdateUrl(String updateUrl) {
		this.updateUrl = updateUrl;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	
}
