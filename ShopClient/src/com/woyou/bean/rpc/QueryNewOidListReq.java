package com.woyou.bean.rpc;

import com.woyou.bean.SuperModel;
/**
 * 获取新订单列表请求bean
 * @author 荣
 *
 */
public class QueryNewOidListReq extends SuperModel {
	/**
	 * 店铺Id
	 */
	private String sId="";
	/**
	 * 密码
	 */
	private String pwd="";
	public String getsId() {
		return sId;
	}
	public void setsId(String sId) {
		this.sId = sId;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	
}
