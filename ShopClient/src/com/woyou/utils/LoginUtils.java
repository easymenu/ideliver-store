package com.woyou.utils;

import com.woyou.bean.UserInfo;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class LoginUtils {
	
	/**
	 * 登陆成功后保存用户信息
	 * @param info
	 * @param context
	 */
    public static void saveUserInfo( UserInfo info, Context context ){
    	SharedPreferences preferences = context.getSharedPreferences("User", Context.MODE_PRIVATE);
		Editor editor = preferences.edit();
		editor.putString("ShopCode", info.getsId());
		editor.putString("ShopName", info.getsName()); 
		editor.putString("BdjCode", info.getbId());
		editor.putString("ShopUserCode", info.getuCode());
		editor.putString("Code", info.getCode());
		editor.putString("password", info.getPwd());
		editor.putInt("LoginType", info.getLoginType());
		editor.putString("Version", info.getVersion());
		editor.putString("DeviceId", info.getDeviceId());
		editor.putString("ServerIP", info.getServerIP());
		editor.putString("ServerPort", info.getServerPort());
		editor.commit();
    }
    
    /**
     * 获取商铺id
     * @param context
     * @return
     */
    public static UserInfo getUserInfo(Context context){
    	SharedPreferences preferences = null;
    	UserInfo userInfo = null;
    	if(context!=null)
    	{
    		userInfo = new UserInfo();
    		preferences = context.getSharedPreferences("User", Context.MODE_PRIVATE);
        	userInfo.setbId(preferences.getString("BdjCode", ""));
        	userInfo.setsId(preferences.getString("ShopCode", ""));
        	userInfo.setsName(preferences.getString("ShopName", ""));
        	userInfo.setuCode(preferences.getString("ShopUserCode", ""));
        	userInfo.setCode(preferences.getString("Code", ""));
        	userInfo.setPwd(preferences.getString("password", ""));
        	userInfo.setLoginType(preferences.getInt("LoginType", -1));
        	userInfo.setDeviceId(preferences.getString("DeviceId", ""));
        	userInfo.setVersion(preferences.getString("Version", ""));
        	userInfo.setServerIP(preferences.getString("ServerIP", ""));
        	userInfo.setServerPort(preferences.getString("ServerPort", ""));
    	}
    	return userInfo;
    }
    
    /**
     * 移除所有用户信息
     * @param context
     */
    public static void removeUserInfo(Context context){
    	SharedPreferences preferences = context.getSharedPreferences("User", Context.MODE_PRIVATE);
		Editor editor = preferences.edit();
		editor.clear();
		editor.commit();
    }
    
    
    /**
     * 移除所有用户信息
     * @param context
     */
    public static void removeUser(Context context){
    	SharedPreferences preferences = context.getSharedPreferences("UserInfo", Context.MODE_PRIVATE);
		Editor editor = preferences.edit();
		editor.clear();
		editor.commit();
    }
    /**
     * 获取老数据的商铺id
     * @param context
     * @return
     */
    public static String getBdjCode(Context context){
    	SharedPreferences preferences = context.getSharedPreferences("UserInfo", Context.MODE_PRIVATE);
    	return preferences.getString("BdjCode", "");
    }
    /**
     * 获取老数据的用户名
     * @param context
     * @return
     */
    public static String getShopUserCode(Context context){
    	SharedPreferences preferences = context.getSharedPreferences("UserInfo", Context.MODE_PRIVATE);
    	return preferences.getString("ShopUserCode", "");
    }
    /**
     * 获取老数据的密码
     * @param context
     * @return
     */
    public static String getPwd(Context context){
    	SharedPreferences preferences = context.getSharedPreferences("UserInfo", Context.MODE_PRIVATE);
    	return preferences.getString("password", "");
    }
}
