package com.woyou.model.rpc;

import retrofit.RetrofitError;
import android.content.Context;

import com.woyou.bean.UserInfo;
import com.woyou.bean.rpc.MD5Req;
import com.woyou.bean.rpc.QueryVerificationCodeReq;
import com.woyou.bean.rpc.Result;
import com.woyou.bean.rpc.UpdateVersionReq;
import com.woyou.bean.rpc.UpdateVersionRes;
import com.woyou.bean.rpc.UserLoginReq;
import com.woyou.bean.rpc.UserLoginRes;
import com.woyou.service.RetrofitWrapper;
import com.woyou.service.UserService;
import com.woyou.utils.LoginUtils;


/**
 * 用户业务逻辑控制类
 * 
 * @author tlc
 * 
 */
public class UserModel extends SuperModel {
	private static Context mContext;
	private static UserService userService;
	private static UserModel userModel = new UserModel();

	/**
	 * 单例模式
	 * 
	 * @return
	 */
	public static UserModel getInstance(Context context) {
		mContext=context;
		if (userModel == null) {
			userModel = new UserModel();
		}
		if (mNeWrapper == null) {
			mNeWrapper = new RetrofitWrapper();
		}
		if (userService == null) {
			userService = mNeWrapper.getNetService(UserService.class);
		}
		return userModel;
	}

	/**
	 * 2.12	获取验证码
	 * 
	 * @param orderList
	 * @return
	 */
	public Result queryVerificationCode(QueryVerificationCodeReq queryVCodeReq) throws RetrofitError {
		MD5Req<QueryVerificationCodeReq> md5Req = new MD5Req<QueryVerificationCodeReq>(mContext,queryVCodeReq);
		Result result = userService.queryVerificationCode(
				md5Req.jsonParams, md5Req.isEncrypted, md5Req.timeStamp,
				md5Req.randomNum, md5Req.sign);
		return result;
	}
	
//	/**
//	 * 2.13	便当机登录
//	 * @return
//	 * @throws RetrofitError
//	 */
//	public Result<UserLoginRes> userLogin(UserLoginReq userLoginReq) throws RetrofitError {
//		MD5Req<UserLoginReq> md5Req = new MD5Req<UserLoginReq>(mContext,userLoginReq);
//		Result<UserLoginRes> result = userService.userLogin(
//				md5Req.jsonParams, md5Req.isEncrypted, md5Req.timeStamp,
//				md5Req.randomNum, md5Req.sign);
//		return result;
//	}
	
	/**
	 * 2.13	便当机登录
	 * @return
	 * @throws RetrofitError
	 */
	public Result<UserLoginRes> v2_3userLogin(UserLoginReq userLoginReq) throws RetrofitError {
		MD5Req<UserLoginReq> md5Req = new MD5Req<UserLoginReq>(mContext,userLoginReq);
		Result<UserLoginRes> result = userService.v2_3userLogin(
				md5Req.jsonParams, md5Req.isEncrypted, md5Req.timeStamp,
				md5Req.randomNum, md5Req.sign);
		return result;
	}
	
	/**
	 * 2.14	应用更新
	 * @return
	 * @throws RetrofitError
	 */
	public Result<UpdateVersionRes> updateVersion(UpdateVersionReq updateVersionReq) throws RetrofitError {
		MD5Req<UpdateVersionReq> md5Req = new MD5Req<UpdateVersionReq>(mContext,updateVersionReq);
		Result<UpdateVersionRes> result = userService.updateVersion(
				md5Req.jsonParams, md5Req.isEncrypted, md5Req.timeStamp,
				md5Req.randomNum, md5Req.sign);
		return result;
	}
}
