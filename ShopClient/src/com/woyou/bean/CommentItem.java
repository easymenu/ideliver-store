package com.woyou.bean;


/**
 * 每一条评论内容
 * @author zhou.ni
 *
 */
public class CommentItem extends SuperModel{

	private static final long serialVersionUID = 1L;
	
	private String cId;				//编号
	private String oId;				//订单id
	private String uPic;			//头像url
	private String uName;			//用户名称
	private long cTime;				//评论时间
	private String comments;		//评论内容
	private String score;			//评分
	private String reply;			//回复内容
	private long rTime;				//回复时间
	
	public String getcId() {
		return cId;
	}
	public void setcId(String cId) {
		this.cId = cId;
	}
	public String getoId() {
		return oId;
	}
	public void setoId(String oId) {
		this.oId = oId;
	}
	public String getuPic() {
		return uPic;
	}
	public void setuPic(String uPic) {
		this.uPic = uPic;
	}
	public String getuName() {
		return uName;
	}
	public void setuName(String uName) {
		this.uName = uName;
	}
	public long getcTime() {
		return cTime;
	}
	public void setcTime(long cTime) {
		this.cTime = cTime;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getScore() {
		return score;
	}
	public void setScore(String score) {
		this.score = score;
	}
	public String getReply() {
		return reply;
	}
	public void setReply(String reply) {
		this.reply = reply;
	}
	public long getrTime() {
		return rTime;
	}
	public void setrTime(long rTime) {
		this.rTime = rTime;
	}
	
	@Override
	public String toString() {
		return "CommentItem [cId=" + cId + ", oId=" + oId + ", uPic=" + uPic
				+ ", uName=" + uName + ", cTime=" + cTime + ", comments="
				+ comments + ", score=" + score + ", reply=" + reply
				+ ", rTime=" + rTime + "]";
	}
	
}
