package com.woyou.activity;

import com.woyou.utils.ACache;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

/**
 * Activity的超类
 * 
 * @author xuron
 * @versionCode 1 <每次修改提交前+1>
 */
public class SuperActivity extends Activity {
	/**
	 * 上下文对象
	 */
	public Context mContext;
	/**
	 * 缓存类
	 */
	public static ACache aCache;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		aCache = getACache();
		mContext = this;
	}

	/**
	 * 缓存对象Acache
	 */
	public ACache getACache() {
		if (aCache == null) {
			aCache = ACache.get(this);
		}
		return aCache;
	}
}
