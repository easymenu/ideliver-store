package com.woyou.model.rpc;

import java.util.List;

import retrofit.RetrofitError;
import android.content.Context;

import com.woyou.bean.GoodsState;
import com.woyou.bean.GoodsTypeInfo;
import com.woyou.bean.rpc.MD5Req;
import com.woyou.bean.rpc.QueryGoodsStateListReq;
import com.woyou.bean.rpc.QueryTypeListReq;
import com.woyou.bean.rpc.Result;
import com.woyou.bean.rpc.UpdateGoodsStateReq;
import com.woyou.service.GoodsService;
import com.woyou.service.RetrofitWrapper;

public class GoodsModel extends SuperModel {
	private static Context mContext;
	private static GoodsService goodsService;
	private static GoodsModel goodsModel = new GoodsModel();

	private GoodsModel() {
	}

	/**
	 * 单例模式
	 * 
	 * @return
	 */
	public static GoodsModel getInstance(Context context) {
		mContext=context;
		if (goodsModel == null) {
			goodsModel = new GoodsModel();
		}
		if (mNeWrapper == null) {
			mNeWrapper = new RetrofitWrapper();
		}
		if (goodsService == null) {
			goodsService = mNeWrapper.getNetService(GoodsService.class);
		}
		return goodsModel;
	}
	
	/**
	 * 2.9	获取商品分类列表（http://xxxx/queryTypeList）
	 * @return
	 * @throws RetrofitError
	 */
	public Result<List<GoodsTypeInfo>> queryTypeList(QueryTypeListReq queryTypeListReq) throws RetrofitError {
		MD5Req<QueryTypeListReq> md5Req = new MD5Req<QueryTypeListReq>(mContext,queryTypeListReq);
		Result<List<GoodsTypeInfo>> result = goodsService.queryTypeList(
				md5Req.jsonParams, md5Req.isEncrypted, md5Req.timeStamp,
				md5Req.randomNum, md5Req.sign);
		return result;
	}

	/**
	 * 查询商品状态
	 * 
	 * @param orderList
	 * @return
	 */
	public Result<List<GoodsState>> queryGoodsStateList(QueryGoodsStateListReq queryGoodsStateListReq) throws RetrofitError {
		MD5Req<QueryGoodsStateListReq> md5Req = new MD5Req<QueryGoodsStateListReq>(mContext,queryGoodsStateListReq);
		Result<List<GoodsState>> result = goodsService.queryGoodsStateList(md5Req.jsonParams, md5Req.isEncrypted, md5Req.timeStamp,md5Req.randomNum, md5Req.sign);
		return result;
	}
	/**
	 * 2.11	修改商品状态（http://xxxx/updateGoodsState）
	 * @return
	 * @throws RetrofitError
	 */
	public Result updateGoodsState(UpdateGoodsStateReq updateGoodsStateReq) throws RetrofitError {
		MD5Req<UpdateGoodsStateReq> md5Req = new MD5Req<UpdateGoodsStateReq>(mContext,updateGoodsStateReq);
		Result result = goodsService.updateGoodsState(md5Req.jsonParams, md5Req.isEncrypted, md5Req.timeStamp,md5Req.randomNum, md5Req.sign);
		return result;
	}
}
